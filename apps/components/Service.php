<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2016, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Service Component
 *
 *
 * @package	    Apps
 * @subpackage	Component
 * @category	Component Controller
 *
 * @version     1.1 Build 01.06.2016
 * @author	    Muhammad Arief
 * @contributor
 * @copyright	Copyright (c) 2013 - 2016, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Service extends BC_Controller
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('service_model');
	}

	public function index()
	{
		print_out("This web service.");
	}

	public function DCmove()
	{
		try{
            $type = $_GET['type'];
            if($type){
                $this->service_model->insertDCMoveIN();
            }else{
                $this->service_model->insertDCMoveRet();
            }
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file register.php */
/* Location: ./site/modules/register/controllers/register.php */
