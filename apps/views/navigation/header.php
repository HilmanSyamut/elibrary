<header class="header">
	<div class="logo-container">
		<a href="<?php echo site_url(""); ?>" class="logo">
			<img src="assets/backend/images/logos.png" height="65" alt="" />
		</a>
		<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<!-- start: search & user box -->
	<div class="header-right">

		<span class="separator"></span>

		<?php
		$curn_lang = $this->lang->lang();
		// $flag = ($curn_lang == 'en') ? 'id' : 'en';
		// echo $this->lang->switch_uri($flag);die;
		 ?>

		 <ul class="notifications">
			<li>
				<a href="javascript:void(0);" class="dropdown-toggle notification-icon" data-toggle="dropdown">
					<img style="margin-top:4px;width:15px;" src="<?php echo site_url().'assets/Content/assets/images/'.$curn_lang.'.png'; ?>">
				</a>

				<div class="dropdown-menu notification-menu">
					<div class="notification-title">
						<!-- <span class="pull-right label label-default">3</span> -->
						<?php echo $curn_lang == "en" ? 'Switch Language' : 'Ganti Bahasa'; ?>
					</div>

					<div class="content">
						<ul>
							<li>
								<a href="<?php echo $this->lang->switch_uri('id'); ?>" class="clearfix">
									<div class="image">
										<img width="24" src="<?php echo site_url(); ?>assets/Content/assets/images/id.png">
									</div>
									<span class="title">Indonesia</span>
									<span class="message">ID</span>
								</a>
							</li>
							<li>
								<a href="<?php echo $this->lang->switch_uri('en'); ?>" class="clearfix">
									<div class="image">
										<img width="24" src="<?php echo site_url(); ?>assets/Content/assets/images/en.png">
									</div>
									<span class="title">English</span>
									<span class="message">US</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</li>
		</ul>

		<!--<ul class="notifications">
			<li>
				<a href="javascript:void(0);" class="dropdown-toggle notification-icon" data-toggle="dropdown">
					<i class="fa fa-bell"></i>
					<span class="badge">3</span>
				</a>

				<div class="dropdown-menu notification-menu">
					<div class="notification-title">
						<span class="pull-right label label-default">3</span>
						Alerts
					</div>

					<div class="content">
						<ul>
							<li>
								<a href="javascript:void(0);" class="clearfix">
									<div class="image">
										<i class="fa fa-thumbs-down bg-danger"></i>
									</div>
									<span class="title">Server is Down!</span>
									<span class="message">Just now</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="clearfix">
									<div class="image">
										<i class="fa fa-lock bg-warning"></i>
									</div>
									<span class="title">User Locked</span>
									<span class="message">15 minutes ago</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="clearfix">
									<div class="image">
										<i class="fa fa-signal bg-success"></i>
									</div>
									<span class="title">Connection Restaured</span>
									<span class="message">10/10/2014</span>
								</a>
							</li>
						</ul>

						<hr />

						<div class="text-right">
							<a href="javascript:void(0);" class="view-more">View All</a>
						</div>
					</div>
				</div>
			</li>
		</ul>-->

		<span class="separator"></span>

		<div id="userbox" class="userbox">
			<a href="javascript:void(0);" data-toggle="dropdown">
				<figure class="profile-picture">
					<?php $photo = !empty($user->meta->t0070f005) ? $user->meta->t0070f005 : "assets/backend/images/no-photo.png"; ?>
					<img src="<?php echo $photo ?>" alt="<?php echo $user->meta->t0070f002." ".$user->meta->t0070f003; ?>" class="img-circle" data-lock-picture="assets/upload/photo/<?php echo $user->meta->t0070f005 ?>" />
				</figure>
				<div class="profile-info" data-lock-name="<?php echo $user->meta->t0070f002." ".$user->meta->t0070f003; ?>" data-lock-email="admin@mail.com">
					<span class="name"><?php echo $user->meta->t0070f002." ".$user->meta->t0070f003; ?></span>
					<span class="role"><?php echo $role; ?></span>
				</div>

				<i class="fa custom-caret"></i>
			</a>

			<div class="dropdown-menu">
				<ul class="list-unstyled">
					<li class="divider"></li>
					<!-- <li>
						<a role="menuitem" tabindex="-1" href="javascript:void(0);"><i class="fa fa-user"></i> My Profile</a>
					</li>
					<!--<li>
						<a role="menuitem" tabindex="-1" href="javascript:void(0);" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
					</li>-->
					<li>
						<a role="menuitem" tabindex="-1" href="<?php echo site_url('logout'); ?>"><i class="fa fa-power-off"></i> Logout</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end: search & user box -->
</header>
