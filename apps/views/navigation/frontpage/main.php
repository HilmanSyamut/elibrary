<div class="main_menu">
	<ul>
		<?php
		$i = 1;
		foreach($nav as $item):
			$url = ($item->js) ? '#'.$item->alias : site_url($item->alias) ;
			$active = ($item->alias === 'home') ? 'active' : '';
			echo '<li><a href="'.$url.'" class="'.$active.' '.$item->alias.'" data-id="'.$i.'"><i class="'.$item->icon.'"></i></a></li>';
		$i++;
		endforeach;  ?>
	</ul>
</div>