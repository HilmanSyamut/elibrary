<html>
    <head>
        <title><?php echo $template['title'] ?></title>
		<base href="<?php echo base_url(); ?>" />
        <!-- Web Fonts  -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">
        <!-- Vendor CSS -->
        <link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" />
        <!-- Invoice Print Style -->
        <link rel="stylesheet" href="assets/backend/stylesheets/invoice-print.css" />
    </head>
    <body>
	<?php echo $template['content']; ?>
 </body>
</html>