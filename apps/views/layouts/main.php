<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling header-dark sidebar-left-sm no-mobile-device custom-scroll js flexbox flexboxlegacy no-touch csstransforms csstransforms3d no-overflowscrolling k-ff k-ff44">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $template['title'] ?></title>
		<base href="<?php echo base_url(); ?>" />
		<meta name="keywords" content="" />
		<meta name="description" content="BluesCode">
		<meta name="author" content="">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css"> -->

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/backend/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/backend/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<link rel="stylesheet" href="assets/backend/vendor/jquery-datatables-bs3/assets/css/datatables.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme-custom.css">

		<link href="assets/Content/kendo/2015.2.720/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/Content/kendo/2015.2.720/kendo.rtl.min.css" rel="stylesheet" type="text/css" />
	    <link href="assets/Content/kendo/2015.2.720/kendo.material.mobile.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/Content/kendo/2015.2.720/kendo.material.min.css" rel="stylesheet" type="text/css" />

		<!-- Head Libs -->
		<script src="assets/backend/vendor/modernizr/modernizr.js"></script>
		<script src="assets/backend/vendor/jquery/jquery.js"></script>
		<script src="assets/Content/assets/vendor/accounting/accounting.min.js"></script>
		<script type="text/javascript">
			var DateFormat = "<?php echo FORMATDATEUI; ?>";
			var DateTimeFormat = "<?php echo FORMATDATETIMEUI; ?>";
			function site_url(url){
				var site_url = "<?php echo site_url('"+url+"'); ?>";
				return site_url;
			}
			function current_url()
			{
				var current_url = "<?php echo current_url(); ?>";
				return current_url;
			}
			function current_module()
			{
				var current_module = "<?php echo $this->router->fetch_module(); ?>";
				return current_module;
			}
			// $(document).ready(function() {
			// 	$( document ).ajaxError(function(event, jqxhr, settings, thrownError) {
			// 		new PNotify({ title: "Failed", text: jqxhr.responseText, type: 'error', shadow: true });
			// 	});
			// });
		</script>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php $this->navigations->header(); ?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->navigations->generate(); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<?php $this->navigations->breadcrumb(); ?>

					<!-- start: page -->
					<?php echo $template['content']; ?>
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		
		<script src="assets/backend/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/backend/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/backend/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/backend/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/backend/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script src="assets/backend/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="assets/backend/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="assets/backend/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/backend/javascripts/tables/examples.datatables.default.js"></script>
		<script src="assets/backend/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="assets/backend/javascripts/tables/examples.datatables.tabletools.js"></script>
		<script src="assets/backend/vendor/pnotify/pnotify.custom.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/backend/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/backend/javascripts/theme.custom.js"></script>
		<script src="assets/backend/javascripts/ui-elements/examples.modals.js"></script>
		<script src="assets/Scripts/kendo/2015.2.720/jszip.min.js"></script>

		<!-- Theme Initialization Files -->
		<script src="assets/backend/javascripts/theme.init.js"></script>
		<script src="assets/Scripts/kendo/2015.2.720/kendo.all.min.js"></script>

		<!-- Examples -->
		<script src="assets/backend/javascripts/ui-elements/examples.notifications.js"></script>
        <script src="assets/backend/javascripts/forms/gridview.lib.js"></script>
        <script type="text/javascript" src="assets/js/script.js"></script>
		<script type="text/javascript">
			function goBack(i) {
				var ID = current_url();
				if(localStorage[ID]){
					$(window).bind('beforeunload', function () {
						return 'Apakah anda yakin akan meninggalkan halaman ini ?';
					});
				    if(i){ localStorage[ID] = ""; }
				}
				window.history.back();
			}
		</script>
	</body>
</html>
