<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * UserAutoLogin Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Userautologin extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserAutoLogin_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/UserAutoLogin/index',$data);
	}

	public function form($id='')
	{
		$data = $this->UserAutoLogin_model->getDetail($id);
		$this->modules->render('/UserAutoLogin/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->UserAutoLogin_model->getDetail($id);
		$this->modules->render('/UserAutoLogin/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemUserAutoLogin_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_SystemUserAutoLogin_key_id   => $this->input->post("key_id"),
					T_SystemUserAutoLogin_user_id   => $this->input->post("user_id"),
					T_SystemUserAutoLogin_user_agent   => $this->input->post("user_agent"),
					T_SystemUserAutoLogin_last_ip   => $this->input->post("last_ip"),
					T_SystemUserAutoLogin_last_login   => $this->input->post("last_login"),

					'detail'			=> $this->input->post("detail"),
				);
				$this->UserAutoLogin_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemUserAutoLogin_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemUserAutoLogin_RecordID   => $this->input->post("RecordID"),
					T_SystemUserAutoLogin_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemUserAutoLogin_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemUserAutoLogin_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemUserAutoLogin_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemUserAutoLogin_key_id   => $this->input->post("key_id"),
					T_SystemUserAutoLogin_user_id   => $this->input->post("user_id"),
					T_SystemUserAutoLogin_user_agent   => $this->input->post("user_agent"),
					T_SystemUserAutoLogin_last_ip   => $this->input->post("last_ip"),
					T_SystemUserAutoLogin_last_login   => $this->input->post("last_login"),

					'detail' => $this->input->post("detail"), 
					);

				$this->UserAutoLogin_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->UserAutoLogin_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file UserAutoLogin.php */
/* Location: ./app/modules/System/controllers/UserAutoLogin.php */
