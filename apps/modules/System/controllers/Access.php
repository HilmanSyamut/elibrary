<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Accesss
 *
 * Access Controller
 *
 * @package	    App
 * @subpackage	Access
 * @category	Access Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Access extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Access_model');
	}

	public function index()
	{
		$data['mod'] = $this->Access_model->getData();
		$this->modules->render('/Access/index',$data);
	}
}

/* End of file Accesss.php */
/* Location: ./app/Accesss/System/controllers/Accesss.php */
