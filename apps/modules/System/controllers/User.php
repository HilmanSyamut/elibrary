<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * User Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class User extends BC_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Options_model');
	}

	public function index()
	{
		$data['data'] = $this->User_model->getData();
		$this->modules->render('/user/index',$data);
	}

	public function getModule()
	{
		$output = $this->User_model->getListModule();
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function form($id='')
	{
		$data = $this->User_model->getDetail($id);
		$data['module'] = $this->User_model->getListModule();
		$data['menu'] = $this->User_model->getListMenu();
		$this->modules->render('/user/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->User_model->getDetail($id);
		$this->modules->render('/user/formDetail', $data);
	}

	public function upload()
	{
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
		$path = 'assets/upload/photo/'; // upload directory

		if(isset($_FILES['image']))
		{
			$img = $_FILES['image']['name'];
			$tmp = $_FILES['image']['tmp_name'];
				
			// get uploaded file's extension
			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			
			// can upload same image using rand function
			$final_image = rand(1000,1000000).".".$ext;
			
			// check's valid format
			if(in_array($ext, $valid_extensions)) 
			{					
				$path = $path.strtolower($final_image);	
					
				if(move_uploaded_file($tmp,$path)) 
				{
					$output['res'] = "<img class='img-responsive' src='$path' width='145px'/>";
					$output['img'] = $path; 
				}
			} 
			else 
			{
				$output['res'] = 'invalid';
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				if(!empty($this->input->post("access"))){
					$access = explode(",",$this->input->post('access'));
					$access = array_filter($access);
					$access = implode(',', $access);
					$access = array_map('intval', explode(',', $access));
					$access = serialize($access);
				}else{
					$access = 'a:1:{i:0;s:1:"1";}';
				}
				if(!empty($this->input->post("acceesMenu"))){
					$accessMenu = explode(",",$this->input->post('acceesMenu'));
					$accessMenu = array_filter($accessMenu);
					$accessMenu = implode(',', $accessMenu);
					$accessMenu = array_map('intval', explode(',', $accessMenu));
					$accessMenu = serialize($accessMenu);
				}else{
					$accessMenu = 'a:1:{i:0;s:1:"1";}';
				}
				$data = array(
					'password' => $this->input->post("password"),
					'email' => $this->input->post("username"),
					'user_role_id' => $this->input->post("user_role_id"),
					'accessMenu' => $accessMenu,
					'access' => $access
				);
				$user_id = $this->ezrbac->createUser($data);

				$data = array(
					T_SystemUserMeta_user_id   => $user_id,
					T_SystemUserMeta_first_name   => $this->input->post("firstname"),
					T_SystemUserMeta_last_name   => $this->input->post("lastname"),
					T_SystemUserMeta_phone   => $this->input->post("phone"),
					T_SystemUserMeta_photo   => $this->input->post("photo"),
					T_SystemUserMeta_address   => $this->input->post("address"),
					T_SystemUserMeta_provinsi_id   => $this->input->post("Country"),
					T_SystemUserMeta_kecamatan_id   => $this->input->post("State"),
					T_SystemUserMeta_kabkot_id   => $this->input->post("City"),
					T_SystemUserMeta_facebook => $this->input->post("facebook"),
					T_SystemUserMeta_twitter => $this->input->post("twitter"),
					T_SystemUserMeta_website => $this->input->post("website"),
				);
				$this->User_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemUser_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				if(!empty($this->input->post("access"))){
					$access = explode(",",$this->input->post('access'));
					$access = array_filter($access);
					$access = implode(',', $access);
					$access = array_map('intval', explode(',', $access));
					$access = serialize($access);
				}else{
					$access = 'a:1:{i:0;s:1:"1";}';
				}
				if(!empty($this->input->post("acceesMenu"))){
					$accessMenu = explode(",",$this->input->post('acceesMenu'));
					$accessMenu = array_filter($accessMenu);
					$accessMenu = implode(',', $accessMenu);
					$accessMenu = array_map('intval', explode(',', $accessMenu));
					$accessMenu = serialize($accessMenu);
				}else{
					$accessMenu = 'a:1:{i:0;s:1:"1";}';
				}
				$data = array(
					'id' => $this->input->post("RecordID"),
					'password' => $this->input->post("password"),
					'email' => $this->input->post("username"),
					'user_role_id' => $this->input->post("user_role_id"),
					'accessMenu' => $accessMenu,
					'access' => $access 
				);
				if($this->input->post("password")==""){ 
					unset($data['password']);
				}
				$this->ezrbac->updateUser($data);

				$data = array(
					T_SystemUserMeta_user_id   => $this->input->post("RecordID"),
					T_SystemUserMeta_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemUserMeta_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemUserMeta_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemUserMeta_first_name   => $this->input->post("firstname"),
					T_SystemUserMeta_last_name   => $this->input->post("lastname"),
					T_SystemUserMeta_phone   => $this->input->post("phone"),
					T_SystemUserMeta_photo   => $this->input->post("photo"),
					T_SystemUserMeta_address   => $this->input->post("address"),
					T_SystemUserMeta_provinsi_id   => $this->input->post("Country"),
					T_SystemUserMeta_kecamatan_id   => $this->input->post("State"),
					T_SystemUserMeta_kabkot_id   => $this->input->post("City"),
					T_SystemUserMeta_facebook => $this->input->post("facebook"),
					T_SystemUserMeta_twitter => $this->input->post("twitter"),
					T_SystemUserMeta_website => $this->input->post("website"),
				);

				$this->User_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->User_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file User.php */
/* Location: ./app/modules/System/controllers/User.php */
