<div class="row">
    <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
                        <a data-panel-dismiss="" class="panel-action panel-action-dismiss" href="#"></a>
                    </div>
                    <h2 class="panel-title">Form User</h2>
                    <p class="panel-subtitle">
                        Manage User in application.
                    </p>
                </header>
                <div class="panel-body">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Firstname</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'firstname',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_first_name}) ? $meta{T_SystemUserMeta_first_name} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Lastname</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'lastname',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_last_name}) ? $meta{T_SystemUserMeta_last_name} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Phone</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'phone',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_phone}) ? $meta{T_SystemUserMeta_phone} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Country</label>
                                    <div class="col-md-9">
                                        <?php $items=array( 'id'=> 'Country', 'class' => 'k-textbox', 'style' => 'width:150px;', 'value' => isset($meta{T_SystemUserMeta_provinsi_id}) ? $meta{T_SystemUserMeta_provinsi_id} : "",  ); 
                                        echo form_input($items); ?>
                                        
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">State</label>
                                    <div class="col-md-9">
                                        <?php $items=array( 'id'=> 'State', 'class' => 'k-textbox', 'value' => isset($meta{T_SystemUserMeta_kecamatan_id}) ? $meta{T_SystemUserMeta_kecamatan_id} : "",  ); 
                                        echo form_input($items); ?>
                                        
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">City</label>
                                    <div class="col-md-9">
                                        <?php $items=array( 'id'=> 'City', 'class' => 'k-textbox', 'value' => isset($meta{T_SystemUserMeta_kabkot_id}) ? $meta{T_SystemUserMeta_kabkot_id} : "",  ); 
                                        echo form_input($items); ?>
                                        
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Address</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'address',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_address}) ? $meta{T_SystemUserMeta_address} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">facebook</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'facebook',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_facebook}) ? $meta{T_SystemUserMeta_facebook} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">twitter</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'twitter',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_twitter}) ? $meta{T_SystemUserMeta_twitter} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">website</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'website',
                                        'class' => 'form-control',
                                        'value' => isset($meta{T_SystemUserMeta_website}) ? $meta{T_SystemUserMeta_website} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Photo</label>
                                    <?php
                                        if(isset($meta{T_SystemUserMeta_photo})){
                                            $voImg = ($meta{T_SystemUserMeta_photo} == '') ? "assets/backend/images/no-photo.png" : $meta{T_SystemUserMeta_photo};
                                        }else{
                                            $voImg = "assets/backend/images/no-photo.png";
                                        }
                                     ?>
                                    <div class="col-md-6" style="margin-bottom:35px">
                                        <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                            <div id="preview"><img class="img-responsive" src="<?php echo $voImg; ?>" / width="145px"></div>
                                        </a>
                                        <form id="form-photo" action="<?php echo site_url('en/system/user/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                            <input id="uploadImage" type="file" accept="image/*" name="image" />
                                            <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                        </form>
                                        <div id="err"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 form-label">username</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'username',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemUser_email}) ? ${T_SystemUser_email} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                        <input type="hidden" id="RecordID" value="<?php echo isset(${T_SystemUser_RecordID}) ? ${T_SystemUser_RecordID} : ''; ?>">
                                        <input type="hidden" id="RecordTimestamp" value="<?php echo isset(${T_SystemUser_RecordTimestamp}) ? ${T_SystemUser_RecordTimestamp} : ''; ?>">
                                        <input type="hidden" id="photo" value="<?php echo isset($meta{T_SystemUserMeta_photo}) ? $meta{T_SystemUserMeta_photo} : ''; ?>">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Password</label>
                                    <div class="col-md-6">
                                        <div id="ChangePass" onclick="changePass();" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning">Change</div>
                                        <?php
                                        $items = array(
                                        'id' => 'password',
                                        'class' => 'form-control',
                                        'value' => ''
                                        );
                                        echo form_password($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Role</label>
                                    <div class="col-md-5">
                                        <?php 
                                        echo form_dropdown('user_role_id', $this->Options_model->get_lookup('',T_SystemUserRole,T_SystemUserRole_RecordID,T_SystemUserRole_role_name,T_SystemUserRole_RecordID), (isset(${T_SystemUser_user_role_id}) ? ${T_SystemUser_user_role_id} : '') ,'class="form-control select2" id="user_role_id"');
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Access Module</label>
                                    <div class="col-md-5">
                                        <div class="demo-section k-content">
                                            <div>
                                                <div id="treeview"></div>
                                            </div>
                                            <div style="padding-top: 2em; display: none;">
                                                <textarea id="access" class="k-textbox" style="width:350px; height:250px;" readonly="true"> </textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Menu Access</label>
                                    <div class="col-md-5">
                                        <div class="demo-section k-content">
                                            <div>
                                                <div id="treeview-menu"></div>
                                            </div>
                                            <div style="padding-top: 2em; display: none;">
                                                <textarea id="access-menu" class="k-textbox" style="width:350px; height:250px;" readonly="true"> </textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                             
                        </div>
                </div>
                <footer class="panel-footer">
                    <button class="btn btn-primary" type="button" onclick="processData();">Submit </button>
                    <button class="btn btn-default" type="button" onclick="goBack();">Cancel</button>
                </footer>
            </section>
    </div>
</div>
<?php
$Country = isset($meta{T_SystemUserMeta_provinsi_id}) ? $meta{T_SystemUserMeta_provinsi_id} : 0;
$State = isset($meta{T_SystemUserMeta_kecamatan_id}) ? $meta{T_SystemUserMeta_kecamatan_id} : 0;
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_SystemUser_RecordID}) ? ${T_SystemUser_RecordID} : 0; ?>;
var Country = "<?php echo $Country; ?>";
var State = "<?php echo $State; ?>";
$(document).ready(function() {
    if(ID){
        $("#password").hide();
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/generalList/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: Country},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            }
        });
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/generalList/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: State},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            }
        });
    }else{
        $("#ChangePass").hide();
        $("#State").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });

        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    }

    $("#Country").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataCountry_CountryName; ?>",
        dataValueField: "<?php echo T_MasterDataCountry_RecordID; ?>",
        optionLabel: "Select",
        change: setState,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/generalList/getCountry"),
                    data: {table: '<?php echo T_MasterDataCountry; ?>'},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });

    function setState(e) {
        var dataItem = this.dataItem(e.item);
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/generalList/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: dataItem.<?php echo T_MasterDataCountry_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            }
        });
        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    };

    function setCity(e) {
        var dataItem = this.dataItem(e.item);
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/generalList/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: dataItem.<?php echo T_MasterDataState_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            }
        });
    }

    $("#treeview").kendoTreeView({
        checkboxes: {
            checkChildren: true
        },

        check: onCheck,

        dataSource: [
            <?php
            if (isset($module)){
                $checked = "false";
                $previlage = isset(${T_SystemUser_access}) ? unserialize(${T_SystemUser_access}) : array();
                foreach ($module as $ListData) {
                    echo ' { id: 0, text: "'.$ListData[T_SystemModules_alias].'", expanded: false, spriteCssClass: "rootfolder", items: [';
                    foreach ($ListData['controller'] as $DataController) {
                        echo '{ id: 0, text: "'.$DataController[T_SystemControllers_alias].'", expanded: false, spriteCssClass: "folder", items: [';
                            foreach ($DataController['methode'] as $DataMethode) {
                                if (in_array($DataMethode[T_SystemMethode_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                                echo '{ id: '.$DataMethode[T_SystemMethode_RecordID].', text: "'.$DataMethode[T_SystemMethode_alias].'", spriteCssClass: "html", checked: '.$checked.' },';

                            }
                        echo '] },';
                    }
                    echo '] },';
                }
            }
            ?>
           
        ]
    });

    $("#treeview-menu").kendoTreeView({
        checkboxes: {
            checkChildren: true
        },

        check: onCheckMenu,

        dataSource: [
            <?php
            if (isset($menu)){
                $checked = "false";
                $previlage = isset(${T_SystemUser_accessMenu}) ? unserialize(${T_SystemUser_accessMenu}) : array();
                foreach ($menu as $ListMenu) {
                    if (in_array($ListMenu[T_SystemNavigation_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                    echo ' { id: '.$ListMenu[T_SystemNavigation_RecordID].', text: "'.$ListMenu[T_SystemNavigation_alias].'", expanded: false, spriteCssClass: "rootfolder", checked: '.$checked.', items: [';
                    if(!empty($ListMenu['sub'])):
                    foreach ($ListMenu['sub'] as $DataMenu) {
                        echo '{ id: '.$DataMenu[T_SystemNavigation_RecordID].', text: "'.$DataMenu[T_SystemNavigation_alias].'", expanded: false, spriteCssClass: "folder", items: [';
                            if(!empty($DataMenu['sub'])):
                            foreach ($DataMenu['sub'] as $DataSub) {
                                if (in_array($DataSub[T_SystemNavigation_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                                echo '{ id: '.$DataSub[T_SystemNavigation_RecordID].', text: "'.$DataSub[T_SystemNavigation_alias].'", spriteCssClass: "html", checked: '.$checked.' },';

                            }
                            endif;
                        echo '] },';
                    }
                    endif;
                    echo '] },';
                }
            }
            ?>
           
        ]
    });

    onCheck();
    onCheckMenu();
});

function changePass()
{
    $("#password").show();
    $("#ChangePass").hide();
}

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}

function insert()
{
    var voData = {
        firstname: $('#firstname').val(),
        lastname: $('#lastname').val(),
        phone: $('#phone').val(),
        Country: $('#Country').val(),
        State: $('#State').val(),
        City: $('#City').val(),
        PostalCode: $('#PostalCode').val(),
        Country: $('#Country').val(),
        address: $('#address').val(),
        facebook: $('#facebook').val(),
        twitter: $('#twitter').val(),
        website: $('#website').val(),
        username: $("#username").val(),
        password: $("#password").val(),
        user_role_id: $("#user_role_id").val(),
        photo: $("#photo").val(),
        acceesMenu : $('#access-menu').val(),
        access: $("#access").val()
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('System/User/insert'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    var ID = current_url();
                    localStorage[ID] = "";
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function update()
{
    var voData = {
        RecordID: $('#RecordID').val(),
        RecordTimestamp: $('#RecordTimestamp').val(),
        firstname: $('#firstname').val(),
        lastname: $('#lastname').val(),
        phone: $('#phone').val(),
        Country: $('#Country').val(),
        State: $('#State').val(),
        City: $('#City').val(),
        PostalCode: $('#PostalCode').val(),
        Country: $('#Country').val(),
        address: $('#address').val(),
        facebook: $('#facebook').val(),
        twitter: $('#twitter').val(),
        website: $('#website').val(),
        username: $("#username").val(),
        password: $("#password").val(),
        user_role_id: $("#user_role_id").val(),
        photo: $("#photo").val(),
        acceesMenu : $('#access-menu').val(),
        access: $("#access").val()
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('System/User/update'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    var ID = current_url();
                    localStorage[ID] = "";
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function checkForm(voData) {
    var valid = 1;
    var msg = "";
    if (voData.firstname == "") { valid = 0; msg += "Firstname is required" + "\r\n"; }
    if (voData.lastname == "") { valid = 0; msg += "Lastname is required" + "\r\n"; }
    if (voData.phone == "") { valid = 0; msg += "Phone is required" + "\r\n"; }
    if (voData.Address == "") { valid = 0; msg += "Address is required" + "\r\n"; }
    if (voData.City == "") { valid = 0; msg += "City is required" + "\r\n"; }
    if (voData.State == "") { valid = 0; msg += "State is required" + "\r\n"; }
    if (voData.Country == "") { valid = 0; msg += "Country is required" + "\r\n"; }
    if (voData.username == "") { valid = 0; msg += "username is required" + "\r\n"; }
    if (voData.RecordID){  }else{ if (voData.password == "") { valid = 0; msg += "password is required" + "\r\n"; } }
    if (voData.user_role_id == "") { valid = 0; msg += "role is required" + "\r\n"; }
    var voRes = {valid: valid, msg: msg};
    return voRes;
}
</script>

<script>

// function that gathers IDs of checked nodes
function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
            checkedNodes.push(nodes[i].id);
        }

        if (nodes[i].hasChildren) {
            checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
    }
}

// show checked node IDs on datasource change
function onCheck() {
    var checkedNodes = [],
        treeView = $("#treeview").data("kendoTreeView"),
        message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length > 0) {
        message = checkedNodes.join(",");
    } else {
        message = "No nodes checked.";
    }

    $("#access").val(message);
}

function onCheckMenu() {
    var checkedNodes = [],
        treeView = $("#treeview-menu").data("kendoTreeView"),
        message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length >= 0) {
        message = checkedNodes.join(",");
    } else {
        message = "No nodes checked.";
    }

    $("#access-menu").val(message);
}
</script>