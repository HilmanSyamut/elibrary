<div class="row">
    <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
                        <a data-panel-dismiss="" class="panel-action panel-action-dismiss" href="#"></a>
                    </div>
                    <h2 class="panel-title">Form User Roles</h2>
                    <p class="panel-subtitle">
                        Manage User Role in application.
                    </p>
                </header>
                <div class="panel-body">
                    <input type="hidden" id="RecordID" value="<?php echo isset(${T_SystemUserRole_RecordID}) ? ${T_SystemUserRole_RecordID} : ""; ?>">
                <input type="hidden" id="RecordTimestamp" value="<?php echo isset(${T_SystemUserRole_RecordTimestamp}) ? ${T_SystemUserRole_RecordTimestamp} : ""; ?>">
                <input type="hidden" id="RecordStatus" value="<?php echo isset(${T_SystemUserRole_RecordStatus}) ? ${T_SystemUserRole_RecordStatus} : ""; ?>">
                <input type="hidden" id="RecordUpdatedOn" value="<?php echo isset(${T_SystemUserRole_RecordUpdatedOn}) ? ${T_SystemUserRole_RecordUpdatedOn} : ""; ?>">
                <input type="hidden" id="RecordUpdatedBy" value="<?php echo isset(${T_SystemUserRole_RecordUpdatedBy}) ? ${T_SystemUserRole_RecordUpdatedBy} : ""; ?>">
                <input type="hidden" id="RecordUpdatedAt" value="<?php echo isset(${T_SystemUserRole_RecordUpdatedAt}) ? ${T_SystemUserRole_RecordUpdatedAt} : ""; ?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Role Name</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'RoleName',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemUserRole_role_name}) ? ${T_SystemUserRole_role_name} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Menu Access</label>
                                    <div class="col-md-5">
                                        <div class="demo-section k-content">
                                            <div>
                                                <div id="treeview-menu"></div>
                                            </div>
                                            <div style="padding-top: 2em; display: none;">
                                                <textarea id="access-menu" class="k-textbox" style="width:350px; height:250px;" readonly="true"> </textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Module Access</label>
                                    <div class="col-md-5">
                                        <div class="demo-section k-content">
                                            <div>
                                                <div id="treeview"></div>
                                            </div>
                                            <div style="padding-top: 2em; display: none;">
                                                <textarea id="access" class="k-textbox" style="width:350px; height:250px;" readonly="true"> </textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                             
                        </div>
                </div>
                <footer class="panel-footer">
                    <button class="btn btn-primary" type="button" onclick="processData();">Submit </button>
                    <button class="btn btn-default" type="button" onclick="goBack();">Cancel</button>
                </footer>
            </section>
    </div>
</div>
<script type="text/javascript">
var ID = <?php echo isset(${T_SystemUserRole_RecordID}) ? ${T_SystemUserRole_RecordID} : 0; ?>;
$(document).ready(function() {

    $("#treeview").kendoTreeView({
        checkboxes: {
            checkChildren: true
        },

        check: onCheck,

        dataSource: [
            <?php
            if (isset($module)){
                $checked = "false";
                $previlage = isset(${T_SystemUserRole_access}) ? unserialize(${T_SystemUserRole_access}) : array();
                foreach ($module as $ListData) {
                    echo ' { id: 0, text: "'.$ListData[T_SystemModules_alias].'", expanded: false, spriteCssClass: "rootfolder", items: [';
                    foreach ($ListData['controller'] as $DataController) {
                        echo '{ id: 0, text: "'.$DataController[T_SystemControllers_alias].'", expanded: false, spriteCssClass: "folder", items: [';
                            foreach ($DataController['methode'] as $DataMethode) {
                                if (in_array($DataMethode[T_SystemMethode_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                                echo '{ id: '.$DataMethode[T_SystemMethode_RecordID].', text: "'.$DataMethode[T_SystemMethode_alias].'", spriteCssClass: "html", checked: '.$checked.' },';

                            }
                        echo '] },';
                    }
                    echo '] },';
                }
            }
            ?>
           
        ]
    });


    $("#treeview-menu").kendoTreeView({
        checkboxes: {
            checkChildren: true
        },

        check: onCheckMenu,

        dataSource: [
            <?php
            if (isset($menu)){
                $checked = "false";
                $previlage = isset(${T_SystemUserRole_default_access}) ? unserialize(${T_SystemUserRole_default_access}) : array();
                foreach ($menu as $ListMenu) {
                    if (in_array($ListMenu[T_SystemNavigation_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                    echo ' { id: '.$ListMenu[T_SystemNavigation_RecordID].', text: "'.$ListMenu[T_SystemNavigation_alias].'", expanded: false, spriteCssClass: "rootfolder", checked: '.$checked.', items: [';
                    if(!empty($ListMenu['sub'])):
                    foreach ($ListMenu['sub'] as $DataMenu) {
                        echo '{ id: '.$DataMenu[T_SystemNavigation_RecordID].', text: "'.$DataMenu[T_SystemNavigation_alias].'", expanded: false, spriteCssClass: "folder", items: [';
                            if(!empty($DataMenu['sub'])):
                            foreach ($DataMenu['sub'] as $DataSub) {
                                if (in_array($DataSub[T_SystemNavigation_RecordID], $previlage)) { $checked = "true"; }else{ $checked="false"; }
                                echo '{ id: '.$DataSub[T_SystemNavigation_RecordID].', text: "'.$DataSub[T_SystemNavigation_alias].'", spriteCssClass: "html", checked: '.$checked.' },';

                            }
                            endif;
                        echo '] },';
                    }
                    endif;
                    echo '] },';
                }
            }
            ?>
           
        ]
    });

    onCheck();
    onCheckMenu();
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}

function insert()
{
    var voData = {
        RoleName: $('#RoleName').val(),
        acceesMenu : $('#access-menu').val(),
        access: $("#access").val()
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('System/UserRole/insert'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function update()
{
    var voData = {
        RecordID: $('#RecordID').val(),
        RecordTimestamp: $('#RecordTimestamp').val(),
        RecordStatus: $('#RecordStatus').val(),
        RecordUpdatedOn: $('#RecordUpdatedOn').val(),
        RecordUpdatedBy: $('#RecordUpdatedBy').val(),
        RecordUpdatedAt: $('#RecordUpdatedAt').val(),
        RoleName: $('#RoleName').val(),
        acceesMenu : $('#access-menu').val(),
        access: $("#access").val()
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('System/UserRole/update'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function checkForm(voData) {
    var valid = 1;
    var msg = "";
    if (voData.RoleName == "") { valid = 0; msg += "Role Name is required" + "\r\n"; }
    if (voData.sccess == "") { valid = 0; msg += "Lastname is required" + "\r\n"; }
    var voRes = {valid: valid, msg: msg};
    return voRes;
}

// function that gathers IDs of checked nodes
function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
            checkedNodes.push(nodes[i].id);
        }

        if (nodes[i].hasChildren) {
            checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
    }
}

// show checked node IDs on datasource change
function onCheck() {
    var checkedNodes = [],
        treeView = $("#treeview").data("kendoTreeView"),
        message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length > 0) {
        message = checkedNodes.join(",");
    } else {
        message = "No nodes checked.";
    }

    $("#access").val(message);
}

function onCheckMenu() {
    var checkedNodes = [],
        treeView = $("#treeview-menu").data("kendoTreeView"),
        message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length >= 0) {
        message = checkedNodes.join(",");
    } else {
        message = "No nodes checked.";
    }

    $("#access-menu").val(message);
}
</script>