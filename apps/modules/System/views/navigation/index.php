<div class="row">
    <div class="col-md-12">
    <section class="panel panel-primary">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            </div>
            <a class="btn btn-primary" href="<?php echo site_url('System/navigation/form'); ?>">+ Add New</a>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                <tr>
                    <th width="15px">Action</th>
                    <th>alias</th>
                    <th>title</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                foreach ($data as $key => $item) {
                    $child = $this->Navigations_model->getDataChild($item->{T_SystemNavigation_RecordID});
                    $tool = '<a href="'.site_url("System/navigation/form/".$item->{T_SystemNavigation_RecordID}).'"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" class="delete-row" onclick="deleteData('.$item->{T_SystemNavigation_RecordID}.');" ><i class="fa fa-trash-o"></i></a>';
                    $title = unserialize($item->{T_SystemNavigation_title});
                    echo "<tr id='listData-".$item->{T_SystemNavigation_RecordID}."'><td class='actions'>".$tool."</td>
                    <td><b>".$item->{T_SystemNavigation_alias}."</b></td>
                    <td><b>".$title[$this->lang->lang()]."</b></td>
                     <td><b>".$item->{T_SystemNavigation_description}."</b></td>
                    </tr>";
                    foreach ($child as $childkey => $childItem) {
                        $childs = $this->Navigations_model->getDataChild($childItem->{T_SystemNavigation_RecordID});
                        $tools = '<a href="'.site_url("System/navigation/form/".$childItem->{T_SystemNavigation_RecordID}).'"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o" onclick="deleteData('.$childItem->{T_SystemNavigation_RecordID}.');"></i></a>';
                        $title = unserialize($childItem->{T_SystemNavigation_title});
                        echo "<tr id='listData-".$childItem->{T_SystemNavigation_RecordID}."'><td class='actions'>".$tools."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$childItem->{T_SystemNavigation_alias}."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$title[$this->lang->lang()]."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$childItem->{T_SystemNavigation_description}."</td>
                        </tr>";
                        foreach($childs as $sub){
                        $tools2 = '<a href="'.site_url("System/navigation/form/".$sub->{T_SystemNavigation_RecordID}).'"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o" onclick="deleteData('.$sub->{T_SystemNavigation_RecordID}.');"></i></a>';
                        $title2 = unserialize($sub->{T_SystemNavigation_title});
                            echo "<tr id='listData-".$sub->{T_SystemNavigation_RecordID}."'><td class='actions'>".$tools2."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$sub->{T_SystemNavigation_alias}."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$title2[$this->lang->lang()]."</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$sub->{T_SystemNavigation_description}."</td>
                        </tr>";
                        }
                    }
                } 
                ?>
                </tbody>
            </table>
        </div>
    </section>
    </div>
</div>
<script type="text/javascript">
    function deleteData(id)
    {
        var json_data = {
            RecordID : id,
        };

        var url = site_url('System/Navigation/delete');

        $.ajax({
            type: 'POST',
            data:json_data,
            url: url,
            dataType: 'JSON',
            beforeSend: function(){
            },
            success: function(data){
                if(data.errorcode < 1){
                    new PNotify({ title: "Success", text: data.msg, type: 'success', shadow: true });
                    $("#listData-"+id).remove();
                }else{
                    new PNotify({ title: "Error", text: data.msg, type: 'error', shadow: true });
                }
            } 
        });
    }
</script>