<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData User
 *
 * Navigation Model
 *
 * @package     App
 * @subpackage  User
 * @category    Navigation Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class User_model extends CI_Model
{    

    public function getData(){
    	$this->db->where(T_SystemUser_status,1);
        $query = $this->db->get(T_SystemUser);
        return $query->result();
    }

    public function getDataChild($id){
    	$this->db->where(T_SystemUser_depth,$id);
        $query = $this->db->get(T_SystemUser);
        return $query->result();
    }

    public function getListModule(){
        $query = $this->db->get(T_SystemModules);
        $data = $query->result('array');
        foreach ($data as $module) {
            $res[] = array(
                T_SystemModules_RecordID => $module[T_SystemModules_RecordID],
                T_SystemModules_alias => $module[T_SystemModules_alias],
                T_SystemModules_title => $module[T_SystemModules_title],
                T_SystemModules_description => $module[T_SystemModules_description],
                'controller' => $this->getListController($module[T_SystemModules_RecordID])
            );
        }
        return $res;
    }

    public function getListController($id){
        $this->db->where(T_SystemControllers_module_id,$id);
        $query = $this->db->get(T_SystemControllers);
        $data = $query->result('array');
        foreach ($data as $controller) {
            $res[] = array(
                T_SystemControllers_RecordID => $controller[T_SystemControllers_RecordID],
                T_SystemControllers_alias => $controller[T_SystemControllers_alias],
                T_SystemControllers_title => $controller[T_SystemControllers_title],
                T_SystemControllers_description => $controller[T_SystemControllers_description],
                'methode' => $this->getListMethode($controller[T_SystemControllers_RecordID])
            );
        }
        return $res;
    }

    public function getListMethode($id)
    {
        $this->db->where(T_SystemMethode_controller_id,$id);
        $query = $this->db->get(T_SystemMethode);
        $data = $query->result('array');
        return $data;
    }

    public function getListMenu($id=0)
    {
        $this->db->where(T_SystemNavigation_depth,$id);
        $query = $this->db->get(T_SystemNavigation);
        $data = $query->result('array');
        $res = "";
        if($query->num_rows() > 0):
        foreach ($data as $module) {
            $res[] = array(
                T_SystemNavigation_RecordID => $module[T_SystemNavigation_RecordID],
                T_SystemNavigation_alias => $module[T_SystemNavigation_alias],
                T_SystemNavigation_title => $module[T_SystemNavigation_title],
                T_SystemNavigation_description => $module[T_SystemNavigation_description],
                'sub' => $this->getListMenu($module[T_SystemNavigation_RecordID])
            );
        }
        endif;
        return $res;
    }

    public function getDetail($id){
        $this->db->where(T_SystemUser_RecordID,$id);
        $query = $this->db->get(T_SystemUser);
        $data = $query->first_row('array');
        $data['meta'] = $this->getMeta($data[T_SystemUser_RecordID]);
        return $data;
    }

    public function getMeta($id)
    {
        $this->db->where(T_SystemUserMeta_user_id,$id);
        $query = $this->db->get(T_SystemUserMeta);
        $data = $query->first_row('array');
        return $data;
    }

    public function insert($data)
    {
        $this->db->trans_begin();
        $this->db->insert(T_SystemUserMeta,$data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function update($data)
    {
        $this->db->trans_begin();
        $userID = $data[T_SystemUserMeta_user_id];
        unset($data[T_SystemUserMeta_user_id]);
        $this->db->where(T_SystemUserMeta_user_id,$userID);
        $this->db->update(T_SystemUserMeta,$data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}
