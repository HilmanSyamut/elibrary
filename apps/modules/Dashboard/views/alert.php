<div class="col-md-6">
 <span class="text-right">
        <button id="IntervalStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="myInterval(1)" style="display:none;">Start</button>
        <button id="IntervalStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="myInterval(0)">Stop</button>
    </span>
    <section class="panel">
        <div class="panel-body bg-secondary">
            <div class="widget-summary">
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Alert Qty - <?php echo date("Y-m-d"); ?></h4>
                        <div class="info">
                            <strong class="amount" id="alert" style="font-size: xx-large; font-weight: 100;">0</strong>
                        </div>
                    </div>
                    <div class="summary-footer">
                        <a class="text-uppercase" onclick="viewalert()">(view all)</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="col-md-12" id="gridalertview" style="display: none">
    <section class="panel panel-secondary">
        <header class="panel-heading">
            <div class="panel-actions">
                <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
            </div>
            <h2 class="panel-title">Alert</h2>
        </header>
        <div class="panel-body">

        <div id="gridAlert"></div>
        <script type="text/javascript">
        $(document).ready(function() {
        //Grid
            $("#gridAlert").kendoGrid({  
                height: "330px",
                width: "100%",
                columns: [
                {
                "title":"Seen",
                "width":"100px",
                "field":"<?php echo T_Alert_RecordTimestamp; ?>",
                },
                {
                "title":"ItemID",
                "width":"50px",
                "field":"<?php echo T_MasterDataItem_ItemID; ?>",
                },
                {
                "title":"Item Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_ItemName; ?>",
                },
                {
                "title":"EPC",
                "width":"150px",
                "field":"<?php echo T_MasterDataItem_EPC; ?>",
                },
                {
                "title":"Barcode",
                "width":"150px",
                "field":"<?php echo T_MasterDataItem_Barcode; ?>",
                },
                {
                "title":"Qty",
                "width":"80px",
                "field":"<?php echo T_Alert_Qty; ?>",
                },
                {
                "title":"UOM",
                "width":"80px",
                "field":"<?php echo T_MasterDataItem_UOMID; ?>",
                }
                ],                     
                dataSource: {
                    transport: {
                        read: {
                            type:"GET",
                            data: { table: 't1022'},
                            url: site_url('Dashboard/alertData'),
                            dataType: "json"
                        }
                    },
                    sync: function(e) {
                        $('#gridAlert').data('kendoGrid').dataSource.read();
                        $('#gridAlert').data('kendoGrid').refresh();
                    },
                    schema: {
                        data: function(data){
                            return data.data;
                        },
                        total: function(data){
                            return data.count;
                        },
                        model: {
                            id: "t1010r001",
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },        
                filterable: false,
                selectable: true,
                autoBind:false,       
                groupable: false,
                sortable: true,
                resizable: true,
                scrollable: true,
                reorderable:true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
            });
        });
    </script>
    </div>
</section>
</div>
<script type="text/javascript">

var realTime = setInterval(function(){ getData(); }, 1000);

function getData()
{
    $.ajax({
    type: 'POST',
    url: site_url('Dashboard/alertCount'),
    dataType: "json",
        success: function (data) {
        $("#alert").text(data.Qty);
        viewalert();
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
    });
}
function viewalert(){
    $('#gridAlert').data('kendoGrid').dataSource.read();
    $('#gridAlert').data('kendoGrid').refresh();
    $("#gridalertview").removeAttr('style');
}
function myInterval(i) {
    if(i){
        $("#IntervalStart").hide();
        $("#IntervalStop").show();
        realTime = setInterval(function(){ getData(); }, 1000);
    }else{
        $("#IntervalStart").show();
        $("#IntervalStop").hide();
        clearInterval(realTime);
    }
}
</script>