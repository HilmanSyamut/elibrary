<div class="col-md-12">
    <span class="text-right">
        <button id="IntervalStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="myInterval(1)" style="display:none;">Start</button>
        <button id="IntervalStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="myInterval(0)">Stop</button>
    </span>
    <div class="row">
        <div class="col-md-6">
            <section class="panel">
                <div class="panel-body bg-primary">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Stock In</h4>
                                <div class="info">
                                    <strong class="amount" id="stockOnhand">0</strong>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-uppercase" onclick="viewstockOnhand()">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="panel">
                <div class="panel-body bg-tertiary">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Stock Out</h4>
                                <div class="info">
                                    <strong class="amount" id="stockSold">0</strong>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-uppercase" onclick="viewstockSold()">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="col-md-12" id="gridStockOnHandview">
    <section class="panel panel-primary">
        <header class="panel-heading">
            <div class="panel-actions">
                <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
            </div>
            <h2 class="panel-title">Stock In</h2>
        </header>
        <div class="panel-body">

        <div id="gridStockOnHand"></div>
        <script type="text/javascript">
        $(document).ready(function() {
        //Grid
            $("#gridStockOnHand").kendoGrid({
                toolbar: [
                {
                    name:"excel",
                    fileName: "StockGudang"
                }
                ],
                height: "330px",
                width: "100%",
                columns: [
                {
                "title":"ItemID",
                "width":"100px",
                "field":"<?php echo T_TransactionStockBalanceHeader_ItemID; ?>",
                },
                {
                "title":"Item Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_ItemName; ?>",
                },
                {
                "title":"EPC",
                "width":"150px",
                "field":"<?php echo T_MasterDataItem_EPC; ?>",
                },
                {
                "title":"Barcode",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_Barcode; ?>",
                },
                {
                "title":"Qty",
                "width":"100px",
                "field":"<?php echo T_TransactionStockBalanceHeader_Quantity; ?>",
                },
                {
                "title":"UOM",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_UOMID; ?>",
                },
                {
                "title":"Location Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataLocation_LocationName; ?>",
                }
                ],                     
                dataSource: {
                    transport: {
                        read: {
                            type:"GET",
                            data: { table: 't1011'},
                            url: site_url('Dashboard/stockOnhand'),
                            dataType: "json"
                        }
                    },
                    sync: function(e) {
                        $('#gridStockOnHand').data('kendoGrid').dataSource.read();
                        $('#gridStockOnHand').data('kendoGrid').refresh();
                    },
                    schema: {
                        data: function(data){
                            return data.data;
                        },
                        total: function(data){
                            return data.count;
                        },
                        model: {
                            id: "t1010r001",
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                autoBind:false,        
                filterable: true,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
            });
        });
    </script>
    </div>
</section>
</div>
<div class="col-md-12" id="gridStockSoldView" style="display: none">
    <section class="panel panel-tertiary" >
        <header class="panel-heading">
            <div class="panel-actions">
                <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
            </div>
            <h2 class="panel-title">Stock Out</h2>
        </header>
        <div class="panel-body">
        <div id="gridStockSold"></div>
        <script type="text/javascript">
        $(document).ready(function() {
        //Grid
            $("#gridStockSold").kendoGrid({
                toolbar: [
                {
                    name:"excel",
                    fileName: "StockGudang"
                }
                ],
                height: "330px",
                width: "100%",
                columns: [
                {
                "title":"ItemID",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_ItemID; ?>",
                },
                {
                "title":"Item Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_ItemName; ?>",
                },
                {
                "title":"EPC",
                "width":"150px",
                "field":"<?php echo T_MasterDataItem_EPC; ?>",
                },
                {
                "title":"Barcode",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_Barcode; ?>",
                },
                {
                "title":"Qty",
                "width":"100px",
                "field":"<?php echo T_TransactionStockMovementDetail_Quantity1; ?>",
                },
                {
                "title":"UOM",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_UOMID; ?>",
                },
                {
                "title":"Location Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataLocation_LocationName; ?>",
                }
                ],                   
                dataSource: {
                    transport: {
                        read: {
                            type:"GET",
                            data: { table: 't1011'},
                            url: site_url('Dashboard/stockSold'),
                            dataType: "json"
                        }
                    },
                    sync: function(e) {
                        $('#gridStockSold').data('kendoGrid').dataSource.read();
                        $('#gridStockSold').data('kendoGrid').refresh();
                    },
                    schema: {
                        data: function(data){
                            return data.data;
                        },
                        total: function(data){
                            return data.count;
                        },
                        model: {
                            id: "t1010r001",
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                autoBind:false,             
                filterable: true,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
            });
        });
    </script>
</div>
</section>
</div>

<script type="text/javascript">
$(document).ready(function() {
    viewstockOnhand();
});
var realTime = setInterval(function(){ getData(); }, 1000);

function getData(){
    $.ajax({
        type: 'POST',
        url: site_url('Dashboard/stockOnhandCount'),
        dataType: "json",
            success: function (data) {
            $("#stockOnhand").text(data.Qty);
            $('#gridStockOnHand').data('kendoGrid').refresh();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });

    $.ajax({
        type: 'POST',
        url: site_url('Dashboard/stockSoldCount'),
        dataType: "json",
            success: function (data) {
            $("#stockSold").text(data.Qty);
            $('#gridStockSold').data('kendoGrid').refresh();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });

}
function viewstockOnhand(){
    $('#gridStockOnHand').data('kendoGrid').dataSource.read();
    $('#gridStockOnHand').data('kendoGrid').refresh();
    $("#gridStockOnHandview").removeAttr('style');
    $("#gridStockSoldView").attr('style', 'display:none');
    $("#gridalertview").attr('style', 'display:none');
}

function viewstockSold(){
    $('#gridStockSold').data('kendoGrid').dataSource.read();
    $('#gridStockSold').data('kendoGrid').refresh();
    $("#gridStockSoldView").removeAttr('style');
    $("#gridStockOnHandview").attr('style', 'display:none');
    $("#gridalertview").attr('style', 'display:none');
}
function myInterval(i) {
    if(i){
        $("#IntervalStart").hide();
        $("#IntervalStop").show();
        realTime = setInterval(function(){ getData(); }, 1000);
    }else{
        $("#IntervalStart").show();
        $("#IntervalStop").hide();
        clearInterval(realTime);
    }
}
</script>