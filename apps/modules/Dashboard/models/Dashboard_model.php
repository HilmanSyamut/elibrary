<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'='.$table.'.'.T_TransactionStockBalanceHeader_LocationID, 'left');
        return $this->db->get($table);
    }

    //Count Data
    public function getListCount($table){
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_TransactionStockMovementDetail.' d', 'd.'.T_TransactionStockMovementDetail_PRI.'='.$table.'.'.T_TransactionStockMovementHeader_RecordID, 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= d.'.T_TransactionStockMovementDetail_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'= d.'.T_TransactionStockMovementDetail_LocationID1, 'left');
        $this->db->where($table.'.'.T_TransactionStockMovementHeader_DocTypeID,'IVSO');
        return $this->db->get($table);
    }

    public function getListstockSoldCount($table){
        $this->db->join(T_TransactionStockMovementDetail.' d', 'd.'.T_TransactionStockMovementDetail_PRI.'='.$table.'.'.T_TransactionStockMovementHeader_RecordID, 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= d.'.T_TransactionStockMovementDetail_ItemID, 'left');
        $this->db->where($table.'.'.T_TransactionStockMovementHeader_DocTypeID,'IVSO');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListAlert($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->where("DATE_FORMAT(".T_Alert_RecordTimestamp.", '%Y-%m-%d') = ", "'".date("Y-m-d")."'", false);
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_EPC.'= '.$table.'.'.T_Alert_EPC, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'= '.$table.'.'.T_Alert_LocationID, 'left');
        return $this->db->get($table);
    }

    public function getListAlertCount($table){
        $this->db->where("DATE_FORMAT(".T_Alert_RecordTimestamp.", '%Y-%m-%d') = ", "'".date("Y-m-d")."'", false);
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_EPC.'= '.$table.'.'.T_Alert_EPC, 'left');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }
}