<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stocktake_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
            $this->db->limit($limit, $offset);
            return $this->db->get($table);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                $this->db->order_by($sortfield, $sortdir);
                return $this->db->get($table);
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get($table);
                }
            }
        }
    }

    //Count Data
    public function getListCount($table){
        $this->db->where(T_TransactionStockMovementHeader_DocTypeID, 'IVST');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getDetail($id){
        $this->db->select('*');
        $this->db->from(T_TransactionStockMovementHeader);
        $this->db->where(T_TransactionStockMovementHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($data[T_TransactionStockMovementHeader_RecordID]);
        }
        return $data;
    }


    public function getDetailItem($id)
    {
        $this->db->select('*');
        $this->db->from(T_TransactionStockMovementDetail);
        $this->db->join(T_MasterDataItem.' i', 
                        'i.'.T_MasterDataItem_ItemID.'='.T_TransactionStockMovementDetail.'.'.T_TransactionStockMovementDetail_ItemID, 
                        'left'
                        );
        $this->db->join(T_MasterDataLocation.' l', 
                        'l.'.T_MasterDataLocation_LocationID.'='.T_TransactionStockMovementDetail.'.'.T_TransactionStockMovementDetail_LocationID1, 
                        'left'
                        );
        $this->db->join(T_MasterDataGeneralTableValue.' m', 'm.'.T_MasterDataGeneralTableValue_RecordID.'= i.'.T_MasterDataItem_AutoIDType, 'left' );
        $this->db->where(T_TransactionStockMovementDetail_PRI, $id);
        $this->db->order_by(T_TransactionStockMovementDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
    }

    public function Insert($data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        unset($data['detail']);
        $this->db->insert(T_TransactionStockMovementHeader, $data);
        $parentID = $this->db->insert_id();
        foreach($detail as $item){
            $val = array(
                T_TransactionStockMovementDetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
                T_TransactionStockMovementDetail_RecordStatus => 0,
                T_TransactionStockMovementDetail_PRI => empty($parentID)? 0:$parentID,
                T_TransactionStockMovementDetail_RowIndex => empty($item['RowIndex'])? 0:$item['RowIndex'],
                T_TransactionStockMovementDetail_ItemID => empty($item['ItemID'])? ' ':$item['ItemID'],
                T_TransactionStockMovementDetail_ItemName => empty($item['ItemName'])? ' ':$item['ItemName'],
                T_TransactionStockMovementDetail_Type => empty($item['ItemType'])? ' ':$item['ItemType'],
                T_TransactionStockMovementDetail_Quantity1 => empty($item['QtyCurrent'])? 0:$item['QtyCurrent'],
                T_TransactionStockMovementDetail_Quantity2 => empty($item['QtyCount'])? 0:$item['QtyCount'],
                T_TransactionStockMovementDetail_LocationID1 => empty($item['LocationID'])? ' ':$item['LocationID'],
                T_TransactionStockMovementDetail_LocationID2 => empty($item['LocationID2'])? ' ':$item['LocationID2'],
                T_TransactionStockMovementDetail_EPC => strtoupper(empty($item['EPC'])? ' ':$item['EPC']),
                T_TransactionStockMovementDetail_Barcode => empty($item['Barcode'])? ' ':$item['Barcode'],
                T_TransactionStockMovementDetail_Remarks => empty($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
            );
            $this->db->insert(T_TransactionStockMovementDetail, $val);
            $parentItemID = $this->db->insert_id();
            if(!empty($item['sub'])){
                foreach ($item['sub'] as $sub) {
                    $valSub = array(
                        T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                        T_TransactionStockMovementDetailSerialNo_SerialNo => isset($sub["SerialNo"]) ? $sub["SerialNo"] : ""
                    );
                    $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                }
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            UpdateDocNo($data[T_TransactionStockMovementHeader_DocTypeID]);
            $this->db->trans_commit();
        }
    }

    public function Update($data){
        $this->db->trans_begin();
        $parentID = $data[T_TransactionStockMovementHeader_RecordID];
        $detail = $data['detail'];
        $DetailRecordID = $data['DoRemoveID'];
        unset($data['detail']);
        unset($data['DoRemoveID']);
        $this->db->where(T_TransactionStockMovementHeader_RecordID,$parentID);
        $this->db->update(T_TransactionStockMovementHeader, $data);
        if(!empty($DetailRecordID)){
            $DoRemoveID = explode(",",$DetailRecordID);
            foreach($DoRemoveID as $detailRemove){
                $this->db->delete(T_TransactionStockMovementDetail,array(T_TransactionStockMovementDetail_RecordID=>$detailRemove));
            }
        }
        foreach($detail as $item){
            $val = array(
                T_TransactionStockMovementDetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
                T_TransactionStockMovementDetail_RecordStatus => 0,
                T_TransactionStockMovementDetail_PRI => empty($parentID)? 0:$parentID,
                T_TransactionStockMovementDetail_RowIndex => empty($item['RowIndex'])? 0:$item['RowIndex'],
                T_TransactionStockMovementDetail_ItemID => empty($item['ItemID'])? ' ':$item['ItemID'],
                T_TransactionStockMovementDetail_ItemName => empty($item['ItemName'])? ' ':$item['ItemName'],
                T_TransactionStockMovementDetail_Type => empty($item['ItemType'])? ' ':$item['ItemType'],
                T_TransactionStockMovementDetail_Quantity1 => empty($item['QtyCurrent'])? 0:$item['QtyCurrent'],
                T_TransactionStockMovementDetail_Quantity2 => empty($item['QtyCount'])? 0:$item['QtyCount'],
                T_TransactionStockMovementDetail_LocationID1 => empty($item['LocationID'])? ' ':$item['LocationID'],
                T_TransactionStockMovementDetail_LocationID2 => empty($item['LocationID2'])? ' ':$item['LocationID2'],
                T_TransactionStockMovementDetail_EPC => strtoupper(empty($item['EPC'])? ' ':$item['EPC']),
                T_TransactionStockMovementDetail_Barcode => empty($item['Barcode'])? ' ':$item['Barcode'],
                T_TransactionStockMovementDetail_Remarks => empty($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
            );
            $parentItemID =0;
            if(empty($item['RecordIDDetail']) && empty($item['RecordFlag'])){ // Insert New
                $this->db->insert(T_TransactionStockMovementDetail, $val);
                $parentItemID = $this->db->insert_id();
            }elseif(!empty($item['RecordIDDetail']) && $item['RecordFlag'] == 1){
                $this->db->where(T_TransactionStockMovementDetail_RecordID,$item['RecordIDDetail']);
                $this->db->update(T_TransactionStockMovementDetail, $val);
                $parentItemID = $item['RecordIDDetail'];
            }
            if(!empty($item['sub'])){
                $this->db->delete(T_TransactionStockMovementDetailSerialNo,array(T_TransactionStockMovementDetailSerialNo_PRI=>$parentItemID));
                foreach ($item['sub'] as $sub) {
                    $valSub = array(
                        T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                        T_TransactionStockMovementDetailSerialNo_SerialNo => isset($sub["SerialNo"]) ? $sub["SerialNo"] : ""
                    );
                    $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                }
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_TransactionStockMovementHeader, array(T_TransactionStockMovementHeader_RecordID => $id));
        $this->db->delete(T_TransactionStockMovementDetail, array(T_TransactionStockMovementDetail_PRI => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateHeader($data,$id){
        $this->db->trans_begin();
        $this->db->where(T_TransactionStockMovementHeader_RecordID, $id);
        $this->db->update(T_TransactionStockMovementHeader,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}
