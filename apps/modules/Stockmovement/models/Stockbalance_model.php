<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stockbalance_model extends CI_Model
{    

    public function getSubItem($id)
    {
        $this->db->where(T_TransactionStockBalanceDetail_PRI,$id);
        $query = $this->db->get(T_TransactionStockBalanceDetail);
        $data = $query->result('array');
        
        return $data;
    }

    public function getList($limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {            
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }      
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }       
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    
                }
            }
        }
        $this->db->select(T_TransactionStockBalanceHeader_ItemID.', mt.*, SUM('.T_TransactionStockBalanceHeader_Quantity.') AS '.T_TransactionStockBalanceHeader_Quantity);
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->group_by(T_TransactionStockBalanceHeader_ItemID);
        // $this->db->group_by(T_TransactionStockBalanceHeader_LocationID);
        $this->db->join(T_MasterDataItem.' mt', 'mt.'.T_MasterDataItem_ItemID.' = '.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID);
        $this->db->join(T_MasterDataLocation.' lc', 'lc.'.T_MasterDataLocation_LocationID.' = '.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_LocationID);
        return $this->db->get();
    }

    //Count Data
    public function getListCount(){
        $this->db->select(T_TransactionStockBalanceHeader_ItemID);
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->group_by(T_TransactionStockBalanceHeader_ItemID);
        // $this->db->group_by(T_TransactionStockBalanceHeader_LocationID);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListLog($ID, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {            
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }           
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }         
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }        
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }         
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->select('*');
        $this->db->from('v1991');
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ID);
        return $this->db->get();
    }

    //Count Data
    public function getListLogCount($ID){
        $this->db->select('*');
        $this->db->from('v1991');
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ID);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListDetail($ItemID, $LocationID, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {            
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }           
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }         
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }        
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }         
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->select('*');
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ItemID);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, $LocationID);
        return $this->db->get();
    }

    //Count Data
    public function getListDetailCount($ItemID, $LocationID){
        $this->db->select('*');
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ItemID);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, $LocationID);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function stock_exist($itemid,$itemloc,$code){
        $this->db->from(T_ItemMovementStockListDetail1);
        $this->db->where(T_ItemMovementStockListDetail1_ItemID, $itemid);
        $this->db->where(T_ItemMovementStockListDetail1_PRI, $itemloc);
        if($code){ 
            $this->db->where(T_ItemMovementStockListDetail1_Code, $code); 
        }else{
            $this->db->where(T_ItemMovementStockListDetail1_Qty." > ",0);
        }
        return $this->db->get();
    }

    public function item_exist($ItemID,$locationID){
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ItemID);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, $locationID);

        return $this->db->get();
    }

    public function InsertStockList($table,$data){
        $this->db->trans_begin();
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return $id;
        }
    }

    public function UpdateStockList($table,$data,$field,$id){
        $this->db->trans_begin();
        $this->db->where($field,$id);
        $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
       
}
