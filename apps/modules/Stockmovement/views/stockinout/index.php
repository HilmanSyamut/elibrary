<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_TransactionStockMovementHeader_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_TransactionStockMovementHeader_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_TransactionStockMovementHeader_RecordStatus  => array(0,1,'50px','RS',1,'string',1),
    T_TransactionStockMovementHeader_DocTypeID  => array(1,1,'50px','Doc Type',1,'string',1),
    T_TransactionStockMovementHeader_DocNo  => array(1,1,'80px','Doc No',0,'string',0),
    T_TransactionStockMovementHeader_DocDate  => array(1,1,'100px','Doc Date',0,'datetime',0),
    T_TransactionStockMovementHeader_Remarks  => array(1,1,'100px','Remarks',0,'string',0),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_TransactionStockMovementHeader,
    'tools' => array(T_TransactionStockMovementHeader_RecordID,T_TransactionStockMovementHeader_RecordTimestamp,T_TransactionStockMovementHeader_RecordStatus,T_TransactionStockMovementHeader_DocTypeID,T_TransactionStockMovementHeader_DocNo,T_TransactionStockMovementHeader_DocDate),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Stockmovement/Stockinout/insert',
        'read' => 'Stockmovement/Stockinout/getlist',
        'update' => 'Stockmovement/Stockinout/update',
        'destroy' => 'Stockmovement/Stockinout/delete',
        'form' => 'Stockmovement/Stockinout/form',
        'post' => 'Stockmovement/Stockinout/post',
        'unpost' => 'Stockmovement/Stockinout/unPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>