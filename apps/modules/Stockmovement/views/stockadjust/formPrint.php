
        <div class="invoice">
            <header class="clearfix">
                <div class="row">
                    <?php $this->navigations->kopsurat(); ?>
                </div>
            </header>
            <div class="bill-info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Date:</span>
                                <span class="value"><?php echo date(FORMATDATEREPORT,strtotime(${T_ItemMovementItemMovementHeader_DocDate})); ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-Left">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="row">
                    <div class="col-sm-12 text-center mt-md mb-md">
                        <h4>STOCK Adjust : <?php echo ${T_ItemMovementItemMovementHeader_DocNo}; ?></h4>
                    </div>
                </div>
            </div>
            <hr></hr>
            <div class="table-responsive">
                <table class="table invoice-items">
                    <thead>
                        <tr class="h4 text-dark">
                            <th data-col="RowIndex">#</th>
                            <th data-col="Code" width="50px">Code</th>
                            <th data-col="ItemID">Item ID</th>
                            <th data-col="ItemName">Item Name</th>
                            <th data-col="UOM">UOM</th>
                            <th data-col="LocationName">Location</th>
                            <th data-col="QtyBalance">Qty Balance</th>
                            <th data-col="QtyAdjust">Qty Adjust</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                            foreach($Detail as $item): echo '<tr id="detail-'.$i. '">
                            <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                            <td id="detailCodev-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_Code]. '">'.$item[T_ItemMovementItemMovementDetail_Code]. '</td>
                            <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_ItemID1]. '">'.$item[T_ItemMovementItemMovementDetail_ItemID1]. '</td>
                            <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_ItemName1]. '">'.$item[T_ItemMovementItemMovementDetail_ItemName1]. '</td>
                            <td id="detailUOMv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_UOM]. '">'.$item[T_ItemMovementItemMovementDetail_UOM]. '</td>
                            <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                            <td id="detailQtyBalancev-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_Quantity2]. '">'.$item[T_ItemMovementItemMovementDetail_Quantity2]. '</td>
                            <td id="detailQtyAdjustv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_Quantity1]. '">'.$item[T_ItemMovementItemMovementDetail_Quantity1]. '</td>
                            
                        </tr>'; $i++; endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="bill-data text-Left">
                    <p class="mb-none">
                        <span class="text-dark">Remarks:</span>
                        <?php echo ${T_ItemMovementItemMovementHeader_Remarks}; ?>
                    </p>
                </div>
            </div>
           
        </div>

        <script>
            window.print();
        </script>
   