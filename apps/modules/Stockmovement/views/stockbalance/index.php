<div id="grid"></div>
<script type="text/x-kendo-template" id="template">
    <div class="tabstrip">
        <ul>
            <li class="k-state-active"> Detail </li>
            <li> Log </li>
            <li> Serial No </li>
        </ul>
        <div>
            <div class="detail"> </div>
        </div>
        <div>
            <div class='log'> </div>
        </div>
        <div>
            <div class='serialno'> </div>
        </div>
    </div>
</script> 

<script type="text/javascript">
$(document).ready(function() {

//Grid
    $("#grid").kendoGrid({
        height: "580px",
        width: "100%",
        columns: [
        {
            "title":"Item ID",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceHeader_ItemID; ?>",
        },
        {
            "title":"Item Name",
            "width":"200px",
            "field":"<?php echo T_MasterDataItem_ItemName; ?>",
        },
        {
            "title":"EPC",
            "width":"150px",
            "field":"<?php echo T_MasterDataItem_EPC; ?>",
        },
        {
            "title":"Barcode",
            "width":"150px",
            "field":"<?php echo T_MasterDataItem_Barcode; ?>",
        },
        {
            "title":"Qty",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceHeader_Quantity; ?>",
        },
        {
            "title":"UOM",
            "width":"50px",
            "field":"<?php echo T_MasterDataItem_UOMID; ?>",
        },
        {
            "width":"80px",
            "template":
            "#if(<?php echo T_MasterDataItem_AutoIDType; ?> == 6){# <a class='k-button k-button-icontext k-grid-edit' onclick='openDetailSNModal(\"#=<?php echo T_TransactionStockBalanceHeader_ItemID; ?>#\",\"#=<?php echo T_MasterDataItem_ItemName; ?>#\",\"#=<?php echo T_TransactionStockBalanceHeader_Quantity; ?>#\")' href='javascript:void(0)'>Serial No</span></a> #}#",

        }
        ],
        detailTemplate: kendo.template($("#template").html()),
        detailInit: detailInit,
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    url: site_url('Stockmovement/Stockbalance/GetList'),
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.countHeader;
                },
                model: {
                    id: "<?php echo T_TransactionStockBalanceHeader_RecordID; ?>",
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });
});

function detailInit(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailRow.find(".detail").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: site_url("Webservice/Read/Getlist")+'?table=v1990&customfilter[t1990f001]='+e.data["<?php echo T_TransactionStockBalanceHeader_ItemID; ?>"],
                    dataType: "json"
                }
            },            
            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.countDetail;
                },
                model: {
                    id: "<?php echo T_TransactionStockBalanceHeader_RecordID; ?>",
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        filterable: true,
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        }, 
        columns: [
        {
            "title":"Item ID",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceHeader_ItemID; ?>",
        },
        {
            "title":"Item Name",
            "width":"50px",
            "field":"<?php echo T_MasterDataItem_ItemName; ?>",
        },
        {
            "title":"Qty",
            "width":"100px",
            "field":"<?php echo T_TransactionStockBalanceHeader_Quantity; ?>",
        },
        {
            "title":"Location",
            "width":"100px",
            "field":"<?php echo T_MasterDataLocation_LocationName; ?>",
        },
        // {
        //     "width":"70px",
        //     "template":
        //     "<a class='k-button k-button-icontext k-grid-edit' onclick='openDetailModal(\"#=<?php echo T_TransactionStockBalanceHeader_ItemID; ?>#\")' href='javascript:void(0)'>Detail</span></a>",
        // },
        ],
    });

    detailRow.find(".log").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: site_url('Stockmovement/Stockbalance/GetListLog')+'?ID='+e.data["<?php echo T_TransactionStockBalanceHeader_ItemID; ?>"],
                    dataType: "json"
                }
            },            
            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.countDetail;
                },
                model: {
                    id: "<?php echo T_TransactionStockBalanceHeader_RecordID; ?>",
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        filterable: true,
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        }, 
        columns: [
        {
            "title":"Ref Doc Type",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceLog_DocTypeID; ?>",
        },
        {
            "title":"Ref Record ID",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceLog_RefRecordID; ?>",
        },
        {
            "title":"Qty",
            "width":"100px",
            "field":"<?php echo T_TransactionStockBalanceLog_Quantity; ?>",
        }
        ],
    });

    detailRow.find(".serialno").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: site_url("Webservice/Read/Getlist")+'?table=v1992&customfilter[t1990f001]='+e.data["<?php echo T_TransactionStockBalanceHeader_ItemID; ?>"]+'&customfilter[t1992f003]=1',
                    dataType: "json"
                }
            },            
            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.countDetail;
                },
                model: {
                    id: "<?php echo T_TransactionStockBalanceHeader_RecordID; ?>",
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        filterable: true,
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        }, 
        columns: [
        {
            "title":"Location",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceHeader_LocationID; ?>",
        },
        {
            "title":"Serial No",
            "width":"50px",
            "field":"<?php echo T_TransactionStockBalanceDetail_SerialNo; ?>",
        }
        ],
    });
    
}
</script>
<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailFormSN">
    
    <div class="row col-md-6">
        <div class="k-edit-label">Item Name</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" id="ItemName" datarequired="0" readonly />
        </div>
    </div>
    <div class="row col-md-5">
        <div class="k-edit-label">Qty</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" id="Quantity" datarequired="0" readonly />
        </div>
    </div>
    <div class="row col-md-12">
        <div id="kgLogStocking"></div>
    </div>
</div>
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="row col-md-12">
        <div id="kgDetailItem"></div>
    </div>
</div>
<!--  End Modal Form Detail -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#detailFormSN").kendoWindow({
            width: "500px",
            title: "Serial NO",
            visible: false,
            modal: true,
            actions: [
            "Close"
            ],
        });

        $("#kgLogStocking").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                    },
                },
                sync: function(e) {
                    // $("#kgLogStocking").data("kendoGrid").dataSource.read();
                    // $("#kgLogStocking").data("kendoGrid").refresh();
                },
                schema: {
                    data: function(datas){
                        return datas.data;
                    },
                    total: function(datas){
                        return datas.count;
                    },
                    model: {
                        id: "RecordID",
                    }
                },                        
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
                autoBind: false,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                    height: "500px",
                    width: "100%",
                    columns: [{"field":"<?php echo T_TransactionStockBalanceDetail_SerialNo; ?>","title":"Serial NO","width":"50px",},
                    ],
            });

    });
    function openDetailSNModal(id,Item,Qty)
    {
        $("#kgLogStocking").data("kendoGrid").dataSource.transport.options.read.data = { table: "v1992", customfilter: {t1990f001:id,t1992f003:1}  };
        $("#kgLogStocking").data("kendoGrid").dataSource.transport.options.read.url = site_url("Webservice/Read/Getlist");
        $("#kgLogStocking").data("kendoGrid").dataSource.read();
        $("#detailFormSN").prev().find(".k-window-title").text("Serial No "+id);
        $("#ItemName").val(Item);
        $("#Quantity").val(Qty);
        $("#detailFormSN").data("kendoWindow").center().open();
    }

    $(document).ready(function() {
        $("#detailForm").kendoWindow({
            width: "750px",
            title: "Detail Item",
            visible: false,
            modal: true,
            actions: [
            "Close"
            ],
        });

        $("#kgDetailItem").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                    },
                },
                sync: function(e) {
                    // $("#kgLogStocking").data("kendoGrid").dataSource.read();
                    // $("#kgLogStocking").data("kendoGrid").refresh();
                },
                schema: {
                    data: function(datas){
                        return datas.data;
                    },
                    total: function(datas){
                        return datas.count;
                    },
                    model: {
                        id: "RecordID",
                    }
                },                        
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
                autoBind: false,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                    height: "500px",
                    width: "100%",
                    columns: [
                    {"field":"<?php echo T_MasterDataItem_ItemName; ?>","title":"Item Name","width":"150px"},
                    {"field":"<?php echo T_MasterDataLocation_LocationName; ?>","title":"Location","width":"100px"},
                    {"field":"<?php echo T_TransactionStockBalanceHeader_Quantity; ?>","title":"Qty","width":"50px"},
                    {"field":"<?php echo T_MasterDataItem_UOMID; ?>","title":"UOM","width":"50px"},
                    ],
            });

    });
    function openDetailModal(id)
    {
        $("#kgDetailItem").data("kendoGrid").dataSource.transport.options.read.data = { table: "v1990", customfilter: {t1990f001:id}  };
        $("#kgDetailItem").data("kendoGrid").dataSource.transport.options.read.url = site_url("Webservice/Read/Getlist");
        $("#kgDetailItem").data("kendoGrid").dataSource.read();
        $("#detailForm").prev().find(".k-window-title").text("Detail Item "+id);
        $("#detailForm").data("kendoWindow").center().open();
    }
</script>

