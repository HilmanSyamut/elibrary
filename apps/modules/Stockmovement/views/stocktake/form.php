<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_TransactionStockMovementHeader_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordID}) ? ${T_TransactionStockMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordTimestamp}) ? ${T_TransactionStockMovementHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-input k-textbox', 'value' => 'IVST', 'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocName', 'class' => 'k-input k-textbox', 'value' => 'Stock Take', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc No</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-input k-textbox', 'value' => isset(${T_TransactionStockMovementHeader_DocNo}) ? ${T_TransactionStockMovementHeader_DocNo} : substr(DocNo('IVST'), 2), 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => isset(${T_TransactionStockMovementHeader_DocDate}) ? ${T_TransactionStockMovementHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocStatus', 'value' => isset(${T_TransactionStockMovementHeader_DocStatus}) ? ${T_TransactionStockMovementHeader_DocStatus} : '0', 'readonly' => true, 'style' => 'margin-top:-9px;' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-textbox" id="Remark" style="width: 300px;"><?php echo (isset(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <a id="DetailModal" class="mb-xs mt-xs mr-xs btn btn-xs btn-success"><i class="fa fa-plus"></i> &nbsp;Add New</a>
                            <a id="removeAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detail');"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
                            <div style="overflow:auto;">
                                <input id="DoRemoveID" type="hidden" />
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="80px"><input type="checkbox" id="detailCheckAll" onclick="CheckAll('detail');"> Action</th>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Item ID</th>
                                            <th data-col="ItemName">Item Name</th>
                                            <th data-col="ItemType">Item Type</th>
                                            <th data-col="LocationID">Loc ID</th>
                                            <th data-col="LocationName">Loc Name</th>
                                            <th data-col="QtyCount">Qty Count</th>
                                            <th data-col="QtyCurrent">Qty Current</th>
                                            <th data-col="QtyDifferent">Qty Different</th>
                                            <th data-col="EPC">EPC</th>
                                            <th data-col="Barcode">Barcode</th>
                                            <th data-col="RemarksDetail">Remarks</th>
                                            <th data-col="RecordIDDetail" style="display:none;"></th>
                                            <th data-col="RecordFlag" style="display:none;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): 
                                            $QtyDifferent = $item[T_TransactionStockMovementDetail_Quantity2]-$item[T_TransactionStockMovementDetail_Quantity1];
                                            $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"> <a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
                                        <td id="detailItemTypev-'.$i. '" data-val="'.$item[T_MasterDataGeneralTableValue_Key]. '">'.$item[T_MasterDataGeneralTableValue_Key]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                                        <td id="detailQtyCountv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity2]. '">'.$item[T_TransactionStockMovementDetail_Quantity2]. '</td>
                                        <td id="detailQtyCurrentv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>                                        
                                        <td id="detailQtyDifferentv-'.$i. '" data-val="'.$QtyDifferent. '">'.$QtyDifferent. '</td>
                                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                                        <td id="detailBarcodev-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Barcode].'">'.$item[T_TransactionStockMovementDetail_Barcode]. '</td>
                                        <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Remarks]. '">'.$item[T_TransactionStockMovementDetail_Remarks]. '</td>
                                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordID]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordID]. '</td>
                                        <td id="detailRecordFlagv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordFlag]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordFlag]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="col-md-12">
        <div class="col-md-5">
            <input id="RecordFlag" type="hidden" />
            <input id="RecordIDDetail" type="hidden" />
            <input id="RecordIDDetail2" type="hidden" />
            <div class="k-edit-label">#</div>
            <div class="k-edit-field">
                <input type="text" id="RowIndex" readonly="true" primary="1" />
            </div>
            <div class="k-edit-label">Item ID</div>
            <div class="k-edit-field">
                <input type="text" id="ItemID" class="k-input k-textbox" style="text-transform: uppercase;" datarequired="1" readonly />
                <input type="hidden" id="DSBRecordID"/>
                <button class="k-button" id="LookupEventItemID">
                    <div class="k-icon k-i-search">
                </button>
            </div>
            <div class="k-edit-label">Item Name</div>
            <div class="k-edit-field">
                <input type="text" class="k-input k-textbox" id="ItemName" datarequired="0" readonly />
            </div>
            <div class="k-edit-label">Item Type</div>
            <div class="k-edit-field">
                <input type="text" class="k-input k-textbox" id="ItemType" datarequired="0" readonly />
            </div>
            <div class="k-edit-label">Location ID</div>
            <div class="k-edit-field">
                <input type="text" id="LocationID" datarequired="1" class="k-input k-textbox" readonly />
            </div>
            <div class="k-edit-label">Location Name</div>
            <div class="k-edit-field">
                <input type="text" class="k-input k-textbox" readonly datarequired="0" readonly id="LocationName" />
            </div>
        </div>
        <div class="col-md-5">
                    
            <div class="k-edit-label">Qty Count</div>
            <div class="k-edit-field">
                <input type="text" id="QtyCount" datarequired="1" />
            </div>
            <div class="k-edit-label">Qty Current</div>
            <div class="k-edit-field">
                <input type="text" id="QtyCurrent" readonly datarequired="0" />
            </div>
            <div class="k-edit-label">Qty Different</div>
            <div class="k-edit-field">
                <input type="text" id="QtyDifferent" readonly  datarequired="0" />
            </div>
            <div class="k-edit-label">EPC</div>
            <div class="k-edit-field">
                <input type="text" class="k-input k-textbox" id="EPC" datarequired="1" readonly />
            </div>
            <div class="k-edit-label">Barcode</div>
            <div class="k-edit-field">
                <input type="text" class="k-input k-textbox" id="Barcode" datarequired="0" readonly />
            </div>
            <div class="k-edit-label">Remarks</div>
            <div class="k-edit-field">
                <textarea class="k-textbox" id="RemarksDetail"></textarea>
            </div>
        </div>
        <a id="DetailModalSub" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" style="display:none;" onclick="openDetailModal();"><i class="fa fa-plus"></i> &nbsp;Add New</a>
        <a id="detailSubRemoveAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detailSub');" style="display:none;"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
        <div id="tableDetailModalSub" style="overflow: visible; display:none;">
            <table id="table-detailSub" class="table table-responsive">
                <thead id="head-detailSub">
                    <tr>
                        <th width="80px"><input type="checkbox" id="detailSubCheckAll" onclick="CheckAll('detailSub');"> Action</th>
                        <th data-col="SerialNo">SerialNo</th>
                    </tr>
                </thead>
                <tbody id="list-detailSub">
                </tbody>
            </table>
        </div>
        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');"><i class="el-icon-file-new"></i> Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="el-icon-remove"></i> Cancel</button>
        </div>
    </div>
</div>

<div style="display:none;" class="k-edit-form-container" id="detailSubForm">
    <div class="vol-mb-12">
 		<div class="k-edit-label">SerialNo</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" id="SerialNo" datarequired="0" />
        </div>
 		
        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetailSub" class="btn btn-primary close-button" onclick="adddetail('detailSub');"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailSubForm');"><i class="fa fa-cancel"></i> &nbsp;Cancel</button>
        </div>
    </div>
</div>
<!--  End Modal Form Detail -->

<?php
//Item Lookup
    $blur = array( 
            array('data' => T_TransactionStockBalanceHeader_ItemID, 'id' => 'ItemID', 'throw' => 'QtyCurrent'),
            array('data' => T_TransactionStockBalanceHeader_LocationID, 'id' => 'LocationID', 'throw' => 'QtyCurrent'),
        );
    
    //Lookup Item
    //field in database data to load
    $dataItem = array(
        array('field' => T_TransactionStockBalanceHeader_ItemID, 'title' => 'Item ID', 'width' => '50px'),
        array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '80px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location', 'width' => '80px'),
        array('field' => T_TransactionStockBalanceHeader_Quantity, 'title' => 'Qty', 'width' => '50px'),
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Type', 'width' => '50px'),
        array('field' => T_MasterDataItem_EPC, 'title' => 'EPC', 'width' => '80px'),
        array('field' => T_MasterDataItem_Barcode, 'title' => 'Barcode', 'width' => '80px')
    );
    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'RecordIDDetail2', 'column' => T_TransactionStockBalanceHeader_RecordID),        
        array('id' => 'ItemID', 'column' => T_TransactionStockBalanceHeader_ItemID),
        array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
        array('id' => 'ItemType', 'column' => T_MasterDataGeneralTableValue_Key),
        array('id' => 'EPC', 'column' => T_MasterDataItem_EPC),
        array('id' => 'Barcode', 'column' => T_MasterDataItem_Barcode),
        array('id' => 'QtyCount', 'column' => T_TransactionStockBalanceHeader_Quantity),
        array('id' => 'QtyCurrent', 'column' => T_TransactionStockBalanceHeader_Quantity),
        array('id' => 'LocationID', 'column' => T_TransactionStockBalanceHeader_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );
    $filter = array('');

    //id, title, size, URL, data field in database, Throw Data To form when click
    echo kendoModalLookup("ItemID", "Data Item", "700px", "Stockmovement/Stocktransfer/GetStockList", $dataItem, $columnItem,T_TransactionStockBalanceHeader,'',$filter);

//Location lookup
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click
    echo kendoModalLookupBlur("Location", "Data Location", "500px", "Webservice/Read/Getlist", $dataLocation, $columnLocation , 'QuantityCurrent', $blur, 'Inventory/StockAdjust/GetListRow', 't1020f003', T_MasterDataLocation);

?>
<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>"; 
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {

    //Numeric
    $("#RowIndex").kendoNumericTextBox(); 
    $("#QtyCurrent").kendoNumericTextBox({min:0});
    $("#QtyDifferent").kendoNumericTextBox();
    $("#QtyCount").kendoNumericTextBox({min:0,change:Quantity,spin:Quantity});

    kendoModal("detailForm","Add Detail","850px");
    kendoModal("detailSubForm","Add Item","400px");

        $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        cleardetail("detailSub", 0);
    });

});

function Quantity(){
    var QuantityCount = $('#QtyCount').data("kendoNumericTextBox").value();
    var QuantitySystem = $('#QtyCurrent').data("kendoNumericTextBox").value();

    var Total = QuantityCount - QuantitySystem;
    $("#QtyDifferent").data("kendoNumericTextBox").value(Total);
}


//Insert
    function insert()
    {
        var detail = getDetailSubItem('detail');
        var voData = {
            DocDate: $('#DocDate').val(),
            DocStatus: $('#DocStatus').val(),
            Remarks: $('#Remark').val(),
            detail: detail
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Stockmovement/Stocktake/Insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailSubItem('detail');
     var voData = {
         RecordID: ID,
         TimeStamp: $('#TimeStamp').val(),
         DocNo: $('#DocNo').val(),
         DocType: $('#DocType').val(),
         DocDate: $('#DocDate').val(),
         DocStatus: $('#DocStatus').val(),
         Remarks: $('#Remark').val(),
         DoRemoveID : $("#DoRemoveID").val(),
         detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Stockmovement/Stocktake/Update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    // window.location.replace(current_url());
                    window.location.replace(site_url('Stockmovement/Stocktake'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.DocDate == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.detail == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }


    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
    customTriger(1);
    Quantity();
}

    function checkField(target){
        var msg = '';
        var field = getDetailField(target);
        var val   = getDetailItem(target);
         for (v = 0; v < val.length; v++) {
            if($("#"+field[i]).attr("primary") == "1"){
                if($("#"+field[i]).val() == val[v].RowIndex)
                {
                    msg+="Row Index Sudah Ada"+"\r\n";
                }            
            }
        }
        return msg;
    }

function customTriger(i){
        var typeItem = $('#ItemType').val();
        if(typeItem === 'SS'){
            $('#DetailModalSub').show()
            $('#tableDetailModalSub').show();
            $('#QtyCount').data('kendoNumericTextBox').readonly(true);
            var qty = document.getElementById("list-detailSub").rows.length;
            $('#QtyCount').data('kendoNumericTextBox').value(qty);
            $('#detailSubRemoveAll').show();
            var pri = $('#RecordIDDetail2').val();
            if(!i){
                Quantity();
                GetDataSub(pri);
            }
        }else{
            $('#DetailModalSub').hide()
            $('#tableDetailModalSub').hide();
            $('#detailSubRemoveAll').hide();
            if(typeItem === 'NS'){
                $('#QtyCount').data('kendoNumericTextBox').readonly(true);
            }else if(typeItem === 'S1'){
                $('#QtyCount').data('kendoNumericTextBox').readonly(true);
                $('#QtyCount').data('kendoNumericTextBox').value(1);
            }else if(typeItem === 'SN')
            {
                $('#QtyCount').data('kendoNumericTextBox').readonly(false);
            }
        }
    }

    function GetDataSub(id)
    {
        var voData = {
            RecordID: id
        }; 
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stockadjust/GetSubItem')+"?editable=1",
            success: function (result) {
                $('#list-detailSub').html(result.html);
                var qty = document.getElementById("list-detailSub").rows.length;
                $('#Qty').data('kendoNumericTextBox').value(qty);
                var target = "detailSub";
                var RowIndex = $("#RowIndex").data("kendoNumericTextBox").value();
                var htmlUpdate = $('#list-'+target).html();
                var ID = current_url()+target+RowIndex;
                localStorage[ID] = htmlUpdate;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
</script>