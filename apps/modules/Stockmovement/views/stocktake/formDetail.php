<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_ItemMovementItemMovementHeader_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_ItemMovementItemMovementHeader_RecordID}) ? ${T_ItemMovementItemMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_ItemMovementItemMovementHeader_RecordTimestamp}) ? ${T_ItemMovementItemMovementHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-input k-textbox', 'value' => 'IVSO', 'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocName', 'class' => 'k-input k-textbox', 'value' => 'Stock Take', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc No</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-input k-textbox', 'value' => isset(${T_ItemMovementItemMovementHeader_DocNo}) ? ${T_ItemMovementItemMovementHeader_DocNo} : substr(DocNo('IVSO'), 2), 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'readonly' => 'TRUE', 'class' => 'KendoDatePicker', 'value' => isset(${T_ItemMovementItemMovementHeader_DocDate}) ? ${T_ItemMovementItemMovementHeader_DocDate} : '' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocStatus', 'value' => isset(${T_ItemMovementItemMovementHeader_DocStatus}) ? ${T_ItemMovementItemMovementHeader_DocStatus} : '0', 'readonly' => true ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-input k-textbox" readonly id="Remark" style="width: 300px;"><?php echo (isset(${T_ItemMovementItemMovementHeader_Remarks}))? ${T_ItemMovementItemMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <div style="overflow:auto;">
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Item ID</th>
                                            <th data-col="ItemName">Item Name</th>
                                            <th data-col="UOM">UOM</th>
                                            <th data-col="LocationID">Loc ID</th>
                                            <th data-col="LocationName">Loc Name</th>
                                            <th data-col="QtyCount">Qty Count</th>
                                            <th data-col="QtyCurrent">Qty Current</th>
                                            <th data-col="QtyDifferent">Qty Different</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"></td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_ItemID1]. '">'.$item[T_ItemMovementItemMovementDetail_ItemID1]. '</td>
                                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_ItemName1]. '">'.$item[T_ItemMovementItemMovementDetail_ItemName1]. '</td>
                                        <td id="detailUOMv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_UOM]. '">'.$item[T_ItemMovementItemMovementDetail_UOM]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_LocationID1]. '">'.$item[T_ItemMovementItemMovementDetail_LocationID1]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                                        <td id="detailQtyCountv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_Quantity1]. '">'.$item[T_ItemMovementItemMovementDetail_Quantity1]. '</td>
                                        <td id="detailQtyCurrentv-'.$i. '" data-val="'.$item[T_ItemMovementItemMovementDetail_Quantity2]. '">'.$item[T_ItemMovementItemMovementDetail_Quantity2]. '</td>
                                        <td id="detailQtyDifferentv-'.$i. '" data-val="'.($item[T_ItemMovementItemMovementDetail_Quantity1]-$item[T_ItemMovementItemMovementDetail_Quantity2]). '">'.($item[T_ItemMovementItemMovementDetail_Quantity1]-$item[T_ItemMovementItemMovementDetail_Quantity2]). '</td>
                                        
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>"; 
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    }
];
$(document).ready(function() {

//Numeric
$("#RowIndex").kendoNumericTextBox(); 
$("#QtyCurrent").kendoNumericTextBox(); 
$("#QtyDifferent").kendoNumericTextBox();

$("#form-insert").bind('submit',function(e){
    e.preventDefault();
    if (validasi == "insert") {
        insert();
    }else{
        update();
    };
});

//Kendo Lookup Data POP up
    
//Detail Form 
    $("#DetailModal").click(function() {
        $("#LocationID").val(null);
        $("#LocationName").val(null);
        $("#detailForm").data("kendoWindow").center().open();
        cleardetail("detail", 0);
    });

    $("#detailForm").kendoWindow({
        width: "650px",
        title: "Data Stock Take",
        visible: false,
        modal: true,
        actions: [
        "Close"
        ],
    });

});

//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

    function checkField(target){
        var msg = '';
        var field = getDetailField(target);
        var val   = getDetailItem(target);
         for (v = 0; v < val.length; v++) {
            if($("#"+field[i]).attr("primary") == "1"){
                if($("#"+field[i]).val() == val[v].RowIndex)
                {
                    msg+="Row Index Sudah Ada"+"\r\n";
                }            
            }
        }
        return msg;
    }

</script>
<style type="text/css">       
        td[class^='actions']
            {
                display:none;
            } 
</style>