<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Salman Fariz
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Take Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Stocktake extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Stocktake_model','StockList_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/stocktake/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Stocktake_model->getDetail($id);
		$this->modules->render('/stocktake/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Stocktake_model->getDetail($id);
		$this->modules->render('/stocktake/formDetail', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Stocktake_model->getDetail($id);
		$this->modules->render('/stocktake/formPrint', $data);
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Stocktake_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Stocktake_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetListRow(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$item = $this->input->post('ItemID');
		$location = $this->input->post('LocationID');
		$getList = $this->Stocktake_model->GetListRow($item, $location);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Ditemukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		try{
			$doctype = "IVST";
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => substr(DocNo($doctype), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'detail' => $this->input->post("detail")
				);

				$this->Stocktake_model->Insert($data);
				
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "IVST";
			if(check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail")
				);

				$this->Stocktake_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Stocktake_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		$this->db->trans_begin();
		try{
			$voMsg = "";
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
					$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
					$different = $qty2 - $qty1;
					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $different;
					if($qty < 0)
					{
						$voMsg .= $temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}

				}else{
					$voMsg .= "item ".$temp[T_TransactionStockMovementDetail_ItemID]." not found in Stockbalance \n ";
				}

				if($typeItem == 6){
					// get sn document
					$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
					$get_serial_no = $this->db->get(T_TransactionStockMovementDetailSerialNo);

					if($get_serial_no->num_rows() > 0){
						// get sn stock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$StockSN = $this->db->get(T_TransactionStockBalanceDetail);
						foreach ($StockSN->result("array") as $dataStockSN) {
							$dataL = array(
								T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
								T_StockTakeLog_SN => $dataStockSN[T_TransactionStockBalanceDetail_SerialNo],
								T_StockTakeLog_StockFlag => $dataStockSN[T_TransactionStockBalanceDetail_StockFlag]
							);
							$this->db->insert(T_StockTakeLog,$dataL);
						}
						// update sn to unstock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							// get sn 
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){ // insert if not exist in stock
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $parent_id,
									T_TransactionStockBalanceDetail_RecordFlag => 2,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{ // update if exist in stock
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				if($voMsg != ""){ throw new Exception($voMsg); }
			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);


				$get_stocklist = $get_stocklist->first_row("array");
				$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

				$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];
				$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];
				$different = $qty2 - $qty1;
				$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $different;
				if($qty < 0)
				{
					$voMsg .= $temp[T_TransactionStockMovementDetail_EPC]." < 0 in Stockbalance \n "; 
				}else{
					$UpdateStockBalance = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
				}

				if($typeItem == 6){
					$this->db->where(T_StockTakeLog_DocNo, $get_data[T_TransactionStockMovementHeader_RecordID]);
					$get_serial_no = $this->db->get(T_StockTakeLog);

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_StockTakeLog_SN]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() != 0){
								$serialNoExistsData = $serialNoExists->first_row('array');
								$flag = $serial_no_temp[T_StockTakeLog_StockFlag];
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_StockTakeLog_SN]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>$flag));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different*-1,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);
			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file stocktake.php */
/* Location: ./app/modules/master/controllers/stocktake.php */
