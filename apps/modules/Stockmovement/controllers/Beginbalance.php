<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Balance Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Beginbalance extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Beginbalance_model','Stockbalance_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/beginbalance/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Beginbalance_model->getDetail($id);
		$this->modules->render('/beginbalance/form', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Beginbalance_model->getDetail($id);
		$this->modules->render('/beginbalance/formPrint', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Beginbalance_model->getDetail($id);
		$this->modules->render('/beginbalance/formDetail', $data);
	}

	public function getDetail()
	{	$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id =  $this->input->get('RecordID');
		$data = $this->Beginbalance_model->getDetail($id);
		if(!empty($data)){
			$info->data = $data;
		}else{
			$info->data = null;
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Beginbalance_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Beginbalance_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		try{
			$doctype = "IMBB";
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => substr(DocNo($doctype), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'detail' => $this->input->post("detail")
				);

				$this->Beginbalance_model->Insert($data);
				
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "IMBB";
			if(check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => $this->input->post("DocNo"),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail")
				);

				$this->Beginbalance_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Beginbalance_model->Delete($this->input->post("RecordID"));
			
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Beginbalance_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Beginbalance_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$get_stocklist = $this->Stockbalance_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_TransactionStockMovementDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $temp[T_TransactionStockMovementDetail_Quantity1];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

				}else{
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_ItemID => $temp[T_TransactionStockMovementDetail_ItemID],
						T_TransactionStockBalanceHeader_LocationID => $temp[T_TransactionStockMovementDetail_LocationID1],
						T_TransactionStockBalanceHeader_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1],
						// T_TransactionStockBalanceHeader_EPC => $temp[T_TransactionStockMovementDetail_EPC],
						// T_TransactionStockBalanceHeader_Barcode => $temp[T_TransactionStockMovementDetail_Barcode]
					);
					$this->Stockbalance_model->InsertStockList(T_TransactionStockBalanceHeader,$data_stocklist);
					$stocklistPRI = $this->db->insert_id();
				}

				if($get_master_item[T_MasterDataItem_AutoIDType] == 6){
					$this->db->from(T_TransactionStockMovementDetailSerialNo);
					$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
					$get_serial_no = $this->db->get();

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $stocklistPRI,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $stocklistPRI,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Beginbalance_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->Beginbalance_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$get_stocklist = $this->Stockbalance_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_TransactionStockMovementDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item

					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $temp[T_TransactionStockMovementDetail_Quantity1];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

					if($get_master_item[T_MasterDataItem_AutoIDType] == 6){
						$this->db->from(T_TransactionStockMovementDetailSerialNo);
						$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$temp[T_TransactionStockMovementDetail_RecordID]);
						$get_serial_no = $this->db->get()->result("array");

						foreach ($get_serial_no as $get_serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $get_serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>0));
						}
					}

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $stocklistPRI,
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
						T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1]*-1,
					);

					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getSubItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->get("RecordID");
 		$data = $this->Beginbalance_model->getSubItem($id);
 		$html = "";
 		if(!empty($data)){
            $target = "'detailSub'";
			$a = 1;
            foreach ($data as $item) {
            	if($this->input->get("editable")){
	                $html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions">-</td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else if($this->input->get("LookupTranfer")){
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else{
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="editdetail('.$target.','.$a.',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }
				$a++;
            }
        }

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data, 'html' => $html);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file stockbalance.php */
/* Location: ./app/modules/master/controllers/stockbalance.php */
