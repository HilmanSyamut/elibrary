<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Salman Fariz
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Transfer Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Stocktransfer extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Stocktransfer_model','StockList_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/stocktransfer/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Stocktransfer_model->getDetail($id);
		$this->modules->render('/stocktransfer/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Stocktransfer_model->getDetail($id);
		$this->modules->render('/stocktransfer/formDetail', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Stocktransfer_model->getDetail($id);
		$this->modules->render('/stocktransfer/formPrint', $data);
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Stocktransfer_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Stocktransfer_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		try{
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo('IVIO'), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo('IVST'), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordStatus => 0,
					T_TransactionStockMovementHeader_DocTypeID => "IVIO",
					T_TransactionStockMovementHeader_DocNo => substr(DocNo('IVIO'), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remark"))? ' ':$this->input->post("Remark"),
					'detail' => $this->input->post("detail"),
					);

				$this->Stocktransfer_model->Insert($data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if (check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $this->input->post("DocType"),
					T_TransactionStockMovementHeader_DocNo => $this->input->post("DocNo"),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remark"))? ' ':$this->input->post("Remark"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail"),
					);

				$this->Stocktransfer_model->update($data);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Stocktransfer_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktransfer_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1
			);
			$this->Stocktransfer_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				if($temp[T_TransactionStockMovementDetail_LocationID1] == $temp[T_TransactionStockMovementDetail_LocationID2]){
					$message = $get_stocklist->row()->{T_TransactionStockBalanceHeader_ItemID}." cant transfer to same location.";
						throw new Exception($message); 	
				}
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				//====  TRANSFER FROM ==//
				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$qty = $get_stocklist->row()->{T_TransactionStockBalanceHeader_Quantity} - $temp[T_TransactionStockMovementDetail_Quantity1];
					if($qty < 0){
						$message = $get_stocklist->row()->{T_TransactionStockBalanceHeader_ItemID}." on location ".$temp[T_TransactionStockMovementDetail_LocationID1]." < 0.";
						throw new Exception($message); 		
					}
					$header_data = array(
						T_TransactionStockBalanceHeader_Quantity => $qty,
					);
					$parent_id1 = $get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID};
					$update_header = $this->StockList_model->UpdateStockList(T_TransactionStockBalanceHeader,$header_data,T_TransactionStockBalanceHeader_RecordID,$get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID});
				}else{
					$message = $get_stocklist->row()->{T_TransactionStockBalanceHeader_ItemID}." on location ".$temp[T_TransactionStockMovementDetail_LocationID1]." not found";
					throw new Exception($message); 					
				}

				

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id1,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1]*-1,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				//====  TRANSFER TO ==//
				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID2]);

				if($get_stocklist->num_rows() > 0){
					$qty = $get_stocklist->row()->{T_TransactionStockBalanceHeader_Quantity} + $temp[T_TransactionStockMovementDetail_Quantity1];

					$header_data[T_TransactionStockBalanceHeader_Quantity] = $qty;

					$parent_id = $get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID};
					$update_header = $this->StockList_model->UpdateStockList(T_TransactionStockBalanceHeader,$header_data,T_TransactionStockBalanceHeader_RecordID,$get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID});
				}else{
					$header_data[T_TransactionStockBalanceHeader_ItemID] = $temp[T_TransactionStockMovementDetail_ItemID];
					$header_data[T_TransactionStockBalanceHeader_LocationID] = $temp[T_TransactionStockMovementDetail_LocationID2];
					$header_data[T_TransactionStockBalanceHeader_Quantity] = $temp[T_TransactionStockMovementDetail_Quantity1];
					
					$parent_id = $this->StockList_model->InsertStockList(T_TransactionStockBalanceHeader,$header_data);
				}

				// begin triger post detail stock item
				if($typeItem == 6){

					// Get stock list
					$this->db->where(T_TransactionStockBalanceDetail_PRI,$parent_id1);
					$stock_exist = $this->db->get(T_TransactionStockBalanceDetail);
					
					if($stock_exist->num_rows() != 0){
						// get data sn
						$parentID = $temp[T_TransactionStockMovementDetail_RecordID];
						$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$parentID);
						$sqlSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
						$sqlSN = $sqlSN->result();
						foreach($sqlSN as $SN)
						{
							$detail_data = array(
								T_TransactionStockBalanceDetail_PRI => $parent_id
							);
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$SN->{T_TransactionStockMovementDetailSerialNo_SerialNo});
							$this->db->update(T_TransactionStockBalanceDetail,$detail_data);
						}
					}
				}
				// end triger post detail stock item

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktransfer_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->Stocktransfer_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				if($temp[T_TransactionStockMovementDetail_LocationID1] == $temp[T_TransactionStockMovementDetail_LocationID2]){
					$message = $get_stocklist->row()->{T_TransactionStockBalanceHeader_ItemID}." cant transfer to same location.";
						throw new Exception($message); 	
				}

				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				//====  TRANSFER FROM ==//
				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$qty = $get_stocklist->row()->{T_TransactionStockBalanceHeader_Quantity} + $temp[T_TransactionStockMovementDetail_Quantity1];

					$header_data = array(
						T_TransactionStockBalanceHeader_RecordStatus => 0,
						T_TransactionStockBalanceHeader_Quantity => $qty,
					);
					$parent_id1 = $get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID};
					$update_header = $this->StockList_model->UpdateStockList(T_TransactionStockBalanceHeader,$header_data,T_TransactionStockBalanceHeader_RecordID,$get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID});
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id1,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				//====  TRANSFER TO ==//
				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID2]);

				if($get_stocklist->num_rows() > 0){
					$qty = $get_stocklist->row()->{T_TransactionStockBalanceHeader_Quantity} - $temp[T_TransactionStockMovementDetail_Quantity1];
					if($qty < 0){
						$message = $get_stocklist->row()->{T_TransactionStockBalanceHeader_ItemID}." on location ".$temp[T_TransactionStockMovementDetail_LocationID2]." < 0.";
						throw new Exception($message); 		
					}
					$header_data[T_TransactionStockBalanceHeader_Quantity] = $qty;

					$parent_id = $get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID};
					$update_header = $this->StockList_model->UpdateStockList(T_TransactionStockBalanceHeader,$header_data,T_TransactionStockBalanceHeader_RecordID,$get_stocklist->row()->{T_TransactionStockBalanceHeader_RecordID});
				}

				// begin triger post detail stock item
				if($typeItem == 6){

					// Get stock list
					$this->db->where(T_TransactionStockBalanceDetail_PRI,$parent_id);
					$stock_exist = $this->db->get(T_TransactionStockBalanceDetail);
					
					if($stock_exist->num_rows() != 0){
						// get data sn
						$parentID = $temp[T_TransactionStockMovementDetail_RecordID];
						$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$parentID);
						$sqlSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
						$sqlSN = $sqlSN->result();
						foreach($sqlSN as $SN)
						{
							$detail_data = array(
								T_TransactionStockBalanceDetail_PRI => $parent_id1
							);
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$SN->{T_TransactionStockMovementDetailSerialNo_SerialNo});
							$this->db->update(T_TransactionStockBalanceDetail,$detail_data);
						}
					}
				}
				// end triger post detail stock item

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1]*-1,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

			}
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetStockList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = T_TransactionStockBalanceHeader_RecordID;
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
		$getList = $this->Stocktransfer_model->GetStockList($limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->countHeader = $this->Stocktransfer_model->GetStockListCount();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getSubItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->get("RecordID");
 		$data = $this->Stocktransfer_model->getSubItem($id);
 		$html = "";
 		if(!empty($data)){
            $target = "'detailSub'";
			$a = 1;
            foreach ($data as $item) {
            	if(!$this->input->get("editable")){
	                $html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions">-</td>
	                        <td id="detailSubItemName2v-'.$a.'" data-val="'.$item[T_TransactionStockBalanceDetail_SerialNo]. '">'.$item[T_TransactionStockBalanceDetail_SerialNo]. '</td>
	                </tr>'; 
	            }else{
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubItemName2v-'.$a.'" data-val="'.$item[T_TransactionStockBalanceDetail_SerialNo]. '">'.$item[T_TransactionStockBalanceDetail_SerialNo]. '</td>
	                </tr>'; 
	            }
				$a++;
            }
        }

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data, 'html' => $html);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file stocktransfer.php */
/* Location: ./app/modules/master/controllers/stocktransfer.php */
