	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Balance Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Pos extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Pos_model')); //,'Pos_model'
	}

	public function index($id='')
	{
		$this->template->set_layout('menucollapse');
		$sql = $this->db->get('t1013');
		$scan = $sql->first_row()->t1013f001;
		$data["scan"] = $scan;
		$this->modules->render('/form',$data);
	}

	public function listData($id='')
	{
		$data = "";
		$this->modules->render('index', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Pos_model->getDetail($id);
		$this->modules->render('/formPrint', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Pos_model->getDetail($id);
		$this->modules->render('/formDetail', $data);
	}

	public function getDetail()
	{	$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id =  $this->input->get('RecordID');
		$data = $this->Pos_model->getDetail($id);
		if(!empty($data)){
			$info->data = $data;
		}else{
			$info->data = null;
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_SalesInvoiceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Pos_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Pos_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		$this->db->trans_begin();
		try{
			$doctype = "SLPS";
			if (check_column(T_SalesInvoiceHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_SalesInvoiceHeader_DocTypeID => $doctype,
					T_SalesInvoiceHeader_DocNo => substr(DocNo($doctype), 2),
					T_SalesInvoiceHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_SalesInvoiceHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_SalesInvoiceHeader_TermofPayment => empty($this->input->post("TermofPayment"))? "":$this->input->post("TermofPayment"),
					T_SalesInvoiceHeader_CurrencyID => empty($this->input->post("CurrencyID"))? "":$this->input->post("CurrencyID"),
					T_SalesInvoiceHeader_ExchangeRate => empty($this->input->post("ExchRate"))? 0:$this->input->post("ExchRate"),
					T_SalesInvoiceHeader_SalespersonID => empty($this->input->post("SalespersonID"))? "":$this->input->post("SalespersonID"),
					T_SalesInvoiceHeader_BizPartnerID => empty($this->input->post("BizPartnerID"))? "":$this->input->post("BizPartnerID"),
					T_SalesInvoiceHeader_AmountSubtotal => empty($this->input->post("AmountVAT"))? 0:$this->input->post("AmountVAT"),
					T_SalesInvoiceHeader_AmountVAT => empty($this->input->post("AmountSubtotal"))? 0:$this->input->post("AmountSubtotal"),
					T_SalesInvoiceHeader_AmountTotal => empty($this->input->post("AmountTotal"))? 0:$this->input->post("AmountTotal"),
					T_SalesInvoiceHeader_Payment => empty($this->input->post("Payment"))? 0:$this->input->post("Payment"),
					T_SalesInvoiceHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'detail' => $this->input->post("detail")
				);

				$id = $this->Pos_model->Insert($data);
				$this->db->trans_commit();
				$this->db->trans_begin();
				// print_r($id); die;
				$this->Post($id);
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->Pos_model->Delete($id);
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "SLPS";
			if(check_column(T_SalesInvoiceHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SalesInvoiceHeader_RecordID => $this->input->post("RecordID"),
					T_SalesInvoiceHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_SalesInvoiceHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_SalesInvoiceHeader_DocTypeID => $doctype,
					T_SalesInvoiceHeader_DocNo => $this->input->post("DocNo"),
					T_SalesInvoiceHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_SalesInvoiceHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_SalesInvoiceHeader_TermofPayment => empty($this->input->post("TermofPayment"))? "":$this->input->post("TermofPayment"),
					T_SalesInvoiceHeader_CurrencyID => empty($this->input->post("CurrencyID"))? 0:$this->input->post("CurrencyID"),
					T_SalesInvoiceHeader_ExchangeRate => empty($this->input->post("ExchRate"))? 0:$this->input->post("ExchRate"),
					T_SalesInvoiceHeader_SalespersonID => empty($this->input->post("SalespersonID"))? "":$this->input->post("SalespersonID"),
					T_SalesInvoiceHeader_BizPartnerID => empty($this->input->post("BizPartnerID"))? "":$this->input->post("BizPartnerID"),
					T_SalesInvoiceHeader_AmountSubtotal => empty($this->input->post("AmountVAT"))? 0:$this->input->post("AmountVAT"),
					T_SalesInvoiceHeader_AmountVAT => empty($this->input->post("AmountSubtotal"))? 0:$this->input->post("AmountSubtotal"),
					T_SalesInvoiceHeader_AmountTotal => empty($this->input->post("AmountTotal"))? 0:$this->input->post("AmountTotal"),
					T_SalesInvoiceHeader_Payment => empty($this->input->post("Payment"))? 0:$this->input->post("Payment"),
					T_SalesInvoiceHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail")
				);

				$this->Pos_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Pos_model->Delete($this->input->post("RecordID"));
			
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function TrigerReader()
	{
		try{
			$html="";
			$msg="";
			$id = $this->input->post("id");
			$this->db->update("t1013",array("t1013f001" => $id));
			if($id==1)
			{
				$this->db->truncate("t1014");
				$msg="Scaning...";
			}else{
				$this->db->select("t1014f001");
				$sql = $this->db->get("t1014");
				$dataEPC = $sql->result();
				$res ='';
				if($sql->num_rows() > 0){
					$i=1;
					foreach($dataEPC as $epc)
					{
						// checking epc for serialno
						$snTag = substr($epc->t1014f001,0,CUTPREFIXSN);
						$this->db->where(T_MasterDataItem_EPC,$snTag);
						$sql2 = $this->db->get(T_MasterDataItem);
						$epcExist = $sql2->first_row();
						
						if($sql2->num_rows() != 0)// serial no
						{
							if($sql->num_rows()==$i){
								$res .= '"'.$snTag.'"';
							}else{
								$res .= '"'.$snTag.'",';
							}
						}else{
							if($sql->num_rows()==$i){
								$res .= '"'.$epc->t1014f001.'"';
							}else{
								$res .= '"'.$epc->t1014f001.'",';
							}
						}
						
						$i++;
					}
				}
				if($res!=""){
					$query = 'SELECT * FROM t8010 where t8010f005 in('.$res.')';
					$sql2 = $this->db->query($query);
					$Detail = $sql2->result('array');
					$i=1; $target="'detail'"; 
					$msg = "empty data";
					if(isset($Detail) && !empty($Detail)):  
						foreach($Detail as $item): 
						$html .= '<tr id="detail-'.$i. '">
						<td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
						<td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
						<td id="detailItemIDv-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemID]. '">'.$item[T_MasterDataItem_ItemID]. '</td>
						<td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
						<td id="detailQtyv-'.$i. '" data-val="1" ><input id="qtyf-'.$i.'" type="number" value="1" style="width:50px;" onchange="calculating('.$i.',this.value);"></td>
						<td id="detailUnitPricev-'.$i. '" data-val="'.$item[T_MasterDataItem_UnitPrice]. '" data-def="'.$item[T_MasterDataItem_UnitPrice]. '">'.$item[T_MasterDataItem_UnitPrice]. '</td>
						<td id="detailLineTotalv-'.$i. '" data-val="'.$item[T_MasterDataItem_UnitPrice]. '">'.$item[T_MasterDataItem_UnitPrice]. '</td>
						<td id="detailRecordIDDetailv-'.$i. '" data-val="" style="display:none;"></td>
						<td id="detailRecordFlagv-'.$i. '" data-val="" style="display:none;"></td>
					</tr>'; $i++; endforeach; 
					$msg = "found ".$sql2->num_rows()." data";
					endif;
				}else{
					$msg = "empty epc data";
				}
			}
			
			$output = array('errorcode' => 0, 'msg' => $msg, 'html' => $html);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post($id){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";
		// $this->db->trans_begin();
		// try{
			
            //get data
			// $id = $this->input->post("RecordID");
			$get_data = $this->Pos_model->getDetail($id);
            
			$data = array(
				T_SalesInvoiceHeader_RecordStatus   => 1,
			);
			$this->Pos_model->UpdateHeader($data,$id);
            foreach ($get_data["Detail"] as $temp) {
				$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item

					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $temp[T_SalesInvoiceDetail_Quantity];
					if($qty < 0){
						$voMsg .= $temp[T_SalesInvoiceDetail_ItemName]." < 0 in Location ".T_SalesInvoiceDetail_LocationID." \n ";
					}
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

					if($get_master_item[T_MasterDataItem_AutoIDType] == 6){
						$this->db->from(T_SalesInvoiceDetailSerialNo);
						$this->db->where(T_SalesInvoiceDetailSerialNo_PRI,$temp[T_SalesInvoiceDetail_RecordID]);
						$get_serial_no = $this->db->get()->result("array");

						foreach ($get_serial_no as $get_serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $get_serial_no_temp[T_SalesInvoiceDetailSerialNo_SerialNo]);
							$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>0));
						}
					}

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $stocklistPRI,
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_SalesInvoiceHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_SalesInvoiceHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_SalesInvoiceDetail_RecordID],
						T_TransactionStockBalanceLog_Quantity => $temp[T_SalesInvoiceDetail_Quantity]*-1,
					);

					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
					if($voMsg != ""){ throw new Exception($voMsg); }
			}

			// $this->db->trans_commit();
		// 	$output = array('errorcode' => 0, 'msg' => 'success');
		// }catch(Exception $e)
		// {
		// 	$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		// 	$this->db->trans_rollback();
		// }

		// $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Pos_model->getDetail($id);
			$data = array(
				T_SalesInvoiceHeader_RecordStatus   => 0,
			);
			$this->Pos_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $temp[T_SalesInvoiceDetail_Quantity];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

				}

				if($get_master_item[T_MasterDataItem_AutoIDType] == 6){
					$this->db->from(T_SalesInvoiceDetailSerialNo);
					$this->db->where(T_SalesInvoiceDetailSerialNo_PRI, $temp[T_SalesInvoiceDetail_RecordID]);
					$get_serial_no = $this->db->get();

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_SalesInvoiceDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $stocklistPRI,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_SalesInvoiceDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_SalesInvoiceDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $stocklistPRI,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_SalesInvoiceHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_SalesInvoiceHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_SalesInvoiceDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_SalesInvoiceDetail_Quantity],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

			}

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getSubItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->get("RecordID");
 		$data = $this->Pos_model->getSubItem($id);
 		$html = "";
 		if(!empty($data)){
            $target = "'detailSub'";
			$a = 1;
            foreach ($data as $item) {
            	if($this->input->get("editable")){
	                $html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions">-</td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '">'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else if($this->input->get("LookupTranfer")){
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '">'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else{
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="editdetail('.$target.','.$a.',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '">'.$item[T_SalesInvoiceDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }
				$a++;
            }
        }

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data, 'html' => $html);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file stockbalance.php */
/* Location: ./app/modules/master/controllers/stockbalance.php */
