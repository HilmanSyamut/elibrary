<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pos_model extends CI_Model
{
    public function getSubItem($id)
    {
        $this->db->where(T_SalesInvoiceDetailSerialNo_PRI,$id);
        $query = $this->db->get(T_SalesInvoiceDetailSerialNo);
        $data = $query->result('array');
        return $data;
    }

    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where(T_SalesInvoiceHeader_DocTypeID, 'SLPS');
        $this->db->order_by($sortfield, $sortdir);
        return $this->db->get($table);
    }

    //Count Data
    public function getListCount($table){
        $this->db->where(T_SalesInvoiceHeader_DocTypeID, 'SLPS');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getDetail($id){
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceHeader);
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($data[T_SalesInvoiceHeader_RecordID]);
        }
        return $data;
    }
    public function item_exist($ItemID){
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $ItemID);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, T_SalesInvoiceDetail_LocationID);

        return $this->db->get();
    }

    public function getDetailItem($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceDetail);
        $this->db->join(T_MasterDataItem.' i', 
                        'i.'.T_MasterDataItem_ItemID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemID, 
                        'left'
                        );
        $this->db->join(T_MasterDataGeneralTableValue.' m', 
        'm.'.T_MasterDataGeneralTableValue_RecordID.'= i.'.T_MasterDataItem_AutoIDType, 
        'left'
        );
        $this->db->where(T_SalesInvoiceDetail_PRI, $id);
        $this->db->order_by(T_SalesInvoiceDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
    }

    public function Insert($data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        unset($data['detail']);
        $this->db->insert(T_SalesInvoiceHeader, $data);
        $parentID = $this->db->insert_id();
        foreach($detail as $item){
            $val = array(
                T_SalesInvoiceDetail_PRI => !isset($parentID)? 0:$parentID,
                T_SalesInvoiceDetail_RowIndex => !isset($item['RowIndex'])? 0:$item['RowIndex'],
                T_SalesInvoiceDetail_ItemID => !isset($item['ItemID'])? ' ':$item['ItemID'],
                T_SalesInvoiceDetail_ItemName => !isset($item['ItemName'])? ' ':$item['ItemName'],
                T_SalesInvoiceDetail_AutoIDType => !isset($item['AutoIDType'])? ' ':$item['AutoIDType'],
                T_SalesInvoiceDetail_EPC => strtoupper(!isset($item['EPC'])? ' ':$item['EPC']),
                T_SalesInvoiceDetail_Barcode => !isset($item['Barcode'])? ' ':$item['Barcode'],
                T_SalesInvoiceDetail_Quantity => !isset($item['Qty'])? 0:$item['Qty'],
                T_SalesInvoiceDetail_UoMID => !isset($item['UOM'])? 0:$item['UOM'],
                T_SalesInvoiceDetail_UnitPrice => !isset($item['UnitPrice'])? 0:$item['UnitPrice'],
                T_SalesInvoiceDetail_LineTotal => !isset($item['LineTotal'])? 0:$item['LineTotal'],
                T_SalesInvoiceDetail_Remarks => !isset($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
            );
            $this->db->insert(T_SalesInvoiceDetail, $val);
            $parentItemID = $this->db->insert_id();
            if(!empty($item['sub'])){
                foreach ($item['sub'] as $sub) {
                    $valSub = array(
                        T_SalesInvoiceDetailSerialNo_PRI => $parentItemID,
                        T_SalesInvoiceDetailSerialNo_SN => isset($sub["SerialNo"]) ? $sub["SerialNo"] : ""
                    );
                    $this->db->insert(T_SalesInvoiceDetailSerialNo,$valSub);
                }
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            UpdateDocNo($data[T_SalesInvoiceHeader_DocTypeID]);
            $this->db->trans_commit();
        }
        return $parentID;
    }

    public function Update($data){
        $this->db->trans_begin();
        $parentID = $data[T_SalesInvoiceHeader_RecordID];
        $detail = $data['detail'];
        $DetailRecordID = $data['DoRemoveID'];
        unset($data['detail']);
        unset($data['DoRemoveID']);
        $this->db->where(T_SalesInvoiceHeader_RecordID,$parentID);
        $this->db->update(T_SalesInvoiceHeader, $data);
        if(!empty($DetailRecordID)){
            $DoRemoveID = explode(",",$DetailRecordID);
            foreach($DoRemoveID as $detailRemove){
                $this->db->delete(T_SalesInvoiceDetail,array(T_SalesInvoiceDetail_RecordID=>$detailRemove));
            }
        }
        foreach($detail as $item){
            $val = array(
                T_SalesInvoiceDetail_PRI => !isset($parentID)? 0:$parentID,
                T_SalesInvoiceDetail_RowIndex => !isset($item['RowIndex'])? 0:$item['RowIndex'],
                T_SalesInvoiceDetail_ItemID => !isset($item['ItemID'])? ' ':$item['ItemID'],
                T_SalesInvoiceDetail_ItemName => !isset($item['ItemName'])? ' ':$item['ItemName'],
                T_SalesInvoiceDetail_AutoIDType => !isset($item['AutoIDType'])? ' ':$item['AutoIDType'],
                T_SalesInvoiceDetail_EPC => strtoupper(!isset($item['EPC'])? ' ':$item['EPC']),
                T_SalesInvoiceDetail_Barcode => !isset($item['Barcode'])? ' ':$item['Barcode'],
                T_SalesInvoiceDetail_Quantity => !isset($item['Qty'])? 0:$item['Qty'],
                T_SalesInvoiceDetail_UoMID => !isset($item['UOM'])? 0:$item['UOM'],
                T_SalesInvoiceDetail_UnitPrice => !isset($item['UnitPrice'])? 0:$item['UnitPrice'],
                T_SalesInvoiceDetail_LineTotal => !isset($item['LineTotal'])? 0:$item['LineTotal'],
                T_SalesInvoiceDetail_Remarks => !isset($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
            );
            $parentItemID =0;
            if(empty($item['RecordIDDetail']) && empty($item['RecordFlag'])){ // Insert New
                $this->db->insert(T_SalesInvoiceDetail, $val);
                $parentItemID = $this->db->insert_id();
            }elseif(!empty($item['RecordIDDetail']) && $item['RecordFlag'] == 1){
                $this->db->where(T_SalesInvoiceDetail_RecordID,$item['RecordIDDetail']);
                $this->db->update(T_SalesInvoiceDetail, $val);
                $parentItemID = $item['RecordIDDetail'];
            }
            if(!empty($item['sub'])){
                $this->db->delete(T_SalesInvoiceDetailSerialNo,array(T_SalesInvoiceDetailSerialNo_PRI=>$parentItemID));
                foreach ($item['sub'] as $sub) {
                    $valSub = array(
                        T_SalesInvoiceDetailSerialNo_PRI => $parentItemID,
                        T_SalesInvoiceDetailSerialNo_SN => isset($sub["SerialNo"]) ? $sub["SerialNo"] : ""
                    );
                    $this->db->insert(T_SalesInvoiceDetailSerialNo,$valSub);
                }
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_SalesInvoiceHeader, array(T_SalesInvoiceHeader_RecordID => $id));
        $this->db->delete(T_SalesInvoiceDetail, array(T_SalesInvoiceDetail_PRI => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateHeader($data,$id){
        $this->db->trans_begin();
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $this->db->update(T_SalesInvoiceHeader,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}
