<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_SalesInvoiceHeader_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_SalesInvoiceHeader_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_SalesInvoiceHeader_RecordStatus  => array(0,1,'50px','RS',1,'string',1),
    T_SalesInvoiceHeader_DocTypeID  => array(1,1,'100px','Doc Type',1,'string',1),
    T_SalesInvoiceHeader_DocNo  => array(1,1,'100px','Doc No',0,'string',0),
    T_SalesInvoiceHeader_DocDate  => array(1,1,'100px','Doc Date',0,'datetime',0),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_SalesInvoiceHeader,
    'tools' => array(T_SalesInvoiceHeader_RecordID,T_SalesInvoiceHeader_RecordTimestamp,T_SalesInvoiceHeader_RecordStatus,T_SalesInvoiceHeader_DocTypeID,T_SalesInvoiceHeader_DocNo,T_SalesInvoiceHeader_DocDate),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Pos/insert',
        'read' => 'Pos/getlist',
        'update' => 'Pos/update',
        'destroy' => 'Pos/delete',
        'form' => 'Pos/form',
        'post' => 'Pos/post',
        'unpost' => 'Pos/unPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>