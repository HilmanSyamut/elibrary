<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Point of Sale</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_SalesInvoiceHeader_RecordID}) ? ${T_SalesInvoiceHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_SalesInvoiceHeader_RecordTimestamp}) ? ${T_SalesInvoiceHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <!--<div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-textbox', 'value' => 'SLPS',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>-->
                        <div class="row">
                            <label class="col-md-3 form-label">Transaction No:</label>
                            <div class="col-md-9">
                                 <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : substr(DocNo('SLPS'), 2), 'style' => 'background-color:#eee;', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Salesperson</label>
                            <div class="col-md-9">
                                <input type="hidden" id="SalespersonID" />
                                <?php $items=array( 'id'=> 'SalespersonName', 'class' => 'k-input k-textbox', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "", ); echo form_input($items); ?>
                                <div class="k-button" id="LookupEventSalespersonID"> <div class="k-icon k-i-search"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Transaction Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Customer</label>
                            <div class="col-md-9">
                                <input type="hidden" id="BizPartnerID" />
                                <?php $items=array( 'id'=> 'BizPartnerName', 'class' => 'k-input k-textbox', 'value' => '', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                                <div class="k-button" id="LookupEventBizPartnerID"> <div class="k-icon k-i-search"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Term Of Payment</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'TermofPayment', 'class' => 'k-input k-textbox', 'value' => 'Cash' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount VAT</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountVAT', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountVAT}) ? ${T_SalesInvoiceHeader_AmountVAT} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Currency</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'CurrencyID', 'class' => 'k-input k-textbox', 'value' => 'IDR',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount Subtotal</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountSubtotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? ${T_SalesInvoiceHeader_AmountSubtotal} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Exchange Rate</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'ExchRate', 'class' => 'k-input', 'value' => '1',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount Total</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountTotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountTotal}) ? ${T_SalesInvoiceHeader_AmountTotal} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                       
                    </div>
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                 <textarea class="k-input k-textbox" id="Remarks"><?php echo (isset(${T_SalesInvoiceHeader_Remarks}))? ${T_SalesInvoiceHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <!--<a id="DetailModal" class="mb-xs mt-xs mr-xs btn btn-xs btn-success"><i class="fa fa-plus"></i> &nbsp;Add New</a>-->
                        <a id="removeAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detail');"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
                        <a id="scanTagStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="scanTag(1);"><i class="fa fa-barcode"></i> &nbsp;Start Scan</a>
                        <a id="scanTagStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="scanTag(0);" style="display:none;"><i class="fa fa-barcode"></i> &nbsp;Stop Scan</a>
                    </div>
                    <div class="col-md-9">
                        <div style="overflow:auto;">
                        <input id="DoRemoveID" type="hidden" />
                            <table id="table-detail" class="table table-bordered mb-none">
                                <thead id="head-detail">
                                    <tr>
                                        <th width="80px"><input type="checkbox" id="detailCheckAll" onclick="CheckAll('detail');"> Action</th>
                                        <th data-col="RowIndex">#</th>
                                        <th data-col="ItemID">Item ID</th>
                                        <th data-col="ItemName">Item Name</th>
                                        <th data-col="Qty">Quantity</th>
                                        <th data-col="UnitPrice">Unit Price</th>
                                        <th data-col="LineTotal">Line Total</th>
                                        <th data-col="RecordIDDetail" style="display:none;"></th>
                                        <th data-col="RecordFlag" style="display:none;"></th>
                                    </tr>
                                </thead>
                                <tbody id="list-detail">
                                    <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                    foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                    <td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"> <a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a> <a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
                                    <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                    <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemID]. '">'.$item[T_SalesInvoiceDetail_ItemID]. '</td>
                                    <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                    <td id="detailQtyv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Quantity]. '">'.$item[T_SalesInvoiceDetail_Quantity]. '</td>
                                    <td id="detailUnitPricev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_UnitPrice]. '">'.$item[T_SalesInvoiceDetail_UnitPrice]. '</td>
                                    <td id="detailLineTotalv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_LineTotal]. '">'.$item[T_SalesInvoiceDetail_LineTotal]. '</td>
                                    <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Remarks]. '">'.$item[T_SalesInvoiceDetail_Remarks]. '</td>
                                    <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_RecordID]. '" style="display:none;">'.$item[T_SalesInvoiceDetail_RecordID]. '</td>
                                </tr>'; $i++; endforeach; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <table id="table-summary" class="table table-bordered mb-none">
                            <thead id="head-summary"><tr><th colspan="2">Summary</th></tr></thead>
                            <tbody>
                            <tr><td>Subtotal</td><td><?php $items=array( 'id'=> 'AmountSubtotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? ${T_SalesInvoiceHeader_AmountSubtotal} : "", ); echo form_input($items); ?></td></tr>
                            <tr><td>GST</td><td><?php $items=array( 'id'=> 'AmountVAT', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountVAT}) ? ${T_SalesInvoiceHeader_AmountVAT} : "", ); echo form_input($items); ?></td></tr>
                            <tr><td>Total</td><td><?php $items=array( 'id'=> 'AmountTotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountTotal}) ? ${T_SalesInvoiceHeader_AmountTotal} : "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                            <thead id="head-cash"><tr><th colspan="2">Cash</th></tr></thead>
                            <tbody>
                            <tr><td>Payment</td><td><?php $items=array( 'id'=> 'Payment', 'class' => 'k-input', 'value' =>  "", ); echo form_input($items); ?></td></tr>
                            <tr><td>Change</td><td><?php $items=array( 'id'=> 'Change', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                            <thead id="head-credit"><tr><th colspan="2">Credit</th></tr></thead>
                            <tbody>
                            <tr><td>Credit Card No</td><td><?php $items=array( 'id'=> 'CreditCardNo', 'class' => 'k-input', 'value' =>  "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <footer class="panel-footer text-right">
                <button class="btn btn-primary" type="submit">Tender</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-5">
                <input id="RecordFlag" type="hidden" />
                <input id="RecordIDDetail" type="hidden" />
                <div class="k-edit-label">#</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="RowIndex" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Item ID</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemID" datarequired="0" readonly />
                    <div class="k-button" id="LookupEventItemID"> <div class="k-icon k-i-search"></div> </div>
                </div>
                <div class="k-edit-label">Item Name</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemName" datarequired="0" readonly />
                    <input type="hidden" id="ItemType" class="k-input k-textbox" datarequired="1"  readonly/>
                </div>
                 <div class="k-edit-label">Qty</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="Qty" datarequired="0" />
                </div>
                <div class="k-edit-label">UOM</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="UOM" datarequired="0" readonly/>
                </div>
            </div>
            <div class="col-md-5">
                <div class="k-edit-label">Unit Price</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="UnitPrice" datarequired="0" />
                </div>
                <div class="k-edit-label">Line Total</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="LineTotal" datarequired="0" />
                </div>
                <div class="k-edit-label">Remarks</div>
                <div class="k-edit-field">
                    <textarea class="k-textbox" id="RemarksDetail"></textarea>
                </div>
            </div>
        </div>
        <a id="DetailModalSub" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" style="display:none;" onclick="openDetailModal();"><i class="fa fa-plus"></i> &nbsp;Add New</a>
        <a id="detailSubRemoveAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detailSub');" style="display:none;"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
        <div id="tableDetailModalSub" style="overflow:auto; display:none;">
            <table id="table-detailSub" class="table table-bordered mb-none">
                <thead id="head-detailSub">
                    <tr>
                        <th width="80px"><input type="checkbox" id="detailSubCheckAll" onclick="CheckAll('detailSub');"> Action</th>
                        <th data-col="SerialNo">SerialNo</th>
                    </tr>
                </thead>
                <tbody id="list-detailSub">
                </tbody>
            </table>
        </div>

        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="fa fa-cancel"></i> &nbsp;Cancel</button>
        </div>
    </div>
</div>

<div style="display:none;" class="k-edit-form-container" id="detailSubForm">
    <div class="row col-md-12">
        <div id="kgSerialNo"></div>
    </div>
</div>
<!--  End Modal Form Detail -->

<?php
 $dataItem = array(
    array('field' => T_TransactionStockBalanceHeader_ItemID, 'title' => 'Item ID', 'width' => '50px'),
    array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '80px'),
    array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location', 'width' => '80px'),
    array('field' => T_TransactionStockBalanceHeader_Quantity, 'title' => 'Qty', 'width' => '50px'),
    array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Type', 'width' => '50px'),
    array('field' => T_MasterDataItem_EPC, 'title' => 'EPC', 'width' => '80px'),
    array('field' => T_MasterDataItem_Barcode, 'title' => 'Barcode', 'width' => '80px')
);
//Double Click Throw Data to Form
$columnItem = array(
    array('id' => 'RecordIDDetail2', 'column' => T_TransactionStockBalanceHeader_RecordID),        
    array('id' => 'ItemID', 'column' => T_TransactionStockBalanceHeader_ItemID),
    array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
    array('id' => 'ItemType', 'column' => T_MasterDataGeneralTableValue_Key),
    array('id' => 'EPC', 'column' => T_MasterDataItem_EPC),
    array('id' => 'Barcode', 'column' => T_MasterDataItem_Barcode),
    array('id' => 'UnitPrice', 'column' => T_MasterDataItem_UnitPrice),
    array('id' => 'UOM', 'column' => T_MasterDataItem_UOMID),
);
$filter = array('');

//id, title, size, URL, data field in database, Throw Data To form when click
echo kendoModalLookup("ItemID", "Data Item", "700px", "Stockmovement/Stocktransfer/GetStockList", $dataItem, $columnItem,T_TransactionStockBalanceHeader,'',$filter);

//Location Lookup
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("Location", "Data Location", "500px", "Webservice/Read/Getlist", $dataLocation, $columnLocation,T_MasterDataLocation);

    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataperson_PersonID, 'title' => 'ID', 'width' => '200px'),
        array('field' => T_MasterDataperson_PersonName, 'title' => 'Name', 'width' => '200px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'SalespersonID', 'column' => T_MasterDataperson_PersonID),
        array('id' => 'SalespersonName', 'column' => T_MasterDataperson_PersonName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("SalespersonID", "Sales Person", "700px", "Webservice/Read/getlist", $dataItem, $columnItem,T_MasterDataperson);

    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataBizPartnerHeader_BizPartnerID, 'title' => 'ID', 'width' => '200px'),
        array('field' => T_MasterDataBizPartnerHeader_BizPartnerName, 'title' => 'Name', 'width' => '200px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'BizPartnerID', 'column' => T_MasterDataBizPartnerHeader_BizPartnerID),
        array('id' => 'BizPartnerName', 'column' => T_MasterDataBizPartnerHeader_BizPartnerName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("BizPartnerID", "Customer", "700px", "Webservice/Read/getlist", $dataItem, $columnItem,T_MasterDataBizPartnerHeader);

?>

<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>
<script type="text/javascript" src="assets/js/apps.js"></script>

<?php 

if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>";
var SCAN = "<?php echo $scan; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {

    $("#RowIndex").kendoNumericTextBox(); 
    $("#RowIndex2").kendoNumericTextBox(); 
    $("#ExchRate").kendoNumericTextBox(); 
    $("#AmountVAT").kendoNumericTextBox({spinners: false}); 
    $("#AmountSubtotal").kendoNumericTextBox({spinners: false}); 
    $("#AmountTotal").kendoNumericTextBox({spinners: false}); 
    $("#Change").kendoNumericTextBox({spinners: false}); 
    $("#CreditCardNo").kendoMaskedTextBox({
        mask: "0000 0000 0000 0000"
    });
    $("#Payment").kendoNumericTextBox({
        change:totalPayment,
        spinners: false
    });
    $("#UnitPrice").kendoNumericTextBox(); 
    $("#LineTotal").kendoNumericTextBox(); 

    kendoModal("detailForm","Add Detail","850px");
    kendoModal("detailSubForm","Add Item","400px");

     $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        cleardetail("detailSub", 0);
    });
    sumTotal('detail');
    $("#kgSerialNo").delegate("tbody>tr", "dblclick", dblclickSerialNo);

    if(SCAN==1){
        $("#scanTagStart").hide();
        $("#scanTagStop").show();
    }else{
        $("#scanTagStart").show();
        $("#scanTagStop").hide();
    }
});

function scanTag(id)
{
    var msg ="";
    var voData = {
        id: id,
    };
    $.ajax({
        type: 'POST',
        data: voData,
        url:  site_url('Pos/TrigerReader'),
        success: function (result) {
        if (result.errorcode > 0) {
            new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
        } else {
            if(id){
                $("#scanTagStart").hide();
                $("#scanTagStop").show();
                msg = "Start"; 
            }else{
                $("#scanTagStart").show();
                $("#scanTagStop").hide();
                msg = "Stop";         
                $("#list-detail").html(result.html);
                sumTotal('detail');
            }
            new PNotify({ title: "Reader "+msg, text: result.msg , type: 'info', shadow: true });
        }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
}
function insert()
    {
        var detail = getDetailSubItem('detail');
        var voData = {
            DocDate: $('#DocDate').val(),
            DocStatus: $('#DocStatus').val(),
            TermofPayment: $('#TermofPayment').val(),
            CurrencyID: $('#CurrencyID').val(),
            ExchRate: $('#ExchRate').val(),
            SalespersonID: $('#SalespersonID').val(),
            BizPartnerID: $('#BizPartnerID').val(),
            AmountVAT: $('#AmountVAT').val(),
            AmountSubtotal: $('#AmountSubtotal').val(),
            AmountTotal: $('#AmountTotal').val(),
            Payment: $('#Payment').val(),
            Remarks: $('#Remarks').val(),
            detail: detail
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Pos/insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailSubItem('detail');
     var voData = {
        RecordID: ID,
        TimeStamp: $('#TimeStamp').val(),
        DocNo: $('#DocNo').val(),
        DocDate: $('#DocDate').val(),
        DocStatus: $('#DocStatus').val(),
        TermofPayment: $('#TermofPayment').val(),
        CurrencyID: $('#CurrencyID').val(),
        ExchRate: $('#ExchRate').val(),
        SalespersonID: $('#SalespersonID').val(),
        BizPartnerID: $('#BizPartnerID').val(),
        AmountVAT: $('#AmountVAT').val(),
        AmountSubtotal: $('#AmountSubtotal').val(),
        AmountTotal: $('#AmountTotal').val(),
        Payment: $('#Payment').val(),
        DoRemoveID : $("#DoRemoveID").val(),
        detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Pos/update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(site_url('Pos'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.DocDate == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.detail == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }


    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}

//Sum Total
function sumTotal(target){
    var subTotal = getTotal(target,"float",6,2);
    var VAT = 7/100 * subTotal;
    var Total = VAT + subTotal;
    $("#AmountSubtotal").data("kendoNumericTextBox").value(subTotal);
    $("#AmountVAT").data("kendoNumericTextBox").value(VAT);
    $("#AmountTotal").data("kendoNumericTextBox").value(Total);
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);
        Calculate();
    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}
function GetDataSub(id)
    {
        var voData = {
            RecordID: id
        }; 
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stocktransfer/GetSubItem')+"?editable=1",
            success: function (result) {
                $('#list-detailSub').append(result.html);
                var qty = document.getElementById("list-detailSub").rows.length;
                $('#Qty').data('kendoNumericTextBox').value(qty);
                var target = "detailSub";
                var RowIndex = $("#RowIndex").data("kendoNumericTextBox").value();
                var htmlUpdate = $('#list-'+target).html();
                var ID = current_url()+target+RowIndex;
                localStorage[ID] = htmlUpdate;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#detailSubForm").kendoWindow({
            width: "500px",
            title: "Serial NO",
            visible: false,
            modal: true,
            actions: [
            "Close"
            ],
        });

        $("#kgSerialNo").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                    },
                },
                sync: function(e) {
                    // $("#kgLogStocking").data("kendoGrid").dataSource.read();
                    // $("#kgLogStocking").data("kendoGrid").refresh();
                },
                schema: {
                    data: function(datas){
                        return datas.data;
                    },
                    total: function(datas){
                        return datas.count;
                    },
                    model: {
                        id: "RecordID",
                    }
                },                        
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
                autoBind: false,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                    height: "500px",
                    width: "100%",
                    columns: [{"field":"<?php echo T_TransactionStockBalanceDetail_SerialNo; ?>","title":"Serial NO","width":"50px",},
                    ],
            });

    });
    function openDetailModal()
    {
        var id = $("#RecordIDDetail2").val();
        $.ajax(
        {
            type: 'GET',
            url: site_url("Webservice/Read/Getlist"),
            dataType: 'json',
            data: { table: "t1992", customfilter: {t1992f001:id,t1992f003:1}  },
            success: function (result) {
                $("#kgSerialNo").data("kendoGrid").dataSource.data(result.data);
            }
        });
    }

    function dblclickSerialNo(e)
    {
        var grid = $("#kgSerialNo").data("kendoGrid");
        var voRow = grid.dataItem(grid.select());
        var SNexist = getTotal('detailSub','string',1);
        if(SNexist){
            if(SNexist.indexOf(voRow.t1992f002) == -1){
                GetDataSub(voRow.t1992r001);
            }
        }else{
            GetDataSub(voRow.t1992r001);
        }
    }

    function calculating(i,qty){
        var UP = $("#detailUnitPricev-"+i).attr("data-val");
        var LT = UP * qty;
        $("#detailQtyv-"+i).attr("data-val",qty);
        $("#detailLineTotalv-"+i).attr("data-val",LT);
        $("#detailLineTotalv-"+i).html(LT);
        sumTotal('detail');
    }

    function totalPayment()
    {
        var payment = $('#Payment').data('kendoNumericTextBox').value();
        var AmountTotal = $('#AmountTotal').data('kendoNumericTextBox').value();
        var change = payment - AmountTotal;
        $('#Change').data('kendoNumericTextBox').value(change);

    }
</script>