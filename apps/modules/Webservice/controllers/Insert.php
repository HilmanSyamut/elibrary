<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Insert Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Insert extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Insert_model');
	}

//Master Item
	public function Insert(){
		try{
			//$json = file_get_contents('php://input');
			//$obj = json_decode($json);
			
			if (check_column('t8010f001', 'ItemID') == TRUE) {
			 	$output = array('errorcode' => 200, 'msg' => 'duplicate primary key');
			}else{		
				$data = array(
					't8010r002' => date("Y-m-d H:i:s",now()),
					't8010r003' => 0,
					't8010f001' => strtoupper($this->input->post("ItemID")),
					't8010f002' => $this->input->post("ItemName"),
	                't8010f003' => $this->input->post("ItemType"),
	                't8010f004' => $this->input->post("Barcode"),
	                't8010f005' => $this->input->post("EPC"),
				);
				
				$this->Insert_model->InsertItem($data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Insert new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Balance

}

/* End of file insert.php */
/* Location: ./app/modules/master/controllers/insert.php */
