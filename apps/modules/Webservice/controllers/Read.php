<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Read Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Read extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
		
    	$this->load->model('Read_model');
	}

	public function index(){
		print_out("This is Webservice Read");
	}

//Get List Data
	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Read_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
		
	}

	//Lookup Data Item 
	public function GetListItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getListItem($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Read_model->getListItemCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getExchangeRate($currency,$type)
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$this->db->where(T_MasterDataKurs_Currency,$currency);
		$this->db->where(T_MasterDataKurs_KursType,$type);
		$query = $this->db->get(T_MasterDataKurs);
		if($query->num_rows() > 0){
			$info->data =  $query->last_row();
			$info->msg = "Success";
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetListItemForCOA(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getListItemForCOA($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $getList->num_rows();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetStockWithoutCOA(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->post("ItemID");
 		$data['Detail'] = $this->Read_model->GetStockWithoutCOA($id);

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file read.php */
/* Location: ./app/modules/webservice/controllers/read.php */
