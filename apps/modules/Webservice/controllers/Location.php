<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Master Module
 *
 * Location Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 14.12.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Location extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('location/Location_model');
	}

	public function index()
	{
		$data = '';
		print_out("webservice");
	}
	
	public function Insert(){
		try{		
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

			if (check_column(T_LOCATION_ID, 'LocationID') == TRUE)
				{
					$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.strtoupper($this->input->post("LocationID")).' sudah ada');
				}else{
					$data = array(
						T_LOCATION_RecordTimeStamp => date("Y-m-d H:i:s",now()),
						T_LOCATION_RecordStatus => 0,
						T_LOCATION_UpdateAt => now(),
						T_LOCATION_UpdateBy => $this->ezrbac->getCurrentUserID(),
						T_LOCATION_UpdateOn => $this->input->ip_address(),
						T_LOCATION_ID => strtoupper($this->input->post("LocationID")),
						T_LOCATION_Name => $this->input->post("LocationName"),
					);
					
					$this->Location_model->Insert($data);
					$id = $this->db->insert_id();
		            $activity_log = array(
		                'msg'=> 'Insert new list',
		                'kategori'=> 7,
		                'jenis'=> 1,
		                'object'=> $id
		            );

		            activity_log($activity_log);
					$output = array('errorcode' => 0, 'msg' => 'success');
		        }
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}


	public function Update(){
		try{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

			if (check_column(T_LOCATION_RecordTimeStamp, 'RecordTimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
        	}else{
				$RecordID = $this->input->post("RecordID");
				$data = array(
						T_LOCATION_RecordTimeStamp => date("Y-m-d H:i:s",now()),
						T_LOCATION_RecordStatus => 0,
						T_LOCATION_UpdateAt => now(),
						T_LOCATION_UpdateBy => $this->ezrbac->getCurrentUserID(),
						T_LOCATION_UpdateOn => $this->input->ip_address(),
						T_LOCATION_ID => strtoupper($this->input->post("LocationID")),
						T_LOCATION_Name => $this->input->post("LocationName"),
						);
				
				$this->Location_model->Update($RecordID, $data);

	            $activity_log = array(
	                'msg'=> 'Update list',
	                'kategori'=> 7,
	                'jenis'=> 2,
	                'object'=> $this->input->post("t8030r001")
	            );

	            activity_log($activity_log);
				$output = array('errorcode' => 0, 'msg' => 'success');
	        }
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}


	public function Delete()
	{
		try{
			$this->Location_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->post("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Skpd.php */
/* Location: ./app/modules/master/controllers/Skpd.php */
