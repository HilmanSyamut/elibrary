<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model
{
    public function InsertPost($id, $data){
        $this->db->trans_begin();
        $this->db->insert("t1020", $data);
        $this->db->set('t1010r003', 1);
        $this->db->where('t1010r001', $id);
        $this->db->update('t1010');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdatePost($id, $idItem, $total){
        $this->db->trans_begin();
        $this->db->set('t1020f003', $total);
        $this->db->where('t1020r001', $idItem);
        $this->db->update('t1020');
        $this->db->set('t1010r003', 1);
        $this->db->where('t1010r001', $id);
        $this->db->update('t1010');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateUnPost($id, $idItem, $total){
        $this->db->trans_begin();
        $this->db->set('t1020f003', $total);
        $this->db->where('t1020r001', $idItem);
        $this->db->update('t1020');
        $this->db->set('t1010r003', 0);
        $this->db->where('t1010r001', $id);
        $this->db->update('t1010');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function GetPost($id){
        $this->db->select('d.*, i.*, t.*');
        $this->db->from('t1011 d');
        $this->db->join('t8010 i', 'd.t1011f003=i.t8010f001', 'left');
        $this->db->join('t1010 t', 'd.t1011f001=t.t1010r001', 'left');
        $this->db->where('d.t1011f001',$id);
        return $this->db->get();
    }

    public function GetDetailPost($item, $location){
        $this->db->select('t.*');
        $this->db->from('t1020 t');
        $this->db->where('t.t1020f001', $item);
        $this->db->where('t.t1020f002', $location);
        return $this->db->get();
    }

}
