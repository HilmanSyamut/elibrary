<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Insert_model extends CI_Model
{

//Insert
    public function Insert($table, $data){
        $this->db->trans_begin();
            unset($data[0]);
            $this->db->insert($table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}
