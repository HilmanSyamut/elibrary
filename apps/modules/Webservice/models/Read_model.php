<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Read_model extends CI_Model
{

//Get List Data
    //List Data Global
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }
            }
        }
        if($customfilter != ''){
            $last = '';
            foreach ($customfilter as $col => $val) {
                $unix = explode('_', $col);
                if(is_array($unix))
                {
                    $this->db->where_in($unix[0],$val);
                }else{
                    $this->db->where($col,$val);
                }
            }
        }
        return $this->db->get($table);
    }

    //Count Data Global
    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $unix = explode('_', $col);
                if(is_array($unix))
                {
                    $this->db->where_in($unix[0],$val);
                }else{
                    $this->db->where($col,$val);
                }
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    //GetList Data ITEM Lookup
        public function getListItem($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->select('*');
            if($table == T_TransactionStockBalanceHeader){
                $this->db->join(T_MasterDataItem.' a', 'a.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID);
                $this->db->join(T_MasterDataGeneralTableValue.' b', 'b.'.T_MasterDataGeneralTableValue_RecordID.'='.T_MasterDataItem_AutoIDType);
                $this->db->join(T_MasterDataLocation.' c', 'c.'.T_MasterDataLocation_LocationID.'='.T_TransactionStockBalanceHeader_LocationID);
            }
            if($table == T_MasterDataItem){
                $this->db->join(T_MasterDataGeneralTableValue.' a', 'a.'.T_MasterDataGeneralTableValue_RecordID.'='.$table.'.'.T_MasterDataItem_AutoIDType);
            }
            // if($table==T_ItemMovementStockListDetail1){
            //     $this->db->join(T_MasterDataLocation.' b', 'b.'.T_MasterDataLocation_LocationID.'= a.'.T_ItemMovementStocklistHeader_LocationID);
            // }
            $this->db->order_by($sortfield, $sortdir);   
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                }
            }
        }
        // if($table==T_ItemMovementStockListDetail1){
        //     $this->db->join(T_ItemMovementStocklistHeader.' a', 'a.'.T_ItemMovementStocklistHeader_RecordID.'='.$table.'.'.T_ItemMovementStockListDetail1_PRI);
        //     $this->db->join(T_MasterDataLocation.' b', 'b.'.T_MasterDataLocation_LocationID.'= a.'.T_ItemMovementStocklistHeader_LocationID);
        // }
        return $this->db->get($table);
        // print_out($this->db->last_query());
    }

    //Count Data Item Lookup
    public function getListItemCount($table){
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

}
