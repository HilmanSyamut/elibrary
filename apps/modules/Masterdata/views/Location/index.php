<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataLocation_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataLocation_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataLocation_LocationID  => array(1,1,'50px','Location ID',1,'string',1),
    T_MasterDataLocation_LocationName  => array(1,1,'100px','Location Name',0,'string',0)
);
// Column DropdownList => |Text|URL|a
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'location',
    'table' => T_MasterDataLocation,
    'tools' => array(T_MasterDataLocation_RecordID,T_MasterDataLocation_RecordTimestamp,T_MasterDataLocation_LocationID,T_MasterDataLocation_LocationName),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Location/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Location/Update',
        'destroy' => 'Masterdata/Location/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>