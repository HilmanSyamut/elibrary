<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataAccount_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
				<input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataAccount_RecordID}) ? ${T_MasterDataAccount_RecordID} : ""; ?>">
				<input type="hidden" id="RecordTimestamp" value="<?php echo isset(${T_MasterDataAccount_RecordTimestamp}) ? ${T_MasterDataAccount_RecordTimestamp} : ""; ?>">
				<input type="hidden" id="RecordStatus" value="<?php echo isset(${T_MasterDataAccount_RecordStatus}) ? ${T_MasterDataAccount_RecordStatus} : ""; ?>">
				<input type="hidden" id="RecordUpdatedOn" value="<?php echo isset(${T_MasterDataAccount_RecordUpdatedOn}) ? ${T_MasterDataAccount_RecordUpdatedOn} : ""; ?>">
				<input type="hidden" id="RecordUpdatedBy" value="<?php echo isset(${T_MasterDataAccount_RecordUpdatedBy}) ? ${T_MasterDataAccount_RecordUpdatedBy} : ""; ?>">
				<input type="hidden" id="RecordUpdatedAt" value="<?php echo isset(${T_MasterDataAccount_RecordUpdatedAt}) ? ${T_MasterDataAccount_RecordUpdatedAt} : ""; ?>">
                <div class="form-group">    
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Account ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'AccountID', 'class' => 'k-input k-textbox', 'value' => isset(${T_MasterDataAccount_AccountID}) ? ${T_MasterDataAccount_AccountID} : "", "readonly" => TRUE ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">M/S</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'MasterSub', 'class' => 'k-input', 'value' => isset(${T_MasterDataAccount_MasterSub}) ? ${T_MasterDataAccount_MasterSub} : "", "onChange" => "setLevel();"  ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group"> 
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Account Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'AccountName', 'class' => 'k-input k-textbox', 'value' => isset(${T_MasterDataAccount_AccountName}) ? ${T_MasterDataAccount_AccountName} : "", "style" => "width:60%;"  ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>   
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Level</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'AccountLevel', 'class' => 'k-input', 'value' => isset(${T_MasterDataAccount_Level}) ? ${T_MasterDataAccount_Level} : "", "onChange" => "generateID();"  ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group"> 
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Account Type</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'AccountType', 'class' => 'k-input', 'value' => isset(${T_MasterDataAccount_AccountType}) ? ${T_MasterDataAccount_AccountType} : "" ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>   
                    <div class="col-md-6">
                        <div class="row" id="form-parent">
                            <label class="col-md-3 form-label">Parent</label>
                            <div class="col-md-9">
                            <?php 
                            if(isset(${T_MasterDataAccount_RecordID})) {
                            $lvl = isset(${T_MasterDataAccount_Level}) ? ${T_MasterDataAccount_Level} : 2;
                            echo form_dropdown('depth', $this->options_model->get_lookup($lvl-1,T_MasterDataAccount,T_MasterDataAccount_Level,T_MasterDataAccount_AccountName,T_MasterDataAccount_AccountID), (isset(${T_MasterDataAccount_Depth}) ? ${T_MasterDataAccount_Depth} : '') ,'class="k-input" id="depth", onChange="regenerateID(this.value);" '); 
                            } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group"> 
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Cash / Bank</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'AccountBank', 'class' => 'k-input', 'value' => isset(${T_MasterDataAccount_CashBankAccount}) ? ${T_MasterDataAccount_CashBankAccount} : "",  ); 
                                echo form_input($items); ?>
                                
                            </div>
                        </div>
                    </div>   
                    <div class="col-md-6">
                        
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<?php
echo ListGeneralList('BizType', 5);

?>
<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset(${T_MasterDataAccount_RecordID})) {
    $ID = ${T_MasterDataAccount_RecordID};
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: ""
    }
];

function setLevel()
{
    var type = $("#MasterSub").val();
    if(type==0){
        $("#AccountLevel").data("kendoDropDownList").value(5);
        $("#AccountLevel").data("kendoDropDownList").readonly(true);
    }else{
        $("#AccountLevel").data("kendoDropDownList").value(0);
        $("#AccountLevel").data("kendoDropDownList").readonly(false);
    }
    generateID();
}

function generateID()
{
    var level = $("#AccountLevel").val();
    var type = $("#MasterSub").val();
    var depth = $("#depth").val();
    console.log(level);
    if(!depth){
        depth = 1;
    }
    StreamData(level,type,depth,1);
}

function regenerateID(id)
{
    var level = $("#AccountLevel").val();
    var type = $("#MasterSub").val();
    var depth = id
    if(!depth){
        depth = 1;
    }
    StreamData(level,type,depth);
}

function StreamData(lvl,sub,depth,b="")
{
    var voData = {
        lvl: lvl,
        sub: sub,
        depth: depth
    };
    $.ajax({
        type: 'GET',
        data: voData,
        url:  site_url('MasterData/Account/getAccountNo'),
        success: function (result) {
            if (result.errorcode > 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
            } else {
                $("#AccountID").val(result.data);
                if(b){
                    $("#form-parent").html(result.form);
                    $("#depth").kendoDropDownList({});
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            new PNotify({ title: errorThrown, text: result.msg, type: 'error', shadow: true });
            // alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
}

$(document).ready(function() {

    $("#form-insert").bind('submit',function(e){
        e.preventDefault();
        if (validasi == "insert") {
            insert();
        }else{
            update();
        };
    });

    if (validasi == "update") {
        $('#Date').attr('readonly','TRUE');
        $("#Date").kendoDatePicker({
            enable: true
        });
    }

    $("#DetailModal").click(function() {
        $("#detailForm").data("kendoWindow").center().open();
        cleardetail("detail", 0);
    });

    $("#detailForm").kendoWindow({
        width: "550px",
        title: "Add Detail",
        visible: false,
        modal: true,
        actions: [
        "Close"
        ],
    });

    //Doc Status Dropdown 
    var data = [
        {text: "Balance Sheet", value:"BL"},
        {text: "Provit Lost", value:"PL"}
    ];
    $("#AccountType").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

    var data = [
        {text: "Cash", value:"D"},
        {text: "Bank", value:"C"}
    ];
    $("#AccountBank").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

    var data = [
        {text: "1", value:"1"},
        {text: "2", value:"2"},
        {text: "3", value:"3"},
        {text: "4", value:"4"},
        {text: "5", value:"5"},
    ];
    $("#AccountLevel").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

    var data = [
        {text: "Master", value:"1"},
        {text: "Sub", value:"0"},
    ];
    $("#MasterSub").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

    $("#depth").kendoDropDownList({});
});
//Insert
function insert()
{
    var voData = {
		AccountID: $('#AccountID').val(),
		AccountName: $('#AccountName').val(),
		AccountType: $('#AccountType').val(),
        CashBank: $('#AccountBank').val(),
        AccountLevel: $('#AccountLevel').val(),
		MasterSub: $('#MasterSub').val(),
        depth: $('#depth').val(),
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('MasterData/Account/insert'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

//Update
function update()
{
 var voData = {
		RecordID: $('#RecordID').val(),
		RecordTimestamp: $('#RecordTimestamp').val(),
		RecordStatus: $('#RecordStatus').val(),
		RecordUpdatedOn: $('#RecordUpdatedOn').val(),
		RecordUpdatedBy: $('#RecordUpdatedBy').val(),
		RecordUpdatedAt: $('#RecordUpdatedAt').val(),
		AccountID: $('#AccountID').val(),
        AccountName: $('#AccountName').val(),
        AccountType: $('#AccountType').val(),
        CashBank: $('#AccountBank').val(),
        AccountLevel: $('#AccountLevel').val(),
        MasterSub: $('#MasterSub').val(),
        depth: $('#depth').val(),
 };
 var valid = checkForm(voData);
     if(valid.valid)
     {
        $.ajax({
            type: 'POST',
            data: voData,
            url: "<?php echo site_url('MasterData/Account/update'); ?>",
           success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";
	if (voData.AccountID == "") { valid = 0; msg += "Account ID is required" + "\r\n"; }
	if (voData.AccountName == "") { valid = 0; msg += "Account Name is required" + "\r\n"; }
	if (voData.AccountType == "") { valid = 0; msg += "Account Type is required" + "\r\n"; }
	if (voData.AccountBank == "") { valid = 0; msg += "Account Bank is required" + "\r\n"; }
    var voRes = {valid: valid, msg: msg};
    return voRes;
}

//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

</script>