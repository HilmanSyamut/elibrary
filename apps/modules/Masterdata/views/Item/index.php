
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataItem_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataItem_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataItem_ItemID  => array(1,1,'50px','Item ID',1,'string',1),
    T_MasterDataItem_ItemName  => array(1,1,'100px','Item Name',0,'string',0),
    T_MasterDataItem_UOMID => array(1,1,'50px','UOM ID',0,'string',0),
    T_MasterDataItem_AutoIDType => array(1,1,'50px','Auto ID Type',0,'string',0),
    T_MasterDataItem_EPC => array(1,1,'100px','EPC',0,'string',0),
    T_MasterDataItem_Barcode => array(1,1,'100px','Barcode',0,'string',0)
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    T_MasterDataItem_UOMID => array('UOMID','Masterdata/Generaltable/getItemType/1',T_MasterDataGeneralTableValue_Key),
    T_MasterDataItem_AutoIDType => array('AutoIDType','Masterdata/Generaltable/getItemBiz/2',T_MasterDataGeneralTableValue_RecordID)

);
// variable attribute for gridview

$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataItem,
    'tools' => array(
        T_MasterDataItem_RecordID,
        T_MasterDataItem_RecordTimestamp,
        T_MasterDataItem_ItemID,
        T_MasterDataItem_ItemName,
        T_MasterDataItem_UOMID,
        T_MasterDataItem_AutoIDType,
        T_MasterDataItem_EPC,
        T_MasterDataItem_Barcode),
    'column' => $column,
    'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/Item/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Item/Update',
        'destroy' => 'Masterdata/Item/Delete',
        'form' => 'Masterdata/Item/Form'
    )
);
// generate gridView
echo simpleGridViewCustom($attr); 
?>