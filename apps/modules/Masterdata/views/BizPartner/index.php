
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
	T_MasterDataBizPartnerHeader_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataBizPartnerHeader_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataBizPartnerHeader_BizPartnerID => array(1,1,'50px','Biz Partner ID',0,'string',0),
    T_MasterDataBizPartnerHeader_BizPartnerName => array(1,1,'100px','Biz Partner Name',0,'string',0),
    T_MasterDataBizPartnerHeader_BizPartnerType => array(1,1,'100px','Biz Partner Type',0,'string',0)
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    T_MasterDataBizPartnerHeader_BizPartnerType => array('BizPartnerType','Masterdata/Generaltable/getitembiz/3',T_MasterDataGeneralTableValue_Key)
);
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataBizPartnerHeader,
    'tools' => array(T_MasterDataBizPartnerHeader_RecordID,T_MasterDataBizPartnerHeader_RecordTimestamp,T_MasterDataBizPartnerHeader_BizPartnerID,T_MasterDataBizPartnerHeader_BizPartnerName,T_MasterDataBizPartnerHeader_BizPartnerType),
    'column' => $column,
    'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/Bizpartner/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Bizpartner/Update',
        'destroy' => 'Masterdata/Bizpartner/Delete',
        'form' => 'Masterdata/Bizpartner/Form'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>