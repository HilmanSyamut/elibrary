
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|propertyForm
$column = array(
    T_MasterDataKurs_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataKurs_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataKurs_KursDate  => array(1,1,'50px','KursDate',0,'datetime',1),
    T_MasterDataKurs_Currency  => array(1,1,'100px','Currency',0,'string',1),
    T_MasterDataKurs_KursType  => array(1,1,'100px','KursType',0,'string',1),
    T_MasterDataKurs_KursJual  => array(1,1,'100px','KursJual',0,'int',1,
        array(
            array('property' => 'change', 'value' => 'KursTengah'),
            array('property' => 'spin', 'value' => 'KursTengah'),
        )),
    T_MasterDataKurs_KursBeli  => array(1,1,'100px','Kurs Beli',0,'int',1,
        array(
            array('property' => 'change', 'value' => 'KursTengah'),
            array('property' => 'spin', 'value' => 'KursTengah'),
        )),
    T_MasterDataKurs_KursTengah  => array(1,1,'100px','Kurs Tengah',0,'int',1)
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    T_MasterDataKurs_KursType => array('KursType','masterdata/generallist/GetItemType/1',T_MasterDataGeneralList_NameCode),
    T_MasterDataKurs_Currency => array('Currency','masterdata/generallist/GetItemType/2',T_MasterDataGeneralList_NameCode),
);
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataKurs,
    'actBTN' => "75px",
    'tools' => array(T_MasterDataKurs_RecordID,T_MasterDataKurs_RecordTimestamp,T_MasterDataKurs_KursDate,T_MasterDataKurs_Currency,T_MasterDataKurs_KursType,T_MasterDataKurs_KursJual,T_MasterDataKurs_KursBeli,T_MasterDataKurs_KursTengah),
    'column' => $column,
    // 'propertyForm' => $propertyForm,
    'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/Kurs/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Kurs/Update',
        'destroy' => 'Masterdata/Kurs/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>
<script type="text/javascript">    
    function KursTengah(e){
        var wd = e.sender.element.closest("[data-role='window']");
        var id = wd.attr("id") == "ActEdit" ? "_Edit":"";

        var KursJual = $("#KursJual"+id).data("kendoNumericTextBox").value();
        var KursBeli = $("#KursBeli"+id).data("kendoNumericTextBox").value();
        var KursTengah = (KursJual + KursBeli) / 2;
        
        $("#KursTengah"+id).data("kendoNumericTextBox").value(KursTengah);
    }
    
</script>