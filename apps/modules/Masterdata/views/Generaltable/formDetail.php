<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataGeneralTable_RecordID}) ? ${T_MasterDataGeneralTable_RecordID} : ''; ?>">
                <input type="hidden" id="RecordTimeStamp" value="<?php echo isset(${T_MasterDataGeneralTable_RecordTimestamp}) ? ${T_MasterDataGeneralTable_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                            <div class="k-edit-label">ID</div>
                                <div class="k-edit-field">
                                    <?php $items=array( 'id'=> 'ID', 'class' => 'k-input k-textbox', 'value' => isset(${T_MasterDataGeneralTable_ID}) ? ${T_MasterDataGeneralTable_ID} : "",); echo form_input($items); ?>
                                </div>
                    </div>
                    <div class="col-md-6">
                            <div class="k-edit-label">Name</div>
                                <div class="k-edit-field">
                                    <?php $items=array( 'id'=> 'Name', 'class' => 'k-input k-textbox', 'value' => isset(${T_MasterDataGeneralTable_Name}) ? ${T_MasterDataGeneralTable_Name} : "",); echo form_input($items); ?>
                                </div>
                    </div>
                </div>
                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <a id="DetailModal" class="mb-xs mt-xs mr-xs btn btn-xs btn-success"><i class="fa fa-plus"></i> &nbsp;Add New</a>
                            <div style="overflow:auto;">
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="300px">Action</th>
                                            <th width="300px" data-col="Key">Key</th>
                                            <th width="300px" data-col="Description">Description</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"><a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
                                        <td id="detailKeyv-'.$i. '" data-val="'.$item[T_MasterDataGeneralTableValue_Key]. '">'.$item[T_MasterDataGeneralTableValue_Key]. '</td>
                                        <td id="detailDescriptionv-'.$i. '" data-val="'.$item[T_MasterDataGeneralTableValue_Description]. '">'.$item[T_MasterDataGeneralTableValue_Description]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="vol-mb-12">
        <div class="k-edit-label">Key</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" style="text-transform: uppercase;" id="Key" />
        </div>
        <div class="k-edit-label">Description</div>
        <div class="k-edit-field">
            <input type="text" id="Description" class="k-input k-textbox" style="" datarequired="1"/>
        </div>
        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');"><i class="el-icon-file-new"></i> Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="el-icon-remove"></i> Cancel</button>
        </div>
    </div>
</div>
<!--  End Modal Form Detail -->

<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset($t8990r001)) {
    $ID = $t8990r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>"; 
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    }
];
$(document).ready(function() {

    //Numeric

    if (validasi == "update") {
        $('#Date').attr('readonly','TRUE');
        $("#Date").kendoDatePicker({
            enable: true
        });
    }

    $("#detailForm").kendoWindow({
        width: "650px",
        title: "General Table Value",
        visible: false,
        modal: true,
        actions: [
        "Close"
        ],
    });

});

//Insert
    function insert()
    {
        var detail = getDetailItem('detail');
        var voData = {
            ID: $('#ID').val(),
            Name: $('#Name').val(),
            detail: detail
        };
        var valid = checkForm(voData);
        console.log(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('MasterData/Generaltable/Insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailItem('detail');
     var voData = {
         RecordID: $('#RecordID').val(),
         TimeStamp: $('#RecordTimeStamp').val(),
         ID: $('#ID').val(),
         Name: $('#Name').val(),
         detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('MasterData/Generaltable/Update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    // window.location.replace(current_url());
                    window.location.replace(site_url('MasterData/Generaltable'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ID == "") { valid = 0; msg += "ID is required" + "\r\n"; }
    if (voData.Name == "") { valid = 0; msg += "Name Data is required" + "\r\n"; }
    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

    function checkField(target){
        var msg = '';
        var field = getDetailField(target);
        var val   = getDetailItem(target);
         for (v = 0; v < val.length; v++) {
            if($("#"+field[i]).attr("primary") == "1"){
                if($("#"+field[i]).val() == val[v].RowIndex)
                {
                    msg+="Row Index Sudah Ada"+"\r\n";
                }            
            }
        }
        return msg;
    }
</script>