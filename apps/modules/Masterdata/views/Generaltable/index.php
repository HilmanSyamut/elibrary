<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataGeneralTable_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataGeneralTable_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataGeneralTable_RecordStatus  => array(0,1,'50px','RS',1,'string',1),
    T_MasterDataGeneralTable_ID  => array(1,1,'100px','General Table ID',1,'string',1),
    T_MasterDataGeneralTable_Name  => array(1,1,'100px','General Table Name',0,'string',1),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataGeneralTable,
    'tools' => array(T_MasterDataGeneralTable_RecordID,T_MasterDataGeneralTable_RecordTimestamp,T_MasterDataGeneralTable_RecordStatus,T_MasterDataGeneralTable_ID,T_MasterDataGeneralTable_Name),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'MasterData/Generaltable/insert',
        'read' => 'Webservice/Read/getlist',
        'update' => 'MasterData/Generaltable/update',
        'destroy' => 'MasterData/Generaltable/delete',
        'form' => 'MasterData/Generaltable/form'
    )
);
// generate gridView
echo simpleGridView($attr);
?>