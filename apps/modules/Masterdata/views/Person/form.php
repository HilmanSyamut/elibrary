<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataperson_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
				<input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataperson_RecordID}) ? ${T_MasterDataperson_RecordID} : ""; ?>">
				<input type="hidden" id="RecordTimestamp" value="<?php echo isset(${T_MasterDataperson_RecordTimestamp}) ? ${T_MasterDataperson_RecordTimestamp} : ""; ?>">
				<input type="hidden" id="RecordStatus" value="<?php echo isset(${T_MasterDataperson_RecordStatus}) ? ${T_MasterDataperson_RecordStatus} : ""; ?>">
				<input type="hidden" id="RecordUpdatedOn" value="<?php echo isset(${T_MasterDataperson_UpdatedOn}) ? ${T_MasterDataperson_UpdatedOn} : ""; ?>">
				<input type="hidden" id="RecordUpdatedBy" value="<?php echo isset(${T_MasterDataperson_UpdatedBy}) ? ${T_MasterDataperson_UpdatedBy} : ""; ?>">
				<input type="hidden" id="RecordUpdatedAt" value="<?php echo isset(${T_MasterDataperson_UpdatedAt}) ? ${T_MasterDataperson_UpdatedAt} : ""; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Person Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'PersonName', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_PersonName}) ? ${T_MasterDataperson_PersonName} : "",  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Address</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'Address', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_Address}) ? ${T_MasterDataperson_Address} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Country</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'Country', 
                                    'class' => 'k-textbox', 
                                    'style' => 'width:150px;', 
                                    'value' => isset($meta{T_MasterDataperson_Country}) ? $meta{T_MasterDataperson_Country} : "",  
                                ); 
                                echo form_input($items); ?></div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Person TypeID</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'PersonTypeID', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_PersonTypeID}) ? ${T_MasterDataperson_PersonTypeID} : "",
                                    'style' => 'background-color:#eee;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                                <div class="k-button" id="LookupEventPersonType"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Photo</label>
                            <?php
                                if(isset($meta{T_SystemUserMeta_photo})){
                                    $voImg = ($meta{T_SystemUserMeta_photo} == '') ? "assets/backend/images/no-photo.png" : $meta{T_SystemUserMeta_photo};
                                }else{
                                    $voImg = "assets/backend/images/no-photo.png";
                                }
                                ?>
                            <div class="col-md-6" style="margin-bottom:35px">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview"><img class="img-responsive" src="<?php echo $voImg; ?>" / width="145px"></div>
                                </a>
                                <form id="form-photo" action="<?php echo site_url('en/system/user/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                    <input id="uploadImage" type="file" accept="image/*" name="image" />
                                    <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                </form>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity TypeID</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'IdentityTypeID', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_IdentityTypeID}) ? ${T_MasterDataperson_IdentityTypeID} : "",
                                    'style' => 'background-color:#eee;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                                <div class="k-button" id="LookupEventIdentityID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">State</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'State', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_State}) ? ${T_MasterDataperson_State} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity No</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id' => 'IdentityNo', 
                                    'class' => 'k-textbox',
                                    'value' => isset(${T_MasterDataperson_IdentityNo}) ? ${T_MasterDataperson_IdentityNo} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">City</label>
                            <div class="col-md-9">
                                <?php $items=array(
                                    'id'=> 'City', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_City}) ? ${T_MasterDataperson_City} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Telephone</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'Telephone', 
                                    'class'=>'k-input k-textbox', 
                                    'value' => isset(${T_MasterDataperson_Telephone}) ? ${T_MasterDataperson_Telephone} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Postal Code</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'PostalCode', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_PostalCode}) ? ${T_MasterDataperson_PostalCode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">EPC</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'EPC', 
                                    'class'=>'k-input k-textbox', 
                                    'value' => isset(${T_MasterDataperson_EPC}) ? ${T_MasterDataperson_EPC} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Barcode</label>
                            <div class="col-md-9">
                                <?php $items=array( 
                                    'id'=> 'Barcode', 
                                    'class' => 'k-textbox', 
                                    'value' => isset(${T_MasterDataperson_Barcode}) ? ${T_MasterDataperson_Barcode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>
<script type="text/javascript" src="assets/js/apps.js"></script>
<?php 
if (isset(${T_MasterDataperson_RecordID})) {
    $ID = ${T_MasterDataperson_RecordID};
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "Insert";
}
?>
<?php
//Item Lookup
    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataItem_ItemID, 'title' => 'Item ID', 'width' => '80px'),
        array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '150px'),
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Type', 'width' => '50px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'ItemID', 'column' => T_MasterDataItem_ItemID),
        array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
        array('id' => 'ItemType', 'column' => T_MasterDataGeneralTableValue_Key),
        array('id' => 'EPC', 'column' => T_MasterDataItem_EPC),
        array('id' => 'Barcode', 'column' => T_MasterDataItem_Barcode),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("ItemID", "Data Item", "600px", "Webservice/Read/Getlistitem", $dataItem, $columnItem, T_MasterDataItem);

//Location Lookup
    //field in database data to load
    $dataPersonType = array(
        array('field' => T_MasterDataGeneralTable_ID, 'title' => 'ID', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTable_Name, 'title' => 'Name', 'width' => '100px'),
    );

    $dataIdentity = array(
        array('field' => T_MasterDataGeneralTable_ID, 'title' => 'ID', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTable_Name, 'title' => 'Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnPersonType = array(
        //array('id' => 'PersonTypeID', 'column' => T_MasterDataGeneralTable_ID),
        array('id' => 'PersonTypeID', 'column' => T_MasterDataGeneralTable_Name),
    );

    $columnIdentity = array(
        //array('id' => 'PersonTypeID', 'column' => T_MasterDataGeneralTable_ID),
        array('id' => 'IdentityTypeID', 'column' => T_MasterDataGeneralTable_Name),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("PersonType", "Person Type", "500px", "Webservice/Read/Getlist", $dataPersonType, $columnPersonType,T_MasterDataGeneralTable);
    echo kendoModalLookup("IdentityID", "Identity", "500px", "Webservice/Read/Getlist", $dataIdentity, $columnIdentity,T_MasterDataGeneralTable);

?>
<script>
var ID = "<?php echo $ID; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: ""
    }
];
$(document).ready(function() {

    $("#RowIndex").kendoNumericTextBox();
    $("#RowIndex2").kendoNumericTextBox();
    $("#Qty").kendoNumericTextBox();
    $("#Country").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

});

function insert()
    {
        var voData = {
            PersonName: $('#PersonName').val(),
            PersonID: $('#PersonID').val(),
            PersonTypeID: $('#PersonTypeID').val(),
            IdentityTypeID: $('#IdentityTypeID').val(),
            IdentityNo: $('#IdentityNo').val(),
            Picture: $('#Picture').val(),
            Address: $('#Address').val(),
            Country: $('#Country').val(),
            State: $('#State').val(),
            City: $('#City').val(),
            PostalCode: $('#PostalCode').val(),
            Telephone: $('#Telephone').val(),
            EPC: $('#EPC').val(),
            Barcode: $('#Barcode').val(),
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Person/insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailSubItem('detail');
     var voData = {
         RecordID: ID,
         TimeStamp: $('#TimeStamp').val(),
         DocNo: $('#DocNo').val(),
         DocDate: $('#DocDate').val(),
         DocStatus: $('#DocStatus').val(),
         Remarks: $('#Remarks').val(),
         DoRemoveID : $("#DoRemoveID").val(),
         detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Stockmovement/Beginbalance/Update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(site_url('Stockmovement/Beginbalance/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.PersonName == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.PersonID == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }
    if (voData.Address == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }
    if (voData.City == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}

//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}
</script>