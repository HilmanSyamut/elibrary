<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataperson_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataperson_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataperson_Picture  => array(0,1,'100px','Photo',1,'string',1),
    T_MasterDataperson_PersonName  => array(1,1,'100px','Name',1,'string',1),
    //T_MasterDataperson_PersonTypeID  => array(0,1,'100px','Type ID',1,'string',1),
    //T_MasterDataperson_IdentityTypeID  => array(1,1,'100px','Identity Type',1,'string',1),
    //T_MasterDataperson_IdentityNo  => array(0,1,'100px','Identity No',1,'string',1),
    T_MasterDataperson_Address  => array(1,1,'100px','Address',1,'string',1),
    T_MasterDataperson_Country  => array(0,1,'100px','Country',1,'string',1),
    //T_MasterDataperson_EPC  => array(1,1,'100px','EPC',1,'string',1),
    //T_MasterDataperson_Barcode  => array(1,1,'100px','Barcode',1,'string',1),
    T_MasterDataperson_Telephone  => array(1,1,'100px','Telephone',1,'string',1),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataperson,
    'tools' => array(
        T_MasterDataperson_RecordID,
        T_MasterDataperson_RecordTimestamp,
        T_MasterDataperson_Picture,
        T_MasterDataperson_PersonName,
        //T_MasterDataperson_PersonTypeID,
        //T_MasterDataperson_IdentityTypeID,
        //T_MasterDataperson_IdentityTypeID,
        //T_MasterDataperson_IdentityNo,
        T_MasterDataperson_Address,
        T_MasterDataperson_Country,
        //T_MasterDataperson_EPC,
        //T_MasterDataperson_Barcode,
        T_MasterDataperson_Telephone,
        ),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Person/insert',
        'read' => 'Masterdata/Person/getlist',
        'update' => 'Masterdata/Person/update',
        'destroy' => 'Masterdata/Person/delete',
        'form' => 'Masterdata/Person/form',
        'post' => 'Masterdata/Person/post',
        'unpost' => 'Masterdata/Person/unPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>