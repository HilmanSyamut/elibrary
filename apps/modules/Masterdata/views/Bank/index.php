<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataBank_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataBank_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataBank_AccountNo  => array(1,1,'50px','Account No',1,'string',1),
    T_MasterDataBank_BankName  => array(1,1,'100px','Bank Name',0,'string',0)
);
// Column DropdownList => |Text|URL|a
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'Bank',
    'table' => T_MasterDataBank,
    'tools' => array(T_MasterDataBank_RecordID,T_MasterDataBank_RecordTimestamp,T_MasterDataBank_AccountNo,T_MasterDataBank_BankName),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Bank/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Bank/Update',
        'destroy' => 'Masterdata/Bank/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>