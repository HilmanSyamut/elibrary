<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arif
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Account Modules
 *
 * GeneralLedger Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arif
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Generaltable extends BC_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Generaltable_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Generaltable/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Generaltable_model->getDetail($id);
		$this->modules->render('/Generaltable/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Generaltable_model->getDetail($id);
		$this->modules->render('/Generaltable/formDetail', $data);
	}

	public function getList()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataGeneralTable;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Generaltable_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Generaltable_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function Insert(){
        $this->db->trans_begin();
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index tersebut sudah ada');
			}else{
				$data = array(
					T_MasterDataGeneralTable_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_MasterDataGeneralTable_ID   => $this->input->post("ID"),
					T_MasterDataGeneralTable_Name =>  $this->input->post("Name"),

					'detail' => $this->input->post("detail"),
				);
				$this->Generaltable_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new SalesPayment',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

            	$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
            $this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
        $this->db->trans_begin();
		try{
			
			if(FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_MasterDataGeneralTable_RecordID   => $this->input->post("RecordID"),
					T_MasterDataGeneralTable_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_MasterDataGeneralTable_ID   => $this->input->post("ID"),
					T_MasterDataGeneralTable_Name =>  $this->input->post("Name"),
					'detail' => $this->input->post("detail"),
					);

				$this->Generaltable_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new Generaltable',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

            	$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
            $this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
        $this->db->trans_begin();
		try{
			$this->Generaltable_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete General Table',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);
            $this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
            $this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetItemType($id="")
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$GroupCode = !empty($id) ? $id : $this->input->post('GroupCode');
		$getList = $this->Generaltable_model->DropdownList($GroupCode);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
	
	public function GetItemBiz($id="")
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$GroupCode = !empty($id) ? $id : $this->input->post('GroupCode');
		$getList = $this->Generaltable_model->BizDropdownList($GroupCode);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file GeneralLedger.php */
/* Location: ./app/modules/Account/controllers/GeneralLedger.php */
