<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Item Model
 *
 * @package     App
 * @subpackage  Modules
 * @category    Module Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Person_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataperson_".$col);
                $this->db->where($col,$val);
            }
        }
        $this->db->select(T_MasterDataperson_RecordID." AS 'RecordID',".T_MasterDataperson_RecordTimestamp." AS 'RecordTimestamp',".T_MasterDataperson_RecordStatus." AS 'RecordStatus',".T_MasterDataperson_PersonID." AS 'PersonID',".T_MasterDataperson_PersonName." AS 'PersonName',".T_MasterDataperson_UOMID." AS 'UOMID',".T_MasterDataperson_AutoIDType." AS 'AutoIDType',".T_MasterDataperson_EPC." AS 'EPC',".T_MasterDataperson_Barcode." AS 'Barcode'");
        return $this->db->get($table);
        //$this->db->select(T_MasterDataItem_RecordID." AS 'RecordID',
        //".T_MasterDataItem_RecordTimestamp." AS 'RecordTimestamp',
        //".T_MasterDataItem_RecordStatus." AS 'RecordStatus',
        //".T_MasterDataItem_ItemID." AS 'ItemID',
        //".T_MasterDataItem_ItemName." AS 'ItemName',
        //".T_MasterDataItem_UOMID." AS 'UOMID',
        //".T_MasterDataItem_AutoIDType." AS 'AutoIDType',
        //".T_MasterDataGeneralTableValue_key." AS 'Key',
        //".T_MasterDataItem_EPC." AS 'EPC',
        //".T_MasterDataItem_Barcode." AS 'Barcode'");
        //$this->db->from('T_MasterDataItem');
        //$this->db->join('T_MasterDataGeneralTableValue', 'T_MasterDataGeneralTableValue.T_MasterDataGeneralTableValue_RecordId = T_MasterDataItem.T_MasterDataItem_AutoIDType');
        //return $this->db->get($table);
    }

    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataItem_".$col);
                $this->db->where($col,$val);
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getDetail($id){
        $this->db->select('*');
        $this->db->from(T_MasterDataperson);
        $this->db->where(T_MasterDataperson_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($data[T_MasterDataperson_RecordID]);
        }
        return $data;
    }
    
    public function Insert($data){
    $this->db->trans_begin();
        unset($data[0]);
        $this->db->insert(T_MasterDataperson, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Update($data){
    $this->db->trans_begin();
        $this->db->where(T_MasterDataItem_RecordID,$data[T_MasterDataItem_RecordID]);
        $this->db->update(T_MasterDataItem, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($data){
    $this->db->trans_begin();
        $this->db->where(T_MasterDataItem_RecordID,$data);
        $this->db->delete(T_MasterDataItem);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }


}
