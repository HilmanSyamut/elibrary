<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Muhammad Arif
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Account Modules
 *
 * GeneralLedger Model
 *
 * @package     App
 * @subpackage  Modules
 * @category    Module Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Muhammad Arif
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Generaltable_model extends CI_Model
{    

    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataGeneralTable_".$col);
                $this->db->where($col,$val);
            }
        }
        $this->db->select(T_MasterDataGeneralTable_RecordID." AS 'RecordID',".T_MasterDataGeneralTable_RecordTimestamp." AS 'RecordTimestamp',".T_MasterDataGeneralTable_RecordStatus." AS 'RecordStatus',".T_MasterDataGeneralTable_ID." AS 'GeneraltableID.',".T_MasterDataGeneralTable_Name." AS 'GeneraltableName'");
        return $this->db->get($table);
    }

    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataGeneralTable_".$col);
                $this->db->where($col,$val);
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    public function getDetail($id){
        $this->db->where(T_MasterDataGeneralTable_RecordID,$id);
        $query = $this->db->get(T_MasterDataGeneralTable);
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($id);
        }
        return $data;
    }

    public function getDetailItem($id)
    {
        $this->db->where(T_MasterDataGeneralTableValue_PRI,$id);
        $this->db->order_by(T_MasterDataGeneralTableValue_RecordID);
        $query = $this->db->get(T_MasterDataGeneralTableValue);
        $data = $query->result("array");
        //echo $ci->db->last_query();die;
        return $data;
    }


    public function Insert($data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        unset($data['detail']);
        $this->db->insert(T_MasterDataGeneralTable, $data);
        $parentID = $this->db->insert_id();
        foreach($detail as $item){
            $val = array(
				T_MasterDataGeneralTableValue_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_MasterDataGeneralTableValue_PRI => $parentID,
				T_MasterDataGeneralTableValue_Key => $item["Key"],
				T_MasterDataGeneralTableValue_Description => $item["Description"],

            );
            $this->db->insert(T_MasterDataGeneralTableValue,$val);
        }
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            //UpdateDocNo($data[T_SalesPaymentHeader_DocTypeID]);
            $this->db->trans_commit();
        }
    }

    public function Update($data){
        $this->db->trans_begin();
        $detail = $data["detail"];
        unset($data["detail"]);
        $this->db->where(T_MasterDataGeneralTable_RecordID, $data[T_MasterDataGeneralTable_RecordID]);
        $this->db->update(T_MasterDataGeneralTable,$data);
        $this->db->delete(T_MasterDataGeneralTableValue, array(T_MasterDataGeneralTableValue_PRI => $data[T_MasterDataGeneralTable_RecordID]));
        foreach($detail as $item){
            $val = array(
                T_MasterDataGeneralTableValue_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_MasterDataGeneralTableValue_PRI => $data[T_MasterDataGeneralTable_RecordID],
				T_MasterDataGeneralTableValue_Key => $item["Key"],
				T_MasterDataGeneralTableValue_Description => $item["Description"],

            );
            $this->db->insert(T_MasterDataGeneralTableValue,$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_MasterDataGeneralTable, array(T_MasterDataGeneralTable_RecordID => $id));
        $this->db->delete(T_MasterDataGeneralTableValue, array(T_MasterDataGeneralTableValue_PRI => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
    public function DropdownList($GroupCode){
        $this->db->select('i.t8991r001, i.t8991f002, i.t8991f003');
        $this->db->from('t8991 as i');
        $this->db->where('i.t8991f001', $GroupCode);
        //$this->db->where('i.t8991f003', 1);
        return $this->db->get();
        //echo $this->db->last_query();die;
    }
    public function BizDropdownList($GroupCode){
        $this->db->select('i.t8991r001, i.t8991f001, i.t8991f002');
        $this->db->from('t8991 as i');
        $this->db->where('i.t8991f001', $GroupCode);
        //$this->db->where('i.t8991f003', 1);
        return $this->db->get();
        //echo $this->db->last_query();die;
    }
}
