<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model
{
	var $view = 'master_inventory';
    var $table = 'inventory';

    public function get_data()
    {
    	$query = $this->db->get($this->view);
        $result = $query->result();
    	return $result;
    }

    public function get_detail($id)
    {
    	$res = array();
    	$this->db->where('id',$id);
    	$query = $this->db->get($this->view);
    	if($query->num_rows() > 0):
    		$res = $query->first_row();
    	endif;
    	return $res;
    }

    public function insert($data)
    {
        $data['created_date'] = now();
        $data['created_user'] = $this->ezrbac->getCurrentUserID();
        $this->db->insert($this->table,$data);
    }

    public function update($data)
    {
        $items = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'address' => $data['address'],

        );
        $this->db->where('user_id',$this->ezrbac->getCurrentUserID())
                 ->update('user_meta',$items);
    }

    public function remove($data)
    {
        $this->db->where('id',$data['data_id'])
                 ->delete($this->table);
    }
}