<?php
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Navigation Model
 *
 * @package	    Site
 * @subpackage	Model
 * @category	Navigation
 * 
 * @version     1.1 Build 02.08.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Navigation_Model extends CI_Model
{

    public function __construct(){
        parent::__construct();
	}

    public function getDataAccess($userID,$roleID)
    {
        $this->db->where(T_SystemUser_RecordID,$roleID);
        $queryU = $this->db->get(T_SystemUser);
        $accessUser = $queryU->first_row()->{T_SystemUser_accessMenu};
        $accessUser = unserialize($accessUser);

        $this->db->where(T_SystemUserRole_RecordID,$roleID);
        $query = $this->db->get(T_SystemUserRole);
        $accessRole = $query->first_row()->{T_SystemUserRole_default_access};
        $accessRole = unserialize($accessRole);

        $access = array_merge($accessUser, $accessRole);
        $access = array_unique($access);
        $access = serialize($access);

        return $access;
    }

    public function lookup($id, $database, $filed, $value)
    {
        $this->db->select($value);
        $query = $this->db->get_where($database, array($filed => $id));
        if($query->num_rows() > 0):
            $row = $query->result();
            if(!empty($row[0])):
                $data = $row[0]->$value;
                return $data;
            else:
                return false;
            endif;
        endif;
        return false;
    }

	public function get_data()
    {
        $ret = array();
        $this->db->where('t1000f014',1);
        $query = $this->db->get('t1000');
        if($query->num_rows() > 0):
            return $query->result();
        endif;
        return $ret;
    }

    public function nav_backend()
    {
        $ret = array();
        $this->db->where('t1000f014',1)
                 ->where('t1000f015', 2)
                 ->where('t1000f012', 0)
                 ->order_by('t1000f013','acs');
        $query = $this->db->get('t1000');
        if($query->num_rows() > 0):
            $result = $query->result();
            return $result;
        endif;
        return $ret;
    }

    public function sub_nav_backend($id)
    {
        $ret = array();
        $this->db->where('t1000f014',1)
                 ->where('t1000f015', 2)
                 ->where('t1000f012', $id)
                 ->order_by('t1000f013','acs');
        $query = $this->db->get('t1000');
        if($query->num_rows() > 0):
            $result = $query->result();
            return $result;
        endif;
        return $ret;
    }
    
    private function sub($id,$uri)
    {
        $ret = array();
        $this->db->where('module_id',$id);
        $query = $this->db->get('menu');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $title = unserialize($item->title);
                $url = site_url($uri."/".$item->alias);
                $data[$item->id] =array(
                    'alias' => $item->alias,
                    'title' => $title[$this->lang->lang()],
                    'icon' => $item->icon,
                    'url' => $url            
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
    // get menu data
    public function get_menu()
    {
		$this->db->order_by('ordering','asc');
		$this->db->where('publish',1);
		$result = $this->db->get('site_navigation');
		return $result;
	}
    // get current user data
    public function user_data($id)
    {
        $ret = array();
        $this->db->where('t0040r001',$id);
		$query= $this->db->get(SYSTEM_USERS);
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $data = array(
                    'id' => $item->t0040r001,
                    'email' => $item->t0040f002,
                    'password' => $item->t0040f003,
                    'salt' => $item->t0040f004,
                    'user_role_id' => $item->t0040f007,
                    'last_login' => $item->t0040f008,
                    'last_login_ip' => $item->t0040f009,
                    'verification_status' => $item->t0040f013,
                    'status' => $item->t0040f014,
                    'meta' => (!empty($item->t0040f015) ? $this->pegawai($item->t0040f015) : $this->meta($item->t0040r001) )
                );
            endforeach;
            return $data;
        endif;
		return $ret;
	}
    // get meta data user
    private function meta($id)
    {
        $ret = array();
        $this->db->where('t0070r001',$id);
        $query = $this->db->get('t0070');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $photo = (!empty($item->t0070f005) ? "assets/uploads/thumbs/".$item->t0070f005." " : "assets/admin/layout/img/no-photo.png" );
                $data = array(
                    'first_name' => $item->t0070f002,
                    'last_name' => $item->t0070f003,
                    'full_name' => $item->t0070f002." ".$item->t0070f003,
                    'phone' => $item->t0070f004,
                    'photo' => "<img src='".$photo."' class='img-circle hide1' alt='' />",
                    'address' => $item->t0070f006,
                    'provinsi' => $item->t0070f007,
                    'kecamatan' => $item->t0070f008,
                    'kabkot' => $item->t0070f009,
                    'facebook' => $item->t0070f010,
                    'twitter' => $item->t0070f011,
                    'website' => $item->t0070f012,
                    'office' => $item->t0070f013
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
    // get current breadcrumb
    public function breadcrumb()
    {
        $mod = !empty($this->uri->segment(2)) ? $this->uri->segment(2) : "";
        $controller = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : "";
        $this->db->where('t0010f002',$mod);
        $query = $this->db->get('t0010');

        if($query->num_rows() > 0):
            $module = $query->first_row();
            $this->db->where('t0020f002',$module->t0010r001);
            $this->db->where('t0020f003',$controller);
            $child = $this->db->get('t0020');
            if($child->num_rows() > 0){
                $item = $child->first_row();
                $title = unserialize($item->t0020f004);
                if(isset($item->title)):
                    $description = unserialize($item->t0010f003);
                endif;
                $data = array(
                    'title' => $title[$this->lang->lang()],
                    'description' => isset($item->t0010f003) ? $description[$this->lang->lang()] : $controller
                );
                return $data;
            }
        endif;

        $this->db->where('t0020f003',$mod);
        $query = $this->db->get('t0020');
        if($query->num_rows() > 0):
            $item = $query->first_row();
            $title = unserialize($item->t0020f004);
            $data = array(
                'title' => $title[$this->lang->lang()]
            );
        return $data;
        endif;
        $data = array('title'=> '','description' => '');
        return $data;
    }
    
    public function get_module()
    {
        $ret = array();
        $alias = $this->uri->segment(2);
        $this->db->where('t0010f010',1)
                 ->where('t0010f002',$alias);
        $query = $this->db->get('t0010');
        if($query->num_rows() > 0):
                $item = $query->first_row();
                $title = unserialize($item->t0010f003);
                $data =array(
                    'title' => $title[$this->lang->lang()],
                    'icon' => $item->t0010f005,
                    'quick_menu' => $this->quick_menu($item->t0010r001,$item->t0010f002)            
                );
            return $data;
        print_out($data);
        endif;
        return $ret;
    }
    
    private function quick_menu($id,$uri)
    {
        $ret = array();
        $this->db->where('module_id',$id);
        $query = $this->db->get('menu');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $title = unserialize($item->title);
                $url = site_url($uri."/".$item->alias);
                $data[$item->id] =array(
                    'title' => $title[$this->lang->lang()],
                    'icon' => $item->icon,
                    'url' => $url            
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
}

/* End of file navigation_model.php */
/* Location: ./site/model/navigation/navigation_model.php */