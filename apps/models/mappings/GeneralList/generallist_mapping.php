<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('T_GENERALLIST')   OR define('T_GENERALLIST', 't8000');
defined('T_GENERALLIST_RecordID')   OR define('T_GENERALLIST_RecordID', 't8000r001');
defined('T_GENERALLIST_RecordTimeStamp')   OR define('T_GENERALLIST_RecordTimeStamp', 't8000r002');
defined('T_GENERALLIST_RecordStatus')   OR define('T_GENERALLIST_RecordStatus', 't8000r003');
defined('T_GENERALLIST_UpdateAt')   OR define('T_GENERALLIST_UpdateAt', 't8000r004');
defined('T_GENERALLIST_UpdateBy')   OR define('T_GENERALLIST_UpdateBy', 't8000r005');
defined('T_GENERALLIST_UpdateOn')   OR define('T_GENERALLIST_UpdateOn', 't8000r006');
defined('T_GENERALLIST_ID')   OR define('T_GENERALLIST_ID', 't8000f001');
defined('T_GENERALLIST_Name')   OR define('T_GENERALLIST_Name', 't8000f002');
defined('T_GENERALLIST_GROUPNAME')   OR define('T_GENERALLIST_GROUPNAME', 't8000f003');
defined('T_GENERALLIST_GROUPCODE')   OR define('T_GENERALLIST_GROUPCODE', 't8000f004');
defined('T_GENERALLIST_STATUS')   OR define('T_GENERALLIST_STATUS', 't8000f005');

