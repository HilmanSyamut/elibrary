<?php
class Widget_Model extends CI_Model
{
	public function __construct()
	{
        parent::__construct();
	}

    public function social_account()
	{
        $ret = array();
		$query = $this->db->get('social_accounts');
        if($query->num_rows() > 0 ):
            $result = $query->result('array');
            return $result;
        endif;
        return $ret;
	}
}
?>
