<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Telerik Lookup Helper
 *
 * @param   array
 * @access  public
 * @return  object element
 */
/**
 * Get nomor pendaftaran
 *
 *
 * @access  public
 * @return  integer
 */
if ( ! function_exists('getItemType'))
{
    function getItemType($id)
    {
        if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
        $ci->db->where(T_MasterDataItem_ItemID,$id);
        $query = $ci->db->get(T_MasterDataItem);
        $data = $query->first_row('array');
        $result = $data[T_MasterDataItem_ItemType];
        return $result;
    }
}

//Lookup Default
if ( ! function_exists('kendoModalLookup'))
{
    function kendoModalLookup($id,$title, $size, $url, $data, $column, $table,$addDetail=null,$customfilter=null)
    {
        $DetailTable = "";
        if(!empty($addDetail))
        {
            $constants = get_defined_constants();
            $input = $addDetail["table"]; 
            $result = array_filter($constants, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });

            $DetailTable = "
                    var DetailTable = {";
                            foreach ($result as $key => $value) {
                                $key = ($input==T_MasterDataFormula) ? $key."2" : $key;
                                $name = explode("_", $key);
                                if(!empty($name[2])){
                                    $DetailTable .= $name[2].":'".$value."',";
                                }
                            }
                            $DetailTable = rtrim($DetailTable,',');
                            $DetailTable .= "
                        };
            ";
        }


        $form       = '<div class="k-edit-form-container" id="kw'.$id.'Lookup">
                            <div id="kg'.$id.'Lookup"></div>
                        </div>';
        $kendoModal = '<script type="text/javascript">
                            '.$DetailTable.'
                            $(document).ready(function() {
                                $("#kw'.$id.'Lookup").kendoWindow({
                                    width: "'.$size.'",
                                    title: "'.$title.'",
                                    modal:true,
                                    visible: false
                                    });';
        $filter = "";
        if(!empty($customfilter)){
            $filter = "customfilter: {";
            foreach($customfilter as $field => $value){
                $filter .= '"'.$field.'":"'.$value.'",';
            }
            $filter .= "}";
        }
        $grid       = '$("#kg'.$id.'Lookup").kendoGrid({
                        dataSource: {
                            type: "json",
                            transport: {
                                read: {
                                    type:"GET",
                                    data: { 
                                        table: "'.$table.'",
                                        '.$filter.'                                        
                                    },
                                    url: site_url("'.$url.'"),
                                    dataType: "json"
                                },
                            },
                            sync: function(e) {
                                $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                                $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            },
                            schema: {
                                data: function(datas){
                                    return datas.data;
                                },
                                total: function(datas){
                                    return datas.count;
                                },
                                model: {
                                    id: "RecordID",
                                }
                            },                        
                            pageSize: 10,
                            serverPaging: true,
                            serverFiltering: true,
                            serverSorting: true
                        },
                            autoBind: false,
                            sortable: true,
                            pageable: true,
                            groupable: true,
                            resizable: true,
                            selectable: true,
                            scrollable: true,
                            reorderable:true,
                            filterable: {
                                mode: "row",
                            },
                            pageable: {
                                refresh: true,
                                pageSizes: true,
                                buttonCount: 5
                            },
                                height: "500px",
                                width: "100%",
                                columns: [';
                                foreach ($data as $key => $row) {
                                    $grid .='{';
                                        $grid .='"field":"'.$row['field'].'",'; 
                                        $grid .='"title":"'.$row['title'].'",'; 
                                        $grid .='"width":"'.$row['width'].'",'; 
                                    $grid .='},';
                                }
        
        $grid       .=                  '],
                            });
                        });';

        $event          =  '$("#LookupEvent'.$id.'").click(function(){
                            $("#itemType").val("");
                            $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                            $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            $("#kw'.$id.'Lookup").data("kendoWindow").center().open();
                            });
                            $("#kg'.$id.'Lookup").delegate("tbody>tr", "dblclick", LookupDBLClick'.$id.'Lookup);';
        $doubleclick   =   'function LookupDBLClick'.$id.'Lookup(e){
                                var voGrid = $("#kg'.$id.'Lookup").data("kendoGrid");
                                var voRow = voGrid.dataItem(voGrid.select());';
        $dataColumn =          'var field = [';
                                foreach ($column as $key => $row) {
        $dataColumn .=             "{id:'".$row['id']."', column:'".$row['column']."'},";
                                }

        $dataColumn = rtrim($dataColumn,',');
        $doubleclick .=         $dataColumn.'];';
        $doubleclick .= '
                                for (i = 0; i < field.length; i++) {
                                    var data = voRow[field[i].column];
                                    $("#"+field[i].id).val(data);
                                    if($("#"+field[i].id).data("kendoDropDownList")){
                                        $("#"+field[i].id).data("kendoDropDownList").value(data);
                                    }
                                    if($("#"+field[i].id).data("kendoNumericTextBox")){
                                        $("#"+field[i].id).data("kendoNumericTextBox").value(data);
                                    }
                                    if($("#"+field[i].id).data("kendoDatePicker")){
                                        $("#"+field[i].id).data("kendoDatePicker").value(data); 
                                    }
                                }
                        ';

        $doubleclick .=         'var voWindowLookup = $("#kw'.$id.'Lookup").data("kendoWindow");
                                voWindowLookup.close(); customTriger();';

        if(!empty($addDetail)){
        $voData = '';
                                foreach ($addDetail["filter"] as $key => $row) {
                                    $voData .= $row["name"].':voRow.'.$row["value"].',';
                                }
                                $voData = rtrim($voData,',');
        $doubleclick .=         'var voData = {'.$voData.'};';
        $doubleclick .= '
                                    $.ajax({
                                        type: "POST",
                                        data: voData,
                                        url:  site_url("'.$addDetail["url"].'"),
                                        success: function (result) {
                                            addDetailRef(result.data.Detail,"'.$addDetail["id"].'","'.$addDetail['table'].'");
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            new PNotify({ title: "Warning", text: jQuery.parseJSON(jqXHR.responseText), type: "Warning", shadow: true });
                                        }
                                    });
                                ';
            }
        $doubleclick .=    '};';

        $end          =   'function customTriger(){}</script>';
        $return = $form.$kendoModal.$grid.$event.$doubleclick.$end;
        return $return;
    }
}

//Stock Balance Lookup Custom
if ( ! function_exists('kendoModalLookupStockBalance'))
{
    function kendoModalLookupStockBalance($id,$title, $size, $url, $data, $column, $table)
    {
        $form       = '<div class="k-edit-form-container" id="kw'.$id.'Lookup">
                            <div id="kg'.$id.'Lookup"></div>
                        </div>';
        $kendoModal = '<script type="text/javascript">
                            $(document).ready(function() {
                                $("#kw'.$id.'Lookup").kendoWindow({
                                    width: "'.$size.'",
                                    title: "'.$title.'",
                                    modal:true,
                                    visible: false
                                    });';
        $grid       = '$("#kg'.$id.'Lookup").kendoGrid({
                        dataSource: {
                            type: "json",
                            transport: {
                                read: {
                                    type:"GET",
                                    data: { table: "'.$table.'"},
                                    url: site_url("'.$url.'"),
                                    dataType: "json"
                                },
                            },
                            sync: function(e) {
                                $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                                $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            },
                            schema: {
                                data: function(datas){
                                    return datas.data;
                                },
                                total: function(datas){
                                    return datas.count;
                                },
                                model: {
                                    id: "RecordID",
                                }
                            },
                            pageSize: 10,
                            serverPaging: true,
                            serverFiltering: true,
                            serverSorting: true
                        },
                            autoBind: false,
                            sortable: true,
                            pageable: true,
                            groupable: true,
                            resizable: true,
                            selectable: true,
                            scrollable: true,
                            reorderable:true,
                            filterable: {
                                mode: "row",
                            },
                            pageable: {
                                refresh: true,
                                pageSizes: true,
                                buttonCount: 5
                            },
                                height: "500px",
                                width: "100%",
                                columns: [';
                                foreach ($data as $key => $row) {
                                    $grid .='{';
                                        $grid .='"field":"'.$row['field'].'",'; 
                                        $grid .='"title":"'.$row['title'].'",'; 
                                        $grid .='"width":"'.$row['width'].'",'; 
                                    $grid .='},';
                                }
        $grid       .=      '],     
                            });
                        });';

        $event          =  '$("#LookupEvent'.$id.'").click(function(){
                            $("#kw'.$id.'Lookup").data("kendoWindow").center().open();
                            $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                            $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            $("#kg'.$id.'Lookup").delegate("tbody>tr", "dblclick", LookupDBLClick'.$id.'Lookup);
                            });';
        $doubleclick   =   'function checkItem(item){
                                if (item == "NS"){
                                    $("#Qty").data("kendoNumericTextBox").enable();
                                    $("#Qty").data("kendoNumericTextBox").value(0);
                                }else if (item == "S1") {
                                    $("#Qty").data("kendoNumericTextBox").enable(false);
                                    $("#Qty").data("kendoNumericTextBox").value(1);
                                }else if (item == "SN") {
                                    $("#Qty").data("kendoNumericTextBox").enable();
                                    $("#Qty").data("kendoNumericTextBox").value(0);
                                }else if (item == "SS") {
                                    $("#Qty").data("kendoNumericTextBox").enable(false);
                                    $("#Qty").data("kendoNumericTextBox").value(1);
                                }
                            }
                            function LookupDBLClick'.$id.'Lookup(e){
                                var voGrid = $("#kg'.$id.'Lookup").data("kendoGrid");
                                var voRow = voGrid.dataItem(voGrid.select());';
                                foreach ($column as $key => $row) {
        $doubleclick .=         'var vs'.$row['id'].' = voRow.'.$row['column'].';';
                                }
        $doubleclick .=         'var voWindowLookup = $("#kw'.$id.'Lookup").data("kendoWindow");
                                voWindowLookup.close();';

                                foreach ($column as $key => $row) {
        $doubleclick .=         '$("#'.$row['id'].'").val(vs'.$row['id'].');';
                                }
        $doubleclick .=     '
                            (vsItemType) ? checkItem(vsItemType): "";
                            };';                  
        $end          =   '</script>';
        $return = $form.$kendoModal.$grid.$event.$doubleclick.$end;
        return $return;
    }
}

//Lookup Custom When Field on Blur
if ( ! function_exists('kendoModalLookupBlur')) {
    
    function kendoModalLookupBlur($id,$title, $size, $url, $data, $column, $blurName, $rowblur, $urlblur, $value, $table)
    {
        $form       = '<div class="k-edit-form-container" id="kw'.$id.'Lookup">
                            <div id="kg'.$id.'Lookup"></div>
                        </div>';

        $blur       = '<script type="text/javascript">
                            function '.$blurName.'(){
                                var voData = {';
                                foreach ($rowblur as $key => $row) {
        $blur       .=          ''.$row['id'].': $("#'.$row['id'].'").val(),';
                                 }
        $blur       .=          '};';                
        $blur       .=          '$.ajax({type: "POST",
                                        data: voData,
                                        url: site_url("'.$urlblur.'"),
                                         success: function (data) {
                                            if (data.errorcode > 0) {';                 
        $blur       .=                   '$("#'.$row['throw'].'").data("kendoNumericTextBox").value("0");';
        $blur       .=                   '} else {';
                                foreach ($rowblur as $key => $row) {
        $blur       .=                     '$("#'.$row['throw'].'").data("kendoNumericTextBox").value(data.data[0].'.$value.');';
                                        }
        $blur       .=                  '}
                                       },
                                       error: function (jqXHR, textStatus, errorThrown) {
                                           alert(jQuery.parseJSON(jqXHR.responseText));
                                       }
                                    });
                                }
                                </script>';
        $kendoModal = '<script type="text/javascript">
                            $(document).ready(function() {
                                $("#kw'.$id.'Lookup").kendoWindow({
                                    width: "'.$size.'",
                                    title: "'.$title.'",
                                    modal:true,
                                    visible: false
                                    });';
        $grid       = '$("#kg'.$id.'Lookup").kendoGrid({
                        dataSource: {
                            type: "json",
                            transport: {
                                read: {
                                    type:"GET",
                                    data: { table: "'.$table.'"},
                                    url: site_url("'.$url.'"),
                                    dataType: "json"
                                },
                            },
                            sync: function(e) {
                                $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                                $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            },
                            schema: {
                                data: function(datas){
                                    return datas.data;
                                },
                                total: function(datas){
                                    return datas.count;
                                },
                                model: {
                                    id: "RecordID",
                                }
                            },
                            pageSize: 10,
                            serverPaging: true,
                            serverFiltering: true,
                            serverSorting: true
                            },
                            autoBind: false,
                            sortable: true,
                            pageable: true,
                            groupable: true,
                            resizable: true,
                            selectable: true,
                            scrollable: true,
                            reorderable:true,
                            filterable: {
                                mode: "row",
                            },
                            pageable: {
                                refresh: true,
                                pageSizes: true,
                                buttonCount: 5
                            },
                                height: "500px",
                                width: "100%",
                                columns: [';
                                foreach ($data as $key => $row) {
                                    $grid .='{';
                                        $grid .='"field":"'.$row['field'].'",'; 
                                        $grid .='"title":"'.$row['title'].'",'; 
                                        $grid .='"width":"'.$row['width'].'",'; 
                                    $grid .='},';
                                }
        $grid       .=          '],
                            });
                        });';

        $event          =  '$("#LookupEvent'.$id.'").click(function(){
                            $("#kg'.$id.'Lookup").data("kendoGrid").dataSource.read();
                            $("#kg'.$id.'Lookup").data("kendoGrid").refresh();
                            $("#kw'.$id.'Lookup").data("kendoWindow").center().open();
                            $("#kg'.$id.'Lookup").delegate("tbody>tr", "dblclick", LookupDBLClick'.$id.'Lookup);
                            });';
        $doubleclick   =   'function LookupDBLClick'.$id.'Lookup(e){
                                var voGrid = $("#kg'.$id.'Lookup").data("kendoGrid");
                                var voRow = voGrid.dataItem(voGrid.select());';
                                foreach ($column as $key => $row) {
        $doubleclick .=         'var vs'.$row['id'].' = voRow.'.$row['column'].';';
                                }
        $doubleclick .=         'var voWindowLookup = $("#kw'.$id.'Lookup").data("kendoWindow");
                                voWindowLookup.close();';

                                foreach ($column as $key => $row) {
        $doubleclick .=         '$("#'.$row['id'].'").val(vs'.$row['id'].');';
                                }
        $doubleclick .=     ''.$blurName.'();};';                  
        $end          =   '</script>';
        $return = $form.$blur.$kendoModal.$grid.$event.$doubleclick.$end;
        return $return;
    }
}

if ( ! function_exists('ListGeneralList'))
{
    function ListGeneralList($id, $data,$property=null)
    {
        $list =   '<script type="text/javascript">
                        $(document).ready(function() {
                            $("#'.$id.'").kendoDropDownList({
                                dataTextField: "t8000f002",
                                dataValueField: "t8000f001",
                                filter: "contains",
                                optionLabel: "Select",';
                                if(!empty($property)){
                                    foreach ($property as $key => $row) {
        $list .=                        $row["property"].':'.$row["value"].',';
                                    }                     
                                }          
        $list .=                'dataSource: {
                                    transport: {
                                        read: {
                                            url: site_url("Masterdata/GeneralList/GetItemType"),
                                            dataType: "json",
                                            data: {GroupCode: "'.$data.'"},
                                            type: "POST"
                                        }
                                    },
                                    schema: {
                                        data: function(data){
                                            return data.data;
                                        }
                                    }
                                }
                            });
                        });
                    </script>';
        return $list;
    }
}

if ( ! function_exists('kendoModalGrid'))
{
    function kendoModalGrid($id, $size, $title, $id_delete, $url)
    {
        $kendoModal = '<script type="text/javascript">
                            $(document).ready(function() {
                                $("#'.$id.'").kendoWindow({
                                    width: "'.$size.'",
                                    title: "'.$title.'",
                                    modal:true,
                                    visible: false
                                    });';

        $close      = '$("#no'.$id.'").click(function(){
                            $("#'.$id.'").closest("[data-role=window]").data("kendoWindow").close();
                       });';

        $act        = '$("#yes'.$id.'").click(function(){
                            var voData = {
                                 '.$id_delete.': $("#id_Delete").val()
                             };
                             
                            $.ajax({
                                type: "POST",
                                data: voData,
                                url:  site_url("'.$url.'"),
                                   success: function (result) {
                                        $("#ActDelete").closest("[data-role=window]").data("kendoWindow").close();
                                        $("#grid").data("kendoGrid").dataSource.read();
                                        $("#grid").data("kendoGrid").refresh();
                                },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                    alert(jQuery.parseJSON(jqXHR.responseText));
                                }
                            });
                        });
                    });';

        $end        = '</script>';
        $return = $kendoModal.$close.$act.$end;
        return $return;
    }
}
/* End of file lookup_helper.php */
/* Location: ./system/helpers/lookup_helper.php */
