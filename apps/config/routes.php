<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = "Dashboard/alert";
$route['404_override'] = '';
$route['^(en|id|ar)/blog'] = 'site/blog';
$route['^(en|id|ar)/logout'] = 'welcomes/index/access/logout';
$route['^(en|id|ar)/(.+)$'] = "$2";
$route['^(en|id|ar)$'] = $route['default_controller'];
$route['translate_uri_dashes'] = FALSE;
