<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
defined('SYSTEM')        	   OR define('SYSTEM', 't0000');
defined('SYSTEM_APP_NAME')     OR define('SYSTEM_APP_NAME', 't0000f002');
defined('SYSTEM_COMPANY')      OR define('SYSTEM_COMPANY', 't0000f003');
defined('SYSTEM_SN')           OR define('SYSTEM_SN', 't0000f004');
defined('SYSTEM_REGISTERED')   OR define('SYSTEM_REGISTERED', 't0000f005');
defined('SYSTEM_USERS')        OR define('SYSTEM_USERS', 't0040');
defined('USER_META')           OR define('USER_META', 't0070');
defined('USER_ROLE')           OR define('USER_ROLE', 't0080');
defined('USER_ACCESS_MAP')     OR define('USER_ACCESS_MAP', 't0050');

defined('FORMATDATEUI')     OR define('FORMATDATEUI', 'dd-MM-yyyy');
defined('FORMATDATETIMEUI')     OR define('FORMATDATETIMEUI', 'dd-MM-yyyy hh:mm tt');
defined('FORMATDATE')     OR define('FORMATDATE', 'd-m-Y');
defined('FORMATDATETIME')     OR define('FORMATDATETIME', 'd-m-Y G:I:S');
defined('DBFORMATDATE')     OR define('DBFORMATDATE', 'Y-m-d');
defined('DBFORMATDATETIME')     OR define('DBFORMATDATETIME', 'Y-m-d G:I:S');

defined('LENGTHEPC')     OR define('LENGTHEPC', 20);
defined('LENGTHSN')     OR define('LENGTHSN', 24);
defined('CUTPREFIX')  OR define('CUTPREFIX',4);
defined('CUTPREFIXSN')  OR define('CUTPREFIXSN',20);
defined('EPCPREFIX')  OR define('EPCPREFIX',"EEEE");

// == mapping SystemSystem == //
defined('T_SystemSystem') OR define('T_SystemSystem', 't0000');
defined('T_SystemSystem_RecordID') OR define('T_SystemSystem_RecordID', 't0000r001');
defined('T_SystemSystem_RecordTimestamp') OR define('T_SystemSystem_RecordTimestamp', 't0000r002');
defined('T_SystemSystem_RecordStatus') OR define('T_SystemSystem_RecordStatus', 't0000r003');
defined('T_SystemSystem_RecordUpdatedOn') OR define('T_SystemSystem_RecordUpdatedOn', 't0000r004');
defined('T_SystemSystem_RecordUpdatedBy') OR define('T_SystemSystem_RecordUpdatedBy', 't0000r005');
defined('T_SystemSystem_RecordUpdatedAt') OR define('T_SystemSystem_RecordUpdatedAt', 't0000r006');
defined('T_SystemSystem_id') OR define('T_SystemSystem_id', 't0000f001');
defined('T_SystemSystem_app_name') OR define('T_SystemSystem_app_name', 't0000f002');
defined('T_SystemSystem_company') OR define('T_SystemSystem_company', 't0000f003');
defined('T_SystemSystem_serial_number') OR define('T_SystemSystem_serial_number', 't0000f004');
defined('T_SystemSystem_registered') OR define('T_SystemSystem_registered', 't0000f005');


// == mapping SystemModules == //
defined('T_SystemModules') OR define('T_SystemModules', 't0010');
defined('T_SystemModules_RecordID') OR define('T_SystemModules_RecordID', 't0010r001');
defined('T_SystemModules_RecordTimestamp') OR define('T_SystemModules_RecordTimestamp', 't0010r002');
defined('T_SystemModules_RecordStatus') OR define('T_SystemModules_RecordStatus', 't0010r003');
defined('T_SystemModules_RecordUpdatedOn') OR define('T_SystemModules_RecordUpdatedOn', 't0010r004');
defined('T_SystemModules_RecordUpdatedBy') OR define('T_SystemModules_RecordUpdatedBy', 't0010r005');
defined('T_SystemModules_RecordUpdatedAt') OR define('T_SystemModules_RecordUpdatedAt', 't0010r006');
defined('T_SystemModules_id') OR define('T_SystemModules_id', 't0010f001');
defined('T_SystemModules_alias') OR define('T_SystemModules_alias', 't0010f002');
defined('T_SystemModules_title') OR define('T_SystemModules_title', 't0010f003');
defined('T_SystemModules_description') OR define('T_SystemModules_description', 't0010f004');
defined('T_SystemModules_icon') OR define('T_SystemModules_icon', 't0010f005');
defined('T_SystemModules_access') OR define('T_SystemModules_access', 't0010f006');
defined('T_SystemModules_author') OR define('T_SystemModules_author', 't0010f007');
defined('T_SystemModules_contributors') OR define('T_SystemModules_contributors', 't0010f008');
defined('T_SystemModules_created_date') OR define('T_SystemModules_created_date', 't0010f009');
defined('T_SystemModules_publish') OR define('T_SystemModules_publish', 't0010f010');
defined('T_SystemModules_status') OR define('T_SystemModules_status', 't0010f011');


// == mapping SystemControllers == //
defined('T_SystemControllers') OR define('T_SystemControllers', 't0020');
defined('T_SystemControllers_RecordID') OR define('T_SystemControllers_RecordID', 't0020r001');
defined('T_SystemControllers_RecordTimestamp') OR define('T_SystemControllers_RecordTimestamp', 't0020r002');
defined('T_SystemControllers_RecordStatus') OR define('T_SystemControllers_RecordStatus', 't0020r003');
defined('T_SystemControllers_RecordUpdatedOn') OR define('T_SystemControllers_RecordUpdatedOn', 't0020r004');
defined('T_SystemControllers_RecordUpdatedBy') OR define('T_SystemControllers_RecordUpdatedBy', 't0020r005');
defined('T_SystemControllers_RecordUpdatedAt') OR define('T_SystemControllers_RecordUpdatedAt', 't0020r006');
defined('T_SystemControllers_id') OR define('T_SystemControllers_id', 't0020f001');
defined('T_SystemControllers_module_id') OR define('T_SystemControllers_module_id', 't0020f002');
defined('T_SystemControllers_alias') OR define('T_SystemControllers_alias', 't0020f003');
defined('T_SystemControllers_title') OR define('T_SystemControllers_title', 't0020f004');
defined('T_SystemControllers_description') OR define('T_SystemControllers_description', 't0020f005');
defined('T_SystemControllers_author') OR define('T_SystemControllers_author', 't0020f006');
defined('T_SystemControllers_contributors') OR define('T_SystemControllers_contributors', 't0020f007');
defined('T_SystemControllers_created_date') OR define('T_SystemControllers_created_date', 't0020f008');
defined('T_SystemControllers_status') OR define('T_SystemControllers_status', 't0020f009');


// == mapping SystemMethode == //
defined('T_SystemMethode') OR define('T_SystemMethode', 't0030');
defined('T_SystemMethode_RecordID') OR define('T_SystemMethode_RecordID', 't0030r001');
defined('T_SystemMethode_RecordTimestamp') OR define('T_SystemMethode_RecordTimestamp', 't0030r002');
defined('T_SystemMethode_RecordStatus') OR define('T_SystemMethode_RecordStatus', 't0030r003');
defined('T_SystemMethode_RecordUpdatedOn') OR define('T_SystemMethode_RecordUpdatedOn', 't0030r004');
defined('T_SystemMethode_RecordUpdatedBy') OR define('T_SystemMethode_RecordUpdatedBy', 't0030r005');
defined('T_SystemMethode_RecordUpdatedAt') OR define('T_SystemMethode_RecordUpdatedAt', 't0030r006');
defined('T_SystemMethode_id') OR define('T_SystemMethode_id', 't0030f001');
defined('T_SystemMethode_controller_id') OR define('T_SystemMethode_controller_id', 't0030f002');
defined('T_SystemMethode_alias') OR define('T_SystemMethode_alias', 't0030f003');
defined('T_SystemMethode_title') OR define('T_SystemMethode_title', 't0030f004');
defined('T_SystemMethode_description') OR define('T_SystemMethode_description', 't0030f005');
defined('T_SystemMethode_created_status') OR define('T_SystemMethode_created_status', 't0030f006');
defined('T_SystemMethode_status') OR define('T_SystemMethode_status', 't0030f007');


// == mapping SystemUser == //
defined('T_SystemUser') OR define('T_SystemUser', 't0040');
defined('T_SystemUser_RecordID') OR define('T_SystemUser_RecordID', 't0040r001');
defined('T_SystemUser_RecordTimestamp') OR define('T_SystemUser_RecordTimestamp', 't0040r002');
defined('T_SystemUser_RecordStatus') OR define('T_SystemUser_RecordStatus', 't0040r003');
defined('T_SystemUser_RecordUpdatedOn') OR define('T_SystemUser_RecordUpdatedOn', 't0040r004');
defined('T_SystemUser_RecordUpdatedBy') OR define('T_SystemUser_RecordUpdatedBy', 't0040r005');
defined('T_SystemUser_RecordUpdatedAt') OR define('T_SystemUser_RecordUpdatedAt', 't0040r006');
defined('T_SystemUser_id') OR define('T_SystemUser_id', 't0040f001');
defined('T_SystemUser_email') OR define('T_SystemUser_email', 't0040f002');
defined('T_SystemUser_password') OR define('T_SystemUser_password', 't0040f003');
defined('T_SystemUser_salt') OR define('T_SystemUser_salt', 't0040f004');
defined('T_SystemUser_language') OR define('T_SystemUser_language', 't0040f005');
defined('T_SystemUser_theme') OR define('T_SystemUser_theme', 't0040f006');
defined('T_SystemUser_user_role_id') OR define('T_SystemUser_user_role_id', 't0040f007');
defined('T_SystemUser_last_login') OR define('T_SystemUser_last_login', 't0040f008');
defined('T_SystemUser_last_login_ip') OR define('T_SystemUser_last_login_ip', 't0040f009');
defined('T_SystemUser_reset_request_code') OR define('T_SystemUser_reset_request_code', 't0040f010');
defined('T_SystemUser_reset_request_time') OR define('T_SystemUser_reset_request_time', 't0040f011');
defined('T_SystemUser_reset_request_ip') OR define('T_SystemUser_reset_request_ip', 't0040f012');
defined('T_SystemUser_verification_status') OR define('T_SystemUser_verification_status', 't0040f013');
defined('T_SystemUser_status') OR define('T_SystemUser_status', 't0040f014');
defined('T_SystemUser_pegawai_id') OR define('T_SystemUser_pegawai_id', 't0040f015');
defined('T_SystemUser_online') OR define('T_SystemUser_online', 't0040f016');
defined('T_SystemUser_access') OR define('T_SystemUser_access', 't0040f017');
defined('T_SystemUser_accessMenu') OR define('T_SystemUser_accessMenu', 't0040f018');


// == mapping SystemUserAccessMap == //
defined('T_SystemUserAccessMap') OR define('T_SystemUserAccessMap', 't0050');
defined('T_SystemUserAccessMap_RecordID') OR define('T_SystemUserAccessMap_RecordID', 't0050r001');
defined('T_SystemUserAccessMap_RecordTimestamp') OR define('T_SystemUserAccessMap_RecordTimestamp', 't0050r002');
defined('T_SystemUserAccessMap_RecordStatus') OR define('T_SystemUserAccessMap_RecordStatus', 't0050r003');
defined('T_SystemUserAccessMap_RecordUpdatedOn') OR define('T_SystemUserAccessMap_RecordUpdatedOn', 't0050r004');
defined('T_SystemUserAccessMap_RecordUpdatedBy') OR define('T_SystemUserAccessMap_RecordUpdatedBy', 't0050r005');
defined('T_SystemUserAccessMap_RecordUpdatedAt') OR define('T_SystemUserAccessMap_RecordUpdatedAt', 't0050r006');
defined('T_SystemUserAccessMap_user_role_id') OR define('T_SystemUserAccessMap_user_role_id', 't0050f001');
defined('T_SystemUserAccessMap_controller') OR define('T_SystemUserAccessMap_controller', 't0050f002');
defined('T_SystemUserAccessMap_permission') OR define('T_SystemUserAccessMap_permission', 't0050f003');
defined('T_SystemUserAccessMap_method') OR define('T_SystemUserAccessMap_method', 't0050f004');


// == mapping SystemUserAutoLogin == //
defined('T_SystemUserAutoLogin') OR define('T_SystemUserAutoLogin', 't0060');
defined('T_SystemUserAutoLogin_RecordID') OR define('T_SystemUserAutoLogin_RecordID', 't0060r001');
defined('T_SystemUserAutoLogin_RecordTimestamp') OR define('T_SystemUserAutoLogin_RecordTimestamp', 't0060r002');
defined('T_SystemUserAutoLogin_RecordStatus') OR define('T_SystemUserAutoLogin_RecordStatus', 't0060r003');
defined('T_SystemUserAutoLogin_RecordUpdatedOn') OR define('T_SystemUserAutoLogin_RecordUpdatedOn', 't0060r004');
defined('T_SystemUserAutoLogin_RecordUpdatedBy') OR define('T_SystemUserAutoLogin_RecordUpdatedBy', 't0060r005');
defined('T_SystemUserAutoLogin_RecordUpdatedAt') OR define('T_SystemUserAutoLogin_RecordUpdatedAt', 't0060r006');
defined('T_SystemUserAutoLogin_key_id') OR define('T_SystemUserAutoLogin_key_id', 't0060f001');
defined('T_SystemUserAutoLogin_user_id') OR define('T_SystemUserAutoLogin_user_id', 't0060f002');
defined('T_SystemUserAutoLogin_user_agent') OR define('T_SystemUserAutoLogin_user_agent', 't0060f003');
defined('T_SystemUserAutoLogin_last_ip') OR define('T_SystemUserAutoLogin_last_ip', 't0060f004');
defined('T_SystemUserAutoLogin_last_login') OR define('T_SystemUserAutoLogin_last_login', 't0060f005');


// == mapping SystemUserMeta == //
defined('T_SystemUserMeta') OR define('T_SystemUserMeta', 't0070');
defined('T_SystemUserMeta_RecordID') OR define('T_SystemUserMeta_RecordID', 't0070r001');
defined('T_SystemUserMeta_RecordTimestamp') OR define('T_SystemUserMeta_RecordTimestamp', 't0070r002');
defined('T_SystemUserMeta_RecordStatus') OR define('T_SystemUserMeta_RecordStatus', 't0070r003');
defined('T_SystemUserMeta_RecordUpdatedOn') OR define('T_SystemUserMeta_RecordUpdatedOn', 't0070r004');
defined('T_SystemUserMeta_RecordUpdatedBy') OR define('T_SystemUserMeta_RecordUpdatedBy', 't0070r005');
defined('T_SystemUserMeta_RecordUpdatedAt') OR define('T_SystemUserMeta_RecordUpdatedAt', 't0070r006');
defined('T_SystemUserMeta_user_id') OR define('T_SystemUserMeta_user_id', 't0070f001');
defined('T_SystemUserMeta_first_name') OR define('T_SystemUserMeta_first_name', 't0070f002');
defined('T_SystemUserMeta_last_name') OR define('T_SystemUserMeta_last_name', 't0070f003');
defined('T_SystemUserMeta_phone') OR define('T_SystemUserMeta_phone', 't0070f004');
defined('T_SystemUserMeta_photo') OR define('T_SystemUserMeta_photo', 't0070f005');
defined('T_SystemUserMeta_address') OR define('T_SystemUserMeta_address', 't0070f006');
defined('T_SystemUserMeta_provinsi_id') OR define('T_SystemUserMeta_provinsi_id', 't0070f007');
defined('T_SystemUserMeta_kecamatan_id') OR define('T_SystemUserMeta_kecamatan_id', 't0070f008');
defined('T_SystemUserMeta_kabkot_id') OR define('T_SystemUserMeta_kabkot_id', 't0070f009');
defined('T_SystemUserMeta_facebook') OR define('T_SystemUserMeta_facebook', 't0070f010');
defined('T_SystemUserMeta_twitter') OR define('T_SystemUserMeta_twitter', 't0070f011');
defined('T_SystemUserMeta_website') OR define('T_SystemUserMeta_website', 't0070f012');
defined('T_SystemUserMeta_office') OR define('T_SystemUserMeta_office', 't0070f013');


// == mapping SystemUserRole == //
defined('T_SystemUserRole') OR define('T_SystemUserRole', 't0080');
defined('T_SystemUserRole_RecordID') OR define('T_SystemUserRole_RecordID', 't0080r001');
defined('T_SystemUserRole_RecordTimestamp') OR define('T_SystemUserRole_RecordTimestamp', 't0080r002');
defined('T_SystemUserRole_RecordStatus') OR define('T_SystemUserRole_RecordStatus', 't0080r003');
defined('T_SystemUserRole_RecordUpdatedOn') OR define('T_SystemUserRole_RecordUpdatedOn', 't0080r004');
defined('T_SystemUserRole_RecordUpdatedBy') OR define('T_SystemUserRole_RecordUpdatedBy', 't0080r005');
defined('T_SystemUserRole_RecordUpdatedAt') OR define('T_SystemUserRole_RecordUpdatedAt', 't0080r006');
defined('T_SystemUserRole_id') OR define('T_SystemUserRole_id', 't0080f001');
defined('T_SystemUserRole_role_name') OR define('T_SystemUserRole_role_name', 't0080f002');
defined('T_SystemUserRole_default_access') OR define('T_SystemUserRole_default_access', 't0080f003');
defined('T_SystemUserRole_access') OR define('T_SystemUserRole_access', 't0080f004');


// == mapping SystemActivityUser == //
defined('T_SystemActivityUser') OR define('T_SystemActivityUser', 't0090');
defined('T_SystemActivityUser_RecordID') OR define('T_SystemActivityUser_RecordID', 't0090r001');
defined('T_SystemActivityUser_RecordTimestamp') OR define('T_SystemActivityUser_RecordTimestamp', 't0090r002');
defined('T_SystemActivityUser_RecordStatus') OR define('T_SystemActivityUser_RecordStatus', 't0090r003');
defined('T_SystemActivityUser_RecordUpdatedOn') OR define('T_SystemActivityUser_RecordUpdatedOn', 't0090r004');
defined('T_SystemActivityUser_RecordUpdatedBy') OR define('T_SystemActivityUser_RecordUpdatedBy', 't0090r005');
defined('T_SystemActivityUser_RecordUpdatedAt') OR define('T_SystemActivityUser_RecordUpdatedAt', 't0090r006');
defined('T_SystemActivityUser_id') OR define('T_SystemActivityUser_id', 't0090f001');
defined('T_SystemActivityUser_message') OR define('T_SystemActivityUser_message', 't0090f002');
defined('T_SystemActivityUser_activity_kategori_id') OR define('T_SystemActivityUser_activity_kategori_id', 't0090f003');
defined('T_SystemActivityUser_activity_jenis_id') OR define('T_SystemActivityUser_activity_jenis_id', 't0090f004');
defined('T_SystemActivityUser_object_id') OR define('T_SystemActivityUser_object_id', 't0090f005');
defined('T_SystemActivityUser_create_datetime') OR define('T_SystemActivityUser_create_datetime', 't0090f006');
defined('T_SystemActivityUser_create_user') OR define('T_SystemActivityUser_create_user', 't0090f007');


// == mapping SystemNavigation == //
defined('T_SystemNavigation') OR define('T_SystemNavigation', 't1000');
defined('T_SystemNavigation_RecordID') OR define('T_SystemNavigation_RecordID', 't1000r001');
defined('T_SystemNavigation_RecordTimestamp') OR define('T_SystemNavigation_RecordTimestamp', 't1000r002');
defined('T_SystemNavigation_RecordStatus') OR define('T_SystemNavigation_RecordStatus', 't1000r003');
defined('T_SystemNavigation_RecordUpdatedOn') OR define('T_SystemNavigation_RecordUpdatedOn', 't1000r004');
defined('T_SystemNavigation_RecordUpdatedBy') OR define('T_SystemNavigation_RecordUpdatedBy', 't1000r005');
defined('T_SystemNavigation_RecordUpdatedAt') OR define('T_SystemNavigation_RecordUpdatedAt', 't1000r006');
defined('T_SystemNavigation_id') OR define('T_SystemNavigation_id', 't1000f001');
defined('T_SystemNavigation_alias') OR define('T_SystemNavigation_alias', 't1000f002');
defined('T_SystemNavigation_title') OR define('T_SystemNavigation_title', 't1000f003');
defined('T_SystemNavigation_description') OR define('T_SystemNavigation_description', 't1000f004');
defined('T_SystemNavigation_icon') OR define('T_SystemNavigation_icon', 't1000f005');
defined('T_SystemNavigation_access') OR define('T_SystemNavigation_access', 't1000f006');
defined('T_SystemNavigation_created_user') OR define('T_SystemNavigation_created_user', 't1000f007');
defined('T_SystemNavigation_created_date') OR define('T_SystemNavigation_created_date', 't1000f008');
defined('T_SystemNavigation_modified_user') OR define('T_SystemNavigation_modified_user', 't1000f009');
defined('T_SystemNavigation_modified_date') OR define('T_SystemNavigation_modified_date', 't1000f010');
defined('T_SystemNavigation_js') OR define('T_SystemNavigation_js', 't1000f011');
defined('T_SystemNavigation_depth') OR define('T_SystemNavigation_depth', 't1000f012');
defined('T_SystemNavigation_ordering') OR define('T_SystemNavigation_ordering', 't1000f013');
defined('T_SystemNavigation_publish') OR define('T_SystemNavigation_publish', 't1000f014');
defined('T_SystemNavigation_type') OR define('T_SystemNavigation_type', 't1000f015');


// == mapping TransactionStockMovementHeader == //
defined('T_TransactionStockMovementHeader') OR define('T_TransactionStockMovementHeader', 't1010');
defined('T_TransactionStockMovementHeader_RecordID') OR define('T_TransactionStockMovementHeader_RecordID', 't1010r001');
defined('T_TransactionStockMovementHeader_RecordTimestamp') OR define('T_TransactionStockMovementHeader_RecordTimestamp', 't1010r002');
defined('T_TransactionStockMovementHeader_RecordStatus') OR define('T_TransactionStockMovementHeader_RecordStatus', 't1010r003');
defined('T_TransactionStockMovementHeader_RecordFlag') OR define('T_TransactionStockMovementHeader_RecordFlag', 't1010r004');
defined('T_TransactionStockMovementHeader_UpdatedOn') OR define('T_TransactionStockMovementHeader_UpdatedOn', 't1010r005');
defined('T_TransactionStockMovementHeader_UpdatedBy') OR define('T_TransactionStockMovementHeader_UpdatedBy', 't1010r006');
defined('T_TransactionStockMovementHeader_UpdatedAt') OR define('T_TransactionStockMovementHeader_UpdatedAt', 't1010r007');
defined('T_TransactionStockMovementHeader_DocTypeID') OR define('T_TransactionStockMovementHeader_DocTypeID', 't1010f001');
defined('T_TransactionStockMovementHeader_DocNo') OR define('T_TransactionStockMovementHeader_DocNo', 't1010f002');
defined('T_TransactionStockMovementHeader_DocDate') OR define('T_TransactionStockMovementHeader_DocDate', 't1010f003');
defined('T_TransactionStockMovementHeader_DocStatus') OR define('T_TransactionStockMovementHeader_DocStatus', 't1010f004');
defined('T_TransactionStockMovementHeader_Remarks') OR define('T_TransactionStockMovementHeader_Remarks', 't1010f005');


// == mapping TransactionStockMovementDetail == //
defined('T_TransactionStockMovementDetail') OR define('T_TransactionStockMovementDetail', 't1011');
defined('T_TransactionStockMovementDetail_RecordID') OR define('T_TransactionStockMovementDetail_RecordID', 't1011r001');
defined('T_TransactionStockMovementDetail_RecordTimestamp') OR define('T_TransactionStockMovementDetail_RecordTimestamp', 't1011r002');
defined('T_TransactionStockMovementDetail_RecordStatus') OR define('T_TransactionStockMovementDetail_RecordStatus', 't1011r003');
defined('T_TransactionStockMovementDetail_RecordFlag') OR define('T_TransactionStockMovementDetail_RecordFlag', 't1011r004');
defined('T_TransactionStockMovementDetail_UpdatedOn') OR define('T_TransactionStockMovementDetail_UpdatedOn', 't1011r005');
defined('T_TransactionStockMovementDetail_UpdatedBy') OR define('T_TransactionStockMovementDetail_UpdatedBy', 't1011r006');
defined('T_TransactionStockMovementDetail_UpdatedAt') OR define('T_TransactionStockMovementDetail_UpdatedAt', 't1011r007');
defined('T_TransactionStockMovementDetail_PRI') OR define('T_TransactionStockMovementDetail_PRI', 't1011f001');
defined('T_TransactionStockMovementDetail_RowIndex') OR define('T_TransactionStockMovementDetail_RowIndex', 't1011f002');
defined('T_TransactionStockMovementDetail_ItemID') OR define('T_TransactionStockMovementDetail_ItemID', 't1011f003');
defined('T_TransactionStockMovementDetail_ItemName') OR define('T_TransactionStockMovementDetail_ItemName', 't1011f004');
defined('T_TransactionStockMovementDetail_Type') OR define('T_TransactionStockMovementDetail_Type', 't1011f005');
defined('T_TransactionStockMovementDetail_EPC') OR define('T_TransactionStockMovementDetail_EPC', 't1011f006');
defined('T_TransactionStockMovementDetail_Barcode') OR define('T_TransactionStockMovementDetail_Barcode', 't1011f007');
defined('T_TransactionStockMovementDetail_LocationID1') OR define('T_TransactionStockMovementDetail_LocationID1', 't1011f008');
defined('T_TransactionStockMovementDetail_LocationID2') OR define('T_TransactionStockMovementDetail_LocationID2', 't1011f009');
defined('T_TransactionStockMovementDetail_Quantity1') OR define('T_TransactionStockMovementDetail_Quantity1', 't1011f010');
defined('T_TransactionStockMovementDetail_Quantity2') OR define('T_TransactionStockMovementDetail_Quantity2', 't1011f011');
defined('T_TransactionStockMovementDetail_Remarks') OR define('T_TransactionStockMovementDetail_Remarks', 't1011f012');

// == mapping TransactionStockMovementDetailSerialNo == //
defined('T_TransactionStockMovementDetailSerialNo') OR define('T_TransactionStockMovementDetailSerialNo', 't1012');
defined('T_TransactionStockMovementDetailSerialNo_RecordID') OR define('T_TransactionStockMovementDetailSerialNo_RecordID', 't1012r001');
defined('T_TransactionStockMovementDetailSerialNo_RecordTimestamp') OR define('T_TransactionStockMovementDetailSerialNo_RecordTimestamp', 't1012r002');
defined('T_TransactionStockMovementDetailSerialNo_RecordStatus') OR define('T_TransactionStockMovementDetailSerialNo_RecordStatus', 't1012r003');
defined('T_TransactionStockMovementDetailSerialNo_RecordFlag') OR define('T_TransactionStockMovementDetailSerialNo_RecordFlag', 't1012r004');
defined('T_TransactionStockMovementDetailSerialNo_UpdatedOn') OR define('T_TransactionStockMovementDetailSerialNo_UpdatedOn', 't1012r005');
defined('T_TransactionStockMovementDetailSerialNo_UpdatedBy') OR define('T_TransactionStockMovementDetailSerialNo_UpdatedBy', 't1012r006');
defined('T_TransactionStockMovementDetailSerialNo_UpdatedAt') OR define('T_TransactionStockMovementDetailSerialNo_UpdatedAt', 't1012r007');
defined('T_TransactionStockMovementDetailSerialNo_PRI') OR define('T_TransactionStockMovementDetailSerialNo_PRI', 't1012f001');
defined('T_TransactionStockMovementDetailSerialNo_SerialNo') OR define('T_TransactionStockMovementDetailSerialNo_SerialNo', 't1012f002');

// == mapping Reader Scan == //
defined('T_ReaderScan') OR define('T_ReaderScan', 't1013');
defined('T_ReaderScan_RecordID') OR define('T_ReaderScan_RecordID', 't1013r001');
defined('T_ReaderScan_RecordTimestamp') OR define('T_ReaderScan_RecordTimestamp', 't1013r002');
defined('T_ReaderScan_RecordStatus') OR define('T_ReaderScan_RecordStatus', 't1013r003');
defined('T_ReaderScan_RecordFlag') OR define('T_ReaderScan_RecordFlag', 't1013r004');
defined('T_ReaderScan_UpdatedOn') OR define('T_ReaderScan_UpdatedOn', 't1013r005');
defined('T_ReaderScan_UpdatedBy') OR define('T_ReaderScan_UpdatedBy', 't1013r006');
defined('T_ReaderScan_UpdatedAt') OR define('T_ReaderScan_UpdatedAt', 't1013r007');
defined('T_ReaderScan_Scan') OR define('T_ReaderScan_Scan', 't1013f001');
defined('T_ReaderScan_Write') OR define('T_ReaderScan_Write', 't1013f002');

// == mapping Log EPC == //
defined('T_LogEPC') OR define('T_LogEPC', 't1014');
defined('T_LogEPC_RecordID') OR define('T_LogEPC_RecordID', 't1014r001');
defined('T_LogEPC_RecordTimestamp') OR define('T_LogEPC_RecordTimestamp', 't1014r002');
defined('T_LogEPC_RecordStatus') OR define('T_LogEPC_RecordStatus', 't1014r003');
defined('T_LogEPC_RecordFlag') OR define('T_LogEPC_RecordFlag', 't1014r004');
defined('T_LogEPC_UpdatedOn') OR define('T_LogEPC_UpdatedOn', 't1014r005');
defined('T_LogEPC_UpdatedBy') OR define('T_LogEPC_UpdatedBy', 't1014r006');
defined('T_LogEPC_UpdatedAt') OR define('T_LogEPC_UpdatedAt', 't1014r007');
defined('T_LogEPC_EPC') OR define('T_LogEPC_EPC', 't1014f001');

// == mapping TransactionStockBalanceHeader == //
defined('T_TransactionStockBalanceHeader') OR define('T_TransactionStockBalanceHeader', 't1990');
defined('T_TransactionStockBalanceHeader_RecordID') OR define('T_TransactionStockBalanceHeader_RecordID', 't1990r001');
defined('T_TransactionStockBalanceHeader_RecordTimestamp') OR define('T_TransactionStockBalanceHeader_RecordTimestamp', 't1990r002');
defined('T_TransactionStockBalanceHeader_RecordStatus') OR define('T_TransactionStockBalanceHeader_RecordStatus', 't1990r003');
defined('T_TransactionStockBalanceHeader_RecordFlag') OR define('T_TransactionStockBalanceHeader_RecordFlag', 't1990r004');
defined('T_TransactionStockBalanceHeader_UpdatedOn') OR define('T_TransactionStockBalanceHeader_UpdatedOn', 't1990r005');
defined('T_TransactionStockBalanceHeader_UpdatedBy') OR define('T_TransactionStockBalanceHeader_UpdatedBy', 't1990r006');
defined('T_TransactionStockBalanceHeader_UpdatedAt') OR define('T_TransactionStockBalanceHeader_UpdatedAt', 't1990r007');
defined('T_TransactionStockBalanceHeader_ItemID') OR define('T_TransactionStockBalanceHeader_ItemID', 't1990f001');
defined('T_TransactionStockBalanceHeader_LocationID') OR define('T_TransactionStockBalanceHeader_LocationID', 't1990f002');
defined('T_TransactionStockBalanceHeader_Quantity') OR define('T_TransactionStockBalanceHeader_Quantity', 't1990f003');
defined('T_TransactionStockBalanceHeader_EPC') OR define('T_TransactionStockBalanceHeader_EPC', 't1990f004');
defined('T_TransactionStockBalanceHeader_Barcode') OR define('T_TransactionStockBalanceHeader_Barcode', 't1990f005');

// == mapping TransactionStockMovementLog == //
defined('T_TransactionStockBalanceLog') OR define('T_TransactionStockBalanceLog', 't1991');
defined('T_TransactionStockBalanceLog_RecordID') OR define('T_TransactionStockBalanceLog_RecordID', 't1991r001');
defined('T_TransactionStockBalanceLog_RecordTimestamp') OR define('T_TransactionStockBalanceLog_RecordTimestamp', 't1991r002');
defined('T_TransactionStockBalanceLog_RecordStatus') OR define('T_TransactionStockBalanceLog_RecordStatus', 't1991r003');
defined('T_TransactionStockBalanceLog_RecordFlag') OR define('T_TransactionStockBalanceLog_RecordFlag', 't1991r004');
defined('T_TransactionStockBalanceLog_UpdatedOn') OR define('T_TransactionStockBalanceLog_UpdatedOn', 't1991r005');
defined('T_TransactionStockBalanceLog_UpdatedBy') OR define('T_TransactionStockBalanceLog_UpdatedBy', 't1991r006');
defined('T_TransactionStockBalanceLog_UpdatedAT') OR define('T_TransactionStockBalanceLog_UpdatedAT', 't1991r007');
defined('T_TransactionStockBalanceLog_PRI') OR define('T_TransactionStockBalanceLog_PRI', 't1991f001');
defined('T_TransactionStockBalanceLog_DocTypeID') OR define('T_TransactionStockBalanceLog_DocTypeID', 't1991f002');
defined('T_TransactionStockBalanceLog_RefRecordID') OR define('T_TransactionStockBalanceLog_RefRecordID', 't1991f003');
defined('T_TransactionStockBalanceLog_RefDetailRecordID') OR define('T_TransactionStockBalanceLog_RefDetailRecordID', 't1991f004');
defined('T_TransactionStockBalanceLog_Quantity') OR define('T_TransactionStockBalanceLog_Quantity', 't1991f005');

// == mapping TransactionStockMovementDetail == //
defined('T_TransactionStockBalanceDetail') OR define('T_TransactionStockBalanceDetail', 't1992');
defined('T_TransactionStockBalanceDetail_RecordID') OR define('T_TransactionStockBalanceDetail_RecordID', 't1992r001');
defined('T_TransactionStockBalanceDetail_RecordTimestamp') OR define('T_TransactionStockBalanceDetail_RecordTimestamp', 't1992r002');
defined('T_TransactionStockBalanceDetail_RecordStatus') OR define('T_TransactionStockBalanceDetail_RecordStatus', 't1992r003');
defined('T_TransactionStockBalanceDetail_RecordFlag') OR define('T_TransactionStockBalanceDetail_RecordFlag', 't1992r004');
defined('T_TransactionStockBalanceDetail_UpdatedOn') OR define('T_TransactionStockBalanceDetail_UpdatedOn', 't1992r005');
defined('T_TransactionStockBalanceDetail_UpdatedBy') OR define('T_TransactionStockBalanceDetail_UpdatedBy', 't1992r006');
defined('T_TransactionStockBalanceDetail_UpdatedAt') OR define('T_TransactionStockBalanceDetail_UpdatedAt', 't1992r007');
defined('T_TransactionStockBalanceDetail_PRI') OR define('T_TransactionStockBalanceDetail_PRI', 't1992f001');
defined('T_TransactionStockBalanceDetail_SerialNo') OR define('T_TransactionStockBalanceDetail_SerialNo', 't1992f002');
defined('T_TransactionStockBalanceDetail_StockFlag') OR define('T_TransactionStockBalanceDetail_StockFlag', 't1992f003');

// == mapping TransactionSalesInvoice == //
defined('T_SalesInvoiceHeader') OR define('T_SalesInvoiceHeader', 't2040');
defined('T_SalesInvoiceHeader_RecordID') OR define('T_SalesInvoiceHeader_RecordID', 't2040r001');
defined('T_SalesInvoiceHeader_RecordTimestamp') OR define('T_SalesInvoiceHeader_RecordTimestamp', 't2040r002');
defined('T_SalesInvoiceHeader_RecordStatus') OR define('T_SalesInvoiceHeader_RecordStatus', 't2040r003');
defined('T_SalesInvoiceHeader_RecordFlag') OR define('T_SalesInvoiceHeader_RecordFlag', 't2040r004');
defined('T_SalesInvoiceHeader_UpdatedOn') OR define('T_SalesInvoiceHeader_UpdatedOn', 't2040f005');
defined('T_SalesInvoiceHeader_UpdatedBy') OR define('T_SalesInvoiceHeader_UpdatedBy', 't2040f006');
defined('T_SalesInvoiceHeader_UpdatedAt') OR define('T_SalesInvoiceHeader_UpdatedAt', 't2040f007');
defined('T_SalesInvoiceHeader_DocTypeID') OR define('T_SalesInvoiceHeader_DocTypeID', 't2040f001');
defined('T_SalesInvoiceHeader_DocNo') OR define('T_SalesInvoiceHeader_DocNo', 't2040f002');
defined('T_SalesInvoiceHeader_DocDate') OR define('T_SalesInvoiceHeader_DocDate', 't2040f003');
defined('T_SalesInvoiceHeader_DocStatus') OR define('T_SalesInvoiceHeader_DocStatus', 't2040f004');
defined('T_SalesInvoiceHeader_RefDocTypeID') OR define('T_SalesInvoiceHeader_RefDocTypeID', 't2040f005');
defined('T_SalesInvoiceHeader_RefDocNo') OR define('T_SalesInvoiceHeader_RefDocNo', 't2040f006');
defined('T_SalesInvoiceHeader_BizPartnerID') OR define('T_SalesInvoiceHeader_BizPartnerID', 't2040f007');
defined('T_SalesInvoiceHeader_BizPartnerRefDocNo') OR define('T_SalesInvoiceHeader_BizPartnerRefDocNo', 't2040f008');
defined('T_SalesInvoiceHeader_BizPartnerRefDocDate') OR define('T_SalesInvoiceHeader_BizPartnerRefDocDate', 't2040f009');
defined('T_SalesInvoiceHeader_TermofPayment') OR define('T_SalesInvoiceHeader_TermofPayment', 't2040f010');
defined('T_SalesInvoiceHeader_CurrencyID') OR define('T_SalesInvoiceHeader_CurrencyID', 't2040f011');
defined('T_SalesInvoiceHeader_ExchangeRate') OR define('T_SalesInvoiceHeader_ExchangeRate', 't2040f012');
defined('T_SalesInvoiceHeader_SalespersonID') OR define('T_SalesInvoiceHeader_SalespersonID', 't2040f013');
defined('T_SalesInvoiceHeader_AmountSubtotal') OR define('T_SalesInvoiceHeader_AmountSubtotal', 't2040f014');
defined('T_SalesInvoiceHeader_AmountVAT') OR define('T_SalesInvoiceHeader_AmountVAT', 't2040f015');
defined('T_SalesInvoiceHeader_AmountTotal') OR define('T_SalesInvoiceHeader_AmountTotal', 't2040f016');
defined('T_SalesInvoiceHeader_Payment') OR define('T_SalesInvoiceHeader_Payment', 't2040f017');
defined('T_SalesInvoiceHeader_Remarks') OR define('T_SalesInvoiceHeader_Remarks', 't2040f018');

// == mapping TransactionSalesInvoiceDetail == //
defined('T_SalesInvoiceDetail') OR define('T_SalesInvoiceDetail', 't2041');
defined('T_SalesInvoiceDetail_RecordID') OR define('T_SalesInvoiceDetail_RecordID', 't2041r001');
defined('T_SalesInvoiceDetail_RecordTimestamp') OR define('T_SalesInvoiceDetail_RecordTimestamp', 't2041r002');
defined('T_SalesInvoiceDetail_RecordStatus') OR define('T_SalesInvoiceDetail_RecordStatus', 't2041r003');
defined('T_SalesInvoiceDetail_RecordFlag') OR define('T_SalesInvoiceDetail_RecordFlag', 't2041r004');
defined('T_SalesInvoiceDetail_UpdatedOn') OR define('T_SalesInvoiceDetail_UpdatedOn', 't2041f005');
defined('T_SalesInvoiceDetail_UpdatedBy') OR define('T_SalesInvoiceDetail_UpdatedBy', 't2041f006');
defined('T_SalesInvoiceDetail_UpdatedAt') OR define('T_SalesInvoiceDetail_UpdatedAt', 't2041f007');
defined('T_SalesInvoiceDetail_PRI') OR define('T_SalesInvoiceDetail_PRI', 't2041f001');
defined('T_SalesInvoiceDetail_RowIndex') OR define('T_SalesInvoiceDetail_RowIndex', 't2041f002');
defined('T_SalesInvoiceDetail_ItemID') OR define('T_SalesInvoiceDetail_ItemID', 't2041f003');
defined('T_SalesInvoiceDetail_ItemName') OR define('T_SalesInvoiceDetail_ItemName', 't2041f004');
defined('T_SalesInvoiceDetail_AutoIDType') OR define('T_SalesInvoiceDetail_AutoIDType', 't2041f005');
defined('T_SalesInvoiceDetail_EPC') OR define('T_SalesInvoiceDetail_EPC', 't2041f006');
defined('T_SalesInvoiceDetail_Barcode') OR define('T_SalesInvoiceDetail_Barcode', 't2041f007');
defined('T_SalesInvoiceDetail_Quantity') OR define('T_SalesInvoiceDetail_Quantity', 't2041f008');
defined('T_SalesInvoiceDetail_UoMID') OR define('T_SalesInvoiceDetail_UoMID', 't2041f009');
defined('T_SalesInvoiceDetail_UnitPrice') OR define('T_SalesInvoiceDetail_UnitPrice', 't2041f010');
defined('T_SalesInvoiceDetail_LineTotal') OR define('T_SalesInvoiceDetail_LineTotal', 't2041f011');
defined('T_SalesInvoiceDetail_Remarks') OR define('T_SalesInvoiceDetail_Remarks', 't2041f012');
defined('T_SalesInvoiceDetail_LocationID') OR define('T_SalesInvoiceDetail_LocationID', 'LC02');

// == mapping TransactionSalesInvoiceDetailSerialNo == //
defined('T_SalesInvoiceDetailSerialNo') OR define('T_SalesInvoiceDetailSerialNo', 't2042');
defined('T_SalesInvoiceDetailSerialNo_RecordID') OR define('T_SalesInvoiceDetailSerialNo_RecordID', 't2042r001');
defined('T_SalesInvoiceDetailSerialNo_RecordTimestamp') OR define('T_SalesInvoiceDetailSerialNo_RecordTimestamp', 't2042r002');
defined('T_SalesInvoiceDetailSerialNo_RecordStatus') OR define('T_SalesInvoiceDetailSerialNo_RecordStatus', 't2042r003');
defined('T_SalesInvoiceDetailSerialNo_RecordFlag') OR define('T_SalesInvoiceDetailSerialNo_RecordFlag', 't2042r004');
defined('T_SalesInvoiceDetailSerialNo_UpdatedOn') OR define('T_SalesInvoiceDetailSerialNo_UpdatedOn', 't2042f005');
defined('T_SalesInvoiceDetailSerialNo_UpdatedBy') OR define('T_SalesInvoiceDetailSerialNo_UpdatedBy', 't2042f006');
defined('T_SalesInvoiceDetailSerialNo_UpdatedAt') OR define('T_SalesInvoiceDetailSerialNo_UpdatedAt', 't2042f007');
defined('T_SalesInvoiceDetailSerialNo_PRI') OR define('T_SalesInvoiceDetailSerialNo_PRI', 't2042f001');
defined('T_SalesInvoiceDetailSerialNo_SN') OR define('T_SalesInvoiceDetailSerialNo_SN', 't2042f002');

// == mapping MasterDataItem == //
defined('T_MasterDataItem') OR define('T_MasterDataItem', 't8010');
defined('T_MasterDataItem_RecordID') OR define('T_MasterDataItem_RecordID', 't8010r001');
defined('T_MasterDataItem_RecordTimestamp') OR define('T_MasterDataItem_RecordTimestamp', 't8010r002');
defined('T_MasterDataItem_RecordStatus') OR define('T_MasterDataItem_RecordStatus', 't8010r003');
defined('T_MasterDataItem_RecordFlag') OR define('T_MasterDataItem_RecordFlag', 't8010r004');
defined('T_MasterDataItem_UpdatedOn') OR define('T_MasterDataItem_UpdatedOn', 't8010r005');
defined('T_MasterDataItem_UpdatedBy') OR define('T_MasterDataItem_UpdatedBy', 't8010r006');
defined('T_MasterDataItem_UpdatedAt') OR define('T_MasterDataItem_UpdatedAt', 't8010r007');
defined('T_MasterDataItem_ItemID') OR define('T_MasterDataItem_ItemID', 't8010f001');
defined('T_MasterDataItem_ItemName') OR define('T_MasterDataItem_ItemName', 't8010f002');
defined('T_MasterDataItem_UOMID') OR define('T_MasterDataItem_UOMID', 't8010f003');
defined('T_MasterDataItem_AutoIDType') OR define('T_MasterDataItem_AutoIDType', 't8010f004');
defined('T_MasterDataItem_EPC') OR define('T_MasterDataItem_EPC', 't8010f005');
defined('T_MasterDataItem_Barcode') OR define('T_MasterDataItem_Barcode', 't8010f006');
defined('T_MasterDataItem_Picture') OR define('T_MasterDataItem_Picture', 't8010f007');
defined('T_MasterDataItem_UnitPrice') OR define('T_MasterDataItem_UnitPrice', 't8010f008');

// == mapping MasterDataBizPartnerHeader == //
defined('T_MasterDataBizPartnerHeader') OR define('T_MasterDataBizPartnerHeader', 't8020');
defined('T_MasterDataBizPartnerHeader_RecordID') OR define('T_MasterDataBizPartnerHeader_RecordID', 't8020r001');
defined('T_MasterDataBizPartnerHeader_RecordTimestamp') OR define('T_MasterDataBizPartnerHeader_RecordTimestamp', 't8020r002');
defined('T_MasterDataBizPartnerHeader_RecordStatus') OR define('T_MasterDataBizPartnerHeader_RecordStatus', 't8020r003');
defined('T_MasterDataBizPartnerHeader_RecordFlag') OR define('T_MasterDataBizPartnerHeader_RecordFlag', 't8020r004');
defined('T_MasterDataBizPartnerHeader_UpdatedOn') OR define('T_MasterDataBizPartnerHeader_UpdatedOn', 't8020r005');
defined('T_MasterDataBizPartnerHeader_UpdatedBy') OR define('T_MasterDataBizPartnerHeader_UpdatedBy', 't8020r006');
defined('T_MasterDataBizPartnerHeader_UpdatedAt') OR define('T_MasterDataBizPartnerHeader_UpdatedAt', 't8020r007');
defined('T_MasterDataBizPartnerHeader_BizPartnerID') OR define('T_MasterDataBizPartnerHeader_BizPartnerID', 't8020f001');
defined('T_MasterDataBizPartnerHeader_BizPartnerName') OR define('T_MasterDataBizPartnerHeader_BizPartnerName', 't8020f002');
defined('T_MasterDataBizPartnerHeader_BizPartnerType') OR define('T_MasterDataBizPartnerHeader_BizPartnerType', 't8020f003');

// == mapping MasterDataLocation == //
defined('T_MasterDataLocation') OR define('T_MasterDataLocation', 't8030');
defined('T_MasterDataLocation_RecordID') OR define('T_MasterDataLocation_RecordID', 't8030r001');
defined('T_MasterDataLocation_RecordTimestamp') OR define('T_MasterDataLocation_RecordTimestamp', 't8030r002');
defined('T_MasterDataLocation_RecordStatus') OR define('T_MasterDataLocation_RecordStatus', 't8030r003');
defined('T_MasterDataLocation_RecordFlag') OR define('T_MasterDataLocation_RecordFlag', 't8030r004');
defined('T_MasterDataLocation_UpdatedOn') OR define('T_MasterDataLocation_UpdatedOn', 't8030r005');
defined('T_MasterDataLocation_UpdatedBy') OR define('T_MasterDataLocation_UpdatedBy', 't8030r006');
defined('T_MasterDataLocation_UpdatedAt') OR define('T_MasterDataLocation_UpdatedAt', 't8030r007');
defined('T_MasterDataLocation_LocationID') OR define('T_MasterDataLocation_LocationID', 't8030f001');
defined('T_MasterDataLocation_LocationName') OR define('T_MasterDataLocation_LocationName', 't8030f002');

// == mapping MasterDataPerson == //
defined('T_MasterDataperson') OR define('T_MasterDataperson', 't8040');
defined('T_MasterDataperson_RecordID') OR define('T_MasterDataperson_RecordID', 't8040r001');
defined('T_MasterDataperson_RecordTimestamp') OR define('T_MasterDataperson_RecordTimestamp', 't8040r002');
defined('T_MasterDataperson_RecordStatus') OR define('T_MasterDataperson_RecordStatus', 't8040r003');
defined('T_MasterDataperson_RecordFlag') OR define('T_MasterDataperson_RecordFlag', 't8040r004');
defined('T_MasterDataperson_UpdatedOn') OR define('T_MasterDataperson_UpdatedOn', 't8040f005');
defined('T_MasterDataperson_UpdatedBy') OR define('T_MasterDataperson_UpdatedBy', 't8040f006');
defined('T_MasterDataperson_UpdatedAt') OR define('T_MasterDataperson_UpdatedAt', 't8040f007');
defined('T_MasterDataperson_PersonID') OR define('T_MasterDataperson_PersonID', 't8040f001');
defined('T_MasterDataperson_PersonName') OR define('T_MasterDataperson_PersonName', 't8040f002');
defined('T_MasterDataperson_PersonTypeID') OR define('T_MasterDataperson_PersonTypeID', 't8040f003');
defined('T_MasterDataperson_IdentityTypeID') OR define('T_MasterDataperson_IdentityTypeID', 't8040f004');
defined('T_MasterDataperson_IdentityNo') OR define('T_MasterDataperson_IdentityNo', 't8040f005');
defined('T_MasterDataperson_Picture') OR define('T_MasterDataperson_Picture', 't8040f006');
defined('T_MasterDataperson_Address') OR define('T_MasterDataperson_Address', 't8040f007');
defined('T_MasterDataperson_Country') OR define('T_MasterDataperson_Country', 't8040f008');
defined('T_MasterDataperson_State') OR define('T_MasterDataperson_State', 't8040f009');
defined('T_MasterDataperson_City') OR define('T_MasterDataperson_City', 't8040f0010');
defined('T_MasterDataperson_PostalCode') OR define('T_MasterDataperson_PostalCode', 't8040f0011');
defined('T_MasterDataperson_Telephone') OR define('T_MasterDataperson_Telephone', 't8040f0012');
defined('T_MasterDataperson_EPC') OR define('T_MasterDataperson_EPC', 't8040f0013');
defined('T_MasterDataperson_Barcode') OR define('T_MasterDataperson_Barcode', 't8040f0014');

// == mapping country == // 
defined('T_MasterDataCountry') OR define('T_MasterDataCountry', 't8060');
defined('T_MasterDataCountry_RecordID') OR define('T_MasterDataCountry_RecordID', 't8060r001');
defined('T_MasterDataCountry_RecordTimestamp') OR define('T_MasterDataCountry_RecordTimestamp', 't8060r002');
defined('T_MasterDataCountry_RecordStatus') OR define('T_MasterDataCountry_RecordStatus', 't8060r003');
defined('T_MasterDataCountry_RecordUpdatedOn') OR define('T_MasterDataCountry_RecordUpdatedOn', 't8060r004');
defined('T_MasterDataCountry_RecordUpdatedBy') OR define('T_MasterDataCountry_RecordUpdatedBy', 't8060r005');
defined('T_MasterDataCountry_RecordUpdatedAt') OR define('T_MasterDataCountry_RecordUpdatedAt', 't8060r006');
defined('T_MasterDataCountry_CountryID') OR define('T_MasterDataCountry_CountryID', 't8060f001');
defined('T_MasterDataCountry_CountryName') OR define('T_MasterDataCountry_CountryName', 't8060f002');

// == mapping state == // 
defined('T_MasterDataState') OR define('T_MasterDataState', 't8061');
defined('T_MasterDataState_RecordID') OR define('T_MasterDataState_RecordID', 't8061r001');
defined('T_MasterDataState_RecordTimestamp') OR define('T_MasterDataState_RecordTimestamp', 't8061r002');
defined('T_MasterDataState_RecordStatus') OR define('T_MasterDataState_RecordStatus', 't8061r003');
defined('T_MasterDataState_RecordUpdatedOn') OR define('T_MasterDataState_RecordUpdatedOn', 't8061r004');
defined('T_MasterDataState_RecordUpdatedBy') OR define('T_MasterDataState_RecordUpdatedBy', 't8061r005');
defined('T_MasterDataState_RecordUpdatedAt') OR define('T_MasterDataState_RecordUpdatedAt', 't8061r006');
defined('T_MasterDataState_StateName') OR define('T_MasterDataState_StateName', 't8061f001');
defined('T_MasterDataState_CountryID') OR define('T_MasterDataState_CountryID', 't8061f002');

// == mapping city == // 
defined('T_MasterDataCity') OR define('T_MasterDataCity', 't8062');
defined('T_MasterDataCity_RecordID') OR define('T_MasterDataCity_RecordID', 't8062r001');
defined('T_MasterDataCity_RecordTimestamp') OR define('T_MasterDataCity_RecordTimestamp', 't8062r002');
defined('T_MasterDataCity_RecordStatus') OR define('T_MasterDataCity_RecordStatus', 't8062r003');
defined('T_MasterDataCity_RecordUpdatedOn') OR define('T_MasterDataCity_RecordUpdatedOn', 't8062r004');
defined('T_MasterDataCity_RecordUpdatedBy') OR define('T_MasterDataCity_RecordUpdatedBy', 't8062r005');
defined('T_MasterDataCity_RecordUpdatedAt') OR define('T_MasterDataCity_RecordUpdatedAt', 't8062r006');
defined('T_MasterDataCity_CityName') OR define('T_MasterDataCity_CityName', 't8062f001');
defined('T_MasterDataCity_StateID') OR define('T_MasterDataCity_StateID', 't8062f002');

// == mapping MasterDataGeneralTable == //
defined('T_MasterDataGeneralTable') OR define('T_MasterDataGeneralTable', 't8990');
defined('T_MasterDataGeneralTable_RecordID') OR define('T_MasterDataGeneralTable_RecordID', 't8990r001');
defined('T_MasterDataGeneralTable_RecordTimestamp') OR define('T_MasterDataGeneralTable_RecordTimestamp', 't8990r002');
defined('T_MasterDataGeneralTable_RecordStatus') OR define('T_MasterDataGeneralTable_RecordStatus', 't8990r003');
defined('T_MasterDataGeneralTable_RecordFlag') OR define('T_MasterDataGeneralTable_RecordFlag', 't8990r004');
defined('T_MasterDataGeneralTable_UpdatedOn') OR define('T_MasterDataGeneralTable_UpdatedOn', 't8990r005');
defined('T_MasterDataGeneralTable_UpdatedBy') OR define('T_MasterDataGeneralTable_UpdatedBy', 't8990r006');
defined('T_MasterDataGeneralTable_UpdatedAt') OR define('T_MasterDataGeneralTable_UpdatedAt', 't8990r007');
defined('T_MasterDataGeneralTable_ID') OR define('T_MasterDataGeneralTable_ID', 't8990f001');
defined('T_MasterDataGeneralTable_Name') OR define('T_MasterDataGeneralTable_Name', 't8990f002');

// == mapping MasterDataGeneralTable == //
defined('T_MasterDataGeneralTableValue') OR define('T_MasterDataGeneralTableValue', 't8991');
defined('T_MasterDataGeneralTableValue_RecordID') OR define('T_MasterDataGeneralTableValue_RecordID', 't8991r001');
defined('T_MasterDataGeneralTableValue_RecordTimestamp') OR define('T_MasterDataGeneralTableValue_RecordTimestamp', 't8991r002');
defined('T_MasterDataGeneralTableValue_RecordStatus') OR define('T_MasterDataGeneralTableValue_RecordStatus', 't8991r003');
defined('T_MasterDataGeneralTableValue_RecordFlag') OR define('T_MasterDataGeneralTableValue_RecordFlag', 't8991r004');
defined('T_MasterDataGeneralTableValue_UpdatedOn') OR define('T_MasterDataGeneralTableValue_UpdatedOn', 't8991r005');
defined('T_MasterDataGeneralTableValue_UpdatedBy') OR define('T_MasterDataGeneralTableValue_UpdatedBy', 't8991r006');
defined('T_MasterDataGeneralTableValue_UpdatedAt') OR define('T_MasterDataGeneralTableValue_UpdatedAt', 't8991r007');
defined('T_MasterDataGeneralTableValue_PRI') OR define('T_MasterDataGeneralTableValue_PRI', 't8991f001');
defined('T_MasterDataGeneralTableValue_Key') OR define('T_MasterDataGeneralTableValue_Key', 't8991f002');
defined('T_MasterDataGeneralTableValue_Description') OR define('T_MasterDataGeneralTableValue_Description', 't8991f003');

// == mapping SystemDocumentType == //
defined('T_SystemDocumentType') OR define('T_SystemDocumentType', 't9050');
defined('T_SystemDocumentType_RecordID') OR define('T_SystemDocumentType_RecordID', 't9050r001');
defined('T_SystemDocumentType_RecordTimestamp') OR define('T_SystemDocumentType_RecordTimestamp', 't9050r002');
defined('T_SystemDocumentType_RecordStatus') OR define('T_SystemDocumentType_RecordStatus', 't9050r003');
defined('T_SystemDocumentType_RecordFlag') OR define('T_SystemDocumentType_RecordFlag', 't9050r004');
defined('T_SystemDocumentType_UpdatedOn') OR define('T_SystemDocumentType_UpdatedOn', 't9050r005');
defined('T_SystemDocumentType_UpdatedBy') OR define('T_SystemDocumentType_UpdatedBy', 't9050r006');
defined('T_SystemDocumentType_UpdatedAt') OR define('T_SystemDocumentType_UpdatedAt', 't9050r007');
defined('T_SystemDocumentType_DocTypeID') OR define('T_SystemDocumentType_DocTypeID', 't9050f001');
defined('T_SystemDocumentType_DocTypeName') OR define('T_SystemDocumentType_DocTypeName', 't9050f002');


// == mapping SystemDocumentPeriod == //
defined('T_SystemDocumentPeriod') OR define('T_SystemDocumentPeriod', 't9051');
defined('T_SystemDocumentPeriod_RecordID') OR define('T_SystemDocumentPeriod_RecordID', 't9051r001');
defined('T_SystemDocumentPeriod_RecordTimestamp') OR define('T_SystemDocumentPeriod_RecordTimestamp', 't9051r002');
defined('T_SystemDocumentPeriod_RecordStatus') OR define('T_SystemDocumentPeriod_RecordStatus', 't9051r003');
defined('T_SystemDocumentPeriod_RecordFlag') OR define('T_SystemDocumentPeriod_RecordFlag', 't9051r004');
defined('T_SystemDocumentPeriod_UpdatedOn') OR define('T_SystemDocumentPeriod_UpdatedOn', 't9051r005');
defined('T_SystemDocumentPeriod_UpdatedBy') OR define('T_SystemDocumentPeriod_UpdatedBy', 't9051r006');
defined('T_SystemDocumentPeriod_UpdatedAt') OR define('T_SystemDocumentPeriod_UpdatedAt', 't9051r007');
defined('T_SystemDocumentPeriod_PRI') OR define('T_SystemDocumentPeriod_PRI', 't9051f001');
defined('T_SystemDocumentPeriod_Period') OR define('T_SystemDocumentPeriod_Period', 't9051f002');
defined('T_SystemDocumentPeriod_LastNo') OR define('T_SystemDocumentPeriod_LastNo', 't9051f003');
defined('T_SystemDocumentPeriod_IsCurrent') OR define('T_SystemDocumentPeriod_IsCurrent', 't9051f004');

defined('T_StockTakeLog') OR define('T_StockTakeLog','t7010');
defined('T_StockTakeLog_RecordID') OR define('T_StockTakeLog_RecordID', 't7010r001');
defined('T_StockTakeLog_RecordTimestamp') OR define('T_StockTakeLog_RecordTimestamp', 't7010r002');
defined('T_StockTakeLog_RecordStatus') OR define('T_StockTakeLog_RecordStatus', 't7010r003');
defined('T_StockTakeLog_DocNo') OR define('T_StockTakeLog_DocNo', 't7010f001');
defined('T_StockTakeLog_SN') OR define('T_StockTakeLog_SN', 't7010f002');
defined('T_StockTakeLog_StockFlag') OR define('T_StockTakeLog_StockFlag', 't7010f003');

defined('T_Alert') OR define('T_Alert','t7011');
defined('T_Alert_RecordID') OR define('T_Alert_RecordID', 't7011r001');
defined('T_Alert_RecordTimestamp') OR define('T_Alert_RecordTimestamp', 't7011r002');
defined('T_Alert_RecordStatus') OR define('T_Alert_RecordStatus', 't7011r003');
defined('T_Alert_DocNo') OR define('T_Alert_DocNo', 't7011f001');
defined('T_Alert_EPC') OR define('T_Alert_EPC', 't7011f002');
defined('T_Alert_Qty') OR define('T_Alert_Qty', 't7011f003');
defined('T_Alert_LocationID') OR define('T_Alert_LocationID', 't7011f004');