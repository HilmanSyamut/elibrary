<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?php echo APPNAME." | Login"; ?></title>
		<base href="<?php echo base_url(); ?>" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/backend/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/backend/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/backend/vendor/modernizr/modernizr.js"></script>
	</head>
	<body style="background: #1d2127;">
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="" class="logo pull-left">
					<img src="assets/backend/images/logos.png" height="65" alt="" />
				</a>
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
					</div>
					<div class="panel-body">
						<?php if ($this->input->post('action') != 'recover_password'): ?>
			                <div class="alert alert-error" style="<?php echo ($form_error) ? '' : 'display:none;'?>">
			                  <button data-dismiss="alert" class="close"></button>
			                  <?php echo $form_error; ?>
			                </div>
			            <?php endif; ?>
						<?php echo form_open('',array('id'=>'login-form', 'class'=>'login-form')); ?>
							<div class="form-group mb-lg">
								<label>Username</label>
								<div class="input-group input-group-icon">
									<?php 
				                    $attr = array(
				                      'name' => 'username',
				                      'id' => 'txtusername',
				                      'class' => 'form-control input-lg'
				                    );
				                    echo form_input($attr);
				                    ?> 
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Password</label>
								</div>
								<div class="input-group input-group-icon">
									<?php 
				                    $attr = array(
				                      'name' => 'password',
				                      'id' => 'txtpassword',
				                      'class' => 'form-control input-lg'
				                    );
				                    echo form_password($attr);
				                    ?>  
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
								</div>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="assets/backend/vendor/jquery/jquery.js"></script>
		<script src="assets/backend/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/backend/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/backend/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/backend/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/backend/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/backend/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/backend/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/backend/javascripts/theme.init.js"></script>
		<script src="assets/js/login.js" type="text/javascript"></script>
	</body>
</html>
