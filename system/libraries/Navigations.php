<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


// ------------------------------------------------------------------------
class CI_Navigations
{
    /**
     * CodeIgniter Instance
     * @access private
     */    
    private $ci;
    // --------------------------------------------------------------------
    /**
     * Constructor
     * @access	public
     */
    public function __construct()
    {
        $this->ci =& get_instance();
        #$this->generate();
    }
    /**
     * Generate Navigations Array
     * @access	public
     */    
    public function generate()
    {
        $role_id = $this->ci->ezrbac->getCurrentUser()->{T_SystemUser_user_role_id};
        $this->ci->load->model("navigations/navigation_model");
        $data['nav'] = $this->ci->navigation_model->nav_backend();
        $data['user'] = $this->ci->ezrbac->getCurrentUser();
        $data['user_role'] = $role_id;
        $data['access'] = $this->ci->navigation_model->getDataAccess($data['user']->{T_SystemUser_RecordID},$role_id);
        $this->ci->load->view("navigation/main",$data);
    }

    public function generate_site()
    {
        $this->ci->load->model("navigations/navigation_model");
        $data['nav'] = $this->ci->navigation_model->get_data();
        $this->ci->load->view("navigation/frontpage/main",$data);
    }

    public function header()
    {
        $id = $this->ci->ezrbac->getCurrentUserID();
        $this->ci->load->model("navigations/navigation_model");
    	$data = $this->ci->navigation_model->user_data($id);
        $data['user'] = $this->ci->ezrbac->getCurrentUser();
        $data['role'] =$this->ci->navigation_model->lookup($data['user']->{T_SystemUser_user_role_id},T_SystemUserRole,T_SystemUserRole_RecordID,T_SystemUserRole_role_name);
    	$this->ci->load->view("navigation/header",$data);
    } 
    
    public function breadcrumb()
    {
        $this->ci->load->model("navigations/navigation_model");
        $data = $this->ci->navigation_model->breadcrumb();
        $this->ci->load->view("layouts/breadcrumb",$data);
    }
}
