<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Custom
 *
 * Application development framework for PHP 5.1.6 or newer
 *
 * @package	     Widgets
 * @author	     Muhammad Arief R
 * @copyright	 Copyright (c) 2014 Creative Plus Studio
 * @license
 * @link
 *
 **/
// ------------------------------------------------------------------------
class CI_Widgets
{
    /**
     * CodeIgniter Instance
     * @access private
     */
    private $ci;
    // --------------------------------------------------------------------
    /**
     * Constructor
     * @access	public
     */
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('/widget/widget_model');
    }
    /**
     * Generate social widget
     * @access	public
     */
    public function social()
    {
        $data['social'] = $this->ci->widget_model->social_account();
        $this->ci->load->view("widgets/social",$data);
    }

    /**
     * Generate social mobile widget
     * @access  public
     */
    public function social_mobile()
    {
        $this->ci->load->view("widgets/social_mobile");
    }


}
