 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Application development framework for PHP 5.1.6 or newer
 *
 * @package	     Sidebar
 * @author	     Muhammad Arief R
 * @contibutor   -
 * @copyright	 Copyright (c) 2014 Creative Plus Studio
 * @license
 * @link         http://muhammad-arief.com
 *
 **/
// ------------------------------------------------------------------------
class CI_Sidebar
{
    /**
     * CodeIgniter Instance
     * @access private
     */
    private $ci;
    // --------------------------------------------------------------------
    /**
     * Constructor
     * @access	public
     */
    public function __construct()
    {
        $this->ci =& get_instance();
    }
    /**
     * Generate Sidebar
     * @access	public
     */
    public function generate()
    {
        $this->ci->load->view("sidebar/sidebar");
    }
    /**
     * Generate Chat on sidebar
     * @access	public
     */
    public function chat()
    {
        $id = $this->ci->ezrbac->getCurrentUser()->id;
        $this->ci->load->model("sidebar/chat_model");
        $data['user'] = $this->ci->chat_model->data_user($id);
        $data['users'] = $this->ci->chat_model->get_users($id);
        $data['user_id'] = $id;
        $this->ci->load->view("sidebar/chat",$data);
    }
    /**
     * Generate Activity on sidebar
     * @access	public
     */
    public function activities($in_page='')
    {
        $id = $this->ci->ezrbac->getCurrentUser()->id;
        $this->ci->load->model("sidebar/aktivity_model");
        $data['list'] = $this->ci->aktivity_model->get_list($id);
        $data['user_id'] = $id;
        $data['inpage'] = $in_page;
        $this->ci->load->view("sidebar/activities",$data);
    }
	/**
     * Generate Setting on sidebar
     * @access	public
     */
    public function setting()
    {
        $id = $this->ci->ezrbac->getCurrentUser()->id;
        $this->ci->load->model("sidebar/aktivity_model");
        $data['user_id'] = $id;
        $this->ci->load->view("sidebar/setting",$data);
    }

}
