<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */
if ( ! function_exists('form_open'))
{
	function form_open($action = '', $attributes = '', $hidden = array())
	{
		$CI =& get_instance();

		if ($attributes == '')
		{
			$attributes = 'method="post"';
		}

		// If an action is not a full URL then turn it into one
		if ($action && strpos($action, '://') === FALSE)
		{
			$action = $CI->config->site_url($action);
		}

		// If no action is provided then set to the current url
		$action OR $action = $CI->config->site_url($CI->uri->uri_string());

		$form = '<form action="'.$action.'"';

		$form .= _attributes_to_string($attributes, TRUE);

		$form .= '>';

		// Add CSRF field if enabled, but leave it out for GET requests and requests to external websites
		if ($CI->config->item('csrf_protection') === TRUE AND ! (strpos($action, $CI->config->base_url()) === FALSE OR strpos($form, 'method="get"')))
		{
			$hidden[$CI->security->get_csrf_token_name()] = $CI->security->get_csrf_hash();
		}

		if (is_array($hidden) AND count($hidden) > 0)
		{
			$form .= sprintf("<div style=\"display:none\">%s</div>", form_hidden($hidden));
		}

		return $form;
	}
}

// ------------------------------------------------------------------------

/**
 * Form Declaration - Multipart type
 *
 * Creates the opening portion of the form, but with "multipart/form-data".
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */
if ( ! function_exists('form_open_multipart'))
{
	function form_open_multipart($action = '', $attributes = array(), $hidden = array())
	{
		if (is_string($attributes))
		{
			$attributes .= ' enctype="multipart/form-data"';
		}
		else
		{
			$attributes['enctype'] = 'multipart/form-data';
		}

		return form_open($action, $attributes, $hidden);
	}
}

// ------------------------------------------------------------------------

/**
 * Hidden Input Field
 *
 * Generates hidden fields.  You can pass a simple key/value string or an associative
 * array with multiple values.
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_hidden'))
{
	function form_hidden($name, $value = '', $recursing = FALSE)
	{
		static $form;

		if ($recursing === FALSE)
		{
			$form = "\n";
		}

		if (is_array($name))
		{
			foreach ($name as $key => $val)
			{
				form_hidden($key, $val, TRUE);
			}
			return $form;
		}

		if ( ! is_array($value))
		{
			$form .= '<input type="hidden" name="'.$name.'" value="'.form_prep($value, $name).'" />'."\n";
		}
		else
		{
			foreach ($value as $k => $v)
			{
				$k = (is_int($k)) ? '' : $k;
				form_hidden($name.'['.$k.']', $v, TRUE);
			}
		}

		return $form;
	}
}

// ------------------------------------------------------------------------

/**
 * Text Input Field
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_input'))
{
	function form_input($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'text', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}
}

// ------------------------------------------------------------------------

/**
 * Password Field
 *
 * Identical to the input function but adds the "password" type
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_password'))
{
	function form_password($data = '', $value = '', $extra = '')
	{
		if ( ! is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'password';
		return form_input($data, $value, $extra);
	}
}

// ------------------------------------------------------------------------

/**
 * Upload Field
 *
 * Identical to the input function but adds the "file" type
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_upload'))
{
	function form_upload($data = '', $value = '', $extra = '')
	{
		if ( ! is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'file';
		return form_input($data, $value, $extra);
	}
}

// ------------------------------------------------------------------------

/**
 * Textarea field
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_textarea'))
{
	function form_textarea($data = '', $value = '', $extra = '')
	{
		$defaults = array('name' => (( ! is_array($data)) ? $data : ''), 'cols' => '40', 'rows' => '10');

		if ( ! is_array($data) OR ! isset($data['value']))
		{
			$val = $value;
		}
		else
		{
			$val = $data['value'];
			unset($data['value']); // textareas don't use the value attribute
		}

		$name = (is_array($data)) ? $data['name'] : $data;
		return "<textarea "._parse_form_attributes($data, $defaults).$extra.">".form_prep($val, $name)."</textarea>";
	}
}

// ------------------------------------------------------------------------

/**
 * Multi-select menu
 *
 * @access	public
 * @param	string
 * @param	array
 * @param	mixed
 * @param	string
 * @return	type
 */
if ( ! function_exists('form_multiselect'))
{
	function form_multiselect($name = '', $options = array(), $selected = array(), $extra = '')
	{
		if ( ! strpos($extra, 'multiple'))
		{
			$extra .= ' multiple="multiple"';
		}

		return form_dropdown($name, $options, $selected, $extra);
	}
}

// --------------------------------------------------------------------

/**
 * Drop-down Menu
 *
 * @access	public
 * @param	string
 * @param	array
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_dropdown'))
{
	function form_dropdown($name = '', $options = array(), $selected = array(), $extra = '')
	{
		if ( ! is_array($selected))
		{
			$selected = array($selected);
		}

		// If no selected state was submitted we will attempt to set it automatically
		if (count($selected) === 0)
		{
			// If the form name appears in the $_POST array we have a winner!
			if (isset($_POST[$name]))
			{
				$selected = array($_POST[$name]);
			}
		}

		if ($extra != '') $extra = ' '.$extra;

		$multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

		$form = '<select name="'.$name.'"'.$extra.$multiple.">\n";

		foreach ($options as $key => $val)
		{
			$key = (string) $key;

			if (is_array($val) && ! empty($val))
			{
				$form .= '<optgroup label="'.$key.'">'."\n";

				foreach ($val as $optgroup_key => $optgroup_val)
				{
					$sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

					$form .= '<option value="'.$optgroup_key.'"'.$sel.'>'.(string) $optgroup_val."</option>\n";
				}

				$form .= '</optgroup>'."\n";
			}
			else
			{
				$sel = (in_array($key, $selected)) ? ' selected="selected"' : '';

				$form .= '<option value="'.$key.'"'.$sel.'>'.(string) $val."</option>\n";
			}
		}

		$form .= '</select>';

		return $form;
	}
}

// ------------------------------------------------------------------------

/**
 * Checkbox Field
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	bool
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_checkbox'))
{
	function form_checkbox($data = '', $value = '', $checked = FALSE, $extra = '')
	{
		$defaults = array('type' => 'checkbox', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		if (is_array($data) AND array_key_exists('checked', $data))
		{
			$checked = $data['checked'];

			if ($checked == FALSE)
			{
				unset($data['checked']);
			}
			else
			{
				$data['checked'] = 'checked';
			}
		}

		if ($checked == TRUE)
		{
			$defaults['checked'] = 'checked';
		}
		else
		{
			unset($defaults['checked']);
		}

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}
}

// ------------------------------------------------------------------------

/**
 * Radio Button
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	bool
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_radio'))
{
	function form_radio($data = '', $value = '', $checked = FALSE, $extra = '')
	{
		if ( ! is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'radio';
		return form_checkbox($data, $value, $checked, $extra);
	}
}

// ------------------------------------------------------------------------

/**
 * Submit Button
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_submit'))
{
	function form_submit($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'submit', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}
}

// ------------------------------------------------------------------------

/**
 * Reset Button
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_reset'))
{
	function form_reset($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'reset', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}
}

// ------------------------------------------------------------------------

/**
 * Form Button
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_button'))
{
	function form_button($data = '', $content = '', $extra = '')
	{
		$defaults = array('name' => (( ! is_array($data)) ? $data : ''), 'type' => 'button');

		if ( is_array($data) AND isset($data['content']))
		{
			$content = $data['content'];
			unset($data['content']); // content is not an attribute
		}

		return "<button "._parse_form_attributes($data, $defaults).$extra.">".$content."</button>";
	}
}

// ------------------------------------------------------------------------

/**
 * Form Label Tag
 *
 * @access	public
 * @param	string	The text to appear onscreen
 * @param	string	The id the label applies to
 * @param	string	Additional attributes
 * @return	string
 */
if ( ! function_exists('form_label'))
{
	function form_label($label_text = '', $id = '', $attributes = array(), $required= '')
	{

		$label = '<label';

		if ($id != '')
		{
			$label .= " for=\"$id\"";
		}
        
        if ($required != '')
		{
			$required = "<span class='required'>*</span>";
		}

		if (is_array($attributes) AND count($attributes) > 0)
		{
			foreach ($attributes as $key => $val)
			{
				$label .= ' '.$key.'="'.$val.'"';
			}
		}

		$label .= ">$label_text".(!empty($required) ? $required : '' )."</label>";

		return $label;
	}
}

// ------------------------------------------------------------------------
/**
 * Fieldset Tag
 *
 * Used to produce <fieldset><legend>text</legend>.  To close fieldset
 * use form_fieldset_close()
 *
 * @access	public
 * @param	string	The legend text
 * @param	string	Additional attributes
 * @return	string
 */
if ( ! function_exists('form_fieldset'))
{
	function form_fieldset($legend_text = '', $attributes = array())
	{
		$fieldset = "<fieldset";

		$fieldset .= _attributes_to_string($attributes, FALSE);

		$fieldset .= ">\n";

		if ($legend_text != '')
		{
			$fieldset .= "<legend>$legend_text</legend>\n";
		}

		return $fieldset;
	}
}

// ------------------------------------------------------------------------

/**
 * Fieldset Close Tag
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_fieldset_close'))
{
	function form_fieldset_close($extra = '')
	{
		return "</fieldset>".$extra;
	}
}

// ------------------------------------------------------------------------

/**
 * Form Close Tag
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_close'))
{
	function form_close($extra = '')
	{
		return "</form>".$extra;
	}
}

// ------------------------------------------------------------------------

/**
 * Form Prep
 *
 * Formats text so that it can be safely placed in a form field in the event it has HTML tags.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_prep'))
{
	function form_prep($str = '', $field_name = '')
	{
		static $prepped_fields = array();

		// if the field name is an array we do this recursively
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = form_prep($val);
			}

			return $str;
		}

		if ($str === '')
		{
			return '';
		}

		// we've already prepped a field with this name
		// @todo need to figure out a way to namespace this so
		// that we know the *exact* field and not just one with
		// the same name
		if (isset($prepped_fields[$field_name]))
		{
			return $str;
		}

		$str = htmlspecialchars($str);

		// In case htmlspecialchars misses these.
		$str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

		if ($field_name != '')
		{
			$prepped_fields[$field_name] = $field_name;
		}

		return $str;
	}
}

// ------------------------------------------------------------------------

/**
 * Form Value
 *
 * Grabs a value from the POST array for the specified field so you can
 * re-populate an input field or textarea.  If Form Validation
 * is active it retrieves the info from the validation class
 *
 * @access	public
 * @param	string
 * @return	mixed
 */
if ( ! function_exists('set_value'))
{
	function set_value($field = '', $default = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			if ( ! isset($_POST[$field]))
			{
				return $default;
			}

			return form_prep($_POST[$field], $field);
		}

		return form_prep($OBJ->set_value($field, $default), $field);
	}
}

// ------------------------------------------------------------------------

/**
 * Set Select
 *
 * Let's you set the selected value of a <select> menu via data in the POST array.
 * If Form Validation is active it retrieves the info from the validation class
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	bool
 * @return	string
 */
if ( ! function_exists('set_select'))
{
	function set_select($field = '', $value = '', $default = FALSE)
	{
		$OBJ =& _get_validation_object();

		if ($OBJ === FALSE)
		{
			if ( ! isset($_POST[$field]))
			{
				if (count($_POST) === 0 AND $default == TRUE)
				{
					return ' selected="selected"';
				}
				return '';
			}

			$field = $_POST[$field];

			if (is_array($field))
			{
				if ( ! in_array($value, $field))
				{
					return '';
				}
			}
			else
			{
				if (($field == '' OR $value == '') OR ($field != $value))
				{
					return '';
				}
			}

			return ' selected="selected"';
		}

		return $OBJ->set_select($field, $value, $default);
	}
}

// ------------------------------------------------------------------------

/**
 * Set Checkbox
 *
 * Let's you set the selected value of a checkbox via the value in the POST array.
 * If Form Validation is active it retrieves the info from the validation class
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	bool
 * @return	string
 */
if ( ! function_exists('set_checkbox'))
{
	function set_checkbox($field = '', $value = '', $default = FALSE)
	{
		$OBJ =& _get_validation_object();

		if ($OBJ === FALSE)
		{
			if ( ! isset($_POST[$field]))
			{
				if (count($_POST) === 0 AND $default == TRUE)
				{
					return ' checked="checked"';
				}
				return '';
			}

			$field = $_POST[$field];

			if (is_array($field))
			{
				if ( ! in_array($value, $field))
				{
					return '';
				}
			}
			else
			{
				if (($field == '' OR $value == '') OR ($field != $value))
				{
					return '';
				}
			}

			return ' checked="checked"';
		}

		return $OBJ->set_checkbox($field, $value, $default);
	}
}

// ------------------------------------------------------------------------

/**
 * Set Radio
 *
 * Let's you set the selected value of a radio field via info in the POST array.
 * If Form Validation is active it retrieves the info from the validation class
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	bool
 * @return	string
 */
if ( ! function_exists('set_radio'))
{
	function set_radio($field = '', $value = '', $default = FALSE)
	{
		$OBJ =& _get_validation_object();

		if ($OBJ === FALSE)
		{
			if ( ! isset($_POST[$field]))
			{
				if (count($_POST) === 0 AND $default == TRUE)
				{
					return ' checked="checked"';
				}
				return '';
			}

			$field = $_POST[$field];

			if (is_array($field))
			{
				if ( ! in_array($value, $field))
				{
					return '';
				}
			}
			else
			{
				if (($field == '' OR $value == '') OR ($field != $value))
				{
					return '';
				}
			}

			return ' checked="checked"';
		}

		return $OBJ->set_radio($field, $value, $default);
	}
}

// ------------------------------------------------------------------------

/**
 * Form Error
 *
 * Returns the error for a specific form field.  This is a helper for the
 * form validation class.
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('form_error'))
{
	function form_error($field = '', $prefix = '', $suffix = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}

		return $OBJ->error($field, $prefix, $suffix);
	}
}

// ------------------------------------------------------------------------

/**
 * Validation Error String
 *
 * Returns all the errors associated with a form submission.  This is a helper
 * function for the form validation class.
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('validation_errors'))
{
	function validation_errors($prefix = '', $suffix = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}

		return $OBJ->error_string($prefix, $suffix);
	}
}

// ------------------------------------------------------------------------

/**
 * Parse the form attributes
 *
 * Helper function used by some of the form helpers
 *
 * @access	private
 * @param	array
 * @param	array
 * @return	string
 */
if ( ! function_exists('_parse_form_attributes'))
{
	function _parse_form_attributes($attributes, $default)
	{
		if (is_array($attributes))
		{
			foreach ($default as $key => $val)
			{
				if (isset($attributes[$key]))
				{
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}

			if (count($attributes) > 0)
			{
				$default = array_merge($default, $attributes);
			}
		}

		$att = '';

		foreach ($default as $key => $val)
		{
			if ($key == 'value')
			{
				$val = form_prep($val, $default['name']);
			}

			$att .= $key . '="' . $val . '" ';
		}

		return $att;
	}
}

// ------------------------------------------------------------------------

/**
 * Attributes To String
 *
 * Helper function used by some of the form helpers
 *
 * @access	private
 * @param	mixed
 * @param	bool
 * @return	string
 */
if ( ! function_exists('_attributes_to_string'))
{
	function _attributes_to_string($attributes, $formtag = FALSE)
	{
		if (is_string($attributes) AND strlen($attributes) > 0)
		{
			if ($formtag == TRUE AND strpos($attributes, 'method=') === FALSE)
			{
				$attributes .= ' method="post"';
			}

			if ($formtag == TRUE AND strpos($attributes, 'accept-charset=') === FALSE)
			{
				$attributes .= ' accept-charset="'.strtolower(config_item('charset')).'"';
			}

		return ' '.$attributes;
		}

		if (is_object($attributes) AND count($attributes) > 0)
		{
			$attributes = (array)$attributes;
		}

		if (is_array($attributes) AND count($attributes) > 0)
		{
			$atts = '';

			if ( ! isset($attributes['method']) AND $formtag === TRUE)
			{
				$atts .= ' method="post"';
			}

			if ( ! isset($attributes['accept-charset']) AND $formtag === TRUE)
			{
				$atts .= ' accept-charset="'.strtolower(config_item('charset')).'"';
			}

			foreach ($attributes as $key => $val)
			{
				$atts .= ' '.$key.'="'.$val.'"';
			}

			return $atts;
		}
	}
}

// ------------------------------------------------------------------------

/**
 * Validation Object
 *
 * Determines what the form validation class was instantiated as, fetches
 * the object and returns it.
 *
 * @access	private
 * @return	mixed
 */
if ( ! function_exists('_get_validation_object'))
{
	function &_get_validation_object()
	{
		$CI =& get_instance();

		// We set this as a variable since we're returning by reference.
		$return = FALSE;

		if (FALSE !== ($object = $CI->load->is_loaded('form_validation')))
		{
			if ( ! isset($CI->$object) OR ! is_object($CI->$object))
			{
				return $return;
			}

			return $CI->$object;
		}

		return $return;
	}
}

// ------------------------------------------------------------------------

/**
 * Telerik GridView Helper
 *
 * @param	array
 * @access	public
 * @return	object element
 */
if ( ! function_exists('gridViewMaster'))
{
	function gridViewMaster($data)
	{
        $confirm  = '<div id="'.$data['id'].'confirm"></div>';
        $confirm .= '<script type="text/x-kendo-template" id="'.$data['id'].'confirmTemplate">';
        $confirm .= '<div class="k-edit-form-container">';
        foreach($data['column'] as $column => $attr){
            if($attr[1]){
                $confirm .= '<div class="k-edit-label"><label for="'.$column.'">'.$attr[3].'</label></div>';
                $confirm .= '<div class="k-edit-field">#= '.$column.' #</div>';
            }
        }
        $confirm .= '<div class="k-edit-buttons k-state-default"><button class="k-button" id="yesButton">Yes</button><button class="k-button" id="noButton">No</button></div></div>';
        $confirm .= '</script>';
		$html = '<div id="'.$data['id'].'"></div>';
        $js = '<script type="text/javascript">
                $(document).ready(function() {
                    var column = 
                    [';
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='"width":"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='"title":"'.$attr[3].'",'; }
                            $js .='"field":"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='];
                    var data = {
                        id : "'.$data['id'].'",
                        height: "500px",
                        columnz: column,
                        DList : {';
                        if(isset($data['dropdownlist'])){
                            foreach($data['dropdownlist'] as $column => $attr){
                                $js .= $column.': { text: "'.$attr[0].'", url: "'.$attr[1].'"},'; 
                            }
                        }
                    $js .='},
                    	table: "'.$data['table'].'",
                        url: {
                            create : "'.$data["url"]["create"].'",
                            read : "'.$data["url"]["read"].'",
                            update : "'.$data["url"]["update"].'",
                            destroy : "'.$data["url"]["destroy"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" },';
                                }
                            }
                    $js .='}
                    }
                    gridMasterA(data);
                });
            </script>';
        
        $popup = '<script id="custom_popup" type="text/x-kendo-template">';
        foreach($data['column'] as $column => $attr){
            if($attr[0]){
                $required = ($attr[6]) ? 'required' : '';
                $popup .='<div class="k-edit-label"><label for="'.$column.'">'.$attr[3].'</label></div>';
                $popup .='<div class="k-edit-field"><input type="text" class="k-input k-textbox" id="'.$column.'" name="'.$column.'" '.$required.'></div>'; 
            }
        }
        $popup .= '</script>';
        
        $return = $html.$js.$popup.$confirm;
		return $return;
	}
}

// ------------------------------------------------------------------------

/**
 * Telerik GridView Helper
 *
 * @param	array
 * @access	public
 * @return	object element
 */
if ( ! function_exists('gridViewGeneral'))
{
	function gridViewGeneral($data)
	{
        $confirm  = '<div id="'.$data['id'].'confirm"></div>';
        $confirm .= '<script type="text/x-kendo-template" id="'.$data['id'].'confirmTemplate">';
        $confirm .= '<div class="k-edit-form-container">';
        foreach($data['column'] as $column => $attr){
            if($attr[1]){
                $confirm .= '<div class="k-edit-label"><label for="'.$column.'">'.$attr[3].'</label></div>';
                $confirm .= '<div class="k-edit-field">#= '.$column.' #</div>';
            }
        }
        $confirm .= '<div class="k-edit-buttons k-state-default"><button class="k-button" id="yesButton">Yes</button><button class="k-button" id="noButton">No</button></div></div>';
        $confirm .= '</script>';
		$html = '<div id="'.$data['id'].'"></div>';
        $js = '<script type="text/javascript">
                $(document).ready(function() {
                    var column = 
                    [';
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='"width":"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='"title":"'.$attr[3].'",'; }
                            $js .='"field":"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='];
                    var data = {
                        id : "'.$data['id'].'",
                        height: "500px",
                        columnz: column,
                        url: {
                            create : "'.$data["url"]["create"].'",
                            read : "'.$data["url"]["read"].'",
                            update : "'.$data["url"]["update"].'",
                            destroy : "'.$data["url"]["destroy"].'",
                            form : "'.$data["url"]["form"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                $format = ($attr[5]=='datetime') ? ',format:"{0:MM-dd-yyyy}"' : "";
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" '.$format.' },';
                                }
                            }
                    $js .='}
                    }
                    gridViewGeneral(data);
                });
            </script>';
        
        $return = $html.$js.$confirm;
		return $return;
	}
}

if ( ! function_exists('simpleGridView'))
{
	function simpleGridView($data)
	{
		$actBTN = isset($data['actBTN']) ? $data['actBTN'] : "55px";
		$postBTN = isset($data['postBTN']) ? $data['postBTN'] : "25px";
		
		$html = '<div id="'.$data['id'].'"></div>
    	<div id="ActAdd" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <label for="undefined"></label>
	                <input type="hidden" id="RecordID">
	            </div><div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
		            	$col = substr($column, 5);
		            	$width = str_replace('px', '', $attr[2]) * 2;
		            	$width = $width."px";
		            	$upper = ($col==="f001") ? 'style="text-transform: uppercase; width:'.$width.';"' : 'style="width:'.$width.';"';
			            $html .= '<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'" '.$upper.' class="k-input k-textbox">
			            </div>';
		        	}
		        }
        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonAdd">Yes</button>
	                <button class="k-button" id="noButtonAdd">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActEdit" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <input id="RecordID_Edit" type="hidden" class="k-input k-textbox">
	                <input id="RecordTimeStamp_Edit" type="hidden" class="k-input k-textbox">
	            </div>
	            <div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
		            	$editable = ($attr[4]) ? "readonly" : "";
		            	$width = str_replace('px', '', $attr[2]) * 2;
		            	$width = $width."px";
		            	$upper = ($col==="f001") ? 'style="text-transform: uppercase; width:'.$width.';"' : 'style="width:'.$width.';"';
		        		$html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'_Edit" '.$upper.' '.$editable.' class="k-input k-textbox">
			            </div>';
			        }
	        	}
        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonEdit">Yes</button>
	                <button class="k-button" id="noButtonEdit">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActDelete" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <label for="undefined"></label>
	                <input type="hidden" id="RecordID_Delete">
	                <input type="hidden" id="RecordTimeStamp_Delete">
	            </div><div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
			            $html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'_Delete" readonly class="k-input k-textbox">
			            </div>';
			        }
		        }
	        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonDelete">Yes</button>
	                <button class="k-button" id="noButtonDelete">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActDetail" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
			            $html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <p id="'.$id.'_Detail"></p>
			            </div>';
			        }
	        	}
	            $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="noButtonDetail">Close</button>
	            </div>
	        </div>
	    </div>';

	    $js = '<script type="text/javascript">
                $(document).ready(function() {
                	var column = 
                    [';
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='"width":"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='"title":"'.$attr[3].'",'; }
                            $js .='"field":"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='];
                    var data = {
                        id : "'.$data['id'].'",
                        height: "500px",
                        actBTN: "'.$actBTN.'",
                        postBTN: "'.$postBTN.'",
                        table: "'.$data['table'].'",
                        tools: [';
                        foreach($data['tools'] as $tool){
		                    $js .='"'.$tool.'",';
		                }
                    $js.='],
                        columnz: column,
                     	DList : {';
                        if(isset($data['dropdownlist'])){
                            foreach($data['dropdownlist'] as $column => $attr){
                                $js .= $column.': { text: "'.$attr[0].'", key: "'.$attr[2].'", url: "'.$attr[1].'"},'; 
                            }
                        }
                $js .='},
           		 		url: {
                            create : "'.$data["url"]["create"].'",
                            read : "'.$data["url"]["read"].'",
                            update : "'.$data["url"]["update"].'",
                            destroy : "'.$data["url"]["destroy"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                $format = ($attr[5]=='datetime') ? ',format:"{0:dd-MM-yyyy}"' : "";
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" '.$format.' },';
                                }
                            }
                    $js .='}
                    }
                    gridViewBasic(data);
                });

                $("#yesButtonAdd").click(function(){
			        var voData = {';
			        foreach($data['column'] as $column => $attr){
		        		$id = str_replace(' ', '',$attr[3]);
			           	$js .= $id.': $("#'.$id.'").val(),';
			       }
	       	$js .='};
			        var valid = checkForm(voData);
			        if(valid.valid)
			        {
			           $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["create"].'"),
			            success: function (result) {                
			                if (result.errorcode > 0) {
			                new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                } else {
			                new PNotify({ title: "success", text: "Save data success", type: "success", shadow: true });';
			                foreach($data['column'] as $column => $attr){
				        		$id = str_replace(' ', '',$attr[3]);
					           	$js .= '
					           			$("#'.$id.'").val("");					           			
					           			if($("#'.$id.'").data("kendoNumericTextBox")){ $("#'.$id.'").data("kendoNumericTextBox").value(""); }
					           			if($("#'.$id.'").data("kendoDropDownList")){ $("#'.$id.'").data("kendoDropDownList").value(""); }
					           		';
					       }
		             $js .='$("#'.$data['id'].'").data("kendoGrid").dataSource.read();
			                $("#'.$data['id'].'").data("kendoGrid").refresh();
			                }
			            },
			            error: function (jqXHR, textStatus, errorThrown) {
			                alert(jQuery.parseJSON(jqXHR.responseText));
			            }
			        });       
			        }else{
			            new PNotify({ title: "Form Validation", text: valid.msg, type: "error", shadow: true });
			        }
			    });

			    $("#yesButtonEdit").click(function(){
	            	var voData = {';
	            	foreach($data['column'] as $column => $attr){
		        		$id = str_replace(' ', '',$attr[3]);
			           	$js .= $id.': $("#'.$id.'_Edit").val(),';
			       }
       		$js.='};

			        var valid = checkForm(voData);
			        if(valid.valid)
			        {
			           $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["update"].'"),
			            success: function (result) {                
			                if (result.errorcode > 0) {
			                    new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                } else {
			                $("#ActEdit").closest("[data-role=window]").data("kendoWindow").close();
			                $("#'.$data['id'].'").data("kendoGrid").dataSource.read();
			                $("#'.$data['id'].'").data("kendoGrid").refresh();
			                }
			            },
			            error: function (jqXHR, textStatus, errorThrown) {
			                alert(jQuery.parseJSON(jqXHR.responseText));
			            }
			        });        
			       }else{
			            new PNotify({ title: "Form Validation", text: valid.msg, type: "error", shadow: true });
			        }
			    });

			    function checkForm(voData) {
		            var valid = 1;
		            var msg = "";';
		            foreach($data['column'] as $column => $attr){
		            	$col = substr($column, 5);
		            	if($col !== "r001"){
			            	if($attr[6]){
				        		$id = str_replace(' ', '',$attr[3]);
					            $js.='if (voData.'.$id.' == "") { valid = 0; msg += "'.$attr[3].' is required" + "\r\n"; }';
					        }
					    }
		            }
		        $js .='var voRes = {
		                valid: valid,
		                msg: msg
		            }
		            return voRes;
			    }

                function actButton(column){
			        if(column){
			            if(column[0] == "Delete"){                    
			                $("#ActDelete").attr("style", "display:");
			                $("#ActDelete").data("kendoWindow").center().open();
			                $(".k-window-title").text("Delete");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Delete").val(column['.$i.']);';
					            $i++;
					        }
			     $js .='}else if(column[0] == "Detail"){            
			                $("#ActDetail").attr("style", "display:");
			                $("#ActDetail").data("kendoWindow").center().open();
			                $(".k-window-title").text("Detail");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Detail").text(column['.$i.']);';
					            $i++;
					        }
			     $js .='}  else if(column[0] == "Edit"){            
			                $("#ActEdit").attr("style", "display:");
			                $("#ActEdit").data("kendoWindow").center().open();
			                $(".k-window-title").text("Edit");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
			                	$property = "{";
			                	if(!empty($attr[7])){
			                		foreach ($attr[7] as $key => $value) {
			                			$property .= $value["property"].":".$value["value"].",";
			                		}
			                	}
			                	$property .= "}";
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Edit").val(column['.$i.']);';
				                
						        $js .='if("'.$attr[5].'"=="int"){
						        	$("#'.$id.'_Edit").removeClass("k-textbox");
						        	$("#'.$id.'_Edit").kendoNumericTextBox('.$property.');
						        }';
						        $js .='if("'.$attr[5].'"=="datetime"){
						        	$("#'.$id.'_Edit").removeClass("k-textbox");
						        	$("#'.$id.'_Edit").kendoDatePicker({format:DateFormat});
						        }';
						        $js .='if($("#'.$id.'_Edit").data("kendoDropDownList")){
				                	$("#'.$id.'_Edit").data("kendoDropDownList").value(column['.$i.']);
				                	$("#'.$id.'_Edit").data("kendoDropDownList").text(column['.$i.']);
						        }';
					            $i++;
					        }
			     $js .='}
					}else{
			            $("#ActAdd").attr("style", "display:");
			            $("#ActAdd").data("kendoWindow").center().open();
			            $(".k-window-title").text("Add");';
			            $i=1;
		                foreach($data['column'] as $column => $attr){
		                	$property = "{";
		                	if(!empty($attr[7])){
		                		foreach ($attr[7] as $key => $value) {
		                			$property .= $value["property"].":".$value["value"].",";
		                		}
		                	}
		                	$property .= "}";


			            	$id = str_replace(' ', '',$attr[3]);
			                $js .='$("#'.$id.'").val("");';
			                $js .='if($("#'.$id.'").data("kendoDropDownList")){
					            $("#'.$id.'").data("kendoDropDownList").value("");
					        }';
					        $js .='if("'.$attr[5].'"=="int"){
					        	$("#'.$id.'").removeClass("k-textbox");
					        	$("#'.$id.'").kendoNumericTextBox('.$property.');
					        }';
					        $js .='if("'.$attr[5].'"=="datetime"){
						        	$("#'.$id.'").removeClass("k-textbox");
						        	$("#'.$id.'").kendoDatePicker({format:DateFormat});
						        }';
				            $i++;
				        }
			    $js .='}
			    }
            </script>';

	    $return = $html.$js;
		return $return;
	}
}

if ( ! function_exists('simpleGridViewCustom'))
{
	function simpleGridViewCustom($data)
	{
		$actBTN = isset($data['actBTN']) ? $data['actBTN'] : "55px";
		$postBTN = isset($data['postBTN']) ? $data['postBTN'] : "25px";
		
		$html = '<div id="'.$data['id'].'"></div>
    	<div id="ActAdd" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <label for="undefined"></label>
	                <input type="hidden" id="RecordID">
	            </div><div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
		            	$col = substr($column, 5);
		            	$width = str_replace('px', '', $attr[2]) * 2;
		            	$width = $width."px";
		            	$upper = ($col==="f001") ? 'style="text-transform: uppercase; width:'.$width.';"' : 'style="width:'.$width.';"';
			            $html .= '<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'" '.$upper.' class="k-input k-textbox">
			            </div>';
		        	}
		        }
        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonAdd">Yes</button>
	                <button class="k-button" id="noButtonAdd">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActEdit" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <input id="RecordID_Edit" type="hidden" class="k-input k-textbox">
	                <input id="RecordTimeStamp_Edit" type="hidden" class="k-input k-textbox">
	            </div>
	            <div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
		            	$editable = ($attr[4]) ? "readonly" : "";
		            	$width = str_replace('px', '', $attr[2]) * 2;
		            	$width = $width."px";
		            	$upper = ($col==="f001") ? 'style="text-transform: uppercase; width:'.$width.';"' : 'style="width:'.$width.';"';
		        		$html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'_Edit" '.$upper.' '.$editable.' class="k-input k-textbox">
			            </div>';
			        }
	        	}
        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonEdit">Yes</button>
	                <button class="k-button" id="noButtonEdit">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActDelete" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-label">
	                <label for="undefined"></label>
	                <input type="hidden" id="RecordID_Delete">
	                <input type="hidden" id="RecordTimeStamp_Delete">
	            </div><div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
			            $html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <input id="'.$id.'_Delete" readonly class="k-input k-textbox">
			            </div>';
			        }
		        }
	        $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="yesButtonDelete">Yes</button>
	                <button class="k-button" id="noButtonDelete">No</button>
	            </div>
	        </div>
	    </div>
	    <div id="ActDetail" class="k-edit-form-container" style="display:none;">
	        <div class="k-edit-form-container">
	            <div class="k-edit-field"></div>';
	            foreach($data['column'] as $column => $attr){
	            	if($attr[0]){
		            	$id = str_replace(' ', '',$attr[3]);
			            $html .='<div class="k-edit-label">
			                <label>'.$attr[3].'</label>
			            </div>
			            <div class="k-edit-field">
			                <p id="'.$id.'_Detail"></p>
			            </div>';
			        }
	        	}
	            $html .='<div class="k-edit-buttons k-state-default">
	                <button class="k-button" id="noButtonDetail">Close</button>
	            </div>
	        </div>
	    </div>';

	    $js = '<script type="text/javascript">
                $(document).ready(function() {
                	var column = 
                    [';
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='"width":"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='"title":"'.$attr[3].'",'; }
                            $js .='"field":"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='];
                    var data = {
                        id : "'.$data['id'].'",
                        height: "500px",
                        actBTN: "'.$actBTN.'",
                        postBTN: "'.$postBTN.'",
                        table: "'.$data['table'].'",
                        tools: [';
                        foreach($data['tools'] as $tool){
		                    $js .='"'.$tool.'",';
		                }
                    $js.='],
                        columnz: column,
                     	DList : {';
                        if(isset($data['dropdownlist'])){
							//$comments=array("NS","S1","SN","SS");
                            foreach($data['dropdownlist'] as $column => $attr){
                                $js .= $column.': { text: "'.$attr[0].'", key: "'.$attr[2].'", url: "'.$attr[1].'"},';
                            }
                        }
                $js .='},
           		 		url: {
                            create : "'.$data["url"]["create"].'",
                            read : "'.$data["url"]["read"].'",
                            update : "'.$data["url"]["update"].'",
                            destroy : "'.$data["url"]["destroy"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                $format = ($attr[5]=='datetime') ? ',format:"{0:dd-MM-yyyy}"' : "";
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" '.$format.' },';
                                }
                            }
                    $js .='}
                    }
                    gridViewBasic(data);
                });

                $("#yesButtonAdd").click(function(){
			        var voData = {';
			        foreach($data['column'] as $column => $attr){
		        		$id = str_replace(' ', '',$attr[3]);
			           	$js .= $id.': $("#'.$id.'").val(),';
			       }
	       	$js .='};
			        var valid = checkForm(voData);
			        if(valid.valid)
			        {
			           $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["create"].'"),
			            success: function (result) {                
			                if (result.errorcode > 0) {
			                new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                } else {
			                new PNotify({ title: "success", text: "Save data success", type: "success", shadow: true });';
			                foreach($data['column'] as $column => $attr){
				        		$id = str_replace(' ', '',$attr[3]);
					           	$js .= '
					           			$("#'.$id.'").val("");					           			
					           			if($("#'.$id.'").data("kendoNumericTextBox")){ $("#'.$id.'").data("kendoNumericTextBox").value(""); }
					           			if($("#'.$id.'").data("kendoDropDownList")){ $("#'.$id.'").data("kendoDropDownList").value(""); }
					           		';
					       }
		             $js .='$("#'.$data['id'].'").data("kendoGrid").dataSource.read();
			                $("#'.$data['id'].'").data("kendoGrid").refresh();
			                }
			            },
			            error: function (jqXHR, textStatus, errorThrown) {
			                alert(jQuery.parseJSON(jqXHR.responseText));
			            }
			        });       
			        }else{
			            new PNotify({ title: "Form Validation", text: valid.msg, type: "error", shadow: true });
			        }
			    });

			    $("#yesButtonEdit").click(function(){
	            	var voData = {';
	            	foreach($data['column'] as $column => $attr){
		        		$id = str_replace(' ', '',$attr[3]);
			           	$js .= $id.': $("#'.$id.'_Edit").val(),';
			       }
       		$js.='};

			        var valid = checkForm(voData);
			        if(valid.valid)
			        {
			           $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["update"].'"),
			            success: function (result) {                
			                if (result.errorcode > 0) {
			                    new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                } else {
			                $("#ActEdit").closest("[data-role=window]").data("kendoWindow").close();
			                $("#'.$data['id'].'").data("kendoGrid").dataSource.read();
			                $("#'.$data['id'].'").data("kendoGrid").refresh();
			                }
			            },
			            error: function (jqXHR, textStatus, errorThrown) {
			                alert(jQuery.parseJSON(jqXHR.responseText));
			            }
			        });        
			       }else{
			            new PNotify({ title: "Form Validation", text: valid.msg, type: "error", shadow: true });
			        }
			    });

			    function checkForm(voData) {
		            var valid = 1;
		            var msg = "";';
		            foreach($data['column'] as $column => $attr){
		            	$col = substr($column, 5);
		            	if($col !== "r001"){
			            	if($attr[6]){
				        		$id = str_replace(' ', '',$attr[3]);
					            $js.='if (voData.'.$id.' == "") { valid = 0; msg += "'.$attr[3].' is required" + "\r\n"; }';
					        }
					    }
		            }
		        $js .='var voRes = {
		                valid: valid,
		                msg: msg
		            }
		            return voRes;
			    }

                function actButton(column){
			        if(column){
			            if(column[0] == "Delete"){                    
			                $("#ActDelete").attr("style", "display:");
			                $("#ActDelete").data("kendoWindow").center().open();
			                $(".k-window-title").text("Delete");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Delete").val(column['.$i.']);';
					            $i++;
					        }
			     $js .='}else if(column[0] == "Detail"){            
			                $("#ActDetail").attr("style", "display:");
			                $("#ActDetail").data("kendoWindow").center().open();
			                $(".k-window-title").text("Detail");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Detail").text(column['.$i.']);';
					            $i++;
					        }
			     $js .='}  else if(column[0] == "Edit"){            
			                $("#ActEdit").attr("style", "display:");
			                $("#ActEdit").data("kendoWindow").center().open();
			                $(".k-window-title").text("Edit");';
			                $i=1;
			                foreach($data['column'] as $column => $attr){
			                	$property = "{";
			                	if(!empty($attr[7])){
			                		foreach ($attr[7] as $key => $value) {
			                			$property .= $value["property"].":".$value["value"].",";
			                		}
			                	}
			                	$property .= "}";
				            	$id = str_replace(' ', '',$attr[3]);
				                $js .='$("#'.$id.'_Edit").val(column['.$i.']);';
				                
						        $js .='if("'.$attr[5].'"=="int"){
						        	$("#'.$id.'_Edit").removeClass("k-textbox");
						        	$("#'.$id.'_Edit").kendoNumericTextBox('.$property.');
						        }';
						        $js .='if("'.$attr[5].'"=="datetime"){
						        	$("#'.$id.'_Edit").removeClass("k-textbox");
						        	$("#'.$id.'_Edit").kendoDatePicker({format:DateFormat});
						        }';
						        $js .='if($("#'.$id.'_Edit").data("kendoDropDownList")){
				                	$("#'.$id.'_Edit").data("kendoDropDownList").value(column['.$i.']);
				                	$("#'.$id.'_Edit").data("kendoDropDownList").text(column['.$i.']);
						        }';
					            $i++;
					        }
			     $js .='}
					}else{
			            $("#ActAdd").attr("style", "display:");
			            $("#ActAdd").data("kendoWindow").center().open();
			            $(".k-window-title").text("Add");';
			            $i=1;
		                foreach($data['column'] as $column => $attr){
		                	$property = "{";
		                	if(!empty($attr[7])){
		                		foreach ($attr[7] as $key => $value) {
		                			$property .= $value["property"].":".$value["value"].",";
		                		}
		                	}
		                	$property .= "}";


			            	$id = str_replace(' ', '',$attr[3]);
			                $js .='$("#'.$id.'").val("");';
			                $js .='if($("#'.$id.'").data("kendoDropDownList")){
					            $("#'.$id.'").data("kendoDropDownList").value("");
					        }';
					        $js .='if("'.$attr[5].'"=="int"){
					        	$("#'.$id.'").removeClass("k-textbox");
					        	$("#'.$id.'").kendoNumericTextBox('.$property.');
					        }';
					        $js .='if("'.$attr[5].'"=="datetime"){
						        	$("#'.$id.'").removeClass("k-textbox");
						        	$("#'.$id.'").kendoDatePicker({format:DateFormat});
						        }';
				            $i++;
				        }
			    $js .='}
			    }
            </script>';

	    $return = $html.$js;
		return $return;
	}
}

if ( ! function_exists('onlyGridView'))
{
	function onlyGridView($data)
	{
		$actBTN = isset($data['actBTN']) ? $data['actBTN'] : "55px";
		$postBTN = isset($data['postBTN']) ? $data['postBTN'] : "25px";
		$html ='<div id="'.$data['id'].'"></div>
			    <div id="ActDelete" style="display:none">
			        <div class="k-edit-form-container">
			            <input type="hidden" id="RecordID_Delete">
	                <input type="hidden" id="RecordTimeStamp_Delete">';
			            foreach($data['column'] as $column => $attr){
		            		if($attr[0]){
			            	$id = str_replace(' ', '',$attr[3]);
				            $html .='<div class="k-edit-label">
						                <label>'.$attr[3].'</label>
						            </div>
						            <div class="k-edit-field">
						                <input id="'.$id.'_Delete" readonly class="k-input k-textbox">
						            </div>';
			            	}
			            }
	              $html.='<div class="k-edit-buttons k-state-default">
			                <button class="k-button" id="yesButtonDelete">Yes</button>
			                <button class="k-button" id="noButtonDelete">No</button>
			            </div>
			        </div>
			    </div>
			    <div id="ActPost" style="display:none">
			        <div class="k-edit-form-container">
			            <input type="hidden" id="RecordID_Post">
	                	<input type="hidden" id="RecordTimeStamp_Post">';
			            foreach($data['column'] as $column => $attr){
		            		if($attr[0]){
			            	$id = str_replace(' ', '',$attr[3]);
				            $html .='<div class="k-edit-label">
						                <label>'.$attr[3].'</label>
						            </div>
						            <div class="k-edit-field">
						                <input id="'.$id.'_Post" readonly class="k-input k-textbox">
						            </div>';
			            	}
			            }
	              $html.='<div class="k-edit-buttons k-state-default">
			                <button class="k-button" id="yesButtonPost">Yes</button>
			                <button class="k-button" id="noButtonPost">No</button>
			            </div>
			        </div>
			    </div>
			    <div id="ActUnPost" style="display:none">
			        <div class="k-edit-form-container">
			            <input type="hidden" id="RecordID_UnPost">
	                	<input type="hidden" id="RecordTimeStamp_UnPost">';
			            foreach($data['column'] as $column => $attr){
		            		if($attr[0]){
			            	$id = str_replace(' ', '',$attr[3]);
				            $html .='<div class="k-edit-label">
						                <label>'.$attr[3].'</label>
						            </div>
						            <div class="k-edit-field">
						                <input id="'.$id.'_UnPost" readonly class="k-input k-textbox">
						            </div>';
			            	}
			            }
	              $html.='<div class="k-edit-buttons k-state-default">
			                <button class="k-button" id="yesButtonUnPost">Yes</button>
			                <button class="k-button" id="noButtonUnPost">No</button>
			            </div>
			        </div>
			    </div>';

	     $js = '<script type="text/javascript">
                $(document).ready(function() {
                	var column = 
                    [';
                    $ddate = "'dd-MM-yyyy'";
                    $fdate = "'yyyy-MM-dd'";
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='width:"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='title:"'.$attr[3].'",'; }
                            if($attr[5]=='datetime'){$js .= 'template: "#= kendo.toString(kendo.parseDate('.$column.', '.$fdate.'), '.$ddate.') #",';}
                            $js .='field:"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='];
                    var data = {
                        id : "'.$data['id'].'",
                        height: "550px",
                        actBTN: "'.$actBTN.'",
                        postBTN: "'.$postBTN.'",
                        table: "'.$data['table'].'",';
                        if(isset($data['customfilter']) && $data['customfilter'] != ''){
                    $js .= 'customfilter: {';
                        foreach($data['customfilter'] as $filCol => $filval){
		                    $js .= '"'.$filCol.'":"'.$filval.'",';
		                }
                    $js.='},';
					}
                    $js.='tools: [';
                        foreach($data['tools'] as $tool){
		                    $js .='"'.$tool.'",';
		                }
                    $js.='],
                        columnz: column,
                        url: {
                            create : "'.$data["url"]["create"].'",
                            read : "'.$data["url"]["read"].'",
                            update : "'.$data["url"]["update"].'",
                            destroy : "'.$data["url"]["destroy"].'",
                            form : "'.$data["url"]["form"].'",
                            post : "'.$data["url"]["post"].'",
                            unpost : "'.$data["url"]["unpost"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                $format = ($attr[5]=='datetime') ? ',template: "#= kendo.toString(kendo.parseDate('.$column.', '.$fdate.'), '.$ddate.') #"' : "";
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" '.$format.' },';
                                }
                            }
                    $js .='}
                    }
                    onlyGridView(data);
                });

                function actButton(column){
                	if(column){
				        if(column[0] == "Delete"){                    
				            $("#ActDelete").attr("style", "display:");
				            $("#ActDelete").data("kendoWindow").center().open();
				            $(".k-window-title").text("Delete");';
				            $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				            	if($id=="DocStatus"){ 
			            		$js.='if(column['.$i.'] == -1){
				            			$("#'.$id.'_Delete").val("Cancel");
				            		 }else if(column['.$i.'] == 0){
				            		 	$("#'.$id.'_Delete").val("Draft");
				            		 }else if(column['.$i.'] == 1){
				            		 	$("#'.$id.'_Delete").val("Open");
				            		 }else{
				            		 	$("#'.$id.'_Delete").val("Close");
				            		 }';
				            	}else{
					                $js .='$("#'.$id.'_Delete").val(column['.$i.']);';
					            }
					            $i++;
					        }
		          $js.='}else if(column[0] == "Post"){ 
		          			$("#ActPost").attr("style", "display:");
				            $("#ActPost").data("kendoWindow").center().open();
				            $(".k-window-title").text("Post");';
				            $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				            	if($id=="DocStatus"){ 
			            		$js.='if(column['.$i.'] == -1){
				            			$("#'.$id.'_Post").val("Cancel");
				            		 }else if(column['.$i.'] == 0){
				            		 	$("#'.$id.'_Post").val("Draft");
				            		 }else if(column['.$i.'] == 1){
				            		 	$("#'.$id.'_Post").val("Open");
				            		 }else{
				            		 	$("#'.$id.'_Post").val("Close");
				            		 }';
				            	}else{
					                $js .='$("#'.$id.'_Post").val(column['.$i.']);';
					            }
					            $i++;
					        }
				  $js.='}else if(column[0] == "UnPost"){ 
		          			$("#ActUnPost").attr("style", "display:");
				            $("#ActUnPost").data("kendoWindow").center().open();
				            $(".k-window-title").text("Unpost");';
				            $i=1;
			                foreach($data['column'] as $column => $attr){
				            	$id = str_replace(' ', '',$attr[3]);
				            	if($id=="DocStatus"){ 
			            		$js.='if(column['.$i.'] == -1){
				            			$("#'.$id.'_UnPost").val("Cancel");
				            		 }else if(column['.$i.'] == 0){
				            		 	$("#'.$id.'_UnPost").val("Draft");
				            		 }else if(column['.$i.'] == 1){
				            		 	$("#'.$id.'_UnPost").val("Open");
				            		 }else{
				            		 	$("#'.$id.'_UnPost").val("Close");
				            		 }';
				            	}else{
					                $js .='$("#'.$id.'_UnPost").val(column['.$i.']);';
					            }
					            $i++;
					        }
          		$js .='}      
				    }
			    }

			    function Post(id)
			    {
			        var voData = {
			            ID: id
			        };

			        $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["post"].'"),
			             success: function (result) {
			                if (result.errorcode > 0) {
			                    new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                } else {
			                    location.reload();
			                }
			           },
			           error: function (jqXHR, textStatus, errorThrown) {
			               alert(jQuery.parseJSON(jqXHR.responseText));
			           }
			        });
			    }

			    function UnPost(id) 
			    {
			        var voData = {
			            ID: id
			        };

			        $.ajax({
			            type: "POST",
			            data: voData,
			            url:  site_url("'.$data["url"]["unpost"].'"),
			             success: function (result) {
			                console.log(result);
			                if (result.errorcode > 0) {
			                    new PNotify({ title: "Failed", text: result.msg, type: "error", shadow: true });
			                }else{
			                    location.reload();
			                }
			           },
			           error: function (jqXHR, textStatus, errorThrown) {
			               alert(jQuery.parseJSON(jqXHR.responseText));
			           }
			        });
			    }
                </script>';
        $return = $html.$js;
		return $return;
	}
}

if ( ! function_exists('GridViewHeaderDetail'))
{
	function GridListHeaderDetail($data)
	{
		$html ='<div id="'.$data['id'].'"></div>
				<script type="text/x-kendo-template" id="template">
				    <div class="tabstrip">
				        <div>
				            <div class="'.$data['detail']['id'].'"></div>
				        </div>
				    </div>
				</script> 
				';
	     $js = '<script type="text/javascript">
                $(document).ready(function() {
                	var column = 
                    [';
                    $ddate = "'dd-MM-yyyy'";
                    $fdate = "'yyyy-MM-dd'";
                    foreach($data['column'] as $column => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='width:"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='title:"'.$attr[3].'",'; }
                            if($attr[5]=='datetime'){$js .= 'template: "#= kendo.toString(kendo.parseDate('.$column.', '.$fdate.'), '.$ddate.') #",';}
                            $js .='field:"'.$column.'",'; 
                            $js .='},';
                        }
                    }
                    $js .= '];';
                    $js .= '
                    var detailcolumn = 
                    [';
                    $ddate = "'dd-MM-yyyy'";
                    $fdate = "'yyyy-MM-dd'";
                    foreach($data['detail']['column'] as $detailcol => $attr){
                        if($attr[1]){
                            $js .='{';
                            if(isset($attr[2])){ $js .='width:"'.$attr[2].'",'; }
                            if(isset($attr[3])){ $js .='title:"'.$attr[3].'",'; }
                            if($attr[5]=='datetime'){$js .= 'template: "#= kendo.toString(kendo.parseDate('.$detailcol.', '.$fdate.'), '.$ddate.') #",';}
                            $js .='field:"'.$detailcol.'",'; 
                            $js .='},';
                        }
                    }
                    $js .='
                    {
			            width:"100px",
			            template:
			            \'<a class="k-button k-button-icontext k-grid-edit" onclick="openDetailModal(#='.$data["detail"]["table"].'r001#)" href="javascript:void(0)">Detail</span></a>\'
			        }];
                    var data = {
                        id : "'.$data['id'].'",
                        excelname: "'.$data['excelname'].'",
                        height: "550px",
                        table: "'.$data['table'].'",';
                        if(isset($data['customfilter']) && $data['customfilter'] != ''){
	                    $js .= 'customfilter: {';
	                        foreach($data['customfilter'] as $filCol => $filval){
			                    $js .= '"'.$filCol.'":"'.$filval.'",';
			                }
	                    $js.='},';
						}
                    $js.='tools: [';
                        foreach($data['tools'] as $tool){
		                    $js .='"'.$tool.'",';
		                }
                    $js.='],
                        columnz: column,
                        detail: {
                        	table: "'.$data['detail']['table'].'",
                        	column: detailcolumn,
                        	id: "'.$data['detail']['id'].'",
                        	url:{
                        		read : "'.$data['detail']["url"]["read"].'"	
                        	},';
                        	if(isset($data['detail']['customfilter']) && $data['detail']['customfilter'] != ''){
		                    $js .= 'customfilter: {';
		                        foreach($data['detail']['customfilter'] as $filCol => $filval){
				                    $js .= '"'.$filCol.'":"'.$filval.'",';
				                }
		                    $js.='},';
							}                        	
                    $js .= '},
                        url: {
                            read : "'.$data["url"]["read"].'"
                        },
                        FieldUse : {
                            RecordID: { editable: false },
                            RecordStatus: { editable: false,validation: { required: true } },
                            RecordTimestamp: { editable: false,type: "datetime"},';
                            foreach($data['column'] as $column => $attr){
                                $format = ($attr[5]=='datetime') ? ',template: "#= kendo.toString(kendo.parseDate('.$column.', '.$fdate.'), '.$ddate.') #"' : "";
                                if($attr[0]){
                                    $js .= $column.': { type: "'.$attr[5].'" '.$format.' },';
                                }
                            }
                    $js .='}
                    }
                    GridListHeaderDetail(data);
                });
                </script>';
        $return = $html.$js;
		return $return;
	}
}

if ( ! function_exists('dropdownlist'))
{
	function dropdownlist($data)
	{
		$property ="";
		            foreach ($data["property"] as $key => $row) {
        				$property .= $row["property"].':'.$row["value"].',';
					}
		$return = '
					<script type="text/javascript">
						$(document).ready(function() {
							$("#'.$data['id'].'").kendoDropDownList({
						        filter: "contains",        					 
						        '.$property.'
								optionLabel: "Select",
						        dataSource: {
						            type: "json",
						            transport: {
						                read: {
						                    url: site_url("'.$data['url'].'"),
						                    data: '.$data['data'].',
						                    type: "POST"
						                }
						            },
						            schema: {
						                data: function(data){
						                    return data.data;
						                }
						            }
						        }
						    });
						});
					</script>
		';
		

	    return $return;
	}
}
/* End of file form_helper.php */
/* Location: ./system/helpers/form_helper.php */
