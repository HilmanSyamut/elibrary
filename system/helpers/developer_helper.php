 <?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * BluesCode
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		BluesCode
 * @author		BluesCode Dev Team
 * @copyright	Copyright (c) 2013 - 2014, Muhammad Arief.
 * @license		http://muhammad-arief.com/bluescode/license.html
 * @link		http://muhammad-arief.com/bluescode
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------
/**
 * BluesCode Developer Helpers
 *
 * @package		BluesCode
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Muhammad Arief
 * @link
 */
// ------------------------------------------------------------------------
/**
 * Print Out - Display Pre-Formatted Output
 *
 *
 * @access	public
 * @param	string
 * @param	array
 * @param	mixed
 * @return	mixed	depends on what the array contains
 */
if (!function_exists('print_out'))
{
    function print_out($data, $trace = true)
    {
        if(function_exists('get_instance'));
        {
            $ci =& get_instance();
            
            if(method_exists($ci, 'helper'))
            {
                $ci->load->helper('cookie');
            }
            
            if(function_exists('get_cookie'))
            {
                $cookie = get_cookie( APPNAME . '_sessions' );
                
                if(method_exists($ci,'encrypt'))
                {
                    $cookie = $ci->encrypt->decode($cookie);
                    $cookie = unserialize($cookie);
                    
                    if(method_exists($ci,'session'))
                    {
                        $session_id = $ci->session->userdata('session_id');
                    }
                }
            }
        }
        
        $ob_level = ob_get_level();

        $data = print_r($data,true);
        $data = htmlentities($data);
        $data = htmlspecialchars( htmlspecialchars_decode($data, ENT_QUOTES), ENT_QUOTES, 'UTF-8' );
        $data = str_replace("&nbsp;", "", $data);
        $data = trim($data);

        if (ob_get_level() > $ob_level + 1)
        {
            ob_end_flush();
        }
        ob_start();
        include ( BASEPATH . 'errors/print_out' . EXT );
        $buffer = ob_get_contents();
        ob_end_clean();

        echo $buffer;

        if($trace == true)
        {
            echo '<br /><strong style="color:#f00">Debug Tracer:</strong><br />';    

            static $start_time = NULL;
            static $start_code_line = 0;
            $a = debug_backtrace();
            $call_info = array_shift( $a );
            $code_line = $call_info['line'];
            $b = explode('/', $call_info['file']);
            $file = array_pop( $b );

            if( $start_time === NULL )
            {
                print $file." > initialize<br />";
                $start_time = time() + microtime();
                $start_code_line = $code_line;
            }

            printf("%s > code-lines: %d lines / loaded time: %.4f / memory usage: %d KB<br />", $file, $start_code_line, $code_line, (time() + microtime() - $start_time), memory_get_usage()/1024);
            $start_time = time() + microtime();
            $start_code_line = $code_line;    

            echo '<ol class="tracer">';
            //array_walk( array_reverse( debug_backtrace() ),create_function('$a,$b','print "<li>Function: <strong>{$a[\'function\']}()</strong> <span><br />File: ".$a[\'file\']." <br />Line: {$a[\'line\']}</span></li>";'));
            $c = debug_backtrace();
            $d = array_reverse( $c );
            array_walk( $d,'tracer_print');
            echo '</ol>';

            list($usec, $sec) = explode(' ', microtime());
            $script_start = (float) $sec + (float) $usec;
            list($usec, $sec) = explode(' ', microtime());
            $script_end = (float) $sec + (float) $usec;
            $elapsed_time = round($script_end - $script_start, 5);

            echo '<span style="color:#666">';
            echo 'Page Rendered ' . pow(10, $elapsed_time) . ' seconds<br />';
            echo 'Memory Usage ' . memory_get_usage() / 1000000 . ' MB';
            echo '</span>';           
        }
        die();        
    }
}

function tracer_print($trace)
{
    echo "<li>Function: <strong>" . $trace['function'] . "()</strong>";
    echo (isset($trace['file']) ? "<span><br />File: ".$trace['file']." <br />" : "");
    echo (isset($trace['line']) ? "Line: ".$trace['line'] ."</span></li>" : "");
}
/**
 * Outputs/Debugs a variable and shows where it was called from
 * @param mixed $var
 * @param boolean $dump
 * @param boolean $backtrace
 * @return string
 */
if (!function_exists('debug'))
{
    function debug($var, $dump = false, $backtrace = true)
    {
        if (error_reporting() > 0)
        {
            if ($backtrace)
            {
                $calledFrom = debug_backtrace();
                echo '<strong>' . trim(str_replace($_SERVER['DOCUMENT_ROOT'], '', $calledFrom[0]['file'])) . '</strong> (line <strong>' . $calledFrom[0]['line'] . '</strong>)';
            }
            echo '<pre class="debug">';
            $function = ( $dump ) ? 'var_dump' : 'print_r';
            $function($var);
            echo '</pre>';
        }
    }
}

/**
 * Function for get data from other table with id key
 * @param string
 * @param string
 * @param integer //id
 * @return array
 */
if (!function_exists('lookup_list'))
{
    function lookup_list($table, $column, $id)
    {
        $ci =& get_instance();
        $query =$this->ci->db->query("SELECT id, title FROM $table WHERE $column ='$id'");
        $result = $query->result();
        return $result;  
    }
}

/**
 * Function for get data from other table with id key
 * @param string
 * @param string
 * @param integer //id
 * @return array
 */
if (!function_exists('get_access'))
{
    function get_access()
    {
        $ret = array();
        $ci =& get_instance();
        $query = $ci->db->get(SYSTEM);
        if($query->num_rows() > 0):
            $result = $query->first_row();
            return $result;
        endif;
        return $ret;
    }
}

/**
 * Function for get data from other table with id key
 * @param string
 * @param string
 * @param integer //id
 * @return array
 */
if (!function_exists('scan_method'))
{
    function scan_method()
    {
        $ci =& get_instance();
        $result = get_class_methods($ci->router->fetch_class());
        $result = serialize($result);
        print_out($result);
        return $result;  
    }
}

if (!function_exists('get_age'))
{
    function get_age($date)
    {
        $date = date('d-m-Y',$date);
        $tgl=explode("-",$date);
        $cek_jmlhr1=cal_days_in_month(CAL_GREGORIAN,$tgl['1'],$tgl['2']);
        $cek_jmlhr2=cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
        $sshari=$cek_jmlhr1-$tgl['0'];
        $ssbln=12-$tgl['1']-1;
        $hari=0;
        $bulan=0;
        $tahun=0;
        //hari+bulan
        if($sshari+date('d')>=$cek_jmlhr2){
            $bulan=1;
            $hari=$sshari+date('d')-$cek_jmlhr2;
        }else{
            $hari=$sshari+date('d');
        }
        if($ssbln+date('m')+$bulan>=12){
            $bulan=($ssbln+date('m')+$bulan)-12;
            $tahun=date('Y')-$tgl['2'];
        }else{
            $bulan=($ssbln+date('m')+$bulan);
            $tahun=(date('Y')-$tgl['2'])-1;
        }

          $selisih=$tahun." Tahun ".$bulan." Bulan ".$hari." Hari";
        return $selisih;
    }
}

if (!function_exists('activity_stocklist'))
{
    function activity_stocklist($data=array())
    {
        $ci =& get_instance();
        $item = array(
            't1021r002' => date("Y-m-d g:i:s",now()),
            't1021r003' => $data['recordstatus'],
            't1021f001' => $data['record_id'],
            't1021f002' => $data['doctype'],
            't1021f003' => $data['docno'],
            't1021f004' => $data['IDItem'],
            't1021f005' => $data['IDLoc'],
            't1021f006' => $data['quantity'],
        );
        $ci->db->insert('t1021',$item);

        return TRUE;
    }
}

if (!function_exists('activity_log'))
{
    function activity_log($data=array())
    {
        $ci =& get_instance();
        $item = array(
            't0090f002' => $data['msg'],
            't0090f003' => $data['kategori'],
            't0090f004' => $data['jenis'],
            't0090f005' => $data['object'],
            't0090f006' => now(),
            't0090f007' => $ci->ezrbac->getCurrentUserID() != '' ? $ci->ezrbac->getCurrentUserID() : $data['object']
        );
        $ci->db->insert('t0090',$item);

        return TRUE;
    }
}

//Generate DOCNO
if (!function_exists('DocNo'))
{
    function DocNo($Type)
    {
        $ci =& get_instance();

        $ci->db->SELECT('s.*');
        $ci->db->FROM('t9050 s');
        $ci->db->WHERE('s.t9050f001', $Type);
        $query = $ci->db->get();
        $t9050 = $query->row();
        if($query->num_rows() > 0){
            $ci->db->SELECT('s.*');
            $ci->db->FROM('t9051 s');
            $ci->db->WHERE('s.t9051f004', 1);
            $ci->db->WHERE('s.t9051f001', $t9050->t9050r001);
            $query = $ci->db->get();
            $result = $query->row();
        }
        $DocNo = 0;
        if(!empty($result)){
            $period = substr($result->t9051f002, 2);
            if ($result->t9051f003 < 10) {
                $No = '-0000'.($result->t9051f003+1);
            }elseif ($result->t9051f003 < 100) {
                $No = '-000'.($result->t9051f003+1);
            }elseif ($result->t9051f003 < 1000) {
                $No = '-00'.($result->t9051f003+1);
            }elseif ($result->t9051f003 < 10000) {
                $No = '-0'.($result->t9051f003+1);
            }else{
                $No = '-'.$result->t9051f003;
            }
            $DocNo = $t9050->t9050f001.'-'.$period.$No;
        }

        return $DocNo;
    }
}

//Update Doc Type Last Number +1
if (!function_exists('UpdateDocNo'))
{
    function UpdateDocNo($Type)
    {
        $ci =& get_instance();
        $ci->db->SELECT('s.*');
        $ci->db->FROM('t9050 s');
        $ci->db->WHERE('s.t9050f001', $Type);
        $query = $ci->db->get();
        $result = $query->row();

        $DocRecordID = $result->t9050r001;

        $ci->db->WHERE('t9051.t9051f004', 1);
        $ci->db->WHERE('t9051.t9051f001', $DocRecordID); 
        $ci->db->set('t9051.t9051f003', 't9051.t9051f003 + 1', false);
        $ci->db->update("t9051");

        return TRUE;
    }
}

//Check Data same form primary
if (!function_exists('check_post'))
{
    function check_post($columnCheckID, $columnCheckLoc, $columnID, $columnLoc)
    {
        $ci =& get_instance();
        $nama_table = substr($columnCheckID, 0, 5);
        $ci->db->from($nama_table);
        $ci->db->where($columnCheckID, $columnID);
        $ci->db->where($columnCheckLoc, $columnLoc);
        $query = $ci->db->get();
        $row = $query->num_rows();

        if ($row > 0) {   
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

//Check Data same form primary
if (!function_exists('check_unpost'))
{
    function check_unpost($id)
    {
        $ci =& get_instance();
        $id = $ci->input->post($id);
        $ci->db->select('*');
        $ci->db->from('t1011');
        $ci->db->where('t1011.t1011f001',$id);
        $query = $ci->db->get();
        $result = $query->result();

        if ($row > 0) {   
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

//Check Data same form primary
if (!function_exists('check_column'))
{
    function check_column($column, $post)
    {
        $ci =& get_instance();
        $post = $ci->input->post($post);
        $nama_table = substr($column, 0, 5);
        $id = strtoupper($post);
        $ci->db->from($nama_table);
        $ci->db->where($column, $id);
        $query = $ci->db->get();
        $row = $query->num_rows();
        if ($row > 0) {   
            return $output = array('errorcode' => 200, 'msg' => 'duplicate primary key');
        }else{
            return FALSE;
        }
    }
}

//Check data same form Primary Json
if (!function_exists('check_column_json'))
{
    function check_column_json($column, $post)
    {
        $ci =& get_instance();
        $post = $post;
        $nama_table = substr($column, 0, 5);
        $id = strtoupper($post);
        $ci->db->from($nama_table);
        $ci->db->where($column, $id);
        $query = $ci->db->get();
        $row = $query->num_rows();

        if ($row > 0) {   
            return $output = array('errorcode' => 200, 'msg' => 'duplicate primary key');
        }else{
            return FALSE;
        }
    }
}

//Convert Date To DateTime
if (!function_exists('convert_datetime'))
{
    function convert_datetime($post)
    {
        $ci =& get_instance();
        $post       = str_replace('/', '-', $post);
        $date       = strtotime($post);
        $getDate    = date(DBFORMATDATE,$date);
        $datetime   = $getDate.' '.date("g:i:s",now());
        
        return $datetime;
    }
}

if (!function_exists('sys_log'))
{
    function sys_log($key,$a)
    {
        $ci =& get_instance();
        if($a == 'outoflimit')
        {
            $data = array(
               'serial_number' => $key
            );
            $ci->db->where('serial_number',$a);
            $ci->db->update('system', $data); 
        }
        redirect('/dashboard', 'refresh');
        return TRUE;
    }
}
/**
 * Function for fifo method
 * @param integer
 * @param integer
 * @param string
 */
if (!function_exists('fifo_method'))
{
    function fifo_method($id,$qty,$table)
    {
        $ci =& get_instance();
        $query = $ci->db->query("SELECT id, qty_in, qty_out, (qty_in - qty_out) as currents FROM tt_".$table." WHERE farmasi_id = ".$id." AND qty_out < qty_in;");
        $result = $query->result();
        foreach($result as $value):
            $stock = $value->currents - $qty;
            $sisa = str_replace("-","@",$stock);
            $sisa_request = is_numeric($sisa) ? 0 : str_replace("@","",$sisa);
            $sisa_request = isset($prev_sisa) ? str_replace("-","@",$prev_sisa - $value->currents) :  $sisa_request;
            $sisa_request = is_numeric($sisa_request) ? $sisa_request : 0;
            $request = isset($prev_sisa) ? $prev_sisa : $qty;

            $out = (is_numeric($sisa) ? $qty : $value->currents );
            $out = (isset($prev_sisa) ? $request - $sisa_request : $out );
            $instock = $value->currents - $out;

            $id = isset($prev_id) ? $prev_id : isset($fix_id) ? $fix_id : $value->id;

            $obat[$id] = array(
                'stock_id' => $id,
                'stock_awal' => $value->currents,
                'out' => $out,
                'request' => $request,
                'instock' => $instock,
                'sisa_request' => $sisa_request
            );
            $prev_id = (is_numeric($sisa) ? $value->id  : null);
            $prev_sisa = (is_numeric($sisa) ? 0  : $sisa_request);
            (($request > 0) ? $fix_id = $prev_id : null);
            if($sisa_request == 0)
                break;
        endforeach;
        return $obat;
    }
}

if (!function_exists('modules_mapping'))
{
    function modules_mapping()
    {
        $ci =& get_instance();
        $ci->db->truncate('t0010');
        $ci->db->truncate('t0020');
        $ci->db->truncate('t0030');
        $dir_scan = array_diff(scandir(APPPATH."modules"), array('..', '.'));
        $dirscan = dir_to_array(APPPATH."modules");
            foreach($dirscan as $module => $item)
            {
                $label = array(
                    'id' => ucwords($module),
                    'en' => ucwords($module)
                );
                $title = serialize($label);
                $data = array(
                    't0010f002' => ucwords($module),
                    't0010f003' => $title,
                    't0010f007' => 'Muhammad Arief',
                    't0010f011' => 1,
                    't0010f009' => now()
                );
                $ci->db->insert('t0010',$data);
                $module_id = $ci->db->insert_id();

                $url = site_url('Welcomes/getController');

                $fields = array(
                    'id'      => $module_id,
                    'module'      => $module,
                    'controller'    => $item['controllers']
                );

                //open connection
                $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

                //execute post
                $result = curl_exec($ch);

                //close connection
                curl_close($ch);

                // controllers_mapping($module_id, $module, $item['controllers']);
            }
            $query = $ci->db->get("t0010");
            return $query;
    }
}

if (!function_exists('controllers_mapping'))
{
    function controllers_mapping($id, $module, $string)
    {
        $ci =& get_instance();
        $module = ucfirst($module);
        foreach($string as $item)
        {
            $item = ucfirst($item);
            include_once MODPATH.$module."/controllers/".$item;
            
            $item = str_replace('.php','',strtolower($item));
            $label = array(
                'id' => ucwords($item),
                'en' => ucwords($item)
            );
            $title = serialize($label);
            $data = array(
                't0020f002' => $id,
                't0020f003' => ucwords($item),
                't0020f004' => $title,
                't0020f006' => 'Muhammad Arief',
                't0020f009' => 1,
                't0020f008' => now()
            );
            $ci->db->insert('t0020',$data);
            $c_id = $ci->db->insert_id();
            $method = array_diff(get_class_methods($item),array('__construct','get_instance'));
            method_mapping($c_id,$method);
        }
    }
}

if (!function_exists('method_mapping'))
{
    function method_mapping($id, $string)
    {
        $ci =& get_instance();
        foreach($string as $item)
        {
            $data = array(
                't0030f002' => $id,
                't0030f003' => ucwords($item),
                't0030f004' => ucwords($item),
                't0030f007' => 1,
                't0030f006' => now()
            );
            $ci->db->insert('t0030',$data);
        }
    }
}

if (!function_exists('dir_to_array'))
{
    function dir_to_array($dir)
    {
        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value)
        {
          if (!in_array($value,array(".","..")))
          {
             if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
             {
                $result[strtolower($value)] = dir_to_array($dir . DIRECTORY_SEPARATOR . $value);
             }
             else
             {
                $result[] = strtolower($value);
             }
          }
        }

        return $result;
    }
}

if (!function_exists('all_access'))
{
    function all_access()
    {
        $ci =& get_instance();
        $id = $ci->session->userdata('access_role');
        $ret=array();
        $query = $ci->db->get('t0030');
        if ($query->num_rows() > 0) {
            foreach($query->result() as $item):
                $data[] = $item->t0030r001;
            endforeach;
            $access = serialize($data);
            $up = array('t0080f004' => $access);
            $ci->db->where('t0080r001',$id)
                   ->update('t0080',$up);
        }
        return $ret;
    }
}
if (!function_exists('count_week'))
{
    function count_week($form,$to)
    {
        $sec = 24 * 3600;
        $week = 0;
        for ($i=$form; $i < $to; $i += $sec)
        {
            if (date("w", $i) == "0"){
                $week++;
            }
        }
        return $week;
    }
}
if(!function_exists('email_sender'))
{
    function email_sender($email_to, $title, $content) {
        $ci = & get_instance();
        $ci->load->library('email');
        //Awal Kirim email
        // $config['smtp_crypto'] = 'tls';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.gmail.com';
        $config['smtp_port'] = 587;
        $config['smtp_timeout'] = '5';

        $config['smtp_user']    = 'hikmatfauzy@gmail.com';
        $config['smtp_pass']    = 'Fauzy7197693';

        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        // $config['validation'] = TRUE; // bool whether to validate email or not

        $ci->email->initialize($config);

        $message = message_content($title, $content);

        $ci->email->from("hikmatfauzy@gmail.com", $title);
        $ci->email->to($email_to);
        $ci->email->subject($title);
        $ci->email->message($message);
        $ci->email->send();
        echo $ci->email->print_debugger();
     }
}
if(!function_exists('message_content'))
{
    function message_content($title, $content){
         $container = '<style type="text/css">
    @media only screen and (max-width: 660px) {
        table[class=w0], td[class=w0] { width: 0 !important; }
        table[class=w10], td[class=w10], img[class=w10] { width:10px !important; }
        table[class=w15], td[class=w15], img[class=w15] { width:5px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w60], td[class=w60], img[class=w60] { width:10px !important; }
        table[class=w125], td[class=w125], img[class=w125] { width:80px !important; }
        table[class=w130], td[class=w130], img[class=w130] { width:55px !important; }
        table[class=w140], td[class=w140], img[class=w140] { width:90px !important; }
        table[class=w160], td[class=w160], img[class=w160] { width:180px !important; }
        table[class=w170], td[class=w170], img[class=w170] { width:100px !important; }
        table[class=w180], td[class=w180], img[class=w180] { width:80px !important; }
        table[class=w195], td[class=w195], img[class=w195] { width:80px !important; }
        table[class=w220], td[class=w220], img[class=w220] { width:80px !important; }
        table[class=w240], td[class=w240], img[class=w240] { width:180px !important; }
        table[class=w255], td[class=w255], img[class=w255] { width:185px !important; }
        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w280], td[class=w280], img[class=w280] { width:135px !important; }
        table[class=w300], td[class=w300], img[class=w300] { width:140px !important; }
        table[class=w325], td[class=w325], img[class=w325] { width:95px !important; }
        table[class=w360], td[class=w360], img[class=w360] { width:140px !important; }
        table[class=w410], td[class=w410], img[class=w410] { width:180px !important; }
        table[class=w470], td[class=w470], img[class=w470] { width:200px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        table[class*=hide], td[class*=hide], img[class*=hide], p[class*=hide], span[class*=hide] { display:none !important; }
        table[class=h0], td[class=h0] { height: 0 !important; }
        p[class=footer-content-left] { text-align: center !important; }
        #headline p { font-size: 30px !important; }
        .article-content, #left-sidebar{ -webkit-text-size-adjust: 90% !important; -ms-text-size-adjust: 90% !important; }
        .header-content, .footer-content-left {-webkit-text-size-adjust: 80% !important; -ms-text-size-adjust: 80% !important;}
        img { height: auto; line-height: 100%;}
    }
    #outlook a { padding: 0; }
    body { width: 100% !important; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass { width: 100%; display:block !important; }
    body { background-color: #ececec; margin: 0; padding: 0; }
    img { outline: none; text-decoration: none; display: block;}
    br, strong br, b br, em br, i br { line-height:100%; }
    h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { color: blue !important; }
    h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active { color: red !important; }
    h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }

    table td, table tr { border-collapse: collapse; }
    .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {
        color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;
    }
    code {
        white-space: normal;
        word-break: break-all;
    }
    #background-table { background-color: #ececec; }
    #top-bar { border-radius:6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius:6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #ffffff; color: #ffffff; }
    #top-bar a { font-weight: bold; color: #ffffff; text-decoration: none;}
    #footer { border-radius:0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius:0px 0px 6px 6px; -webkit-font-smoothing: antialiased; }
    body, td { font-family: HelveticaNeue, sans-serif; }
    .header-content, .footer-content-left, .footer-content-right { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; }
    .header-content { font-size: 12px; color: #ffffff; }
    .header-content a { font-weight: bold; color: #ffffff; text-decoration: none; }
    #headline p { color: #ffffff; font-family: \'Lucida Grande\', \'Lucida Sans Unicode\', Verdana, sans-serif; font-size: 18px; text-align: center; margin-top:0px; margin-bottom:30px; }
    #headline p a { color: #ffffff; text-decoration: none; }
    .article-title { font-size: 18px; line-height:24px; color: #51B4F0; font-weight:bold; margin-top:0px; margin-bottom:18px; font-family: HelveticaNeue, sans-serif; }
    .article-title a { color: #51B4F0; text-decoration: none; }
    .article-title.with-meta {margin-bottom: 0;}
    .article-meta { font-size: 13px; line-height: 20px; color: #ccc; font-weight: bold; margin-top: 0;}
    .article-content { font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: HelveticaNeue, sans-serif; }
    .article-content a { color: #d13948; font-weight:bold; text-decoration:none; }
    .article-content img { max-width: 100% }
    .article-content ol, .article-content ul { margin-top:0px; margin-bottom:18px; margin-left:19px; padding:0; }
    .article-content li { font-size: 13px; line-height: 18px; color: #444444; }
    .article-content li a { color: #d13948; text-decoration:underline; }
    .article-content p {margin-bottom: 15px;}
    .footer-content-left { font-size: 12px; line-height: 15px; color: #878787; margin-top: 0px; margin-bottom: 15px; }
    .footer-content-left a { color: #878787; font-weight: bold; text-decoration: none; }
    .footer-content-right { font-size: 11px; line-height: 16px; color: #878787; margin-top: 0px; margin-bottom: 15px; }
    .footer-content-right a { color: #878787; font-weight: bold; text-decoration: none; }
    #footer { background-color: #e3e3e3; color: #878787; }
    #footer a { color: #878787; text-decoration: none; font-weight: bold; }
    #permission-reminder { white-space: normal; }
    #street-address { color: #878787; white-space: normal; }
</style>
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="background-table" style="background-color:#ececec;">
    <tbody>
        <tr style="border-collapse:collapse;">
            <td align="center" bgcolor="#ececec" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0" style="margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;">
                    <tbody>
                        <tr style="border-collapse:collapse;">
                            <td class="w640" width="640" height="20" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                        </tr>
                        <tr style="border-collapse:collapse;">
                            <td id="header" class="w640" width="640" align="center" bgcolor="#51B4F0" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580" width="580" height="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580" width="580" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                                <div align="center" id="headline">
                                                    <p style="color:#ffffff;font-family:\'Lucida Grande\', \'Lucida Sans Unicode\', Verdana, sans-serif;font-size:18px;text-align:center;margin-top:0px;margin-bottom:30px;">
                                                        <strong>'.$title.'</strong>
                                                    </p>
                                                </div>
                                            </td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr id="simple-content-row" style="border-collapse:collapse;">
                            <td class="w640" width="640" bgcolor="#ffffff" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                <table align="left" class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580" width="580" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                                <table class="w580" width="580" cellpadding="0" cellspacing="0" border="0">
                                                    <tbody>
                                                        <tr style="border-collapse:collapse;">
                                                            <td class="w580" width="580" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                                                <p align="left" class="article-title" style="font-size:18px;line-height:24px;color:#51B4F0;font-weight:bold;margin-top:0px;margin-bottom:18px;font-family:HelveticaNeue, sans-serif;overflow:hidden;"></p>
                                                                <div align="left" class="article-content" style="font-size:13px;line-height:18px;color:#444444;margin-top:0px;margin-bottom:18px;font-family:HelveticaNeue, sans-serif;">
                                                                    <p style="margin-bottom:15px;">'.$content.'</p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse;">
                                                            <td class="w580" width="580" height="10" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-collapse:collapse;">
                            <td class="w640" width="640" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                <table id="footer" class="w640" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#e3e3e3" style="border-radius:0px 0px 6px 6px;-moz-border-radius:0px 0px 6px 6px;-webkit-border-radius:0px 0px 6px 6px;-webkit-font-smoothing:antialiased;background-color:#e3e3e3;color:#878787;">
                                    <tbody>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580 h0" width="360" height="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w0" width="60" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w0" width="160" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580" width="360" valign="top" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;">
                                                <p align="left" class="footer-content-left" style="font-size:12px;line-height:15px;color:#878787;margin-top:0px;margin-bottom: 3px;"><a href="http://ppdb.kemdikbud.go.id" lang="id-ID" style="color:#706967;text-decoration:none;font-weight:bold;">SEMASI</a></p>
                                                <span class="hide"><p id="permission-reminder" align="left" class="footer-content-left" style="font-size: 11px;line-height:15px;color:#878787;margin-top:0px;margin-bottom:0px;white-space:normal;font-style: italic;"><span>PT.Sepatu Mas Idaman</span></p></span>
                                            </td>
                                            <td class="hide w0" width="60" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td width="350" valign="top" style="font-family:HelveticaNeue,sans-serif;border-collapse:collapse">
                                                <p align="right" style="font-size:11px;line-height:16px;margin-top:0px;margin-bottom:15px;color:#878787;white-space:normal">
                                                    Jl. Sukaraja No.234, RT.4/RW.1, Pasirlaja, Sukaraja, Bogor, Jawa Barat
                                                </p>
                                            </td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w580 h0" width="360" height="15" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w0" width="60" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w0" width="160" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                            <td class="w30" width="30" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-collapse:collapse;">
                            <td class="w640" width="640" height="60" style="font-family:HelveticaNeue, sans-serif;border-collapse:collapse;"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>';
        return $container;
    }
}
if(!function_exists('send_email'))
{
    function send_email($data){
        $body = message_content($data['Subject'],$data['Body']);
        $data['Body'] = $body;

        //contoh Data
        // $data = array(
        //     'Mailto' => "hikmatfauzy7@gmail.com",
        //     'GroupMail' => "hikmatfauzy@gmail.com",//cc or bcc
        //     'Subject' => "ini test email",
        //     'Body' => "ini test body"
        // );

        $url = SERVICE_EMAIL;
        $mystring = curl($url,$data);
        echo $mystring;
    }
}
if(!function_exists('curl'))
{
    function curl($url,$data){
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
if(!function_exists('truncate'))
{
    function truncate()
    {

    }
}
if(!function_exists('db_rollback'))
{
    function db_rollback(){
        $ci =& get_instance();
        $ci->db->trans_begin();
        $ci->db->trans_rollback();
    }
}
// ------------------------------------------------------------------------
/* End of file developer_helper.php */
/* Location: ./system/helpers/developer_helper.php */
