/*
Name: 			Form table transcation
Author:         Muhammad Arief 	
*/

function getDetailField(target)
{
    var detailItem = [];
    var itemcol = $('#head-'+target+' tr');
    var itemcel = itemcol.eq(0);
    var cellCount = itemcel.find('th').length - 2;
    for (var x = 0; x <= cellCount; x++) {
      var keys = itemcol.find('th').eq(x).attr("data-col");
      var result = itemcel.find('td').eq(x).html();
      detailItem[x] = keys;
    }
    return detailItem;
}

function getDetailItem(target) {
    var detailItem = [];
    var rowCount = document.getElementById("list-"+target).rows.length - 1;
    for (var i = 0; i <= rowCount; i++) {
        var itemcol = $('#head-'+target+' tr');
        var itemcel = $('#list-' + target + ' tr').eq(i);
        var cellCount = itemcel.find('td').length - 2;
        detailItem[i] = {};
        for (var x = 0; x <= cellCount; x++) {
            var keys = itemcol.find('th').eq(x).attr("data-col");
            var result = itemcel.find('td').eq(x).attr("data-val");
            if(keys=="UnitPrice"){ result = accounting.unformat(result); }
            if(keys=="LineTotal"){ result = accounting.unformat(result); }
            detailItem[i][keys] = result;
        }
    }
    
    console.log(detailItem);
    return detailItem;
}

function getTotal(target,type,ts) {
    var result = 0;
    var subTotal = 0;
    var rowCount = document.getElementById("list-" + target).rows.length - 1;
    for (var i = 0; i <= rowCount; i++) {
        var itemcel = $('#list-' + target + ' tr').eq(i);
        subTotal = itemcel.find('td').eq(ts).attr("data-val");
        subTotal = accounting.unformat(subTotal);
        if (type = "float") {
            result += parseFloat(subTotal);
        }
        else
        {
            result += parseInt(subTotal);
        }
    }
    
    return result;
}
function getTotalWithoutLoan(target,type,ts) {
    var result = 0;
    var subTotal = 0;
    var rowCount = document.getElementById("list-" + target).rows.length - 1;
    for (var i = 0; i <= rowCount; i++) {
        var itemcel = $('#list-' + target + ' tr').eq(i);
        subTotal = itemcel.find('td').eq(ts).attr("data-val");
        subTotal = accounting.unformat(subTotal);
        var id = itemcel.attr('id').split("-")[1];

        if(itemcel.find("#"+target+"Statusv-"+id).attr("data-val") != "IS2"){
            if (type = "float") {
                result += parseFloat(subTotal);
            }
            else
            {
                result += parseInt(subTotal);
            }
        }
    }
    
    return result;
}
function adddetail(target)
{
    var valid = 1;
    var msg = '';
    var field = getDetailField(target);
    var no = document.getElementById('list-'+target).rows.length + 1;
    var html = '<tr id="'+target+'-'+no+'">';
    for (i = 0; i < field.length; i++) {
      var get = $("#"+field[i]).val();
      var txt = (get==undefined) ? "" : getTextForm(field[i]);
      var data = (get==undefined) ? "" : get;
      // if(field[i]=="Quantity"){ if(data==""){ valid = 0; msg += field[i]+" is required" + "\r\n"; } txt = accounting.formatNumber(data,2,","); }
      if(field[i]=="UnitPrice"){ txt = accounting.formatNumber(data,2,","); }
      if(field[i]=="LineTotal"){ txt = accounting.formatNumber(data,2,","); }
      // if(field[i]=="Condition"){ if(data==""){ valid = 0; msg += field[i]+" is required" + "\r\n"; } }
      // if(field[i]=="LocationIDFrom"){ if(data==""){ valid = 0; msg += field[i]+" is required" + "\r\n"; } }

      if($("#"+field[i]).attr("data-role") == "datepicker"){
        get = $("#"+field[i]).data("kendoDatePicker").value();
        txt = (get==undefined) ? "" : getTextForm(field[i]);
        data = (get==undefined) ? "" : get;
        var d = new Date(data);
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        data = curr_month + "/" + curr_date + "/" + curr_year;
      }

      // required
      if($("#"+field[i]).attr("datarequired") == "1"){
        if(data==""){valid = 0; msg+=field[i]+" is required"+"\r\n";}  
      }      

      html += '<td id="'+target+field[i]+'v-'+no+'" data-val="'+data+'">'+txt+'</td>';
    }
    if(valid){
        targetDo = "'"+target+"'";
        html += '<td class="actions"><a href="javascript:void(0);" onclick="editdetail('+targetDo+',' + no + ',0);"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removedetail('+targetDo+',' + no + ');"><i class="fa fa-trash-o"></i></a></td></tr>';
        $('#list-'+target).append(html);
        
        sumTotal(target);
        sortTable(1,0,target);
        $('#'+target+'Form').closest("[data-role=window]").kendoWindow("close");

    }else{
        new PNotify({ title: "Warning", text: msg, type: 'warning', shadow: true });
    }
}
function adddetailItem(target,source){
    var field = getDetailField(target);
    var html = "";
    
    $.each( $('tbody#list-'+source+" tr"), function( key, value ) {
        var id = $(value).attr('id');
        id = id.split("-")[1];

        if($("#"+source+"checklistv-"+id).is(":checked")){
            html += '<tr id="'+target+'-'+id+'">';
            for (i = 0; i < field.length; i++) {
                var data = $("#"+source+field[i]+"v-"+id).attr("data-val");
                var txt = $("#"+source+field[i]+"v-"+id).html();
                html += '<td id="'+target+field[i]+'v-'+id+'" data-val="'+data+'">'+txt+'</td>';
            }
            html += "<td></td></tr>";
        }
    });
    if(html != ""){
        resizeHeight(buttonresize, iframeID, 60);
        $("#list-"+target).html(html);
        $('#'+target).closest("[data-role=window]").kendoWindow("close");
    }
}
function getTextForm(target)
{
    var result = "";
    if($("#"+target).data("kendoDropDownList")){
        var DDL = $("#"+target).data("kendoDropDownList");
        result = DDL.text();
    }else{
        result = $("#"+target).val();
    }
    return result;
}
function addDetailRef(DataItem,target)
{
    var html = '';
    var no = 0;
    var field = getDetailField(target);
    for (id = 0; id < DataItem.length; id++) {
        no = id + 1;
        html += '<tr id="'+target+'-'+no+'">';
        for (a = 0; a < field.length; a++) {
            var getfield = field[a];
            var getdata = DataItem[id];
            var data = (getdata[getfield]==undefined) ? "" : getdata[getfield];
            var txt = (getdata[getfield]==undefined) ? "" : getdata[getfield];
            if(getfield=="Description"){ data = getdata["AdditionalDescription"];txt = getdata["AdditionalDescription"]; }
            if(getfield=="Qty"){ data = getdata["Quantity"];txt = getdata["Quantity"]; }
            if(getfield=="ItemTotal"){ data = getdata["LineTotal"];txt = (getdata["LineTotal"]).toFixed(2); }
            if(getfield=="Quantity"){ data = accounting.formatNumber(data,2,","); }
            // if(getfield=="UnitPrice"){ data = accounting.formatNumber(data,2,","); }
            if(getfield=="UnitPrice"){ data = (data).toFixed(2); txt = (txt).toFixed(2); }
            // if(getfield=="LineTotal"){ data = accounting.formatNumber(data,2,","); }
            if(getfield=="LineTotal"){ data = (data).toFixed(2); txt = (txt).toFixed(2); }
            if(getfield=="LocationID"){ txt = "WH1"; data = "WH1";}
            if(getfield=="Status"){ 
                if(getdata["DataStatus"] != undefined)
                    txt = getdata["DataStatus"]["ListName"]; 
            }
            if(getfield=="Condition"){ 
                if(getdata["DataCond"] != undefined)
                    txt = getdata["DataCond"]["ListName"]; 
            }
            if(getfield=="Room"){ txt = getdata["DataRoom"]["ListName"]; }

            //custom purchase invoice
            if(getfield=="AmountBeforeGST"){ 
               data = getdata["LineTotal"] - (getdata["LineTotal"] / 1.07);
               txt = (data).toFixed(2);
            }
            if(getfield=="GST"){ 
               data = getdata["LineTotal"] / 1.07;
               txt = (data).toFixed(2);
            }
            
            //with GST
            // var GST = total / 1.07;
            // var AmountBeforeGST = total - GST;
            // $("#GST").data("kendoNumericTextBox").value(GST);
            // $("#AmountBeforeGST").data("kendoNumericTextBox").value(AmountBeforeGST);


            html += '<td id="'+target+getfield+'v-'+no+'" data-val="'+data+'">'+txt+'</td>';
        }
        targetDo = "'"+target+"'";
        html += '<td class="actions"><a href="javascript:void(0);" onclick="editdetail('+targetDo+',' + no + ',0);"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removedetail('+targetDo+',' + no + ');"><i class="fa fa-trash-o"></i></a></td></tr>';
        
    }
    $('#list-'+target).html(html);
    sumTotal(target);
    PNotify.removeAll();
    new PNotify({ title: 'Notification', text: no+' Detail Added.', type: 'info'});
}
function editdetail(target,id,toDB) 
{
    var action = (toDB) ? "UpdatedetailDB('"+target+"'," + id + ")" : "Updatedetail('"+target+"'," + id + ")";
    var field = getDetailField(target);
    for (i = 0; i < field.length; i++) {
        var get = $("#"+target+field[i]+"v-"+id).attr("data-val");
        var data = (get==undefined) ? "" : get;
        $("#"+field[i]).val(data);
        if($("#"+field[i]).data("kendoDropDownList")){
            $("#"+field[i]).data("kendoDropDownList").value(data);
        }
        if($("#"+field[i]).data("kendoNumericTextBox")){
            $("#"+field[i]).data("kendoNumericTextBox").value(data);
        }
        if(field[i] == "AccountCode"){ 
            $("#BusinessPartnerName").val("");
        }
    }
    $("#submitButton"+target)[0].setAttribute("onclick", action);
    $("#submitButton"+target).html('<i class="el-icon-file-edit"></i> Update');
    if($("#submitButtondetail")[0]){
        $("#submitButtondetail")[0].setAttribute("onclick", action);
        $("#submitButtondetail").html('<i class="el-icon-file-edit"></i> Update');
    }
    $("#"+target+"Form").data("kendoWindow").center().open();
}

function Updatedetail(target,no) {
    var field = getDetailField(target);
    var html = '';
    var valid = 1;
    var msg = "";
    for (i = 0; i < field.length; i++) {
      var get = $("#"+field[i]).val();
      var txt = (get==undefined) ? "" : getTextForm(field[i]);
      var data = (get==undefined) ? "" : get;
      // if(field[i]=="Quantity"){ txt = accounting.formatNumber(data,2,","); }
      if(field[i]=="UnitPrice"){ txt = accounting.formatNumber(data,2,","); }
      if(field[i]=="LineTotal"){ txt = accounting.formatNumber(data,2,","); }
      
      if($("#"+field[i]).attr("data-role") == "datepicker"){
        get = $("#"+field[i]).data("kendoDatePicker").value();
        txt = (get==undefined) ? "" : getTextForm(field[i]);
        data = (get==undefined) ? "" : get;
        var d = new Date(data);
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        data = curr_month + "/" + curr_date + "/" + curr_year;
      }

      //make required
      if($("#"+field[i]).attr("datarequired") == "1"){
        if(data==""){valid = 0; msg+=field[i]+" is required"+"\r\n";}  
      }
      if(field[i] == "Checklist"){
        html+='<td class="checklist"><input class="k-checkbox" id="'+target+'checklistv-'+no+'" name="'+target+'checklistv-'+no+'" type="checkbox" value="false"><label class="k-checkbox-label" for="'+target+'checklistv-'+no+'"></label><input name="'+target+'checklistv-'+no+'" type="hidden" value="false"></td>';
      }else{
        html += '<td id="'+target+field[i]+'v-'+no+'" data-val="'+data+'">'+txt+'</td>';
      }
    }

    if(valid){
        targetDo = "'"+target+"'";
        if($("#ChecklistItem").length){//remove 'delete-icon'
            html += '<td class="actions"><a href="javascript:void(0);" onclick="editdetail('+targetDo+',' + no + ',0);"><i class="fa fa-pencil"></i></a></td>';
        }else{
            html += '<td class="actions"><a href="javascript:void(0);" onclick="editdetail('+targetDo+',' + no + ',0);"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removedetail('+targetDo+',' + no + ');"><i class="fa fa-trash-o"></i></a></td>';
        }
        $('#'+target+"-"+no).html(html);

        sumTotal(target);
        sortTable(1,0,target);
        cleardetail("detail",0);
        $('#'+target+'Form').closest("[data-role=window]").kendoWindow("close");
    }else{
        new PNotify({ title: "Warning", text: msg, type: 'warning', shadow: true });
    }
    
}

function cleardetail(target,id)
{
    var action = (id) ? "add"+target+"ToDB" : "adddetail";
    var field = getDetailField(target);
    var no = document.getElementById('list-'+target).rows.length + 1;
    for (i = 0; i < field.length; i++) {
        $("#"+field[i]).val("");
        if($("#"+field[i]).data("kendoDropDownList")){
            $("#"+field[i]).data("kendoDropDownList").value("");
        }
        if($("#"+field[i]).data("kendoNumericTextBox")){
            $("#"+field[i]).data("kendoNumericTextBox").value("");
            if(field[i] == "RowIndex"){
                $("#RowIndex").data("kendoNumericTextBox").value(no);
            }
        }
    }
    $("#RowIndex").val(no);
    $("#submitButton"+target)[0].setAttribute("onclick", action + "('"+target+"')");
    $("#submitButton"+target).html('<i class="el-icon-file-new"></i> Save');
    if($("#submitButtonOth")[0]){
        $("#submitButtonOth")[0].setAttribute("onclick", action + "('"+target+"')");
        $("#submitButtonOth").html('<i class="el-icon-file-new"></i> Save');
    }
}

function removedetail(target,id) {
    $('#'+target+'-' + id).remove();
    sumTotal(target);
}

function Calculate() {
    var qty = $('#Quantity').val();
    var unitPrice = $('#UnitPrice').val();
    var LineTotal = $("#LineTotal").data("kendoNumericTextBox");
    var total = qty * unitPrice;
    LineTotal.value(total);
    $('#LineTotal').val(total);
}
function CalculateDetail() {
    var qty = $('#Quantity').val();
    var unitPrice = $('#UnitPrice').val();
    var LineTotal = $("#LineTotal").data("kendoNumericTextBox");
    var total = qty * unitPrice;
    LineTotal.value(total);
    $('#LineTotal').val(total);
    //with GST
    var GST = total / 1.07;
    var AmountBeforeGST = total - GST;
    $("#GST").data("kendoNumericTextBox").value(GST);
    $("#AmountBeforeGST").data("kendoNumericTextBox").value(AmountBeforeGST);
}
// function sort row table
function sortTable(f,n,target){
    var rows = $('#'+target+' tbody  tr').get();

    rows.sort(function(a, b) {

        var A = getVal(a);
        var B = getVal(b);

        if(A < B) {
            return -1*f;
        }
        if(A > B) {
            return 1*f;
        }
        return 0;
    });

    function getVal(elm){
        var v = $(elm).children('td').eq(n).text().toUpperCase();
        if($.isNumeric(v)){
            v = parseInt(v,10);
        }
        return v;
    }

    $.each(rows, function(index, row) {
        $('#'+target).children('tbody').append(row);
    });
}

// var f_sl = 1;
// $("#indexRow").click(function(){
//     f_sl *= -1;
//     var n = $(this).prevAll().length;
//     sortTable(f_sl,n);
// });

// function table pagination
function toPagination(){
    $('table.table').each(function() {
        var currentPage = 0;
        var numPerPage = 5;
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function(event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}

// function for slice float
function sliceFloat(val, slc) {
    var dg = (slc == 3) ? 5 : val.lenght;
    var folat = val;
    var str = folat.toString();
    var slice = str.slice(0, dg);
    var res = parseFloat(slice);
    return res.toFixed(slc);
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

//=========================== Group function for Payment ===============================//
function InsertPayment() {
    var type = parseInt($('#PaymentType').val());
    var paymentID = $('#PaymentID').val();
    var action = document.getElementById("payment-form").action;
    if (document.getElementById('PayForAmount').checked) {
        var PayFor = $('#PayForAmount').val();
    }
    if (document.getElementById('PayForWeight').checked) {
        var PayFor = $('#PayForWeight').val();
    }
    if (type == 1) {
        var voData = {
            ParentRecordID: paymentID,
            PayTypeLab: "Cash",
            PaymentType: $('#PaymentType').val(),
            MaterialID: $('#MaterialIDPym').val(),
            TransferFromAccount: $('#AccountID').val(),
            MaterialID: $('#MaterialIDPym').val(),
            DealDate: $('#paymentCashDealDate').val(),
            DealAmount: $('#paymentCash-dealAmount').val(),
            AmountPaid: $('#paymentCash-amoubtpaid').val(),
            AmountLineTotal: $('#paymentCash-total').val(),
            PayFor: PayFor,
            remarks: $('#paymentCash-desc').val()
        };
    }
    if (type == 2) {
        var voData = {
            ParentRecordID: paymentID,
            PayTypeLab: "Gold",
            PaymentType: $('#PaymentType').val(),
            MaterialID: $('#MaterialIDPym').val(),
            TransferFromAccount: $('#AccountID').val(),
            MaterialID: $('#MaterialIDPym').val(),
            DealDate: $('#paymentGoldDealDate').val(),
            Percentage: $('#paymentGold-kadar').val(),
            Weight: $('#paymentGold-barat').val(),
            dbc: $('#paymentGold-dbc').val(),
            dbw: $('#paymentGold-dbw').val(),
            AmountLineTotal: $('#paymentGold-total').val(),
            remarks: $('#paymentGold-desc').val()
        };
    }
    if (type == 3) {
        var voData = {
            ParentRecordID: paymentID,
            PayTypeLab: "Giro",
            PaymentType: $('#PaymentType').val(),
            MaterialID: $('#MaterialIDPym').val(),
            TransferFromAccount: $('#AccountID').val(),
            MaterialID: $('#MaterialIDPym').val(),
            ChequeNo: $('#paymentGiro-ChequeNo').val(),
            ChequeDate: $('#paymentGiroChequeDate').val(),
            ChequeIssuingBank: $('#paymentGiro-ChequeIssuingBank').val(),
            DealDate: $('#paymentGiroDealDate').val(),
            DealAmount: $('#paymentGiro-dealAmount').val(),
            AmountPaid: $('#paymentGiro-amoubtpaid').val(),
            AmountLineTotal: $('#paymentGiro-total').val(),
            remarks: $('#paymentGiro-desc').val()
        };
    }
    $.ajax({
        url: action,
        type: 'POST',
        data: JSON.stringify(voData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (msg) {
            //document.getElementById("btnInvSave").disabled = true;
            //document.getElementById("btnInvSavePost").disabled = true;
        },
        success: function (result) {
            if (result.errorcode > 0) {
                new PNotify({ title: result.title, text: result.msg, type: 'error', shadow: true });
                //document.getElementById("btnInvSave").disabled = true;
                //document.getElementById("btnInvSavePost").disabled = true;
            } else {
                new PNotify({ title: result.title, text: result.msg, type: 'success', shadow: true });
                addPayment(voData, result.RecordID);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        },
        async: true,
        processData: false
    });
}
function addPayment(data, no) {
    //var no = document.getElementById('list-payment').rows.length + 1;
    var html = '<tr id="payment-' + no + '"><td id="paymentTypev-' + no + '">' + data.PayTypeLab + '</td><td id="AmountLineTotalv-' + no + '">' + data.AmountLineTotal + '</td><td id="paymentItemv-' + no + '">' + data.MaterialID + '</td><td id="paymentdescv-' + no + '">' + data.remarks + '</td><td class="actions"><a href="javascript:void(0);" onclick="postPay(' + no + ');"><i class="fa fa-arrow-circle-up"></i></a><a href="javascript:void(0);" onclick="editPay(' + no + ');"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removePay(' + no + ');"><i class="fa fa-trash-o"></i></a></td></tr>'
    $('#list-payment').append(html);
}
function payCashCal() {
    var dealAmount = $('#paymentCash-dealAmount').val();
    var amoubtpaid = $('#paymentCash-amoubtpaid').val();
    if (document.getElementById('PayForAmount').checked) {
        var linetotal = parseFloat(amoubtpaid);
        var slc = 2;
    }
    if (document.getElementById('PayForWeight').checked) {
        var linetotal = parseFloat(amoubtpaid / dealAmount);
        var slc = 3;
    }
    
    $('#paymentCash-total').val(sliceFloat(linetotal, slc));
}
function payGiroCal() {
    var dealAmount = $('#paymentGiro-dealAmount').val();
    var amoubtpaid = $('#paymentGiro-amoubtpaid').val();
    var linetotal = parseFloat(amoubtpaid / dealAmount);
    $('#paymentGiro-total').val(sliceFloat(linetotal, 3));
}
function payGoldCal() {
    var kadar = $('#paymentGold-kadar').val();
    var berat = $('#paymentGold-barat').val();
    var linetotal = parseFloat(kadar / 100 / 0.995 * berat);
    $('#paymentGold-total').val(sliceFloat(linetotal, 3));
}
function SetPaymentForm(id) {
    var type = parseInt(id);
    if (type == 1) {
        $('#form-cash').show();
        $('#form-gold').hide();
        $('#form-giro').hide();
    }
    if (type == 2) {
        $('#form-cash').hide();
        $('#form-gold').show();
        $('#form-giro').hide();
    }
    if (type == 3) {
        $('#form-cash').hide();
        $('#form-gold').hide();
        $('#form-giro').show();
    }
}

function openModalPayment(item) {
    
    SetPaymentForm(item.data.PaymentType);
    var TarnsferToBankdl = $("#AccountID").data("kendoDropDownList");
    TarnsferToBankdl.select(function (dataItem) {
        return dataItem.AccountID === item.data.TransferFromAccount;
    });
    var MaterialIDdl = $("#MaterialIDPym").data("kendoDropDownList");
    MaterialIDdl.select(function (dataItem) {
        return dataItem.MaterialID === item.data.MaterialID;
    });
    $("#PaymentType").val(item.data.PaymentType);
    if (item.data.PaymentType == 1) {
        if (item.data.PayFor == 1) {
            document.getElementById('PayForAmount').checked = true;
        }
        if (item.data.PayFor == 2) {
            document.getElementById('PayForWeight').checked = true;
        }
        var datepicker = $("#paymentCashDealDate").data("kendoDatePicker");
        datepicker.value(item.DateDeal);
        var dealAmount = $("#paymentCash-dealAmount").data("kendoNumericTextBox");
        var amoubtpaid = $("#paymentCash-amoubtpaid").data("kendoNumericTextBox");
        dealAmount.value(item.data.DealAmount);
        amoubtpaid.value(item.data.AmountPaid);
        $('#paymentCash-dealAmount').val(item.data.DealAmount);
        $('#paymentCash-amoubtpaid').val(item.data.AmountPaid);
        $('#paymentCash-total').val(item.data.AmountLineTotal);
        document.getElementById("paymentCash-desc").value = item.data.Remarks;
    }
    if (item.data.PaymentType == 2) {
        var datepicker = $("#paymentGoldDealDate").data("kendoDatePicker");
        datepicker.value(item.DateDeal);
        var kadar = $("#paymentGold-kadar").data("kendoNumericTextBox");
        var barat = $("#paymentGold-barat").data("kendoNumericTextBox");
        kadar.value(item.data.Percentage);
        barat.value(item.data.Weight);
        $('#paymentGold-kadar').val(item.data.Percentage);
        $('#paymentGold-barat').val(item.data.Weight);
        $('#paymentGold-total').val(item.data.AmountLineTotal);
        document.getElementById("paymentGold-desc").value = item.data.Remarks;
    }
    if (item.data.PaymentType == 3) {
        var dateDeal = $("#paymentGiroDealDate").data("kendoDatePicker");
        var dateCheque = $("#paymentGiroChequeDate").data("kendoDatePicker");
        dateDeal.value(item.DateDeal);
        dateCheque.value(item.data.ChequeDate);
        $('#paymentGiro-ChequeNo').val(item.data.ChequeNo);
        $('#paymentGiro-ChequeIssuingBank').val(item.data.ChequeIssuingBank);
        $('#paymentGiro-ChequeIssuingBank').val(item.data.ChequeIssuingBank);
        var dealAmount = $("#paymentGiro-dealAmount").data("kendoNumericTextBox");
        var amoubtpaid = $("#paymentGiro-amoubtpaid").data("kendoNumericTextBox");
        dealAmount.value(item.data.DealAmount);
        amoubtpaid.value(item.data.AmountPaid);
        $('#paymentGiro-dealAmount').val(item.data.DealAmount);
        $('#paymentGiro-amoubtpaid').val(item.data.AmountPaid);
        $('#paymentGiro-total').val(item.data.AmountLineTotal);
        document.getElementById("paymentGiro-desc").value = item.data.Remarks;

    }
    $("#submitButtonPymt")[0].setAttribute("onclick", "UpdatePay(" + item.data.RecordID + ")");
    $("#submitButtonPymt").html('<i class="el-icon-file-edit"></i> Update');
    $("#PaymentForm").data("kendoWindow").center().open();
}

function clearPayment()
{
    var TarnsferToBankdl = $("#AccountID").data("kendoDropDownList");
    TarnsferToBankdl.select(function (dataItem) {
        return dataItem.AccountID === "";
    });
    var MaterialIDdl = $("#MaterialIDPym").data("kendoDropDownList");
    MaterialIDdl.select(function (dataItem) {
        return dataItem.MaterialID === "";
    });
    $("#PaymentType").val(1);
    document.getElementById('PayForAmount').checked = false;
    document.getElementById('PayForWeight').checked = false;
    var dateCashDeal = $("#paymentCashDealDate").data("kendoDatePicker");
    dateCashDeal.value("");
    var dealAmount = $("#paymentCash-dealAmount").data("kendoNumericTextBox");
    var amoubtpaid = $("#paymentCash-amoubtpaid").data("kendoNumericTextBox");
    dealAmount.value("");
    amoubtpaid.value("");
    $('#paymentCash-dealAmount').val("");
    $('#paymentCash-amoubtpaid').val("");
    $('#paymentCash-total').val("");
    document.getElementById("paymentCash-desc").value = "";

    var dateGoldDeal = $("#paymentGoldDealDate").data("kendoDatePicker");
    dateGoldDeal.value("");
    var kadar = $("#paymentGold-kadar").data("kendoNumericTextBox");
    var barat = $("#paymentGold-barat").data("kendoNumericTextBox");
    kadar.value("");
    barat.value("");
    $('#paymentGold-kadar').val("");
    $('#paymentGold-barat').val("");
    $('#paymentGold-total').val("");
    document.getElementById("paymentGold-desc").value = "";
   
    var dateDeal = $("#paymentGiroDealDate").data("kendoDatePicker");
    var dateCheque = $("#paymentGiroChequeDate").data("kendoDatePicker");
    dateDeal.value("");
    dateCheque.value("");
    $('#paymentGiro-ChequeNo').val("");
    $('#paymentGiro-ChequeIssuingBank').val("");
    $('#paymentGiro-ChequeIssuingBank').val("");
    var GirodealAmount = $("#paymentGiro-dealAmount").data("kendoNumericTextBox");
    var Giroamoubtpaid = $("#paymentGiro-amoubtpaid").data("kendoNumericTextBox");
    GirodealAmount.value("");
    Giroamoubtpaid.value("");
    $('#paymentGiro-dealAmount').val("");
    $('#paymentGiro-amoubtpaid').val("");
    $('#paymentGiro-total').val("");
    document.getElementById("paymentGiro-desc").value = "";

    $("#submitButtonPymt")[0].setAttribute("onclick", "InsertPayment()");
    $("#submitButtonPymt").html('<i class="el-icon-file-new"></i> Save');
}

function loadIframe(target,url)
{
    var tab = "Tab"+target;
    var frame = "frame"+target;
    
    document.getElementById(frame).src=""+url+"";
    $("#"+tab)[0].setAttribute("onclick", "javascript:void(0)");
}
function parseCurrency( num ) {
    if(num == ""){num = 0;}
    return parseFloat( num.replace( /,/g, '') );
}
function SQCal_Subtotal()
{
    if($("#MounthlyRental").length)
        var MounthlyRental = parseCurrency($('#MounthlyRental').val());
    else        
        var MounthlyRental = parseCurrency($('#Price').val());

    var Transportation = parseCurrency($('#Transportation').val());
    var MiscCharge = parseCurrency($('#MiscCharge').val());
    var SubTotal = $("#SubTotal").data("kendoNumericTextBox");
    var total = MounthlyRental + Transportation + MiscCharge;
    SubTotal.value(total);
    $('#SubTotal').val(total);
    SQCal_Grandtotal();
}

function SQCal_Grandtotal()
{
    var SubTotal = parseCurrency($('#SubTotal').val());
    var Discount = parseCurrency($('#Discount').val());
    var GST = $('#GST').val();
    var SBD = SubTotal - Discount;
    var GrandTotal = $("#GrandTotal").data("kendoNumericTextBox");
    var GSTAmount = $("#GSTAmount").data("kendoNumericTextBox");
    // INC
    if(GST=="GT1"){ 
        GST = 0.07; 
        // var total = SubTotal - Discount;
        var total = SBD;
        var GSTN = (total / 1.07) * GST;
    }
    // STD
    if(GST=="GT2"){ 
        GST = 0.07;
        var GSTN = SBD * GST;
        // var total = SubTotal - Discount + GSTN;
        var total = SBD + GSTN;
    }
    // ZERO
    if(GST=="GT3"){
        GST = 0;
        var GSTN = 0;
        var total = SubTotal;
    }
    GrandTotal.value(total);
    GSTAmount.value(GSTN);
    $('#GrandTotal').val(total);
    $('#GSTAmount').val(GSTN);
}

function DateFormats(data)
{
    var monthNames = [
      "01", "02", "03",
      "04", "05", "06", "07",
      "08", "09", "10",
      "11", "12"
    ];

    var date = new Date(data);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var result = day+"/"+monthNames[monthIndex]+"/"+year;
    
    return result;
} 
function Cal_ExpiredDate() {
    $('#TerminationDate').val("");
    $('#ActualPeriod').val("");
    var DOC = $("#DOC").data("kendoDatePicker");
    var HiringPeriodDays = $("#HiringPeriodDays").data("kendoNumericTextBox");
    var HiringPeriodMounths = $("#HiringPeriodMounths").data("kendoNumericTextBox");
    var ExpiredDate = $("#ExpiredDate").data("kendoDatePicker");
    var ExpVal = ExpiredDate.value();
    var DocVal = DOC.value();
    var HRDate = HiringPeriodDays.value() - 1;
    var HRMonth = HiringPeriodMounths.value();
    var docYear = DocVal.getFullYear();
    //Cal_TotalAmount(HiringPeriodDays.value(), HiringPeriodMounths.value());
    var d = new Date(DocVal);
    d.setDate(d.getDate() + HRDate);
    d.setMonth(d.getMonth() + HRMonth);
    var Result = DateFormats(d);
    ExpiredDate.value(Result);
    //lastbilldate & actual period
    $("#LastBillDate").data("kendoDatePicker").value(Result);
    ActualPeriod();
    //totalcontract
    TotalContract();
}
function ActualPeriod(){
    var lastbill = $("#LastBillDate").data("kendoDatePicker").value();
    var doc = $("#DOC").data("kendoDatePicker").value();

    var date1 = new Date(lastbill);
    var date2 = new Date(doc);
    var diff = new Date(date1.getTime() - date2.getTime());
    // diff is: Thu Jul 05 1973 04:00:00 GMT+0300 (EEST)
    var diffY = diff.getUTCFullYear() - 1970;
    var diffM = diff.getUTCMonth();
    var diffD = diff.getUTCDate() - 1;
    
    var totalbulan = 0;

    if(diffY == 0){ diffY=""; }else{
        totalbulan = diffY * 12;
        diffY = diffY + " Years ";
    }
    if(diffM == 0){ diffM=""; }else{
        totalbulan = totalbulan + diffM;
        diffM = diffM + " Month ";
    }
    //total min period & notice period
    totalperiod = $("#MinPeriod").val() + $("#NoticePeriod").val();
    if(totalperiod > 0){
        //if actual period < (minperiod+noticeperiod) go notif
        if(totalbulan < totalperiod){
            $("#ActualStatus").val("1");
        }else{
            $("#ActualStatus").val("0");
        }
    }
    if(diffD == 0){ diffD=""; }else{diffD = diffD + " Days";}
    
    $("#ActualPeriodText").val(diffY+diffM+diffD);
    $("#ActualPeriod").data("kendoDatePicker").value(lastbill);
}
function TotalContract(){
    var monthlyrental = $("#MounthlyRentalInc").data("kendoNumericTextBox").value();
    var hiringperiodmonth = $("#HiringPeriodMounths").data("kendoNumericTextBox").value();
    var totalcontract = (monthlyrental*hiringperiodmonth) / 1.07;
    $("#TotalContractAmountExl").data("kendoNumericTextBox").value(totalcontract);
}
function TermCond()
{
    var TermAndConditionID = $("#TermAndConditionID").data("kendoDropDownList");
    $('#TermAndCondition').val(TermAndConditionID.text());
}
function resizeHeight(trigger,target,resize){
	$("#"+trigger).bind("click", function () {
    	var iframeheight = parseInt(window.parent.$(target).height()) + resize;
    	$(target, window.parent.document).attr("style", "background:#fff;border: 1px none; height:" + iframeheight + "px");
    });	
}