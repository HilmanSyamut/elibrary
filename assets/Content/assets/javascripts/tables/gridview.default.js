function FillForm()
{
    var DocumentNo = $("#RefDocNo").val();
    if(DocumentNo == undefined){DocumentNo = $("#RefDocumentNo").val();}
    if(DocumentNo == undefined){DocumentNo = $("#HA").val();}
    console.log(DocumentNo);
    var voData = {
        DocumentNo: DocumentNo
    };

    $.ajax({
        url: link_Lookup,
        type: 'POST',
        data: JSON.stringify(voData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            SetToFrom(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            new PNotify({ title: "Warning", text: jQuery.parseJSON(jqXHR.responseText), type: "Warning", shadow: true });
        },
        async: false,
        processData: false
    });
}

// Begins Kendo Grid Event
function kgEventEdit(e) {
    if (e.model.isNew()) {
        $('.k-window-title').text('Add New');
    }

    e.container.find('#DocumentTypeID').val(vsDocumentTypeID);

    var vsDocumentTypeName = 'Material Movement - ';
    switch (vsDocumentTypeID) {
        case 'MMBB':
            vsDocumentTypeName = vsDocumentTypeName + 'Begin Balance';
            break;
        case 'MMIO':
            vsDocumentTypeName = vsDocumentTypeName + 'Stock Transfer';
            break;
        case 'MMSA':
            vsDocumentTypeName = vsDocumentTypeName + 'Stock Adjust';
            break;
        case 'MMSO':
            vsDocumentTypeName = vsDocumentTypeName + 'Stock Take';
            break;
    }
    e.container.find('#DocumentTypeName').val(vsDocumentTypeName);
}

function kgEventDataBound(e) {
    // var voGrid = $('#kgData').data('kendoGrid');
    // var voGridData = voGrid.dataSource.view();
    // var vvID;
    // var voRow;
    // var voButton;

    // for (var i = 0; i < voGridData.length; i++) {
    //     vvID = voGridData[i].uid;
    //     voRow = voGrid.table.find("tr[data-uid='" + vvID + "']");

    //     if (voGridData[i].RecordStatus == 0) {
    //         voButton = $(voRow).find('.k-grid-unpost');
    //         voButton.hide();
    //     }
    //     else if (voGridData[i].RecordStatus == 1) {
    //         voButton = $(voRow).find('.k-grid-edit');
    //         voButton.hide();
    //         voButton = $(voRow).find('.k-grid-delete');
    //         voButton.hide();
    //         voButton = $(voRow).find('.k-grid-post');
    //         voButton.hide();
    //     }
    //     else if (voGridData[i].RecordStatus == 2) {
    //         voButton = $(voRow).find('.k-grid-edit');
    //         voButton.hide();
    //         voButton = $(voRow).find('.k-grid-delete');
    //         voButton.hide();
    //         voButton = $(voRow).find('.k-grid-post');
    //         voButton.hide();
    //         voButton = $(voRow).find('.k-grid-unpost');
    //         voButton.hide();
    //     }
    // }
}

function kgCommandClick(e) {
    e.preventDefault();

    var voButton = e.target;
    var voRow = this.dataItem($(e.currentTarget).closest('tr'));
    var voMessageTemplate;

    if ($(voButton).hasClass('k-grid-post')) {
        voMessageTemplate = kendo.template($("#message-post").html());
    }
    else if ($(voButton).hasClass('k-grid-unpost')) {
        voMessageTemplate = kendo.template($("#message-unpost").html());
    }

    var voWindow = $('<div />').kendoWindow({
        title: 'Confirmation',
        modal: true,
        draggable: true,
        resizable: false,
        width: 300
    });

    voWindow.data("kendoWindow")
        .content(voMessageTemplate(voRow))
        .center().open();

    voWindow
        .find('.confirm-yes, .confirm-no')
        .click(function () {
            if ($(this).hasClass('confirm-yes')) {

                var voData = {
                    RecordID: voRow.RecordID,
                    DocumentTypeID: voRow.DocumentTypeID
                };

                if ($(voButton).hasClass('k-grid-post')) {
                    $.ajax({
                        url: link_Post,
                        type: 'POST',
                        data: JSON.stringify(voData),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function (result) {
                            new PNotify({ title: "Success", text: "Record successfully Posted.", type: "success", shadow: true });
                            var voGrid = $('#kgData').data('kendoGrid');
                            voGrid.dataSource.read();
                            voGrid.refresh();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            new PNotify({ title: "Warning", text: jQuery.parseJSON(jqXHR.responseText), type: "Warning", shadow: true });
                        },
                        async: true,
                        processData: false
                    });
                }
                else if ($(voButton).hasClass('k-grid-unpost')) {
                    $.ajax({
                        url: link_UnPost,
                        type: 'POST',
                        data: JSON.stringify(voData),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function (result) {
                            new PNotify({ title: "Success", text: "Record successfully UnPosted.", type: "success", shadow: true });
                            var voGrid = $('#kgData').data('kendoGrid');
                            voGrid.dataSource.read();
                            voGrid.refresh();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
							console.log(jqXHR);
                            new PNotify({ title: "Warning", text: jQuery.parseJSON(jqXHR.responseText), type: "Warning", shadow: true });
                        },
                        async: true,
                        processData: false
                    });
                }
            }
            voWindow.data('kendoWindow').close();
        })
        .end();
}
// End Kendo Grid Event


// Begins Kendo Grid DataSource Event
function kgDataSourceEventError(e) {
    if (e.errors) {
        if (e.type == 'destroy') {
            alert('row updated or deleted by other user');
        }
        else {
            var voGrid = $('#kgData').data('kendoGrid');
            var voTemplate = kendo.template($('#FormValidation').html());
            voGrid.one('dataBinding', function (x) {
                x.preventDefault();
                $.each(e.errors, function (propertyName) {
                    var voRendered = voTemplate({ field: propertyName, messages: this.errors });
                    voGrid.editable.element.find('.errors').append(voRendered);
                });
            });
        }
    }
}

function kgDataSourceEventRequestStart(e) {
    if (e.type == 'create' || e.type == 'update' || e.type == 'destroy') {
        //$('div.k-window').block({ message: '<h4>Processing</h4>' });
    }
}

function kgDataSourceEventRequestEnd(e) {
    if (e.type == 'create' || e.type == 'update' || e.type == 'destroy') {
        //$('div.k-window').unblock();

        if (!e.response.Errors) {
            e.sender.read();
        }
    }
}
// End Kendo Grid DataSource Event


// Begins Kendo Grid Detail Event
function kgdEventEdit(e) {
    if (e.model.isNew()) {
        $('.k-window-title').text('Add New');
    }

    $(document).ready(function () {
        $("#ItemID").keyup(function () {
            request = $(this).val().toUpperCase();
            $(this).val(request).change();
        });
    });
    $(document).ready(function () {
        $("#LocationIDFrom").keyup(function () {
            request = $(this).val().toUpperCase();
            $(this).val(request).change();
        });
    });
    $(document).ready(function () {
        $("#LocationIDTo").keyup(function () {
            request = $(this).val().toUpperCase();
            $(this).val(request).change();
        });
    });
}

function kgdEventDataBound(e) {
    var voGrid = $('#kgData').getKendoGrid();
    var voRecord = voGrid.dataItem(this.wrapper.closest('tr').prev('.k-master-row'));
    if (voRecord.RecordStatus == 1 || voRecord.RecordStatus == 2) {
        this.wrapper.find('.k-grid-add').hide();
        this.items().find('.k-grid-edit, .k-grid-delete').hide();
    }
}
// End Kendo Grid Detail Event


// Begins Kendo Grid Detail DataSource Event
function kgdDataSourceEventRequestStart(e) {
    if (e.type == 'create' || e.type == 'update' || e.type == 'destroy') {
        //$('div.k-window').block({ message: '<h4>Processing</h4>' });
    }
}

function kgdDataSourceEventRequestEnd(e) {
    if (e.type == 'create' || e.type == 'update' || e.type == 'destroy') {
        //$('div.k-window').unblock();
        if (!e.response.Errors) {
            e.sender.read();
        }
    }
}
// End Kendo Grid Detail DataSource Event

// Other Event
function kbEventClick(e) {
    var vsButtonName = $(e.event.target).closest('.k-button').attr('id');
    if (vsButtonName == 'kbLocationLookup1') {
        $('#kwLocationLookup1').data('kendoWindow').center().open();
    }
    if (vsButtonName == 'kbLocationLookup2') {
        $('#kwLocationLookup2').data('kendoWindow').center().open();
    }
    console.log(vsButtonName);
    var win = vsButtonName.replace("kb", "kw");
    $('#'+win).data('kendoWindow').center().open();
}

function kwEventOpen(e) {
    var vsElementID = $(this.element).prop('id');
    if (vsElementID == 'kwLocationLookup1') {
        var voGrid = $('#kgLocationLookup1').data('kendoGrid');
        voGrid.dataSource.read();
        voGrid.refresh();
        $('#kgLocationLookup1').on('dblclick', 'tbody>tr', { extra: 'kgLocationLookup1' }, kgEventDblClick);
    }else if (vsElementID == 'kwLocationLookup2') {
        var voGrid = $('#kgLocationLookup2').data('kendoGrid');
        voGrid.dataSource.read();
        voGrid.refresh();
        $('#kgLocationLookup2').on('dblclick', 'tbody>tr', { extra: 'kgLocationLookup2' }, kgEventDblClick);
    }else if (vsElementID == 'kwEndUserLookup') {
        var id = $('#ParentEndUserID').val();
        setWindowPopup(id);
        $('#kgEndUserLookup').on('dblclick', 'tbody>tr', { extra: 'kgEndUserLookup' }, kgEventDblClick);
    }else{
        var grid = vsElementID.replace("kw", "kg");
        var voGrid = $('#'+grid).data('kendoGrid');
        voGrid.dataSource.read();
        voGrid.refresh();

        $('#'+grid).on('dblclick', 'tbody>tr', { extra: grid }, kgEventDblClick);
    }
}

function kgEventDblClick(e) {
    console.log(e.data.extra);
    if (e.data.extra == 'kgMaterialLookup') {
        var voGrid = $('#kgMaterialLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsItemID = voRow.ItemID;
        var vsMaterialName = voRow.ItemDescription;
        var vsRFIDTagID = voRow.RFIDTagID;

        var voWindowLookup = $('#kwMaterialLookup').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#ItemID').val(vsItemID).change();
        voWindowForm.find('#ItemDescription').val(vsMaterialName).change();
        voWindowForm.find('#ItemIDFrom').val(vsItemID).change();
        voWindowForm.find('#ItemDescriptionFrom').val(vsMaterialName).change();
        voWindowForm.find('#RFIDTagID').val(vsRFIDTagID).change();
    }else if (e.data.extra == 'kgMaterialLookup1') {
        var voGrid = $('#kgMaterialLookup1').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsItemID = voRow.ItemID;
        var vsItemName = voRow.ItemDescription;
        var vsCondition = voRow.Condition;
        var vsRFIDTagID = voRow.RFIDTagID;

        var voWindowLookup = $('#kwMaterialLookup1').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#ItemIDFrom').val(vsItemID).change();
        voWindowForm.find('#ItemDescriptionFrom').val(vsItemName).change();
        var DDLFrom = $("#ConditionFrom").data("kendoDropDownList");
        DDLFrom.value(vsCondition);
        voWindowForm.find('#ConditionFrom').val(vsCondition).change();
        voWindowForm.find('#RFIDTagIDFrom').val(vsRFIDTagID).change();
        
        if(voWindowForm.find('#ItemIDTo').val() == ""){
            voWindowForm.find('#ItemIDTo').val(vsItemID).change();
            voWindowForm.find('#ItemDescriptionTo').val(vsItemName).change();
            voWindowForm.find('#RFIDTagIDTo').val(vsRFIDTagID).change();
            var DDL = $("#Condition").data("kendoDropDownList");
            DDL.value(vsCondition);
            voWindowForm.find('#Condition').val(vsCondition).change();
        }
    }
    else if (e.data.extra == 'kgMaterialLookup2') {
        var voGrid = $('#kgMaterialLookup2').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsItemID = voRow.ItemID;
        var vsItemName = voRow.ItemDescription;
        var vsRFIDTagID = voRow.RFIDTagID;

        var voWindowLookup = $('#kwMaterialLookup2').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#ItemIDTo').val(vsItemID).change();
        voWindowForm.find('#ItemDescriptionTo').val(vsItemName).change();
        voWindowForm.find('#RFIDTagIDTo').val(vsRFIDTagID).change();
    }
    else if (e.data.extra == 'kgLocationLookup1') {
        var voGrid = $('#kgLocationLookup1').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsLocationID = voRow.LocationID;
        var vsLocationName = voRow.LocationName;

        var voWindowLookup = $('#kwLocationLookup1').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#LocationIDFrom').val(vsLocationID).change();
    }
    else if (e.data.extra == 'kgLocationLookup2') {
        var voGrid = $('#kgLocationLookup2').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsLocationID = voRow.LocationID;
        var vsLocationName = voRow.LocationName;

        var voWindowLookup = $('#kwLocationLookup2').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#LocationIDTo').val(vsLocationID).change();
    }
    else if (e.data.extra == 'kgSalesPersonLookup') {
        var voGrid = $('#kgSalesPersonLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsEmployeeID = voRow.EmployeeID;
        var vsEmployeeName = voRow.EmployeeName;

        var voWindowLookup = $('#kwSalesPersonLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#SalespersonID').val(vsEmployeeID).change();
        $('#SalespersonName').val(vsEmployeeName).change();
    }
    else if (e.data.extra == 'kgVendorLookup') {
        var voGrid = $('#kgVendorLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBusinessPartnerID = voRow.BusinessPartnerID;
        var vsBusinessPartnerName = voRow.BusinessPartnerName;

        var voWindowLookup = $('#kwVendorLookup').data('kendoWindow');
        voWindowLookup.close();

        if($('#AccountCode') !== undefined){$('#AccountCode').val(vsBusinessPartnerID).change();}
        $('#BusinessPartnerID').val(vsBusinessPartnerID).change();
        $('#BusinessPartnerName').val(vsBusinessPartnerName).change();
    }
    else if (e.data.extra == 'kgCustomerLookup') {
        var voGrid = $('#kgCustomerLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBusinessPartnerID = voRow.BusinessPartnerID;
        var vsBusinessPartnerName = voRow.BusinessPartnerName;

        var voWindowLookup = $('#kwCustomerLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#BusinessPartnerID').val(vsBusinessPartnerID).change();
        $('#BusinessPartnerName').val(vsBusinessPartnerName).change();
    }
    else if (e.data.extra == 'kgHALookup') {
        var voGrid = $('#kgHALookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsDocumentNo = voRow.DocumentNo;

        var voWindowLookup = $('#kwHALookup').data('kendoWindow');
        voWindowLookup.close();
        if($('#HA').val() != undefined){ $('#HA').val(vsDocumentNo).change(); }else{ console.log(vsDocumentNo); $('#RefDocumentNo').val(vsDocumentNo).change(); }
        
        FillForm();
    }
    else if (e.data.extra == 'kgRefPOLookup') {
        var voGrid = $('#kgRefPOLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsDocumentNo = voRow.DocumentNo;

        var voWindowLookup = $('#kwRefPOLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#RefDocNo').val(vsDocumentNo).change();
        FillForm();
    }
    else if (e.data.extra == 'kgRefQTLookup') {
        var voGrid = $('#kgRefQTLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsDocumentNo = voRow.DocumentNo;

        var voWindowLookup = $('#kwRefQTLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#RefDocumentNo').val(vsDocumentNo).change();
        FillForm();
    }
    else if (e.data.extra == 'kgRefQTLookupCS') {
        var voGrid = $('#kgRefQTLookupCS').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsDocumentNo = voRow.DocumentNo;

        var voWindowLookup = $('#kwRefQTLookupCS').data('kendoWindow');
        voWindowLookup.close();

        $('#RefDocumentNo').val(vsDocumentNo).change();
        FillForm();
    }
    else if (e.data.extra == 'kgHirerLookup') {
        var voGrid = $('#kgHirerLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBusinessPartnerID = voRow.BusinessPartnerID;
        var vsBusinessPartnerName = voRow.BusinessPartnerName;
        console.log(voRow);
        var voWindowLookup = $('#kwHirerLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#HirerID').val(vsBusinessPartnerID).change();
        $('#HirerName').val(vsBusinessPartnerName).change();
        $('#PIC').val(voRow.PersonInCharge).change();
        $('#BilltoLine1').val(voRow.AddressLine1).change();
        $('#BilltoLine2').val(voRow.AddressLine2).change();
        $('#BilltoLine3').val(voRow.AddressLine3).change();
        $('#BilltoCountry').val(voRow.CountryData.ListName).change();
        $('#BilltoPostCode').val(voRow.PostalCode).change();
        $('#BilltoPhone').val(voRow.PhoneNo).change();
        $('#BilltoFax').val(voRow.FaxNo).change();
        $('#BilltoEmail').val(voRow.Email).change();
        $('#ParentEndUserID').val(voRow.RecordID);
    }
    else if (e.data.extra == 'kgEndUserLookup') {
        var voGrid = $('#kgEndUserLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBusinessPartnerID = voRow.BusinessPartnerID;
        var vsBusinessPartnerName = voRow.BusinessPartnerName;

        var voWindowLookup = $('#kwEndUserLookup').data('kendoWindow');
        voWindowLookup.close();

        // $('#EndUserID').val(vsBusinessPartnerID).change();
        $('#EndUserID').val(voRow.RecordID).change();
        $('#EndUserName').val(vsBusinessPartnerName).change();
        $('#ShiptoLine1').val(voRow.AddressLine1).change();
        $('#ShiptoLine2').val(voRow.AddressLine2).change();
        $('#ShiptoLine3').val(voRow.AddressLine3).change();
        $('#ShiptoCountry').val(voRow.CountryData.ListName).change();
        $('#ShiptoPostCode').val(voRow.PostalCode).change();
        $('#ShiptoPhone').val(voRow.PhoneNo).change();
        $('#ShiptoFax').val(voRow.FaxNo).change();
        $('#ShiptoEmail').val(voRow.Email).change();
    }
    else if (e.data.extra == 'kgAgentLookup') {
        var voGrid = $('#kgAgentLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBusinessPartnerID = voRow.BusinessPartnerID;
        var vsBusinessPartnerName = voRow.BusinessPartnerName;
        var vsAgentPhone = voRow.PhoneNo;

        var voWindowLookup = $('#kwAgentLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#AgentID').val(vsBusinessPartnerID).change();
        $('#AgentName').val(vsBusinessPartnerName).change();
        $('#AgentPhone').val(vsAgentPhone).change();
    }
    else if (e.data.extra == 'kgBilltoLookup') {
        var voGrid = $('#kgBilltoLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsBilltoLine1 = voRow.AddressLine1;
        var vsBilltoLine2 = voRow.AddressLine2;
        var vsBilltoLine3 = voRow.AddressLine3;
        var vsCountry = voRow.CountryData.ListName;
        var vsPostalCode = voRow.PostalCode;
        var vsPhoneNo = voRow.PhoneNo;
        var vsFaxNo = voRow.FaxNo;
        var vsEmail = voRow.Email;

        var voWindowLookup = $('#kwBilltoLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#BilltoLine1').val(vsBilltoLine1).change();
        $('#BilltoLine2').val(vsBilltoLine2).change();
        $('#BilltoLine3').val(vsBilltoLine3).change();
        $('#BilltoCountry').val(vsCountry).change();
        $('#BilltoPostCode').val(vsPostalCode).change();
        $('#BilltoPhone').val(vsPhoneNo).change();
        $('#BilltoFax').val(vsFaxNo).change();
        $('#BilltoEmail').val(vsEmail).change();
    }
    else if (e.data.extra == 'kgShiptoLookup') {
        var voGrid = $('#kgShiptoLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsShiptoLine1 = voRow.AddressLine1;
        var vsShiptoLine2 = voRow.AddressLine2;
        var vsShiptoLine3 = voRow.AddressLine3;
        var vsCountry = voRow.CountryData.ListName;
        var vsPostalCode = voRow.PostalCode;
        var vsPhoneNo = voRow.PhoneNo;
        var vsFaxNo = voRow.FaxNo;
        var vsEmail = voRow.Email;

        var voWindowLookup = $('#kwShiptoLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#ShiptoLine1').val(vsShiptoLine1).change();
        $('#ShiptoLine2').val(vsShiptoLine2).change();
        $('#ShiptoLine3').val(vsShiptoLine3).change();
        $('#ShiptoCountry').val(vsCountry).change();
        $('#ShiptoPostCode').val(vsPostalCode).change();
        $('#ShiptoPhone').val(vsPhoneNo).change();
        $('#ShiptoFax').val(vsFaxNo).change();
        $('#ShiptoEmail').val(vsEmail).change();
    }
    else if (e.data.extra == 'kgIssueByLookup') {
        var voGrid = $('#kgIssueByLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsEmployeeID = voRow.EmployeeID;
        var vsEmployeeName = voRow.EmployeeName;

        var voWindowLookup = $('#kwIssueByLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#IssueBy').val(vsEmployeeID).change();
        $('#IssueByName').val(vsEmployeeName).change();
    }
    else if (e.data.extra == 'kgPurchaseInvoiceLookup') {
        var voGrid = $('#kgPurchaseInvoiceLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsDetailTotalAmount = voRow.DetailTotalAmount;
        var vsPurchaseInvoiceNo = voRow.DocumentNo; 

        var voWindowLookup = $('#kwPurchaseInvoiceLookup').data('kendoWindow');
        voWindowLookup.close();

        $('#OutstandingAmount').data("kendoNumericTextBox").value(vsDetailTotalAmount);
        $('#PurchaseInvoiceNo').val(vsPurchaseInvoiceNo);

    }
    else if (e.data.extra == 'kgHALookupBAI') {
        var voGrid = $('#kgHALookupBAI').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());

        var voWindowLookup = $('#kwHALookupBAI').data('kendoWindow');
        voWindowLookup.close();
        if($('#HA').val() != undefined){ $('#HA').val(voRow.DocumentNo).change(); }
        if($('#EndUserID').val() != undefined){ $('#EndUserID').val(voRow.EndUserID).change(); }
        if($('#EndUserName').val() != undefined){ $('#EndUserName').val(voRow.EndUser.BusinessPartnerName).change(); }
        if($('#ShiptoLine1').val() != undefined){ $('#ShiptoLine1').val(voRow.EndUser.AddressLine1).change(); }
        if($('#ShiptoLine2').val() != undefined){ $('#ShiptoLine2').val(voRow.EndUser.AddressLine2).change(); }
        
        FillForm();
    }
    else if (e.data.extra == 'kgARAPLookup') {
        var voGrid = $('#kgARAPLookup').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());

        var voWindowLookup = $('#kwARAPLookup').data('kendoWindow');
        voWindowLookup.close();
        if($('#AccountCode').val() != undefined){ $('#AccountCode').val(voRow.BizPartnerID).change(); }
    }
    else if (e.data.extra == 'kgMaterialLookupBA') {
        var voGrid = $('#kgMaterialLookupBA').data('kendoGrid');
        var voRow = voGrid.dataItem(voGrid.select());
        var vsItemID = voRow.ItemID;
        var vsMaterialName = voRow.ItemDescription;

        var voWindowLookup = $('#kwMaterialLookupBA').data('kendoWindow');
        voWindowLookup.close();

        var voWindowForm = $('.k-edit-form-container');
        voWindowForm.find('#ItemID').val(vsItemID).change();
        voWindowForm.find('#ItemDescription').val(vsMaterialName).change();
    }

    $("#"+e.data.extra).unbind();
}

function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}